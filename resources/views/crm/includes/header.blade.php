<header id="m_header" class="m-grid__item m-header"  m-minimize-offset="200" m-minimize-mobile-offset="200" >
  <div class="m-container m-container--fluid m-container--full-height">
    <div class="m-stack m-stack--ver m-stack--desktop">
      <div class="m-stack__item m-brand  m-brand--skin-dark ">
        <div class="m-stack m-stack--ver m-stack--general">
          <!-- <div class="m-stack__item m-stack__item--middle m-brand__logo">
            <a href="{{url('home')}}" class="m-brand__logo-wrapper">
              <img alt="" src="{{asset('assets/crm/demo/default/media/img/logo/logo_default_dark.png')}}"/>
            </a>
          </div> -->
          <div class="m-stack__item m-stack__item--middle m-brand__tools">
            <a href="javascript:;" id="m_aside_left_minimize_toggle" class="m-brand__icon m-brand__toggler m-brand__toggler--left m--visible-desktop-inline-block">
              <span></span>
            </a>
             <!-- m-brand__toggler--left -->
            <a href="javascript:;" id="m_aside_left_offcanvas_toggle" class="pull-left m-brand__toggler m--visible-tablet-and-mobile-inline-block">
              <span></span>
            </a>
            @if (Auth::check())
              @if (Auth::user()->role==1)
              <a id="m_aside_header_menu_mobile_toggle" href="javascript:;" class="m-brand__icon m-brand__toggler m--visible-tablet-and-mobile-inline-block">
                <span></span>
              </a>
              @endif
            @endif
            <a id="m_aside_header_topbar_mobile_toggle" href="javascript:;" class="m-brand__icon m--visible-tablet-and-mobile-inline-block">
              <i class="flaticon-more"></i>
            </a>
          </div>
        </div>
      </div>
      <div class="m-stack__item m-stack__item--fluid m-header-head" id="m_header_nav">
        <button class="m-aside-header-menu-mobile-close  m-aside-header-menu-mobile-close--skin-dark " id="m_aside_header_menu_mobile_close_btn">
          <i class="la la-close"></i>
        </button>
        <div id="m_header_topbar" class="m-topbar  m-stack m-stack--ver m-stack--general">
          <div class="m-stack__item m-stack__item--middle m-dropdown m-dropdown--arrow m-dropdown--large m-dropdown--mobile-full-width m-dropdown--align-right m-dropdown--skin-light m-header-search m-header-search--expandable m-header-search--skin-light" id="m_quicksearch" m-quicksearch-mode="default" m-dropdown-persistent="1">
          	<form class="m-header-search__form" action="{{url('search')}}" method="get">
          		<div class="m-header-search__wrapper">
          			<span class="m-header-search__icon-search" id="m_quicksearch_search">
          				<i class="flaticon-search"></i>
          			</span>
          			<span class="m-header-search__input-wrapper">
          				<input autocomplete="off" name="q" class="m-header-search__input" value="{{Request::input('q')}}" placeholder="Search..." id="m_quicksearch_input" type="text">
          			</span>
          			<span class="m-header-search__icon-close" id="m_quicksearch_close">
          				<i class="la la-remove"></i>
          			</span>
          			<span class="m-header-search__icon-cancel" id="m_quicksearch_cancel">
          				<i class="la la-remove"></i>
          			</span>
          		</div>
          	</form>
          	<!-- <div class="m-dropdown__wrapper" style="z-index: 101;">
          		<div class="m-dropdown__arrow m-dropdown__arrow--center"></div>
          		<div class="m-dropdown__inner">
          			<div class="m-dropdown__body">
          				<div class="m-dropdown__scrollable m-scrollable m-scroller ps" data-scrollable="true" data-height="300" data-mobile-height="200" style="height: 300px; overflow: hidden;">
          					<div class="m-dropdown__content m-list-search m-list-search--skin-light">
          					</div>
          				<div class="ps__rail-x" style="left: 0px; bottom: 0px;"><div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div></div><div class="ps__rail-y" style="top: 0px; right: 4px;"><div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 0px;"></div></div></div>
          			</div>
          		</div>
          	</div> -->
          </div>
          <div class="m-stack__item m-topbar__nav-wrapper">
            <ul class="m-topbar__nav m-nav m-nav--inline">
              @if (Auth::check())
                @if (Auth::user()->role==3)
                  <li class="m-nav__item m-dropdown m-dropdown--large m-dropdown--arrow m-dropdown--align-center m-dropdown--mobile-full-width m-dropdown--skin-light	m-list-search m-list-search--skin-light" m-dropdown-toggle="click" id="m_quicksearch" m-quicksearch-mode="dropdown" m-dropdown-persistent="1">
                    <a href="#" class="m-nav__link m-dropdown__toggle">
                      <span class="m-nav__link-icon">
                        <i class="flaticon-search-1"></i>
                      </span>
                    </a>
                    <div class="m-dropdown__wrapper">
                      <span class="m-dropdown__arrow m-dropdown__arrow--center"></span>
                      <div class="m-dropdown__inner ">
                        <div class="m-dropdown__header">
                          <form  class="m-list-search__form">
                            <div class="m-list-search__form-wrapper">
                              <span class="m-list-search__form-input-wrapper">
                                <input id="m_quicksearch_input" autocomplete="off" type="text" name="q" class="m-list-search__form-input" value="" placeholder="Search...">
                              </span>
                              <span class="m-list-search__form-icon-close" id="m_quicksearch_close">
                                <i class="la la-remove"></i>
                              </span>
                            </div>
                          </form>
                        </div>
                        <div class="m-dropdown__body">
                          <div class="m-dropdown__scrollable m-scrollable" data-scrollable="true" data-max-height="300" data-mobile-max-height="200">
                            <div class="m-dropdown__content"></div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </li>
                  <li class="m-nav__item m-topbar__notifications m-topbar__notifications--img m-dropdown m-dropdown--large m-dropdown--header-bg-fill m-dropdown--arrow m-dropdown--align-center 	m-dropdown--mobile-full-width" m-dropdown-toggle="click" m-dropdown-persistent="1">
                    <a href="#" class="m-nav__link m-dropdown__toggle" id="m_topbar_notification_icon">
                      <span class="m-nav__link-badge m-badge m-badge--dot m-badge--dot-small m-badge--danger"></span>
                      <span class="m-nav__link-icon">
                        <i class="flaticon-music-2"></i>
                      </span>
                    </a>
                    <div class="m-dropdown__wrapper">
                      <span class="m-dropdown__arrow m-dropdown__arrow--center"></span>
                      <div class="m-dropdown__inner">
                        <div class="m-dropdown__header m--align-center" style="background: url({{asset('assets/crm/app/media/img/misc/notification_bg.jpg')}}); background-size: cover;">
                          <span class="m-dropdown__header-title">
                            9 New
                          </span>
                          <span class="m-dropdown__header-subtitle">
                            User Notifications
                          </span>
                        </div>
                        <div class="m-dropdown__body">
                          <div class="m-dropdown__content">
                            <ul class="nav nav-tabs m-tabs m-tabs-line m-tabs-line--brand" role="tablist">
                              <li class="nav-item m-tabs__item">
                                <a class="nav-link m-tabs__link active" data-toggle="tab" href="#topbar_notifications_notifications" role="tab">
                                  Alerts
                                </a>
                              </li>
                              <li class="nav-item m-tabs__item">
                                <a class="nav-link m-tabs__link" data-toggle="tab" href="#topbar_notifications_events" role="tab">
                                  Events
                                </a>
                              </li>
                              <li class="nav-item m-tabs__item">
                                <a class="nav-link m-tabs__link" data-toggle="tab" href="#topbar_notifications_logs" role="tab">
                                  Logs
                                </a>
                              </li>
                            </ul>
                            <div class="tab-content">
                              <div class="tab-pane active" id="topbar_notifications_notifications" role="tabpanel">
                                <div class="m-scrollable" data-scrollable="true" data-max-height="250" data-mobile-max-height="200">
                                  <div class="m-list-timeline m-list-timeline--skin-light">
                                    <div class="m-list-timeline__items">
                                      <div class="m-list-timeline__item">
                                        <span class="m-list-timeline__badge -m-list-timeline__badge--state-success"></span>
                                        <span class="m-list-timeline__text">
                                          12 new users registered
                                        </span>
                                        <span class="m-list-timeline__time">
                                          Just now
                                        </span>
                                      </div>
                                      <div class="m-list-timeline__item">
                                        <span class="m-list-timeline__badge"></span>
                                        <span class="m-list-timeline__text">
                                          System shutdown
                                          <span class="m-badge m-badge--success m-badge--wide">
                                            pending
                                          </span>
                                        </span>
                                        <span class="m-list-timeline__time">
                                          14 mins
                                        </span>
                                      </div>
                                      <div class="m-list-timeline__item">
                                        <span class="m-list-timeline__badge"></span>
                                        <span class="m-list-timeline__text">
                                          New invoice received
                                        </span>
                                        <span class="m-list-timeline__time">
                                          20 mins
                                        </span>
                                      </div>
                                      <div class="m-list-timeline__item">
                                        <span class="m-list-timeline__badge"></span>
                                        <span class="m-list-timeline__text">
                                          DB overloaded 80%
                                          <span class="m-badge m-badge--info m-badge--wide">
                                            settled
                                          </span>
                                        </span>
                                        <span class="m-list-timeline__time">
                                          1 hr
                                        </span>
                                      </div>
                                      <div class="m-list-timeline__item">
                                        <span class="m-list-timeline__badge"></span>
                                        <span class="m-list-timeline__text">
                                          System error -
                                          <a href="#" class="m-link">
                                            Check
                                          </a>
                                        </span>
                                        <span class="m-list-timeline__time">
                                          2 hrs
                                        </span>
                                      </div>
                                      <div class="m-list-timeline__item m-list-timeline__item--read">
                                        <span class="m-list-timeline__badge"></span>
                                        <span href="" class="m-list-timeline__text">
                                          New order received
                                          <span class="m-badge m-badge--danger m-badge--wide">
                                            urgent
                                          </span>
                                        </span>
                                        <span class="m-list-timeline__time">
                                          7 hrs
                                        </span>
                                      </div>
                                      <div class="m-list-timeline__item m-list-timeline__item--read">
                                        <span class="m-list-timeline__badge"></span>
                                        <span class="m-list-timeline__text">
                                          Production server down
                                        </span>
                                        <span class="m-list-timeline__time">
                                          3 hrs
                                        </span>
                                      </div>
                                      <div class="m-list-timeline__item">
                                        <span class="m-list-timeline__badge"></span>
                                        <span class="m-list-timeline__text">
                                          Production server up
                                        </span>
                                        <span class="m-list-timeline__time">
                                          5 hrs
                                        </span>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <div class="tab-pane" id="topbar_notifications_events" role="tabpanel">
                                <div class="m-scrollable" data-scrollable="true" data-max-height="250" data-mobile-max-height="200">
                                  <div class="m-list-timeline m-list-timeline--skin-light">
                                    <div class="m-list-timeline__items">
                                      <div class="m-list-timeline__item">
                                        <span class="m-list-timeline__badge m-list-timeline__badge--state1-success"></span>
                                        <a href="" class="m-list-timeline__text">
                                          New order received
                                        </a>
                                        <span class="m-list-timeline__time">
                                          Just now
                                        </span>
                                      </div>
                                      <div class="m-list-timeline__item">
                                        <span class="m-list-timeline__badge m-list-timeline__badge--state1-danger"></span>
                                        <a href="" class="m-list-timeline__text">
                                          New invoice received
                                        </a>
                                        <span class="m-list-timeline__time">
                                          20 mins
                                        </span>
                                      </div>
                                      <div class="m-list-timeline__item">
                                        <span class="m-list-timeline__badge m-list-timeline__badge--state1-success"></span>
                                        <a href="" class="m-list-timeline__text">
                                          Production server up
                                        </a>
                                        <span class="m-list-timeline__time">
                                          5 hrs
                                        </span>
                                      </div>
                                      <div class="m-list-timeline__item">
                                        <span class="m-list-timeline__badge m-list-timeline__badge--state1-info"></span>
                                        <a href="" class="m-list-timeline__text">
                                          New order received
                                        </a>
                                        <span class="m-list-timeline__time">
                                          7 hrs
                                        </span>
                                      </div>
                                      <div class="m-list-timeline__item">
                                        <span class="m-list-timeline__badge m-list-timeline__badge--state1-info"></span>
                                        <a href="" class="m-list-timeline__text">
                                          System shutdown
                                        </a>
                                        <span class="m-list-timeline__time">
                                          11 mins
                                        </span>
                                      </div>
                                      <div class="m-list-timeline__item">
                                        <span class="m-list-timeline__badge m-list-timeline__badge--state1-info"></span>
                                        <a href="" class="m-list-timeline__text">
                                          Production server down
                                        </a>
                                        <span class="m-list-timeline__time">
                                          3 hrs
                                        </span>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <div class="tab-pane" id="topbar_notifications_logs" role="tabpanel">
                                <div class="m-stack m-stack--ver m-stack--general" style="min-height: 180px;">
                                  <div class="m-stack__item m-stack__item--center m-stack__item--middle">
                                    <span class="">
                                      All caught up!
                                      <br>
                                      No new logs.
                                    </span>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </li>
                @endif
                <li class="m-nav__item m-topbar__quick-actions m-topbar__quick-actions--img m-dropdown m-dropdown--large m-dropdown--header-bg-fill m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push m-dropdown--mobile-full-width m-dropdown--skin-light"  m-dropdown-toggle="click">
                  <a href="#" class="m-nav__link m-dropdown__toggle">
                    <span class="m-nav__link-badge m-badge m-badge--dot m-badge--info m--hide"></span>
                    <span class="m-nav__link-icon">
                      <i class="flaticon-business"></i>
                    </span>
                    <span class="m-nav__link-badge m-badge m-badge--info">{{(isset($carts) ? $carts->count() : '0')}}</span>
                  </a>
                  <div class="m-dropdown__wrapper">
                    <span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"></span>
                    <div class="m-dropdown__inner">
                      <div class="m-dropdown__body">
                        @if ($carts_exists)
                        <div class="m-dropdown__content">
                          <div class="tab-content">
                            <div class="m-scrollable" data-scrollable="true" data-max-height="250" data-mobile-max-height="200">
                              <div class="m-list-timeline m-list-timeline--skin-light">
                                <div class="m-list-timeline__items">
                                  @foreach($carts as $key => $cart)
                                  <div class="m-list-timeline__item">
                                    <span class="m-list-timeline__badge -m-list-timeline__badge--state-success"></span>
                                    <span class="m-list-timeline__text">
                                      <img class="m--marginless m--img-centered s-img-40" src="{{ Storage::disk('public')->url($cart->item->item_images_primary->photo_url) }}" alt=""/>
                                      {{$cart->item->name}}
                                    </span>
                                    <span class="m-list-timeline__time">
                                      <p class="m-0">Rp {{number_format($cart->item->price*$cart->qty)}}</p>
                                      <p class="m-0">{{$cart->qty}} Buah</p>
                                    </span>
                                  </div>
                                  @endforeach
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="mt-4 mb-1">
                            <a href="{{url('cart')}}" class="btn btn-info pull-right text-white">Lihat Semua</a>
                          </div>
                        </div>
                        @else
                        <div class="m-dropdown__content">
                          <div class="tab-content">
                            <p class="text-info">Keranjang belanja anda kosong.</p>
                            <p class="m-0">
                              <small>Silahkan pilih product yang anda ingin beli terlebih dahulu.</small>
                            </p>
                          </div>
                        </div>
                        @endif
                      </div>
                    </div>
                  </div>
                </li>
                <li class="m-nav__item m-topbar__user-profile m-topbar__user-profile--img  m-dropdown m-dropdown--medium m-dropdown--arrow m-dropdown--header-bg-fill m-dropdown--align-right m-dropdown--mobile-full-width m-dropdown--skin-light" m-dropdown-toggle="click">
                  <a href="#" class="m-nav__link m-dropdown__toggle">
                    <span class="m-topbar__userpic">
                      <img class="m--img-rounded m--marginless m--img-centered s-img-40" src="{{ (Auth::user()->photo_url != '' ? Storage::disk('public')->url(Auth::user()->photo_url) : asset('assets/logo/logo-rd.png')) }}" alt=""/>
                    </span>
                    <span class="m-topbar__username m--hide">
                      {{Auth::user()->name}}
                    </span>
                  </a>
                  <div class="m-dropdown__wrapper">
                    <span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"></span>
                    <div class="m-dropdown__inner">
                      <div class="m-dropdown__header m--align-center" style="background:rgb(245, 233, 241) ; background-size: cover;">
                        <div class="m-card-user m-card-user--skin-dark">
                          <div class="m-card-user__pic">
                            <img class="m--img-rounded m--marginless s-img-50" src="{{ (Auth::user()->photo_url != '' ? Storage::disk('public')->url(Auth::user()->photo_url) : asset('assets/logo/logo-rd.png')) }}" alt=""/>
                          </div>
                          <div class="m-card-user__details">
                            <span class="m-card-user__name m--font-weight-500" style="color:rgb(45, 36, 46)">
                              {!! Auth::user()->name !!}
                            </span>
                            <a href="" class="m-card-user__email m--font-weight-300 m-link" style="color:rgb(63, 55, 64)">
                              {!! Auth::user()->email !!}
                            </a>
                          </div>
                        </div>
                      </div>
                      <div class="m-dropdown__body">
                        <div class="m-dropdown__content">
                          <ul class="m-nav m-nav--skin-light">
                            <li class="m-nav__section m--hide">
                              <span class="m-nav__section-text">
                                Section
                              </span>
                            </li>
                            <li class="m-nav__item">
                              <a href="{{url('member/profile')}}" class="m-nav__link">
                                <i class="m-nav__link-icon flaticon-profile-1"></i>
                                <span class="m-nav__link-title">
                                  <span class="m-nav__link-wrap">
                                    <span class="m-nav__link-text">
                                      Profil Saya
                                    </span>
                                    @if (!Auth::user()->verified)
                                      <span class="m-nav__link-badge">
                                        <span class="m-badge m-badge--warning">
                                          1
                                        </span>
                                      </span>
                                    @endif
                                  </span>
                                </span>
                              </a>
                            </li>
                            <li class="m-nav__item">
                              <a href="{{url('member/h/t')}}" class="m-nav__link">
                                <i class="m-nav__link-icon flaticon-list-1"></i>
                                <span class="m-nav__link-title">
                                  <span class="m-nav__link-wrap">
                                    <span class="m-nav__link-text">
                                      History Pembelian
                                    </span>
                                    @if (false)
                                      <span class="m-nav__link-badge">
                                        <span class="m-badge m-badge--warning">
                                          1
                                        </span>
                                      </span>
                                    @endif
                                  </span>
                                </span>
                              </a>
                            </li>
                            <li class="m-nav__separator m-nav__separator--fit"></li>
                            <li class="m-nav__item">
                              <a href="{{url('logout')}}" class="btn m-btn--pill    btn-secondary m-btn m-btn--custom m-btn--label-brand m-btn--bolder">
                                Logout
                              </a>
                            </li>
                          </ul>
                        </div>
                      </div>
                    </div>
                  </div>
                </li>
              @endif
              @if (Auth::check())
                @if (Auth::user()->role==3 || Auth::user()->role==1)
                  <li id="m_quick_sidebar_toggle" class="m-nav__item">
                    <a href="#" class="m-nav__link m-dropdown__toggle">
                      <span class="m-nav__link-icon">
                        <i class="flaticon-chat-1"></i>
                      </span>
                      <span class="m-menu__link-badge" id="h_b_notif"></span>
                    </a>
                  </li>
                @endif
              @endif
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
</header>
