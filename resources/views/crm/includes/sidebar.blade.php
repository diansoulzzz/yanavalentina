<button class="m-aside-left-close m-aside-left-close--skin-dark" id="m_aside_left_close_btn">
  <i class="la la-close"></i>
</button>
<div id="m_aside_left" class="m-grid__item	m-aside-left  m-aside-left--skin-dark">
  <div id="m_ver_menu" class="m-aside-menu m-aside-menu--skin-dark m-aside-menu--submenu-skin-dark" m-menu-vertical="1" m-menu-scrollable="0" m-menu-dropdown-timeout="0">
    <ul class="m-menu__nav  m-menu__nav--dropdown-submenu-arrow ">
      @if (Auth::check())
      @if (Auth::user()->role==0 || Auth::user()->role==3)
      <li class="m-menu__item  {{ Request::is('home') ? 'm-menu__item--active' : '' }}" aria-haspopup="true" >
        <a href="{{url('home')}}" class="m-menu__link ">
          <i class="m-menu__link-icon fa fa-home"></i>
          <span class="m-menu__link-title">
            <span class="m-menu__link-wrap">
              <span class="m-menu__link-text">
                Home
              </span>
            </span>
          </span>
        </a>
      </li>
      @endif
      @if (Auth::user()->role==0 || Auth::user()->role==3)
      <li class="m-menu__item  {{ Request::is('dashboard') ? 'm-menu__item--active' : '' }}" aria-haspopup="true" >
        <a href="{{url('dashboard')}}" class="m-menu__link ">
          <i class="m-menu__link-icon flaticon-line-graph"></i>
          <span class="m-menu__link-title">
            <span class="m-menu__link-wrap">
              <span class="m-menu__link-text">
                Dashboard
              </span>
            </span>
          </span>
        </a>
      </li>
      <li class="m-menu__item  {{ Request::is('chat') ? 'm-menu__item--active' : '' }}" aria-haspopup="true" >
        <a href="{{url('chat')}}" class="m-menu__link ">
          <i class="m-menu__link-icon flaticon-email "></i>
          <span class="m-menu__link-title">
            <span class="m-menu__link-wrap">
              <span class="m-menu__link-text">
                Chat
              </span>
              <span class="m-menu__link-badge" id="s_b_chat">
              </span>
            </span>
          </span>
        </a>
      </li>
      <li class="m-menu__section">
        <h4 class="m-menu__section-text">
          Master
        </h4>
        <i class="m-menu__section-icon flaticon-more-v3"></i>
      </li>
      <li class="m-menu__item  m-menu__item--submenu {{ Request::is('master/users/*') ? 'm-menu__item--open m-menu__item--expanded' : '' }}" aria-haspopup="true"  m-menu-submenu-toggle="hover">
        <a  href="javascript:;" class="m-menu__link m-menu__toggle">
          <i class="m-menu__link-icon flaticon-users "></i>
          <span class="m-menu__link-text">
            User
          </span>
          <i class="m-menu__ver-arrow la la-angle-right"></i>
        </a>
        <div class="m-menu__submenu ">
          <span class="m-menu__arrow"></span>
          <ul class="m-menu__subnav">
            <li class="m-menu__item  m-menu__item--parent" aria-haspopup="true" >
              <span class="m-menu__link">
                <span class="m-menu__link-text">
                  User
                </span>
              </span>
            </li>
            <li class="m-menu__item {{ Request::is('master/users/list') ? 'm-menu__item--active' : '' }}" aria-haspopup="true" >
              <a  href="{{url('master/users/list')}}" class="m-menu__link ">
                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                  <span></span>
                </i>
                <span class="m-menu__link-text">
                  Lihat Data
                </span>
              </a>
            </li>
          </ul>
        </div>
      </li>
      <li class="m-menu__item  m-menu__item--submenu {{ Request::is('cash/*') ? 'm-menu__item--open m-menu__item--expanded' : '' }}" aria-haspopup="true"  m-menu-submenu-toggle="hover">
        <a  href="javascript:;" class="m-menu__link m-menu__toggle">
          <i class="m-menu__link-icon fa fa-dollar-sign"></i>
          <span class="m-menu__link-text">
            Cash
          </span>
          <i class="m-menu__ver-arrow la la-angle-right"></i>
        </a>
        <div class="m-menu__submenu ">
          <span class="m-menu__arrow"></span>
          <ul class="m-menu__subnav">
            <li class="m-menu__item  m-menu__item--parent" aria-haspopup="true" >
              <span class="m-menu__link">
                <span class="m-menu__link-text">
                  Cash
                </span>
              </span>
            </li>
            <li class="m-menu__item {{ Request::is('cash/entry') ? 'm-menu__item--active' : '' }}" aria-haspopup="true" >
              <a  href="{{url('cash/entry')}}" class="m-menu__link ">
                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                  <span></span>
                </i>
                <span class="m-menu__link-text">
                  Daftar Cash
                </span>
              </a>
            </li>
            <li class="m-menu__item {{ Request::is('cash/list') ? 'm-menu__item--active' : '' }}" aria-haspopup="true" >
              <a  href="{{url('cash/list')}}" class="m-menu__link ">
                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                  <span></span>
                </i>
                <span class="m-menu__link-text">
                  Lihat Data
                </span>
              </a>
            </li>
          </ul>
        </div>
      </li>
      <li class="m-menu__item  m-menu__item--submenu {{ Request::is('master/schedule/*') ? 'm-menu__item--open m-menu__item--expanded' : '' }}" aria-haspopup="true"  m-menu-submenu-toggle="hover">
        <a  href="javascript:;" class="m-menu__link m-menu__toggle">
          <i class="m-menu__link-icon fa fa-bullhorn"></i>
          <span class="m-menu__link-text">
            Broadcast
          </span>
          <i class="m-menu__ver-arrow la la-angle-right"></i>
        </a>
        <div class="m-menu__submenu ">
          <span class="m-menu__arrow"></span>
          <ul class="m-menu__subnav">
            <li class="m-menu__item  m-menu__item--parent" aria-haspopup="true" >
              <span class="m-menu__link">
                <span class="m-menu__link-text">
                  Broadcast
                </span>
              </span>
            </li>
            <li class="m-menu__item {{ Request::is('broadcast/entry') ? 'm-menu__item--active' : '' }}" aria-haspopup="true" >
              <a  href="{{url('broadcast/entry')}}" class="m-menu__link ">
                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                  <span></span>
                </i>
                <span class="m-menu__link-text">
                  Daftar Baru
                </span>
              </a>
            </li>
            <!-- <li class="m-menu__item {{ Request::is('broadcast/list') ? 'm-menu__item--active' : '' }}" aria-haspopup="true" >
              <a  href="{{url('broadcast/list')}}" class="m-menu__link ">
                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                  <span></span>
                </i>
                <span class="m-menu__link-text">
                  Lihat Data
                </span>
              </a>
            </li> -->
          </ul>
        </div>
      </li>
      <li class="m-menu__item  m-menu__item--submenu {{ Request::is('master/schedule/*') ? 'm-menu__item--open m-menu__item--expanded' : '' }}" aria-haspopup="true"  m-menu-submenu-toggle="hover">
        <a  href="javascript:;" class="m-menu__link m-menu__toggle">
          <i class="m-menu__link-icon flaticon-calendar"></i>
          <span class="m-menu__link-text">
            Jadwal
          </span>
          <i class="m-menu__ver-arrow la la-angle-right"></i>
        </a>
        <div class="m-menu__submenu ">
          <span class="m-menu__arrow"></span>
          <ul class="m-menu__subnav">
            <li class="m-menu__item  m-menu__item--parent" aria-haspopup="true" >
              <span class="m-menu__link">
                <span class="m-menu__link-text">
                  Jadwal
                </span>
              </span>
            </li>
            <li class="m-menu__item {{ Request::is('master/schedule/entry') ? 'm-menu__item--active' : '' }}" aria-haspopup="true" >
              <a  href="{{url('master/schedule/entry')}}" class="m-menu__link ">
                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                  <span></span>
                </i>
                <span class="m-menu__link-text">
                  Daftar Baru
                </span>
              </a>
            </li>
            <li class="m-menu__item {{ Request::is('master/schedule/list') ? 'm-menu__item--active' : '' }}" aria-haspopup="true" >
              <a  href="{{url('master/schedule/list')}}" class="m-menu__link ">
                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                  <span></span>
                </i>
                <span class="m-menu__link-text">
                  Lihat Data
                </span>
              </a>
            </li>
          </ul>
        </div>
      </li>
      <li class="m-menu__item  m-menu__item--submenu {{ Request::is('master/item-category/*') ? 'm-menu__item--open m-menu__item--expanded' : '' }}" aria-haspopup="true"  m-menu-submenu-toggle="hover">
        <a  href="javascript:;" class="m-menu__link m-menu__toggle">
          <i class="m-menu__link-icon flaticon-file-1 "></i>
          <span class="m-menu__link-text">
            Barang Kategori
          </span>
          <i class="m-menu__ver-arrow la la-angle-right"></i>
        </a>
        <div class="m-menu__submenu ">
          <span class="m-menu__arrow"></span>
          <ul class="m-menu__subnav">
            <li class="m-menu__item  m-menu__item--parent" aria-haspopup="true" >
              <span class="m-menu__link">
                <span class="m-menu__link-text">
                  Barang Kategori
                </span>
              </span>
            </li>
            <li class="m-menu__item {{ Request::is('master/item-category/entry') ? 'm-menu__item--active' : '' }}" aria-haspopup="true" >
              <a  href="{{url('master/item-category/entry')}}" class="m-menu__link ">
                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                  <span></span>
                </i>
                <span class="m-menu__link-text">
                  Daftar Baru
                </span>
              </a>
            </li>
            <li class="m-menu__item {{ Request::is('master/item-category/list') ? 'm-menu__item--active' : '' }}" aria-haspopup="true" >
              <a  href="{{url('master/item-category/list')}}" class="m-menu__link ">
                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                  <span></span>
                </i>
                <span class="m-menu__link-text">
                  Lihat Data
                </span>
              </a>
            </li>
          </ul>
        </div>
      </li>
      <li class="m-menu__item  m-menu__item--submenu {{ Request::is('master/item/*') ? 'm-menu__item--open m-menu__item--expanded' : '' }}" aria-haspopup="true"  m-menu-submenu-toggle="hover">
        <a  href="javascript:;" class="m-menu__link m-menu__toggle">
          <i class="m-menu__link-icon flaticon-open-box"></i>
          <span class="m-menu__link-text">
            Barang
          </span>
          <i class="m-menu__ver-arrow la la-angle-right"></i>
        </a>
        <div class="m-menu__submenu ">
          <span class="m-menu__arrow"></span>
          <ul class="m-menu__subnav">
            <li class="m-menu__item  m-menu__item--parent" aria-haspopup="true" >
              <span class="m-menu__link">
                <span class="m-menu__link-text">
                  Barang
                </span>
              </span>
            </li>
            <li class="m-menu__item {{ Request::is('master/item/entry') ? 'm-menu__item--active' : '' }}" aria-haspopup="true" >
              <a  href="{{url('master/item/entry')}}" class="m-menu__link ">
                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                  <span></span>
                </i>
                <span class="m-menu__link-text">
                  Daftar Baru
                </span>
              </a>
            </li>
            <li class="m-menu__item {{ Request::is('master/item/list') ? 'm-menu__item--active' : '' }}" aria-haspopup="true" >
              <a  href="{{url('master/item/list')}}" class="m-menu__link ">
                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                  <span></span>
                </i>
                <span class="m-menu__link-text">
                  Lihat Data
                </span>
              </a>
            </li>
          </ul>
        </div>
      </li>

      <li class="m-menu__item  m-menu__item--submenu {{ Request::is('cms/*') ? 'm-menu__item--open m-menu__item--expanded' : '' }}" aria-haspopup="true"  m-menu-submenu-toggle="hover">
        <a  href="javascript:;" class="m-menu__link m-menu__toggle">
          <i class="m-menu__link-icon flaticon-confetti"></i>
          <span class="m-menu__link-text">
            Promosi
          </span>
          <i class="m-menu__ver-arrow la la-angle-right"></i>
        </a>
        <div class="m-menu__submenu ">
          <span class="m-menu__arrow"></span>
          <ul class="m-menu__subnav">
            <li class="m-menu__item  m-menu__item--parent" aria-haspopup="true" >
              <span class="m-menu__link">
                <span class="m-menu__link-text">
                  Promosi
                </span>
              </span>
            </li>
            <li class="m-menu__item {{ Request::is('cms/promotion/entry') ? 'm-menu__item--active' : '' }}" aria-haspopup="true" >
              <a  href="{{url('cms/promotion/entry')}}" class="m-menu__link ">
                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                  <span></span>
                </i>
                <span class="m-menu__link-text">
                  Daftar Baru
                </span>
              </a>
            </li>
            <li class="m-menu__item {{ Request::is('cms/promotion/list') ? 'm-menu__item--active' : '' }}" aria-haspopup="true" >
              <a  href="{{url('cms/promotion/list')}}" class="m-menu__link ">
                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                  <span></span>
                </i>
                <span class="m-menu__link-text">
                  Lihat Data
                </span>
              </a>
            </li>
          </ul>
        </div>
      </li>
      @endif
      @endif
      <!-- <li class="m-menu__section">
        <h4 class="m-menu__section-text">

        </h4>
        <i class="m-menu__section-icon flaticon-more-v3"></i>
      </li> -->
      <li class="m-menu__item {{ Request::is('member/profile/*') ? 'm-menu__item--open m-menu__item--expanded' : '' }}" aria-haspopup="true"  m-menu-submenu-toggle="hover">
        <a  href="javascript:;" class="m-menu__link m-menu__toggle">
          <i class="m-menu__link-icon flaticon-profile"></i>
          <span class="m-menu__link-text">
            Profil Saya
          </span>
          <i class="m-menu__ver-arrow la la-angle-right"></i>
        </a>
        <div class="m-menu__submenu ">
          <span class="m-menu__arrow"></span>
          <ul class="m-menu__subnav">
            <li class="m-menu__item  m-menu__item--parent" aria-haspopup="true" >
              <span class="m-menu__link">
                <span class="m-menu__link-text">
                  Profil Saya
                </span>
              </span>
            </li>
            <li class="m-menu__item {{ Request::is('member/profile') ? 'm-menu__item--active' : '' }}" aria-haspopup="true" >
              <a  href="{{url('member/profile')}}" class="m-menu__link ">
                <!-- <i class="m-menu__link-icon flaticon-layers"><span></span></i> -->
                <span class="m-menu__link-text">
                  Pengaturan
                </span>
              </a>
            </li>
            <li class="m-menu__item {{ Request::is('member/profile/address') ? 'm-menu__item--active' : '' }}" aria-haspopup="true" >
              <a  href="{{url('member/profile/address')}}" class="m-menu__link ">
                <!-- <i class="m-menu__link-icon flaticon-layers"><span></span></i> -->
                <span class="m-menu__link-text">
                  Alamat
                </span>
              </a>
            </li>
            <li class="m-menu__item {{ Request::is('member/profile/account') ? 'm-menu__item--active' : '' }}" aria-haspopup="true" >
              <a  href="{{url('member/profile/account')}}" class="m-menu__link ">
                <!-- <i class="m-menu__link-icon flaticon-layers"><span></span></i> -->
                <span class="m-menu__link-text">
                  Keamanan Akun
                </span>
              </a>
            </li>
          </ul>
        </div>
      </li>
    </ul>
  </div>
</div>
