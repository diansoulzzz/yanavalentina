<div id="m_quick_sidebar" class="m-quick-sidebar m-quick-sidebar--tabbed m-quick-sidebar--skin-light" style="background-color: #f9e6ee !important;">
  <div class="m-quick-sidebar__content m--hide">
    <span id="m_quick_sidebar_close" class="m-quick-sidebar__close">
      <i class="la la-close"></i>
    </span>
    <ul id="m_quick_sidebar_tabs" class="nav nav-tabs m-tabs m-tabs-line m-tabs-line--brand" role="tablist">
      <li class="nav-item m-tabs__item">
        <a class="nav-link m-tabs__link active" data-toggle="tab" href="#m_quick_sidebar_tabs_messenger" role="tab">
          Messages
        </a>
      </li>
    </ul>
    <div class="tab-content">
      <div class="tab-pane active" id="m_quick_sidebar_tabs_messenger"  role="tabpanel">
        <div class="m-messenger m-messenger--message-arrow m-messenger--skin-light">
          <div class="m-messenger__messages" id="m_quick_sidebar_tabs_messenger_messages"></div>
          <div class="m-messenger__seperator"></div>
          <div class="m-messenger__form">
            <div class="m-messenger__form-controls">
              <input type="text" id="m_quick_sidebar_chat_message_text" name="" placeholder="Type here..." class="m-messenger__form-input">
            </div>
            <div class="m-messenger__form-tools">
              <a href="javascript:void(0);" id="m_quick_sidebar_chat_message_send" class="m-messenger__form-attachment">
                <i class="la la-send"></i>
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@push('b-css')

@endpush

@push('b-script')
<script>
var mQuickSidebar = function() {
    var t = $("#m_quick_sidebar"),
        e = $("#m_quick_sidebar_tabs"),
        a = t.find(".m-quick-sidebar__content"),
        n = function() {
            var a, n, o, i;
            a = mUtil.find(mUtil.get("m_quick_sidebar_tabs_messenger"), ".m-messenger__messages"), n = $("#m_quick_sidebar_tabs_messenger .m-messenger__form"),
            mUtil.scrollerInit(a, {
                disableForMobile: !0,
                resetHeightOnDestroy: !1,
                handleWindowResize: !0,
                height: function() {
                    return t.outerHeight(!0) - e.outerHeight(!0) - n.outerHeight(!0) - 120
                }
            })
        };
    return {
        init: function() {
            0 !== t.length && new mOffcanvas("m_quick_sidebar", {
                overlay: !0,
                baseClass: "m-quick-sidebar",
                closeBy: "m_quick_sidebar_close",
                toggleBy: "m_quick_sidebar_toggle"
            }).one("afterShow", function() {
                mApp.block(t), setTimeout(function() {
                    masterform.rc_init();
                    mApp.unblock(t), a.removeClass("m--hide"), n()
                }, 1e3)
            })
        }
    }
}();
$(function() {
    mQuickSidebar.init();
    masterform.ch_init();
    setInterval(function(){
      if ($('div.m-quick-sidebar').hasClass("m-quick-sidebar--on")) {
        masterform.rc_init();
      }
    },2000);
}), masterform = {
  first_time : true,
  rc_init : function () {
    $.ajax({
        url: "{{url('member/chat/message')}}",
        type: "GET",
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success : function(data) {
          masterform.bl_init(data);
        }
    });
  },
  bl_init : function (data) {
    $result = data['data'];
    $message = $result['chat']['message'];
    $is_new = $result['chat']['is_new'];
    $msg='';
    $.each($message, function(index, item) {
        $msg = $msg +
        '<div class="m-messenger__datetime">'+
          index+
        '</div>';
      $.each(item, function(index2, message) {
        if (message['is_opposite']) {
          $msg = $msg +
          '<div class="m-messenger__wrapper">'+
            '<div class="m-messenger__message m-messenger__message--in">'+
              '<div class="m-messenger__message-pic">'+
                '<img src="'+message['users_photo_url']+'" alt=""/>'+
              '</div>'+
              '<div class="m-messenger__message-body">'+
                '<div class="m-messenger__message-arrow"></div>'+
                '<div class="m-messenger__message-content">'+
                  '<div class="m-messenger__message-username">'+
                    message['users_name']+
                  '</div>'+
                  '<div class="m-messenger__message-text">'+
                    message['chat']+
                  '</div>'+
                '</div>'+
                '<div class="m-messenger__message-footer">'+
                  '<small>'+message['time_chat']+'</small>'+
                '</div>'+
              '</div>'+
            '</div>'+
          '</div>';
        } else {
          $msg = $msg +
          '<div class="m-messenger__wrapper">'+
            '<div class="m-messenger__message m-messenger__message--out">'+
              '<div class="m-messenger__message-body">'+
                '<div class="m-messenger__message-arrow"></div>'+
                '<div class="m-messenger__message-content">'+
                  '<div class="m-messenger__message-text">'+
                    message['chat']+
                  '</div>'+
                '</div>'+
                '<div class="m-messenger__message-footer">'+
                  ((message['count_read_by_opposite']>0) ?
                  '<span class="fa-stack text-success">'+
                      '<i class="fa fa-check fa-stack-1x" style="margin-left:4px"></i>'+
                      '<i class="fa fa-check  fa-stack-1x" style="margin-left:-4px"></i>'+
                  '</span>' :
                  '<span class="fa-stack text-success">'+
                      '<i class="fa fa-check fa-stack-1x" style="margin-left:4px"></i>'+
                  '</span>')+
                  '<small>'+message['time_chat']+'</small>'+
                '</div>'+
              '</div>'+
            '</div>'+
          '</div>';
        }
      });
    });
    $('#m_quick_sidebar_tabs_messenger_messages').html($msg);
    if ((masterform.first_time) || ($is_new))
    {
        $("#m_quick_sidebar_tabs_messenger_messages").scrollTop($('#m_quick_sidebar_tabs_messenger_messages')[0].scrollHeight);
        masterform.first_time=false;
    }
    // masterform.ps_init();
  },
  ch_init : function () {
    $('#m_quick_sidebar_chat_message_send').on('click',function(){
      $msg = $("#m_quick_sidebar_chat_message_text").val();
      if ($msg != '')
      {
        $("#m_quick_sidebar_chat_message_text").val('');
        $.ajax({
            url: "{{url('member/chat/post')}}",
            type: "POST",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                msg : $msg,
            },
            success : function(data) {
                masterform.bl_init(data);
                $("#m_quick_sidebar_tabs_messenger_messages").scrollTop($('#m_quick_sidebar_tabs_messenger_messages')[0].scrollHeight);
            }
          });
      }
    });
    $("#m_quick_sidebar_chat_message_text").on('keyup', function (e) {
      if (e.keyCode == 13) {
        $('#m_quick_sidebar_chat_message_send').click();
      }
    });
  },
};
</script>
@endpush
