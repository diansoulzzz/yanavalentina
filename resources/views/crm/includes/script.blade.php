<script src="{{asset('assets/crm/vendors/base/vendors.bundle.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/crm/demo/default/base/scripts.bundle.min.js')}}" type="text/javascript"></script>
@if(config('app.debug') == 'false')
  @if(Auth::user()->role != 1)
  <script>
  $(function() {
      ultimate.nf_init();
      setInterval(function(){
        ultimate.nf_init();
      },1000);
  }), ultimate = {
    nf_init : function () {
      $.ajax({
          url: "{{url('nf')}}",
          type: "GET",
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
          success : function(data) {
            $result = data;
            ultimate.nfMessage($result['m_count']);
          }
      });
    },
    nfMessage : function (count) {
      $notif='';
      if ($result['m_count']>0){
        $notif =
        '<span class="m-badge m-badge--info">'+
          count +
        '</span>';
        $('#s_b_chat').html($notif);
        $('#h_b_notif').html($notif);
      } else {
        $('#s_b_chat').html($notif);
        $('#h_b_notif').html($notif);
      }
    },
    nfInfo : function(count) {

    }
  };
  </script>
  @endif
@endif
