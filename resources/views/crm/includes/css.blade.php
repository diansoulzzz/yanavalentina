<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
<script>
      WebFont.load({
        google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},
        active: function() {
            sessionStorage.fonts = true;
        }
      });
</script>
<link href="{{asset('assets/crm/vendors/base/vendors.bundle.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('assets/crm/demo/default/base/style.bundle.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('assets/font/font-stylesheet.css')}}" rel="stylesheet" type="text/css" />
<link rel="shortcut icon" href="{{asset('assets/logo/favicon.ico')}}" />
<style>
.s-img-40 {
  width:40px;
  max-width:40px;
  /* height:40px;
  max-height:40px; */
}
.s-img-50 {
  width:50px;
  max-width:50px;
  /* height:50px;
  max-height:50px; */
}
.s-img-80 {
  width:80px;
  max-width:80px;
  /* height:80px;
  max-height:80px; */
}
.s-img-90 {
  width:90px;
  max-width:90px;
  /* height:90px;
  max-height:90px; */
}
.s-img-100 {
  width:100px;
  max-width:100px;
  /* height:100px;
  max-height:100px; */
}
.s-img-300 {
  width:300px;
  max-width:300px;
  /* height:300px;
  max-height:300px; */
}


.btn.active {
  background-color: #022873 !important;
  border-color: #022873 !important;
}
.dz-image{
  max-width:120px;
  max-height:120px;
}
.dz-image img {
  width: 100%;
  height: auto;
}
.dz-progress {
  top: 65% !important;
}

input.parsley-success,
select.parsley-success,
textarea.parsley-success {
  border-color: green !important;
}

input.parsley-error,
select.parsley-error,
textarea.parsley-error {
  border-color: red !important;;
}

.parsley-errors-list {
  margin: 4px 0 3px;
  padding: 0;
  list-style-type: none;
  font-size: 0.9em;
  line-height: 0.9em;
  opacity: 0;
  transition: all .3s ease-in;
  -o-transition: all .3s ease-in;
  -moz-transition: all .3s ease-in;
  -webkit-transition: all .3s ease-in;
  color:red;
}

.parsley-errors-list.filled {
  opacity: 1;
}

.parsley-error + span.select2 span.select2-selection--single {
  border-color: red;
}
.parsley-success + span.select2 span.select2-selection--single {
  border-color: green;
}
.m-header--fixed-mobile .m-topbar {
  top : -60px;
}
</style>
