<div class="modal fade" id="modal-address" role="dialog" aria-labelledby="" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <form id="modal-address-form" class="m-form m-form--fit m-form--label-align-right" method="post" action="{{url()->current().'#profile'}}">
        @csrf
        <input type="hidden" name="eid" value="{{$users->eid}}"/>
        <div class="modal-header">
          <h5 class="modal-title" id="modal-address-header">Tambah Alamat Baru</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="form-group">
            <label for="recipient-name" class="form-control-label">Alamat Pengiriman:</label>
            <textarea class="form-control" name="address" id="modal-address-name" data-parsley-required="true" data-parsley-required-message="Alamat Pengiriman wajib di isi"></textarea>
          </div>
          <div class="form-group row">
            <div class="col-lg-6">
              <label>Nama Penerima:</label>
              <input type="text" class="form-control m-input" name="person" data-parsley-required="true" data-parsley-required-message="Nama Penerima wajib di isi">
              <span class="m-form__help">Tuan Budi</span>
            </div>
            <div class="col-lg-6">
              <label class="">Nomor HP:</label>
              <input type="text" class="form-control m-input" name="no_tel" data-parsley-required="true" data-parsley-type="digits" data-parsley-type-message="Nomor HP tidak valid" data-parsley-required-message="Nomor HP wajib di isi">
              <span class="m-form__help">Contoh: 081234567890</span>
            </div>
          </div>
          <div class="form-group row">
            <div class="col-lg-9">
              <label>Kota atau Kecamatan:</label>
              <select class="form-control m-select2" id="modal-address-country" name="subdistrict" data-parsley-required="true" data-parsley-errors-container="#select2-errors" data-parsley-required-message="Kota atau Kecamatan wajib di isi">
                <option value="">Cari Kota atau Kecamatan</option>
              </select>
              <div id="select2-errors"></div>
            </div>
            <div class="col-lg-3">
              <label class="form-control-label">Set Sebagai Utama:</label>
              <div class="m-checkbox-inline">
                <label class="m-checkbox">
                  <input type="checkbox" name="primary"> Ya
                  <span></span>
                </label>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <div class="mx-auto btn-group">
            <button type="button" class="btn btn-secondary mr-1" data-dismiss="modal" style="width: 150px;">Batal</button>
            <button type="submit" class="btn btn-primary ml-1" style="width: 150px;">Tambah Alamat</button>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
