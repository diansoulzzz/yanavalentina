<!DOCTYPE html>
<html lang="en" >
	<head>
		<meta charset="utf-8" />
		<title>Register | Yana Valentina</title>
		<meta name="description" content="Latest updates and statistic charts">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
		<script>
					WebFont.load({
						google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},
						active: function() {
								sessionStorage.fonts = true;
						}
					});
		</script>
		<link href="{{asset('assets/crm/vendors/base/vendors.bundle.css')}}" rel="stylesheet" type="text/css" />
		<link href="{{asset('assets/crm/demo/default/base/style.bundle.css')}}" rel="stylesheet" type="text/css" />
		<link href="{{asset('assets/font/font-stylesheet.css')}}" rel="stylesheet" type="text/css" />
		<link rel="shortcut icon" href="{{asset('assets/logo/favicon.ico')}}" />
		<style> @media screen and (max-height: 575px){ #rc-imageselect, .g-recaptcha {transform:scale(0.77);-webkit-transform:scale(0.77);transform-origin:0 0;-webkit-transform-origin:0 0;} } </style>
	</head>
	<body class="m--skin- m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >
		<div class="m-grid m-grid--hor m-grid--root m-page">
			<div class="m-grid__item m-grid__item--fluid m-grid m-grid--hor m-login m-login--signin m-login--2 m-login-2--skin-2" id="m_login" style="background-color: rgb(249, 230, 238);">
				<div class="m-grid__item m-grid__item--fluid	m-login__wrapper">
					<div class="m-login__container">
						<div class="m-login__logo">
							<a href="#" class="no-decoration">
								<img src="{{asset('assets/logo/logo-pk.png')}}"style="width:150px;height:150px">
								<h1 class="nopadding wild-font font-lg col-pink" style="margin-top:-20px !important;">Yana valentina</h1>
								<p class="col-pink" style="margin-top:-18px !important;"><small>Branded Bag's</small></p>
							</a>
						</div>
						<div class="m-login__signin">
							<div class="m-login__head">
								<h3 class="m-login__title">
									Sign Up
								</h3>
								<div class="m-login__desc">
									Silahkan memasukan detail data diri anda:
								</div>
							</div>
							<form id="form_validate" class="m-login__form m-form" action="{{url()->current()}}" method="post">
			          @if (session('success'))
			            <div class="alert alert-success alert-dismissible fade show" role="alert">
			              <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
										  {{ session('success') }}
			            </div>
			          @endif
			          @if ($errors->any())
			            <div class="alert alert-danger alert-dismissible fade show" role="alert">
			              <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
			                <ul>
			                    @foreach ($errors->all() as $message)
			                        <li>{{$message}}</li>
			                    @endforeach
			                </ul>
			            </div>
			          @endif
								@csrf
								<div class="form-group m-form__group">
									<input class="form-control m-input" type="text" placeholder="Nama Lengkap" name="name" value="{{old('name')}}">
								</div>
								<div class="form-group m-form__group">
									<input class="form-control m-input" type="text" placeholder="Email" name="email" autocomplete="off" value="{{old('email')}}">
								</div>
								<div class="form-group m-form__group">
									<input class="form-control m-input" type="text" placeholder="Nomor Telepon" name="no_telp" autocomplete="off" value="{{old('no_telp')}}">
								</div>
								<div class="form-group m-form__group">
									<div class="col" style="margin-top:10px;">
										<label>Jenis Kelamin :</label>
									</div>
									<div class="col-lg-12 col-md-12 col-sm-12">
										<div class="m-radio-inline">
											<label class="m-radio">
												<input type="radio" name="gender" value="pria" {{old('gender','pria')=='pria' ? 'checked' : '' }}>
												Pria
												<span></span>
											</label>
											<label class="m-radio">
												<input type="radio" name="gender" value="wanita" {{old('gender','')=='wanita' ? 'checked' : '' }}>
												Wanita
												<span></span>
											</label>
										</div>
									</div>
								</div>
								<div class="form-group m-form__group">
									<div class="col" style="margin-top:10px; margin-bottom:-20px;">
										<label>Tanggal Lahir :</label>
									</div>
									<input class="form-control m-input" type="date" name="birthday" placeholder="Tanggal Lahir" value="{{old('birthday')}}">
								</div>
								<div id="r_alamat" class="m--margin-top-20">
									<div class="row">
										<div class="col">
											<div class="form-group m-form__group">
												<div class="col" style="margin-bottom:-20px;">
													<label>Alamat :</label>
												</div>
												<div data-repeater-list="alamat">
													@if ($errors->any())
														@foreach (old('alamat') as $alamat)
															<div data-repeater-item >
																<textarea name="keterangan" class="form-control m-input" rows="3">{{$alamat['keterangan']}}</textarea>
																<div class="col mt-2">
																	<div data-repeater-delete="" class="btn-sm btn btn-danger m-btn m-btn--icon m-btn--pill">
																		<span>
																			<i class="la la-trash-o"></i>
																			<span>
																				Delete
																			</span>
																		</span>
																	</div>
																	<label class="pull-right m-checkbox m-checkbox--state-success">
																		<input name="utama" class="make_radio" type="radio" {{array_key_exists('utama',$alamat) ? 'checked' : '' }}>
																		Utama
																		<span></span>
																	</label>
																</div>
															</div>
														@endforeach
													@else
														<div data-repeater-item >
															<textarea name="keterangan" class="form-control m-input" rows="3"></textarea>
															<div class="col mt-2">
																<div data-repeater-delete="" class="btn-sm btn btn-danger m-btn m-btn--icon m-btn--pill">
																	<span>
																		<i class="la la-trash-o"></i>
																		<span>
																			Delete
																		</span>
																	</span>
																</div>
																<label class="pull-right m-checkbox m-checkbox--state-success">
																	<input name="utama" class="make_radio" type="radio" checked>
																	Utama
																	<span></span>
																</label>
															</div>
														</div>
													@endif
												</div>
											</div>
										</div>
									</div>
									<div class="row m--margin-top-10">
										<div class="col">
											<div data-repeater-create="" class="pull-right btn btn btn-sm btn-brand m-btn m-btn--icon m-btn--pill m-btn--wide">
												<span>
													<i class="la la-plus"></i>
													<span>
														Add
													</span>
												</span>
											</div>
										</div>
									</div>
								</div>
								<div class="form-group m-form__group">
									<div class="inner-addon right-addon">
										<i class="fa fa-eye hideshow"></i>
										<input class="form-control m-input m-login__form-input--last password" type="password" placeholder="Password" name="password">
									</div>
								</div>
								<div class="form-group m-form__group">
									<div class="inner-addon right-addon">
										<i class="fa fa-eye hideshow"></i>
										<input class="form-control m-input m-login__form-input--last password" type="password"  placeholder="Confirm Password" name="password_confirmation">
									</div>
								</div>
								<!-- <div class="form-group m-form__group m--margin-top-10">
									<div class="col-md-12" align="center">
										<div class="g-recaptcha" data-sitekey="6LeYq2QUAAAAAJhyMsfgtwd6RT_AQLZPSlukpdCl"></div>
										<div class="m-form__help">
											By Checkin google reCaptcha
											<a class="m-link" href="#" class="m-link m-link--focus">
												I Agree the terms and conditions
											</a>
										</div>
									</div>
								</div> -->
								<div class="m-login__form-action">
									<button type="submit" class="btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air  m-login__btn">
										Sign Up
									</button>
								</div>
							</form>
						</div>
						<div class="m-login__account">
							<span class="m-login__account-msg">
								Sudah punya akun ?
							</span>
							<a href="{{url('login')}}" class="m-link m-link--light m-login__account-link">
								Masuk sekarang
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<script src='https://www.google.com/recaptcha/api.js'></script>
		<script src="{{asset('assets/crm/vendors/base/vendors.bundle.js')}}" type="text/javascript"></script>
		<script src="{{asset('assets/crm/demo/default/base/scripts.bundle.js')}}" type="text/javascript"></script>
		<script src="{{asset('assets/crm/snippets/custom/pages/user/login.js')}}" type="text/javascript"></script>
		<!-- <script src="{{asset('assets/crm/demo/default/custom/components/forms/widgets/form-repeater.js')}}" type="text/javascript"></script> -->
		<script>
		$(function() {
		    thisform.init();
		}), thisform = {
		  init: function()
		  {
		    thisform.r_init();
		    thisform.v_init();
		    thisform.rb_init();
		  },
		  r_init: function()
		  {
				$("#r_alamat").repeater({
            initEmpty: !1,
						isFirstItemUndeletable: true,
            defaultValues: {
                "text-input": ""
            },
            show: function() {
                $(this).slideDown()
								thisform.rb_init();
            },
            hide: function(e) {
                $(this).slideUp(e)
            }
        })
		  },
		  rb_init: function()
		  {
				$('.make_radio').on('click',function(){
					$("input.make_radio").prop("checked", false);
					$(this).prop("checked", true);
				})
		  },
		  v_init: function()
		  {
				$('#form_validate').validate();
		  },
		};
		</script>
		<script>
		$(function() {
			$('.hideshow').on('click',function(){
				$icon = $(this);
				$input = $(this).siblings('.password');
		    if ($input.prop('type') === "password") {
	        $input.prop('type','text');
					$icon.removeClass('fa-eye').addClass('fa-eye-slash');
		    } else {
	        $input.prop('type','password');
					$icon.removeClass('fa-eye-slash').addClass('fa-eye');
		    }
			});
		});
		</script>
	</body>
</html>
<style>
.inner-addon {
  position: relative;
}
/* style glyph */
.inner-addon .fa {
  position: absolute;
  padding: 20px;
  /* pointer-events: none; */
}
/* align glyph */
.left-addon .fa  { left:  0px;}
.right-addon .fa { right: 0px;}
/* add padding  */
.left-addon input  { padding-left:  30px; }
.right-addon input { padding-right: 30px; }
</style>
