<!DOCTYPE html>
<html lang="en" >
	<head>
		<meta charset="utf-8" />
		<title>Forgot | Yana Valentina</title>
		<meta name="description" content="Latest updates and statistic charts">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
		<script>
					WebFont.load({
						google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},
						active: function() {
								sessionStorage.fonts = true;
						}
					});
		</script>
		<link href="{{asset('assets/crm/vendors/base/vendors.bundle.css')}}" rel="stylesheet" type="text/css" />
		<link href="{{asset('assets/crm/demo/default/base/style.bundle.css')}}" rel="stylesheet" type="text/css" />
		<link href="{{asset('assets/font/font-stylesheet.css')}}" rel="stylesheet" type="text/css" />
		<link rel="shortcut icon" href="{{asset('assets/logo/favicon.ico')}}" />
	</head>
	<body  class="m--skin- m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >
		<div class="m-grid m-grid--hor m-grid--root m-page">
			<div class="m-grid__item m-grid__item--fluid m-grid m-grid--hor m-login m-login--signin m-login--2 m-login-2--skin-2" id="m_login" style="background-color: rgb(249, 230, 238);">
				<div class="m-grid__item m-grid__item--fluid	m-login__wrapper">
					<div class="m-login__container">
						<div class="m-login__logo">
							<a href="#" class="no-decoration">
								<img src="{{asset('assets/logo/logo-pk.png')}}"style="width:150px;height:150px">
								<h1 class="nopadding wild-font font-lg col-pink" style="margin-top:-20px !important;">Yana valentina</h1>
								<p class="col-pink" style="margin-top:-18px !important;"><small>Branded Bag's</small></p>
							</a>
						</div>
						<div class="m-login__signin">
							<div class="m-login__head">
								<h3 class="m-login__title">
									Kesulitan mengakses akun Anda?
								</h3>
								<!-- <h5>Masukkan email login Anda di bawah ini. Kami akan mengirimkan pesan email beserta tautan untuk reset kata sandi Anda.</h5> -->
							</div>
							<form id="form_validate" class="m-login__form m-form" action="{{url()->current()}}" method="post">
								<span class="m-form__help">
									<p class="text-center">Masukkan email login Anda di bawah ini. Kami akan mengirimkan pesan email beserta tautan untuk reset kata sandi Anda.</p>
								</span>
			          @if (session('success'))
			            <div class="alert alert-success alert-dismissible fade show" role="alert">
			              <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
										  {{ session('success') }}
			            </div>
			          @endif
			          @if (session('info'))
			            <div class="alert alert-info alert-dismissible fade show" role="alert">
			              <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
										  {{ session('info') }}
			            </div>
			          @endif
			          @if ($errors->any())
			            <div class="alert alert-danger alert-dismissible fade show" role="alert">
			              <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
			                <ul>
			                    @foreach ($errors->all() as $message)
			                        <li>{{$message}}</li>
			                    @endforeach
			                </ul>
			            </div>
			          @endif
								@csrf
								<div class="form-group m-form__group">
									<input class="form-control m-input"   type="text" placeholder="Email" name="email" autocomplete="off">
								</div>
								<div class="m-login__form-action">
									<button type="submit" class="btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air m-login__btn m-login__btn--primary">
										Ganti Kata Sandi
									</button>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
		<script src="{{asset('assets/crm/vendors/base/vendors.bundle.js')}}" type="text/javascript"></script>
		<script src="{{asset('assets/crm/demo/default/base/scripts.bundle.js')}}" type="text/javascript"></script>
	</body>
</html>
