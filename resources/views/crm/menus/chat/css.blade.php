<link href="{{asset('assets/crm/vendors/custom/perfect-scrollbar/perfect-scrollbar.css')}}" rel="stylesheet" type="text/css" />
<style>
.link-chat-item:hover {
  background-color: rgb(246, 217, 238, 0.5) !important;
}
.link-chat-item {
  padding: 0rem 2.2rem !important;
}
.m-widget2 .active {
  -webkit-box-shadow:inset 5px 0px 0px 0px rgb(247, 110, 209) !important;
  -moz-box-shadow:inset 5px 0px 0px 0px rgb(247, 110, 209) !important;
  box-shadow:inset 5px 0px 0px 0px rgb(247, 110, 209) !important;
}
.m-widget2 .link-chat-item {
  border-bottom: 0.5px solid rgb(245, 230, 232);
}
.m-portlet .m-portlet__body .nopadding  {
  padding: 0.02rem 0rem !important;
}
.m-widget2__user-img{
  display: table-cell;
  vertical-align: middle;
  padding-top: 1rem;
  padding-bottom: 1rem;
  padding-right: 1rem;
  vertical-align: top;
}
.m-widget2 .m-widget2__item {
  margin-bottom: 0rem !important;
}
.m-widget2__desc {
  word-break: break-all !important ;
}
.m-widget2__actions-nav {
  display:none;
}
.m-messenger-chat__message-text {
  word-break: break-all !important ;
}
.m-messenger-chat .m-messenger-chat__messages .m-messenger-chat__message.m-messenger-chat__message--in .m-messenger-chat__message-footer {
  float:left;
  margin-left:10px;
}

.m-messenger-chat .m-messenger-chat__messages .m-messenger-chat__message.m-messenger-chat__message--out .m-messenger-chat__message-footer {
	float:right;
  margin-right:10px;
}
/*  MESSAGE */
m-messenger-chat .m-messenger-chat__form {
	margin: 0 0 10px 0;
	display: table;
	table-layout: fixed
}

.m-messenger-chat .m-messenger-chat__form .m-messenger-chat__form-controls {
	width: 100%;
	display: table-cell;
	vertical-align: middle;
	padding: 0
}

.m-messenger-chat .m-messenger-chat__form .m-messenger-chat__form-controls .m-messenger-chat__form-input {
	width: 100%;
	padding: 10px 20px;
	border-radius: 20px
}

.m-messenger-chat .m-messenger-chat__form .m-messenger-chat__form-tools {
	display: table-cell;
	vertical-align: middle;
	padding: 0 0 0 10px
}

.m-messenger-chat .m-messenger-chat__form .m-messenger-chat__form-tools .m-messenger-chat__form-attachment {
	border-radius: 100%;
	display: -webkit-box;
	display: -ms-flexbox;
	display: flex;
	-moz-justify-content: center;
	-ms-justify-content: center;
	-webkit-box-pack: center;
	-ms-flex-pack: center;
	justify-content: center;
	-moz-align-items: center;
	-ms-align-items: center;
	-webkit-box-align: center;
	-ms-flex-align: center;
	align-items: center;
	text-align: center;
	vertical-align: middle;
	height: 40px;
	width: 40px;
	text-align: center;
	vertical-align: middle;
	line-height: 0;
	cursor: pointer
}

.m-messenger-chat .m-messenger-chat__form .m-messenger-chat__form-tools .m-messenger-chat__form-attachment>i {
	font-size: 1.4rem
}

.m-messenger-chat .m-messenger-chat__form .m-messenger-chat__form-tools .m-messenger-chat__form-attachment:hover {
	text-decoration: none
}

.m-messenger-chat .m-messenger-chat__seperator {
	margin: 30px 0
}

.m-messenger-chat .m-messenger-chat__messages .m-messenger-chat__datetime {
	display: table;
	text-align: center;
	padding: 10px 0;
	width: 100%;
	font-size: .85rem;
	font-weight: 500;
	text-transform: uppercase
}

.m-messenger-chat .m-messenger-chat__messages .m-messenger-chat__datetime:after,
.m-messenger-chat .m-messenger-chat__messages .m-messenger-chat__datetime:before {
	content: " ";
	display: table
}

.m-messenger-chat .m-messenger-chat__messages .m-messenger-chat__datetime:after {
	clear: both
}

.m-messenger-chat .m-messenger-chat__messages .m-messenger-chat__wrapper:after,
.m-messenger-chat .m-messenger-chat__messages .m-messenger-chat__wrapper:before {
	content: " ";
	display: table
}

.m-messenger-chat .m-messenger-chat__messages .m-messenger-chat__wrapper:after {
	clear: both
}

.m-messenger-chat .m-messenger-chat__messages .m-messenger-chat__message {
	display: table;
	table-layout: fixed
}

.m-messenger-chat .m-messenger-chat__messages .m-messenger-chat__message:after,
.m-messenger-chat .m-messenger-chat__messages .m-messenger-chat__message:before {
	content: " ";
	display: table
}

.m-messenger-chat .m-messenger-chat__messages .m-messenger-chat__message:after {
	clear: both
}

.m-messenger-chat .m-messenger-chat__messages .m-messenger-chat__message.m-messenger-chat__message--in {
	float: left
}

.m-messenger-chat .m-messenger-chat__messages .m-messenger-chat__message.m-messenger-chat__message--out {
	float: right;
	padding-left: 54px
}

.m-messenger-chat .m-messenger-chat__messages .m-messenger-chat__message .m-messenger-chat__message-pic {
	display: table-cell;
	vertical-align: top;
	width: 40px;
	padding: 6px 10px 0 0
}

.m-messenger-chat .m-messenger-chat__messages .m-messenger-chat__message .m-messenger-chat__message-pic img {
	max-width: 40px;
	border-radius: 100%;
	margin: 0!important
}

.m-messenger-chat .m-messenger-chat__messages .m-messenger-chat__message .m-messenger-chat__message-no-pic {
	height: 40px;
	width: 40px;
	display: inline-block;
	text-align: center;
	position: relative;
	display: -webkit-box;
	display: -ms-flexbox;
	display: flex;
	-moz-justify-content: center;
	-ms-justify-content: center;
	-webkit-box-pack: center;
	-ms-flex-pack: center;
	justify-content: center;
	-moz-align-items: center;
	-ms-align-items: center;
	-webkit-box-align: center;
	-ms-flex-align: center;
	align-items: center;
	text-align: center;
	vertical-align: middle;
	border-radius: 100%
}

.m-messenger-chat .m-messenger-chat__messages .m-messenger-chat__message .m-messenger-chat__message-no-pic>span {
	line-height: 0;
	font-weight: 600;
	font-size: 1.3rem;
	text-transform: uppercase
}

.m-messenger-chat .m-messenger-chat__messages .m-messenger-chat__message .m-messenger-chat__message-body {
	display: table-cell;
	vertical-align: top;
	padding: 0;
	position: relative
}

.m-messenger-chat .m-messenger-chat__messages .m-messenger-chat__message .m-messenger-chat__message-body .m-messenger-chat__message-content {
	padding: 17px;
	margin-right: 5px;
	border-radius: 10px
}

.m-messenger-chat .m-messenger-chat__messages .m-messenger-chat__message .m-messenger-chat__message-body .m-messenger-chat__message-content .m-messenger-chat__message-username {
	font-size: .85rem;
	padding-bottom: 4px
}

.m-messenger-chat .m-messenger-chat__messages .m-messenger-chat__message .m-messenger-chat__message-body .m-messenger-chat__message-content .m-messenger-chat__message-text {
	font-size: 1rem
}

.m-messenger-chat .m-messenger-chat__messages .m-messenger-chat__message .m-messenger-chat__message-body .m-messenger-chat__message-content .m-messenger-chat__message-typing {
	padding-bottom: 4px;
	font-size: .85rem
}

.m-messenger-chat.m-messenger-chat.m-messenger-chat--message-arrow .m-messenger-chat__message.m-messenger-chat__message--in .m-messenger-chat__message-arrow {
	position: relative;
	line-height: 0;
	display: inline-block;
	overflow: hidden;
	width: 10px;
	height: 40px;
	left: 0;
	margin-left: -9px;
	left: 2px;
	right: auto;
	position: absolute;
	margin-left: 0;
	margin-top: 6px
}

.m-messenger-chat.m-messenger-chat.m-messenger-chat--message-arrow .m-messenger-chat__message.m-messenger-chat__message--in .m-messenger-chat__message-arrow:before {
	display: inline-block;
	font-family: Metronic;
	font-style: normal;
	font-weight: 400;
	font-variant: normal;
	line-height: 0;
	text-decoration: inherit;
	text-rendering: optimizeLegibility;
	text-transform: none;
	-moz-osx-font-smoothing: grayscale;
	-webkit-font-smoothing: antialiased;
	font-smoothing: antialiased;
	content: ""
}

.m-messenger-chat.m-messenger-chat.m-messenger-chat--message-arrow .m-messenger-chat__message.m-messenger-chat__message--in .m-messenger-chat__message-arrow:before {
	position: relative;
	top: 0;
	margin-top: 20px;
	font-size: 40px
}

.m-messenger-chat.m-messenger-chat.m-messenger-chat--message-arrow .m-messenger-chat__message.m-messenger-chat__message--in .m-messenger-chat__message-content {
	margin-left: 10px
}

.m-messenger-chat.m-messenger-chat.m-messenger-chat--message-arrow .m-messenger-chat__message.m-messenger-chat__message--out .m-messenger-chat__message-arrow {
	position: relative;
	line-height: 0;
	display: inline-block;
	overflow: hidden;
	width: 10px;
	height: 40px;
	left: auto;
	right: 0;
	margin-left: auto;
	margin-right: -9px;
	right: 2px;
	left: auto;
	position: absolute;
	margin-left: 0;
	margin-right: 0;
	margin-top: 6px
}

.m-messenger-chat.m-messenger-chat.m-messenger-chat--message-arrow .m-messenger-chat__message.m-messenger-chat__message--out .m-messenger-chat__message-arrow:before {
	display: inline-block;
	font-family: Metronic;
	font-style: normal;
	font-weight: 400;
	font-variant: normal;
	line-height: 0;
	text-decoration: inherit;
	text-rendering: optimizeLegibility;
	text-transform: none;
	-moz-osx-font-smoothing: grayscale;
	-webkit-font-smoothing: antialiased;
	font-smoothing: antialiased;
	content: ""
}

.m-messenger-chat.m-messenger-chat.m-messenger-chat--message-arrow .m-messenger-chat__message.m-messenger-chat__message--out .m-messenger-chat__message-arrow:before {
	position: relative;
	top: 0;
	margin-top: 20px;
	font-size: 40px
}

.m-messenger-chat.m-messenger-chat.m-messenger-chat--message-arrow .m-messenger-chat__message.m-messenger-chat__message--out .m-messenger-chat__message-content {
	margin-right: 10px
}

.m-messenger-chat .m-messenger-chat__form .m-messenger-chat__form-input {
	border: 0;
	background: 0 0;
	outline: 0!important;
	-webkit-box-shadow: none;
	box-shadow: none;
	color: #575962;
	background-color: #f4f5f8
}

.m-messenger-chat .m-messenger-chat__form .m-messenger-chat__form-input::-moz-placeholder {
	color: #6f727d;
	opacity: 1
}

.m-messenger-chat .m-messenger-chat__form .m-messenger-chat__form-input:-ms-input-placeholder {
	color: #6f727d
}

.m-messenger-chat .m-messenger-chat__form .m-messenger-chat__form-input::-webkit-input-placeholder {
	color: #6f727d
}

.m-messenger-chat .m-messenger-chat__form .m-messenger-chat__form-input:focus {
	background-color: #ebedf2
}

.m-messenger-chat .m-messenger-chat__form .m-messenger-chat__form-attachment {
	background-color: #f7f8fa;
	color: #cfcedb
}

.m-messenger-chat .m-messenger-chat__form .m-messenger-chat__form-attachment:hover {
	background-color: #ebedf2;
	color: #cfcedb
}

.m-messenger-chat .m-messenger-chat__seperator {
	border-bottom: 1px solid #f4f5f8
}

.m-messenger-chat .m-messenger-chat__messages .m-messenger-chat__datetime {
	color: #afb2c1;
	margin: 0 0 20px 0
}

.m-messenger-chat .m-messenger-chat__messages .m-messenger-chat__message {
	margin: 0 0 20px 0
}

.m-messenger-chat .m-messenger-chat__messages .m-messenger-chat__message.m-messenger-chat__message--in .m-messenger-chat__message-content {
	background: #f4f5f8
}

.m-messenger-chat .m-messenger-chat__messages .m-messenger-chat__message.m-messenger-chat__message--in .m-messenger-chat__message-content .m-messenger-chat__message-username {
	color: #6f727d
}

.m-messenger-chat .m-messenger-chat__messages .m-messenger-chat__message.m-messenger-chat__message--in .m-messenger-chat__message-content .m-messenger-chat__message-text {
	color: #575962
}

.m-messenger-chat .m-messenger-chat__messages .m-messenger-chat__message.m-messenger-chat__message--in .m-messenger-chat__message-content .m-messenger-chat__message-typing {
	color: #7b7e8a
}

.m-messenger-chat .m-messenger-chat__messages .m-messenger-chat__message.m-messenger-chat__message--out .m-messenger-chat__message-content {
	background: #716aca
}

.m-messenger-chat .m-messenger-chat__messages .m-messenger-chat__message.m-messenger-chat__message--out .m-messenger-chat__message-content .m-messenger-chat__message-username {
	color: #f0f0f0
}

.m-messenger-chat .m-messenger-chat__messages .m-messenger-chat__message.m-messenger-chat__message--out .m-messenger-chat__message-content .m-messenger-chat__message-text {
	color: #f7f7f7
}

.m-messenger-chat .m-messenger-chat__messages .m-messenger-chat__message.m-messenger-chat__message--out .m-messenger-chat__message-content .m-messenger-chat__message-typing {
	color: #f0f0f0
}

.m-messenger-chat.m-messenger-chat.m-messenger-chat--message-arrow .m-messenger-chat__message.m-messenger-chat__message--in .m-messenger-chat__message-arrow {
	color: #f4f5f8
}

.m-messenger-chat.m-messenger-chat.m-messenger-chat--message-arrow .m-messenger-chat__message.m-messenger-chat__message--out .m-messenger-chat__message-arrow {
	color: #716aca
}
</style>
