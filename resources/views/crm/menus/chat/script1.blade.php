<script src="{{asset('assets/crm/vendors/custom/perfect-scrollbar/perfect-scrollbar.js')}}" type="text/javascript"></script>
<script>
$(function() {
    thisform.init();
}), thisform = {
  init: function()
  {
    var re = new mPortlet("m_chat_room");
    var me = new mPortlet("m_chat_message");
    re.on("reload", function(e) {
      mApp.block(re.getSelf(), {
        overlayColor: "#000000",
        type: "spinner",
        state: "brand",
        opacity: .05,
        size: "lg"
      });
      thisform.re_init(me);
      mApp.unblock(re.getSelf());
      thisform.ps_init();
    });
    re.reload();
    me.on("reload", function(e) {
      mApp.block(me.getSelf(), {
        overlayColor: "#000000",
        type: "spinner",
        state: "brand",
        opacity: .05,
        size: "lg"
      });
      re.reload();
      $ecr = $("div.link-chat-item.active").data('ecr');
      thisform.rc_init($ecr);
      mApp.unblock(me.getSelf());
      // console.log($("div.link-chat-item.active"));

    });
    setInterval(function(){
      thisform.re_init(me);
      if ($('div.m-portlet__head-caption').hasClass("opened")) {
        $ecr = $("div.m-portlet__head-caption.opened").data('ecr');
        thisform.rc_init($ecr);
      }
    },5000);
  },
  rc_init : function (ecr) {
    $ecr = ecr;
    // alert($ecr);
    $.ajax({
        url: "{{url('chat/message')}}",
        type: "GET",
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        data: {
            ecr : $ecr,
        },
        success : function(data) {
          thisform.bl_init(data);
        }
    });
  },
  ps_init : function () {
    $('.perfect-scroll').each(function(index,element){
      $(this).height($(this).data('perfect-scroll-height'));
      var ps = new PerfectScrollbar(element, {
        wheelSpeed: 2,
        wheelPropagation: true,
        minScrollbarLength: 20
      });
      if($(this).data('perfect-scroll-startbottom')){
        $(this).scrollTop(100000);
      };
      ps.update();
    });
  },
  bl_init : function (data) {
    $result = data['data'];
    $msg='';
    $.each($result['chat'], function(index, item) {
        $msg = $msg +
        '<div class="m-messenger-chat__datetime">'+
          index+
        '</div>';
      $.each(item, function(index2, message) {
        if (message['is_opposite']) {
          $msg = $msg +
          '<div class="m-messenger-chat__wrapper">'+
            '<div class="m-messenger-chat__message m-messenger-chat__message--in">'+
              '<div class="m-messenger-chat__message-pic">'+
                '<img src="'+message['users_photo_url']+'" alt=""/>'+
              '</div>'+
              '<div class="m-messenger-chat__message-body">'+
                '<div class="m-messenger-chat__message-arrow"></div>'+
                '<div class="m-messenger-chat__message-content">'+
                  '<div class="m-messenger-chat__message-username">'+
                    message['users_name']+
                  '</div>'+
                  '<div class="m-messenger-chat__message-text">'+
                    message['chat']+
                  '</div>'+
                '</div>'+
                '<div class="m-messenger-chat__message-footer">'+
                  '<small>'+message['time_chat']+'</small>'+
                '</div>'+
              '</div>'+
            '</div>'+
          '</div>';
        } else {
          $msg = $msg +
          '<div class="m-messenger-chat__wrapper">'+
            '<div class="m-messenger-chat__message m-messenger-chat__message--out">'+
              '<div class="m-messenger-chat__message-body">'+
                '<div class="m-messenger-chat__message-arrow"></div>'+
                '<div class="m-messenger-chat__message-content">'+
                  '<div class="m-messenger-chat__message-text">'+
                    message['chat']+
                  '</div>'+
                '</div>'+
                '<div class="m-messenger-chat__message-footer">'+
                  // (message['is_read'] ?
                  // '<span class="fa-stack text-success">'+
                  //     '<i class="fa fa-check fa-stack-1x" style="margin-left:4px"></i>'+
                  //     '<i class="fa fa-check  fa-stack-1x" style="margin-left:-4px"></i>'+
                  // '</span>' :
                  // '<span class="fa-stack text-success">'+
                  //     '<i class="fa fa-check fa-stack-1x" style="margin-left:4px"></i>'+
                  // '</span>')+
                  '<small>'+message['time_chat']+'</small>'+
                '</div>'+
              '</div>'+
            '</div>'+
          '</div>';
        }
      });
    });
    $msg =
    '<div class="m-messenger-chat m-messenger-chat--message-arrow m-messenger-chat--skin-light">'+
      '<div id="m_chat_message_message" class="m-messenger-chat__messages perfect-scroll" data-perfect-scroll-startbottom="true" data-perfect-scroll-height="450">'+
        $msg+
      '</div>'+
      '<div class="m-messenger-chat__seperator"></div>'+
      '<div class="m-messenger-chat__form">'+
        '<div class="m-messenger-chat__form-controls">'+
          '<input type="text" name="" placeholder="Type here..." id="m_chat_message_text" class="m-messenger-chat__form-input">'+
        '</div>'+
        '<div class="m-messenger-chat__form-tools">'+
          '<a href="javascript:void(0);" id="m_chat_message_send" class="m-messenger-chat__form-attachment">'+
            '<i class="la la-send"></i>'+
          '</a>'+
        '</div>'+
      '</div>'+
    '</div>';
    $head =
    '<div class="m-portlet__head-caption opened" data-ecr="'+$result['room']['ecr']+'">'+
      '<div class="m-portlet__head-title">'+
        '<img class="rounded-circle" style="max-height:40px" id="m_chat_message_photo" src="'+$result['room']['users_photo_url']+'" alt="">'+
        '<h3 class="m-portlet__head-text pl-2" id="m_chat_message_name">'+
          $result['room']['users_name']+
        '</h3>'+
      '</div>'+
    '</div>';
    $('#m_chat_message_head').html($head);
    $('#m_chat_message_body').html($msg);
    thisform.ps_init();
    thisform.ch_init();
  },
  re_init : function (me, opened) {
    $.ajax({
        url: "{{url('chat/room')}}",
        type: "GET",
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success : function(data) {
          $selected = $('div.link-chat-item.active').data('ecr');
          $old = $("#m_chat_message_text").val();
          $result = data['data'];
          $msg='';
          $.each($result['room'], function (index, room){
            $msg = $msg +
            '<div class="link-chat-item '+($selected==room['ecr'] ? 'active' : '')+'" data-ecr="'+room['ecr']+'">'+
              '<div class="m-widget2__item">'+
                '<div class="m-widget2__user-img">'+
                  '<img class="rounded-circle" style="max-height:40px" src="'+room['users_photo_url']+'" alt="">'+
                '</div>'+
                '<div class="m-widget2__desc">'+
                  '<span class="m-widget2__user-name">'+
                    '<a href="#" class="m-widget2__link">'+
                    room['users_name']+
                    '</a>'+
                  '</span>'+
                  '<br>'+
                  '<span class="m-widget2__text">'+
                  room['message_preview']+
                  '</span>'+
                '</div>'+
                '<div class="m-widget2__actions">'+
                  ((room['count_new']>0) ? '<span class="m-menu__link-badge"><span class="m-badge m-badge--info">'+room['count_new']+'</span></span>':'')+
                  '<div class="m-widget2__actions-nav">'+
                    '<div class="m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push" m-dropdown-toggle="hover">'+
                      '<a href="#" class="m-dropdown__toggle">'+
                        '<i class="la la-ellipsis-h"></i>'+
                      '</a>'+
                      '<div class="m-dropdown__wrapper">'+
                        '<span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"></span>'+
                        '<div class="m-dropdown__inner">'+
                          '<div class="m-dropdown__body">'+
                            '<div class="m-dropdown__content">'+
                              '<ul class="m-nav">'+
                                '<li class="m-nav__item">'+
                                  '<a href="#" class="m-nav__link">'+
                                  '<i class="m-nav__link-icon flaticon-share"></i>'+
                                  '<span class="m-nav__link-text">Activity</span>'+
                                  '</a>'+
                                '</li>'+
                                '<li class="m-nav__item">'+
                                  '<a href="#" class="m-nav__link">'+
                                  '<i class="m-nav__link-icon flaticon-chat-1"></i>'+
                                  '<span class="m-nav__link-text">Messages</span>'+
                                  '</a>'+
                                '</li>'+
                                '<li class="m-nav__item">'+
                                  '<a href="#" class="m-nav__link">'+
                                  '<i class="m-nav__link-icon flaticon-info"></i>'+
                                  '<span class="m-nav__link-text">FAQ</span>'+
                                  '</a>'+
                                '</li>'+
                                '<li class="m-nav__item">'+
                                  '<a href="#" class="m-nav__link">'+
                                  '<i class="m-nav__link-icon flaticon-lifebuoy"></i>'+
                                  '<span class="m-nav__link-text">Support</span>'+
                                  '</a>'+
                                '</li>'+
                              '</ul>'+
                            '</div>'+
                          '</div>'+
                        '</div>'+
                      '</div>'+
                    '</div>'+
                  '</div>'+
                '</div>'+
              '</div>'+
            '</div>';
            $('#m_chat_room_room').html($msg);
            $("#m_chat_message_text").val($old);
            $('.link-chat-item').on('click',function(){
              $('.link-chat-item').removeClass("active");
              $(this).addClass('active');
              me.reload();
            });
          })
        }
    });
  },
  ch_init : function () {
    $('#m_chat_message_send').on('click',function(){
      $msg = $("#m_chat_message_text").val();
      $("#m_chat_message_text").val('');
      $.ajax({
          url: "{{url('chat/post')}}",
          type: "POST",
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
          data: {
              ecr : $ecr,
              msg : $msg,
          },
          success : function(data) {
              thisform.bl_init(data);
          }
        });
    });
    $("#m_chat_message_text").on('keyup', function (e) {
      if (e.keyCode == 13) {
        $('#m_chat_message_send').click();
      }
    });
  },
};
</script>
