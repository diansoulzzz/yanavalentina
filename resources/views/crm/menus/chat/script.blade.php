<script src="{{asset('assets/crm/vendors/custom/perfect-scrollbar/perfect-scrollbar.js')}}" type="text/javascript"></script>
<script>
$(function() {
    thisform.init();
}), thisform = {
  first_time : true,
  seleted_room : 0,
  chat_portlet : new mPortlet("m_chat_message"),
  init : function(){
    thisform.re_init();
    thisform.chat_portlet.on("reload", function(e) {
      mApp.block(thisform.chat_portlet.getSelf(), {
        overlayColor: "#000000",
        type: "spinner",
        state: "brand",
        opacity: .05,
        size: "lg"
      });
      thisform.re_init();
      mApp.unblock(thisform.chat_portlet.getSelf());
    });

    thisform.sendInit();
    setInterval(function(){
      thisform.re_init();
    },2000);
  },
  setRoom : function ($room) {
    $msg='';
    $.each($room, function (index, room){
      $msg = $msg +
      '<div class="link-chat-item '+(thisform.seleted_room==room['ecr'] ? 'active' : '')+'" data-ecr="'+room['ecr']+'">'+
        '<div class="m-widget2__item">'+
          '<div class="m-widget2__user-img">'+
            '<img class="rounded-circle s-img-40" src="'+room['users_photo_url']+'" alt="">'+
          '</div>'+
          '<div class="m-widget2__desc">'+
            '<span class="m-widget2__user-name">'+
              '<a href="#" class="m-widget2__link">'+
              room['users_name']+
              '</a>'+
            '</span>'+
            '<br>'+
            '<span class="m-widget2__text">'+
            room['message_preview']+
            '</span>'+
          '</div>'+
          '<div class="m-widget2__actions">'+
            ((room['count_new']>0) ? '<span class="m-menu__link-badge"><span class="m-badge m-badge--info">'+room['count_new']+'</span></span>':'')+
            '<div class="m-widget2__actions-nav">'+
              '<div class="m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push" m-dropdown-toggle="hover">'+
                '<a href="#" class="m-dropdown__toggle">'+
                  '<i class="la la-ellipsis-h"></i>'+
                '</a>'+
                '<div class="m-dropdown__wrapper">'+
                  '<span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"></span>'+
                  '<div class="m-dropdown__inner">'+
                    '<div class="m-dropdown__body">'+
                      '<div class="m-dropdown__content">'+
                        '<ul class="m-nav">'+
                          '<li class="m-nav__item">'+
                            '<a href="#" class="m-nav__link">'+
                            '<i class="m-nav__link-icon flaticon-share"></i>'+
                            '<span class="m-nav__link-text">Activity</span>'+
                            '</a>'+
                          '</li>'+
                          '<li class="m-nav__item">'+
                            '<a href="#" class="m-nav__link">'+
                            '<i class="m-nav__link-icon flaticon-chat-1"></i>'+
                            '<span class="m-nav__link-text">Messages</span>'+
                            '</a>'+
                          '</li>'+
                          '<li class="m-nav__item">'+
                            '<a href="#" class="m-nav__link">'+
                            '<i class="m-nav__link-icon flaticon-info"></i>'+
                            '<span class="m-nav__link-text">FAQ</span>'+
                            '</a>'+
                          '</li>'+
                          '<li class="m-nav__item">'+
                            '<a href="#" class="m-nav__link">'+
                            '<i class="m-nav__link-icon flaticon-lifebuoy"></i>'+
                            '<span class="m-nav__link-text">Support</span>'+
                            '</a>'+
                          '</li>'+
                        '</ul>'+
                      '</div>'+
                    '</div>'+
                  '</div>'+
                '</div>'+
              '</div>'+
            '</div>'+
          '</div>'+
        '</div>'+
      '</div>';
      $('#m_chat_room_room').html($msg);
      $('.link-chat-item').on('click',function(){
        $('.link-chat-item').removeClass("active");
        thisform.seleted_room=$(this).data('ecr');
        $(this).addClass('active');
        // thisform.re_init();
        thisform.chat_portlet.reload();
        $("#m_chat_message_text").val('');
        thisform.first_time = true;
        $('#m_chat_message_welcome').remove();
        $('#m_chat_message_wrap').show();
      });
    })
  },
  setMessage : function ($chat) {
    $msg='';
    $message = $chat['message'];
    $info = $chat['info'];
    $is_new = $chat['is_new'];
    $.each($message, function(index, item) {
      $msg = $msg +
      '<div class="m-messenger-chat__datetime">'+
        index+
      '</div>';
      $.each(item, function(index2, message) {
        if (message['is_opposite']) {
          $msg = $msg +
          '<div class="m-messenger-chat__wrapper">'+
            '<div class="m-messenger-chat__message m-messenger-chat__message--in">'+
              '<div class="m-messenger-chat__message-pic">'+
                '<img src="'+message['users_photo_url']+'" alt=""/>'+
              '</div>'+
              '<div class="m-messenger-chat__message-body">'+
                '<div class="m-messenger-chat__message-arrow"></div>'+
                '<div class="m-messenger-chat__message-content">'+
                  '<div class="m-messenger-chat__message-username">'+
                    message['users_name']+
                  '</div>'+
                  '<div class="m-messenger-chat__message-text">'+
                    message['chat']+
                  '</div>'+
                '</div>'+
                '<div class="m-messenger-chat__message-footer">'+
                  '<small>'+message['time_chat']+'</small>'+
                '</div>'+
              '</div>'+
            '</div>'+
          '</div>';
        } else {
          $msg = $msg +
          '<div class="m-messenger-chat__wrapper">'+
            '<div class="m-messenger-chat__message m-messenger-chat__message--out">'+
              '<div class="m-messenger-chat__message-body">'+
                '<div class="m-messenger-chat__message-arrow"></div>'+
                '<div class="m-messenger-chat__message-content">'+
                  '<div class="m-messenger-chat__message-text">'+
                    message['chat']+
                  '</div>'+
                '</div>'+
                '<div class="m-messenger-chat__message-footer">'+
                  ((message['count_read_by_opposite']>0) ?
                  '<span class="fa-stack text-success">'+
                      '<i class="fa fa-check fa-stack-1x" style="margin-left:4px"></i>'+
                      '<i class="fa fa-check  fa-stack-1x" style="margin-left:-4px"></i>'+
                  '</span>' :
                  '<span class="fa-stack text-success">'+
                      '<i class="fa fa-check fa-stack-1x" style="margin-left:4px"></i>'+
                  '</span>')+
                  '<small>'+message['time_chat']+'</small>'+
                '</div>'+
              '</div>'+
            '</div>'+
          '</div>';
        }
      });
    });
    $head =
    '<div class="m-portlet__head-caption opened" data-ecr="'+$info['ecr']+'">'+
      '<div class="m-portlet__head-title">'+
        '<img class="rounded-circle s-img-40" id="m_chat_message_photo" src="'+$info['users_photo_url']+'" alt="">'+
        '<h3 class="m-portlet__head-text pl-2" id="m_chat_message_name">'+
          $info['users_name']+
        '</h3>'+
      '</div>'+
    '</div>';
    $('#m_chat_message_head').html($head);
    $('#m_chat_message_message').html($msg);
    if ((thisform.first_time) || ($is_new))
    {
      $("#m_chat_message_message").scrollTop($('#m_chat_message_message')[0].scrollHeight);
      thisform.first_time=false;
    }
  },
  re_init : function () {
    if ($('div.link-chat-item').hasClass("active")) {
      thisform.seleted_room = $('div.link-chat-item.active').data('ecr');
    }
    $data = {};
    if (thisform.seleted_room!=0) {
      $data = {ecr : thisform.seleted_room};
    }
    $.ajax({
        url: "{{url('chat/room')}}",
        type: "GET",
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        data : $data,
        success : function(data) {
          $result = data['data'];
          thisform.setRoom($result['room']);
          if($result.hasOwnProperty('chat')){
            thisform.setMessage($result['chat']);
            // thisform.sendInit();
          }
        }
    });
  },
  sendInit : function () {
    $('#m_chat_message_send').on('click',function(){
      if (thisform.seleted_room!=0) {
        $msg = $("#m_chat_message_text").val();
        $("#m_chat_message_text").val('');
        if ($msg!='')
        {
          $.ajax({
            url: "{{url('chat/post')}}",
            type: "POST",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                ecr : thisform.seleted_room,
                msg : $msg,
            },
            success : function(data) {
              $result = data['data'];
              thisform.setRoom($result['room']);
              if($result.hasOwnProperty('chat')){
                thisform.setMessage($result['chat']);
                $("#m_chat_message_message").scrollTop($('#m_chat_message_message')[0].scrollHeight);
              }
            }
          });
        }
      }
    });
    $("#m_chat_message_text").on('keyup', function (e) {
      if (e.keyCode == 13) {
        $('#m_chat_message_send').click();
      }
    });
  },
};
</script>
