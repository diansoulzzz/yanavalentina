@extends('crm.layouts.default')
@section('title', 'Home')
@section('content')
<div class="m-grid__item m-grid__item--fluid m-wrapper">
  <div class="m-content">
    <div class="row">
      <div class="col-xl-4">
        <div class="m-portlet m-portlet--full-height" m-portlet="true"  id="m_chat_room">
          <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
              <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                  Chat
                </h3>
              </div>
            </div>
            <div class="m-portlet__head-tools">
            </div>
          </div>
          <div class="m-portlet__body nopadding" >
            <!-- data-scrollable="true" data-height="450" data-scrollbar-shown="true" -->
            <!-- style="height:400px;"  -->
    				<div id="m_chat_room_scroll" data-scrollable="true" data-height="450" data-scrollbar-shown="true">
              <div class="m-widget2" id="m_chat_room_room">
                @if (false)
                  @for ($i = 0; $i < 20; $i++)
                  <div class="link-chat-item">
                    <div class="m-widget2__item">
                      <div class="m-widget2__user-img" >
                        <img class="rounded-circle s-img-40" src="{{asset('assets/crm/app/media/img/users/user2.jpg')}}" alt="">
                      </div>
                      <div class="m-widget2__desc">
                        <span class="m-widget2__text">
                        Make Metronic Great  Again.
                        </span>
                        <br>
                        <span class="m-widget2__user-name">
                        <a href="#" class="m-widget2__link">
                        By Bob
                        </a>
                        </span>
                      </div>
                    </div>
                  </div>
                  @endfor
                @endif
      				</div>
    				</div>
          </div>
        </div>
      </div>
      <div class="col-xl-8">
        <div class="m-portlet m-portlet--full-height" style="background-color: #f9e6ee !important;" id="m_chat_message">
          <div class="m-portlet__head" id="m_chat_message_head"></div>
          <div class="m-portlet__body" id="m_chat_message_body">
            <div class="m-messenger-chat m-messenger-chat--message-arrow m-messenger-chat--skin-light" id="m_chat_message_wrap" style="display:none;">
              <div id="m_chat_message_message" class="m-messenger-chat__messages" data-scrollable="true" data-height="320" data-scrollbar-shown="true">
                @for ($i = 0; $i < 20; $i++)
                @if(false)
                <div class="m-messenger-chat__wrapper">
                  <div class="m-messenger-chat__message m-messenger-chat__message--in">
                    <div class="m-messenger-chat__message-pic">
                      <img src="{{asset('assets/crm/app/media/img//users/user3.jpg')}}" alt=""/>
                    </div>
                    <div class="m-messenger-chat__message-body">
                      <div class="m-messenger-chat__message-arrow"></div>
                      <div class="m-messenger-chat__message-content">
                        <div class="m-messenger-chat__message-username">
                          Megan wrote
                        </div>
                        <div class="m-messenger-chat__message-text">
                          Hi Bob. What time will be the meeting ?
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                @endif
                @endfor
              </div>
              <div class="m-messenger-chat__seperator"></div>
              <div class="m-messenger-chat__form">
                <div class="m-messenger-chat__form-controls">
                  <input type="text" name="" id="m_chat_message_text"  placeholder="Type here..." class="m-messenger-chat__form-input">
                </div>
                <div class="m-messenger-chat__form-tools">
                  <a href="javascript:void(0);" id="m_chat_message_send" class="m-messenger-chat__form-attachment">
                    <i class="la la-send"></i>
                  </a>
                </div>
              </div>
            </div>
            <div class="m-widget7" id="m_chat_message_welcome">
        			<div class="m-widget7__desc" style="margin-top:4rem !important;">
        				<div class="m-widget7__user-img">
        					<img class="m-widget7__img s-img-300" src="{{asset('assets/logo/logo-pk.png')}}" alt="">
        				</div>
              </div>
        			<div class="m-widget7__user" style="text-align: center;">
        				<div class="m-widget7__info">
        					<span class="m-widget7__username">
                    Selamat Datang di Chat
        					</span>
                  <br>
        					<span class="m-widget7__time">
    					       Silakan memilih pesan untuk memulai percakapan
        					</span>
        				</div>
        			</div>
        		</div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@push('b-css')
  @include('crm.menus.chat.css')
@endpush
@push('b-script')
  @include('crm.menus.chat.script')
@endpush
