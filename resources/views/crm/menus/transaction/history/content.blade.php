@extends('crm.layouts.default')
@section('title', $title)
@section('content')
<div class="m-grid__item m-grid__item--fluid m-wrapper">
  <div class="m-content">
    <div class="m-portlet m-portlet--mobile">
      <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
          <div class="m-portlet__head-title">
            <h3 class="m-portlet__head-text">
              {{$title}}
            </h3>
          </div>
        </div>
      </div>
      <div class="m-portlet__body">
        <ul class="nav nav-tabs" role="tablist">
          <li class="nav-item">
            <a class="nav-link {{ Request::is('member/h/t') ? 'active show' : '' }}" href="{{url('member/h/t')}}">Status Pembayaran</a>
          </li>
          <li class="nav-item">
            <a class="nav-link {{ Request::is('member/h/t/order') ? 'active show' : '' }}" href="{{url('member/h/t/order')}}">Status Pemesanan</a>
          </li>
        </ul>
        <div class="tab-content">
          <div class="tab-pane active show" role="tabpanel">
            {!! $datatable->table() !!}
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@push('a-css')
<link href="{{asset('assets/crm/vendors/custom/datatables/datatables.bundle.css')}}" rel="stylesheet" type="text/css" />
@endpush
@push('b-script')
<script src="{{asset('assets/crm/vendors/custom/datatables/datatables.bundle.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/crm/demo/default/custom/crud/datatables/extensions/buttons.js')}}" type="text/javascript"></script>
{!! $datatable->scripts() !!}
@endpush
