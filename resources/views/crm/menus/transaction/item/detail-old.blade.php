@extends('crm.layouts.default')
@section('title', 'Barang '.$item->name)
@section('content')
<div class="m-grid__item m-grid__item--fluid m-wrapper">
  <div>
    <div class="m-subheader">
      <div class="d-flex align-items-center">
        <div class="mr-auto">
          <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
            <li class="m-nav__item m-nav__item--home">
              <a href="{{url('home')}}" class="m-nav__link m-nav__link--icon">
                <i class="m-nav__link-icon la la-home pr-1" style="width: 20px;"></i><span class="m-nav__link-text">Home</span>
              </a>
            </li>
            <li class="m-nav__separator">></li>
            <li class="m-nav__item">
              <a href="{{$item->item_category->link_url}}" class="m-nav__link">
                <span class="m-nav__link-text">{{ucwords(strtolower($item->item_category->name))}}</span>
              </a>
            </li>
            <li class="m-nav__separator">></li>
            <li class="m-nav__item">
              <a href="{{url()->current()}}" class="m-nav__link">
                <span class="m-nav__link-text text-limit">{{$item->name}}</span>
              </a>
            </li>
          </ul>
        </div>
      </div>
    </div>
    <div class="m-content">
      <div class="row">
        <div class="col-sm-6 pb-4">
          <div class="owl-carousel owl-theme owl-loaded owl-primary" data-items="1" data-nav="true" data-loop="false" data-dots="false" data-hash="true">
            <div class="owl-stage-outer">
              <div class="owl-stage">
                @foreach($item->item_images as $key => $image)
                <div class="owl-item" data-hash="{{$key}}">
                  <div class="card">
                    <a href="#" style="height:400px; position: relative;overflow: hidden;">
                       <!-- style="height:500px;display: block;text-align: center;" -->
                      <img src="{{Storage::disk('public')->url($image->photo_url)}}"  alt="{{$item->name}}" style="display: inline-block; vertical-align: middle; max-width: 100%; max-height: 405px; width: auto; height: auto; border-radius: 3px;">
                      <!-- style="width:auto;margin-left: auto; vertical-align: middle; margin-right: auto;"  -->
                    </a>
                  </div>
                </div>
                @endforeach
              </div>
            </div>
          </div>
          <div class="pt-2 owl-carousel owl-theme owl-loaded" data-items="4" data-loop="false" data-nav="true" data-dots="false" data-hash="false">
            <div class="owl-stage-outer">
              <div class="owl-stage">
                @foreach($item->item_images as $key => $image)
                <div class="owl-item">
                  <div class="card">
                    <a class="owl-item-footer" data-owl-index="{{$key}}" href="javascript:void(0);" style="height:100px; position: relative;overflow: hidden;">
                      <img src="{{Storage::disk('public')->url($image->photo_url)}}"  alt="{{$item->name}}">
                    </a>
                  </div>
                </div>
                @endforeach
              </div>
            </div>
          </div>
        </div>
        <div class="col-sm-6 pb-4">
          <h2>{{$item->name}}</h2>
          <hr />
          <h5 class="price">Harga : <span>Rp {{number_format($item->price)}}</span></h5>
          <h5 class="price">Berat : <span>{{number_format($item->weight)}} Kg</span></h5>
          <h5 class="price">Kategori : <span> {{ucwords(strtolower($item->item_category->name))}}</span></h5>
          <!-- <h5 class="price">Panjang : <span>{{number_format($item->long)}}</span></h5>
          <h5 class="price">Lebar : <span>{{number_format($item->width)}}</span></h5>
          <h5 class="price">Tinggi : <span>{{number_format($item->height)}}</span></h5> -->
          <form class="m-form m-form--fit m-form--label-align-right">
          	<div class="m-portlet__body pull-bottom" >
              <input type="hidden" name="eid" value="{{$item->id}}"/>
              <hr />
          		<div class="form-group row">
          			<label for="example-search-input" class="col-3 col-form-label">Jumlah</label>
          			<div class="col-9">
      						<div class="input-group m-input-group m-input-group--air">
      							<div class="input-group-prepend">
                      <button class="btn btn-danger" type="button">-</button>
                    </div>
      							<input class="form-control m-input" placeholder="Jumlah" type="text">
      							<div class="input-group-append">
                      <button class="btn btn-success" type="button">+</button>
                    </div>
      						</div>
          			</div>
          		</div>
              <div class="btn-group btn-block mt-3" role="group">
                <button type="button" class="btn btn-info"><span class="fa fa-shopping-cart"></span> Tambah Ke Keranjang</button>
                <button type="button" class="ml-3 btn btn-primary btn-block">Beli</button>
              </div>
          	</div>
          </form>
        </div>
        <div class="col-sm-12 pb-4">
          <div class="m-portlet m-portlet--tabs">
            <div class="m-portlet__head">
                <div class="m-portlet__head-tools">
                    <ul class="nav nav-tabs m-tabs-line m-tabs-line--info m-tabs-line--2x m-tabs-line--right" role="tablist">
                        <li class="nav-item m-tabs__item">
                            <a class="nav-link m-tabs__link {{ Request::is('p/'.$item->link_url) ? 'active' : '' }} show" href="{{url('p/'.$item->link_url)}}" role="tab" aria-selected="true">
                               <!-- data-toggle="tab"  -->
                                <i class="fa fa-calendar-check-o" aria-hidden="true"></i>Deskripsi
                            </a>
                        </li>
                        <li class="nav-item m-tabs__item">
                            <a class="nav-link m-tabs__link {{ Request::is('p/'.$item->link_url.'/discussion') ? 'active' : '' }}" href="{{url('p/'.$item->link_url.'/discussion')}}" role="tab" aria-selected="false">
                               <!-- data-toggle="tab" -->
                                <i class="fa fa-bar-chart" aria-hidden="true"></i>Diskusi
                            </a>
                        </li>
                        <li class="nav-item m-tabs__item">
                            <a class="nav-link m-tabs__link {{ Request::is('p/'.$item->link_url.'/cost') ? 'active' : '' }}" href="{{url('p/'.$item->link_url.'/cost')}}" role="tab" aria-selected="false">
                               <!-- data-toggle="tab"  -->
                                <i class="fa fa-bar-chart" aria-hidden="true"></i>Estimasi Ongkir
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="m-portlet__body">
              <div class="tab-content">
                @if(Request::is('p/'.$item->link_url))
                <div class="tab-pane active show" id="description" role="tabpanel">
                  {!! $item->description !!}
                </div>
                @endif
                @if(Request::is('p/'.$item->link_url.'/discussion'))
                <div class="tab-pane" id="discussion" role="tabpanel">

                </div>
                @endif
                @if(Request::is('p/'.$item->link_url.'/cost'))
                <div class="tab-pane" id="cost" role="tabpanel">

                </div>
                @endif
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@push('b-css')
<link href="{{asset('assets/crm/vendors/custom/owlcarousel/assets/owl.carousel.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('assets/crm/vendors/custom/owlcarousel/assets/owl.theme.default.min.css')}}" rel="stylesheet" type="text/css" />
<style>
.owl-carousel{
  position: relative;
}
.owl-dots{
  position: absolute;
  bottom:10%;
  left:25%;
  right:25%;
}

.owl-prev {
  bottom: 45%;
  width: 30px;
  height: 30px;
  position: absolute;
  display: block !important;
  border-radius: 50% !important;
  background-color: white !important;

}
.owl-next {
  bottom: 45%;
  width: 30px;
  height: 30px;
  position: absolute;
  display: block !important;
  border-radius: 50% !important;
  background-color: white !important;
}
.owl-prev i, .owl-next i {
  color: #ccc;
}

.owl-prev, .owl-next {
  transition: visibility 0s, opacity 0.3s ease;
  visibility: hidden;
  opacity: 0;
}

.owl-carousel:hover .owl-nav .owl-prev {
  left: 2%;
  visibility: visible;
  opacity: 1;
}

.owl-carousel:hover .owl-nav .owl-next {
  right: 2%;
  visibility: visible;
  opacity: 1;
}
.text-limit {
  display: block !important;
  max-width: 250px !important;
  overflow: hidden;
  white-space: nowrap;
  text-overflow: ellipsis;
}
.text-limit:hover {
  max-width: 100% !important;
}

.pull-bottom {
  position: absolute;
  bottom: 5%;
  padding-right:30px;
  width:100%;
}

@media only screen and (max-width: 600px) {
    .pull-bottom {
      position: relative;
      bottom: 0;
      padding-right:0;
    }
}

/* @media screen and (-webkit-device-pixel-ratio: 0.75) {
    .pull-bottom {
      position: absolute;
      bottom: 10%;
      padding-right:30px;
      width:100%;
    }
} */
</style>
@endpush

@push('b-script')
<script src="{{asset('assets/crm/vendors/custom/owlcarousel/owl.carousel.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/crm/vendors/custom/scrollreveal/scrollreveal.js')}}" type="text/javascript"></script>

<script>
  $(document).ready(function() {
    $('.owl-carousel').each(function() {
      $items = $(this).data('items');
      $nav = $(this).data('nav');
      $dots = $(this).data('dots');
      $hash = $(this).data('hash');
      $loop = $(this).data('loop');
      $(this).owlCarousel({
        loop: $loop,
        items : $items,
        dots : $dots,
        margin: 10,
        nav : $nav,
        responsiveClass: true,
        URLhashListener:$hash,
        startPosition: 'URLHash',
        navText : ["<i class='fa fa-chevron-left'></i>","<i class='fa fa-chevron-right'></i>"],
      });
    });
    $('.owl-item-footer').on('click',function(){
      $data = $(this).data('owl-index');
      $('.owl-primary').trigger('to.owl.carousel', $data);
    });
  })
  var animate = {
      delay: 250,
  };
  ScrollReveal().reveal('.rv', animate);
</script>
@endpush
