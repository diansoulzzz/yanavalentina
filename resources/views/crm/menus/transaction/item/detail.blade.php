@extends('crm.layouts.default')
@section('title', 'Barang '.$item->name)
@section('content')
<div class="m-grid__item m-grid__item--fluid m-wrapper">
  <div>
    <div class="m-subheader">
      <div class="d-flex align-items-center">
        <div class="mr-auto">
          <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
            <li class="m-nav__item m-nav__item--home">
              <a href="{{url('home')}}" class="m-nav__link m-nav__link--icon">
                <i class="m-nav__link-icon la la-home pr-1" style="width: 20px;"></i><span class="m-nav__link-text">Home</span>
              </a>
            </li>
            <li class="m-nav__separator">></li>
            <li class="m-nav__item">
              <a href="{{$item->item_category->link_url}}" class="m-nav__link">
                <span class="m-nav__link-text">{{ucwords(strtolower($item->item_category->name))}}</span>
              </a>
            </li>
            <li class="m-nav__separator">></li>
            <li class="m-nav__item">
              <a href="{{url()->current()}}" class="m-nav__link">
                <span class="m-nav__link-text text-limit">{{$item->name}}</span>
              </a>
            </li>
          </ul>
        </div>
      </div>
    </div>
    <div class="m-content">
      @if (session('status'))
        <div class="alert {{session('status')['alert']}} alert-dismissible fade show" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
            {!! session('status')['message'] !!}
        </div>
      @endif
      <div class="row">
        <div class="col-sm-6 pb-4">
          <div class="owl-carousel owl-theme owl-loaded owl-primary" data-items="1" data-nav="true" data-loop="false" data-dots="false" data-hash="true">
            <div class="owl-stage-outer">
              <div class="owl-stage">
                @foreach($item->item_images as $key => $image)
                <div class="owl-item" data-hash="{{$key}}">
                  <div class="card">
                    <a href="javascript:void(0);" style="height:400px; position: relative;overflow: hidden;">
                       <!-- style="height:500px;display: block;text-align: center;" -->
                      <img src="{{Storage::disk('public')->url($image->photo_url)}}"  alt="{{$item->name}}" style="display: inline-block; vertical-align: middle; max-width: 100%; max-height: 405px; width: auto; height: auto; border-radius: 3px;">
                      <!-- style="width:auto;margin-left: auto; vertical-align: middle; margin-right: auto;"  -->
                    </a>
                  </div>
                </div>
                @endforeach
              </div>
            </div>
          </div>
          <div class="pt-2 owl-carousel owl-theme owl-loaded" data-items="4" data-loop="false" data-nav="true" data-dots="false" data-hash="false">
            <div class="owl-stage-outer">
              <div class="owl-stage">
                @foreach($item->item_images as $key => $image)
                <div class="owl-item">
                  <div class="card">
                    <a class="owl-item-footer" data-owl-index="{{$key}}" href="javascript:void(0);" style="height:100px; position: relative;overflow: hidden;">
                      <img src="{{Storage::disk('public')->url($image->photo_url)}}"  alt="{{$item->name}}">
                    </a>
                  </div>
                </div>
                @endforeach
              </div>
            </div>
          </div>
        </div>
        <div class="col-sm-6 pb-4">
          <h2>{{$item->name}}</h2>
          <hr />
          <h5 class="price">Harga : <span>Rp {{number_format($item->price)}}</span></h5>
          <h5 class="price">Berat : <span>{{number_format($item->weight)}} Gr</span></h5>
          <h5 class="price">Kategori : <span> {{ucwords(strtolower($item->item_category->name))}}</span></h5>
          <!-- <h5 class="price">Panjang : <span>{{number_format($item->long)}}</span></h5>
          <h5 class="price">Lebar : <span>{{number_format($item->width)}}</span></h5>
          <h5 class="price">Tinggi : <span>{{number_format($item->height)}}</span></h5> -->
          <form class="m-form m-form--fit m-form--label-align-right" method="POST" action="{{url()->current()}}">
            @csrf
          	<div class="m-portlet__body pull-bottom" >
              <input type="hidden" name="eid" value="{{$item->id}}"/>
              <hr />
          		<div class="form-group row">
          			<label for="example-search-input" class="col-3 col-form-label">Jumlah</label>
          			<div class="col-9">
      						<div class="input-group m-input-group m-input-group--air btn-counter">
      							<div class="input-group-prepend">
                      <button class="btn btn-danger minus" type="button">-</button>
                    </div>
      							<input class="form-control m-input counter inputmask numeric" name="qty" placeholder="Jumlah" type="text" required>
      							<div class="input-group-append">
                      <button class="btn btn-success plus" type="button">+</button>
                    </div>
      						</div>
          			</div>
          		</div>
              <div class="btn-group btn-block mt-3" role="group">
                <button type="submit" name="cart" class="btn btn-info"><span class="fa fa-shopping-cart"></span> Tambah Ke Keranjang</button>
                <button type="submit" name="buy" class="ml-3 btn btn-primary btn-block">Beli</button>
              </div>
          	</div>
          </form>
        </div>
        <div class="col-sm-12 pb-4">
          <div class="m-portlet m-portlet--tabs">
            <div class="m-portlet__head">
                <div class="m-portlet__head-tools">
                    <ul class="nav nav-tabs m-tabs-line m-tabs-line--info m-tabs-line--2x m-tabs-line--right" role="tablist">
                        <li class="nav-item m-tabs__item">
                            <a class="nav-link m-tabs__link active show" data-toggle="tab" data-target="" href="#description" role="tab" aria-selected="true">
                                <i class="fa fa-calendar-check-o" aria-hidden="true"></i>Deskripsi
                            </a>
                        </li>
                        <li class="nav-item m-tabs__item">
                            <a class="nav-link m-tabs__link" data-toggle="tab" data-target="" href="#discussion" role="tab" aria-selected="false">
                                <i class="fa fa-bar-chart" aria-hidden="true"></i>Diskusi
                            </a>
                        </li>
                        <li class="nav-item m-tabs__item">
                            <a class="nav-link m-tabs__link" data-toggle="tab" data-target="" href="#cost" role="tab" aria-selected="false">
                                <i class="fa fa-bar-chart" aria-hidden="true"></i>Estimasi Ongkir
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="m-portlet__body">
              <div class="tab-content">
                <div class="tab-pane active show" id="description" role="tabpanel">
                  {!! $item->description !!}
                </div>
                <div class="tab-pane" id="discussion" role="tabpanel">
                  <ul class="list-group">
                    <li class="list-group-item p-4 my-4" style="">
                      <div class="d-flex w-100 justify-content-between align-items-center">
                        <p class="mb-1"><i class="fa fa-question-circle"></i> Ada pertanyaan ? <b>Diskusikan dengan penjual atau pengguna lain</b></p>
                        <button type="button" class="btn btn-primary pull-right">
                          Tulis Pertanyaan
                        </button>
                      </div>
                    </li>

                    @foreach ($item->item_discussions as $key => $discussion)
                    <li class="list-group-item p-4 mb-4">
                      <div class="d-flex w-100 justify-content-between">
                        <div class="w-100">
                          <div class="clearfix content-heading">
                            <img class="img-circle avatar s-img-40 float-left mr-2" alt="{{ucwords($discussion->user->name)}}" src="{{ ($discussion->user->photo_url != '' ? Storage::disk('public')->url($discussion->user->photo_url) : asset('assets/logo/logo-rd.png')) }}">
                            <p class="mb-0 text-primary float-left mr-2">{{ucwords($discussion->user->name)}}</p>
                            <p class="mb-0 text-info"> - {{$discussion->created_at->format('d M')}} Jam {{$discussion->created_at->format('H:i')}}</p>
                            <p class="mb-0">{!! $discussion->discussion !!}</p>
                          </div>
                        </div>
                      </div>
                      <hr />
                      <ul class="list-group mt-4">
                        @foreach ($discussion->item_discussions as $key => $comment)
                        <li class="list-group-item p-4 mb-4">
                          <div class="d-flex w-100 justify-content-between">
                            <div class="w-100">
                              <div class="clearfix content-heading">
                                <img class="img-circle avatar s-img-40 float-left mr-2" alt="{{ucwords($comment->user->name)}}" src="{{ ($comment->user->photo_url != '' ? Storage::disk('public')->url($comment->user->photo_url) : asset('assets/logo/logo-rd.png')) }}">
                                <p class="mb-0 text-primary float-left mr-2">{{ucwords($comment->user->name)}}</p>
                                <p class="mb-0 text-info"> - {{$comment->created_at->format('d M')}} Jam {{$comment->created_at->format('H:i')}}</p>
                                <p class="mb-0">{!! $comment->discussion !!}</p>
                              </div>
                            </div>
                          </div>
                        </li>
                        @endforeach
                        <li class="list-group-item list-group-item-light">
                          <div class="d-flex w-100 justify-content-between">
                            <div class="w-100">
                              <div class="clearfix content-heading">
                                <form class="m-form m-form--fit m-form--label-align-right" method="post" action="{{url('p/'.$item->link_url.'/discussion')}}">
                                  <input type="hidden" name="eid" value="{{$item->id}}"/>
                                  @csrf
                                  <!-- <div class="form-group row">
                                    <label class="col-1 col-form-label text-center" for="form-input-comment">
                                      <img class="img-circle avatar s-img-40" alt="{{ucwords($discussion->user->name)}}" src="{{ ($discussion->user->photo_url != '' ? Storage::disk('public')->url($discussion->user->photo_url) : asset('assets/logo/logo-rd.png')) }}">
                                    </label>
                                    <div class="col-11">
                                      <textarea id="form-input-comment" name="comment" class="form-control m-input" placeholder="Tulis Komentar"></textarea>
                                    </div>
                                  </div> -->
                                  <textarea id="form-input-comment" name="comment" class="form-control col-xs-12 mb-2" placeholder="Tulis Komentar"></textarea>
                                  <button type="submit" name="write_comment" value="{{$discussion->id}}" class="btn btn-primary pull-right">
                                    Kirim
                                  </button>
                                </form>
                              </div>
                            </div>
                          </div>
                        </li>
                      </ul>
                    </li>
                    @endforeach
                    <li class="list-group-item list-group-item-light">
                      <form class="m-form m-form--fit m-form--label-align-right" method="post" action="{{url('p/'.$item->link_url.'/discussion')}}">
                        <input type="hidden" name="eid" value="{{$item->id}}"/>
                          @csrf
                        <textarea class="form-control col-xs-12 mb-2" rows="7" name="discussion" placeholder="Tulis Pertanyaan .."></textarea>
                        <button type="submit" class="btn btn-primary pull-right">
                          Kirim
                        </button>
                      </form>
                    </li>
                  </ul>
                </div>
                <div class="tab-pane" id="cost" role="tabpanel">
                  <form class="m-form m-form--fit m-form--label-align-right">
                    <div class="form-row align-items-center">
                      <div class="col-sm-8 pt-1">
                        <label for="cost-form">Kota atau Kecamatan :</label>
                        <select class="form-control m-select2" id="cost-subdistrict">
                          <option value="">Cari Kota atau Kecamatan</option>
                        </select>
                        <div id="select2-errors"></div>
                      </div>
                      <div class="col-sm-2 pt-1">
                        <label for="cost-form">Berat (gram) :</label>
                          <input type="text" class="form-control m-input" disabled id="cost-weight" value="{{$item->weight}}" name="cost_weight">
                      </div>
                      <div class="col-sm-2 pt-1">
                        <label for="cost-form"> </label>
                        <button type="button" id="cost-claculate" class="btn btn-primary btn-block">Hitung</button>
                      </div>
                    </div>
                  </form>
                  <div class="m-accordion m-accordion--default" id="cost-result" role="tablist" style="display: none;">
                    <hr/>
                    <div class="m-accordion__item">
                      <div class="m-accordion__item-head" role="tab" id="cost-result-head" data-toggle="collapse" href="#cost-result-body" aria-expanded="true">
                        <span class="m-accordion__item-icon"><i class="fa flaticon-truck"></i></span>
                        <span class="m-accordion__item-title">Hasil</span>
                        <span class="m-accordion__item-mode"></span>
                      </div>
                      <div class="m-accordion__item-body collapse show" id="cost-result-body" role="tabpanel" aria-labelledby="cost-result-head" data-parent="#cost-result" style="">
                        <div class="m-accordion__item-content">
                          <table class="table">
            						  	<tbody id="cost-result-tbody"></tbody>
      						        </table>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@push('b-css')
<link href="{{asset('assets/crm/vendors/custom/owlcarousel/assets/owl.carousel.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('assets/crm/vendors/custom/owlcarousel/assets/owl.theme.default.min.css')}}" rel="stylesheet" type="text/css" />
<style>
span.select2-container {
  display: block;
}
.owl-carousel{
  position: relative;
}
.owl-dots{
  position: absolute;
  bottom:10%;
  left:25%;
  right:25%;
}

.owl-prev {
  bottom: 45%;
  width: 30px;
  height: 30px;
  position: absolute;
  display: block !important;
  border-radius: 50% !important;
  background-color: white !important;

}
.owl-next {
  bottom: 45%;
  width: 30px;
  height: 30px;
  position: absolute;
  display: block !important;
  border-radius: 50% !important;
  background-color: white !important;
}
.owl-prev i, .owl-next i {
  color: #ccc;
}

.owl-prev, .owl-next {
  transition: visibility 0s, opacity 0.3s ease;
  visibility: hidden;
  opacity: 0;
}

.owl-carousel:hover .owl-nav .owl-prev {
  left: 2%;
  visibility: visible;
  opacity: 1;
}

.owl-carousel:hover .owl-nav .owl-next {
  right: 2%;
  visibility: visible;
  opacity: 1;
}
.text-limit {
  display: block !important;
  max-width: 250px !important;
  overflow: hidden;
  white-space: nowrap;
  text-overflow: ellipsis;
}
.text-limit:hover {
  max-width: 100% !important;
}

.pull-bottom {
  position: absolute;
  bottom: 5%;
  padding-right:30px;
  width:100%;
}

@media only screen and (max-width: 600px) {
    .pull-bottom {
      position: relative;
      bottom: 0;
      padding-right:0;
    }
}

</style>
@endpush

@push('b-script')
<script src="{{asset('assets/crm/vendors/custom/owlcarousel/owl.carousel.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/crm/vendors/custom/scrollreveal/scrollreveal.js')}}" type="text/javascript"></script>


<script>
$(function() {
    thisform.init();
}), thisform = {
  init: function() {
    thisform.o_init();
    thisform.s_init();
    thisform.sr_init();
    thisform.co_init();
    thisform.mask_init();
    thisform.bpm_init();
  },
  mask_init : function () {
    $('.inputmask.numeric').inputmask(
      "numeric",{
        "min" : 1,
        "max" : 10,
        'autoUnmask' : true
      }
    );
  },
  o_init : function () {
    $('.owl-carousel').each(function() {
      $items = $(this).data('items');
      $nav = $(this).data('nav');
      $dots = $(this).data('dots');
      $hash = $(this).data('hash');
      $loop = $(this).data('loop');
      $(this).owlCarousel({
        loop: $loop,
        items : $items,
        dots : $dots,
        margin: 10,
        nav : $nav,
        responsiveClass: true,
        URLhashListener:$hash,
        startPosition: 'URLHash',
        navText : ["<i class='fa fa-chevron-left'></i>","<i class='fa fa-chevron-right'></i>"],
      });
    });
    $('.owl-item-footer').on('click',function(){
      $data = $(this).data('owl-index');
      $('.owl-primary').trigger('to.owl.carousel', $data);
    });
  },
  sr_init : function () {
    var animate = {
        delay: 250,
    };
    ScrollReveal().reveal('.rv', animate);
  },
  bpm_init : function () {
    // alert($counter);
    $('.btn-counter .minus').on('click', function(){
      $value = $('.counter').inputmask('unmaskedvalue');
      // $value = $('.counter').val();
      $counter = Number($value);
      if($('.counter').val()>=1){
        $counter--;
        $('.counter').val($counter);
      }
      // $('.counter').val($('.counter').val()-1);
    });
    $('.btn-counter .plus').on('click', function(){
      $value = $('.counter').inputmask('unmaskedvalue');
      // $value = $('.counter').val();
      $counter = Number($value);
      $counter++;
      $('.counter').val($counter);
      // if($('.counter').val()<=1){
      //   $counter++;
      //   $('.counter').val($counter);
      // }
    });
  },
  s_init : function () {
    var s = $("#cost-subdistrict");
    s.select2({
        placeholder: "Cari Kota atau Kecamatan",
        allowClear: true,
        ajax: {
            url: "{{url('member/profile/address/country')}}",
            delay: 250,
            data: function(e) {
                return {
                    q: e.term,
                    page: e.page
                }
            },
            processResults: function(e, t) {
                return t.page = t.page || 1, {
                    results: e.items,
                    pagination: {
                        more: 30 * t.page < e.total_count
                    }
                }
            },
            cache: !0
        },
        escapeMarkup: function(e) {
            return e
        },
        minimumInputLength: 3,
        templateResult: function(e) {
            if (e.loading) return e.text;
            var t = "<div class='select2-result-repository clearfix'><div class='select2-result-repository__meta'><div class='select2-result-repository__title'>" + e.province_name +', '+ e.city_name + "</div>";
            return e.name && (t += "<div class='select2-result-repository__description'>" + e.name + "</div>"), t += " </div></div>"
        },
        templateSelection: function(e) {
            if ((e.id=="") || (e.province_name==null) || (e.city_name==null) || (e.name==null)) return e.text;
            return e.province_name +', '+ e.city_name +', ' + e.name
        },
    }), s.on('change', function(){
      s.trigger('input');
    });
    @isset($user->users_addresses_primary->subdistrict)
      var newOption = new Option('{{$user->users_addresses_primary->province_name}}' + ', ' + '{{$user->users_addresses_primary->city_name}}'  + ', ' + '{{$user->users_addresses_primary->subdistrict_name}}', '{{$user->users_addresses_primary->subdistrict_id}}', true, true);
      s.append(newOption).trigger('change');
      s.trigger({
          type: 'select2:select',
          params: {
              data: '{{$user->users_addresses_primary->subdistrict_id}}'
          }
      });
    @endif
  },
  co_init: function (){
    $('#cost-claculate').on('click',function(){
      $('#cost-result-body').collapse('hide');
      $weight = '{{$item->weight}}';
      $destination = $("#cost-subdistrict").val();
      $.ajax({
          url: "{{url('p/estimate/cost')}}",
          type: "GET",
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
          data: {
              weight : $weight,
              destination : $destination,
          },
          success : function(data) {
            $result = data['data'];
            // console.log($result);
            $table = '';
            $.each($result, function( index, estimate ) {
              if (estimate['costs'].length>0 && true){
                $table = $table +
                '<tr>'+
                  '<th colspan="1" class="text-left">'+estimate['name']+'</th>'+
                  '<th colspan="1" class="text-left">Hari</th>'+
                  '<th colspan="1" class="text-right">Harga</th>'+
                '</tr>';
              }
              $.each(estimate['costs'], function( index, courier ) {
                $.each(courier['cost'], function( index, cost ) {
                  $table = $table +
                  '<tr>'+
                    '<td class="text-left">'+courier['description']+'</td>'+
                    '<td class="text-left">'+(cost['etd'] ? cost['etd'] : 'Langsung')+'</td>'+
                    '<td class="text-right"> Mulai dari : Rp '+Inputmask.format(cost['value'], {'alias': 'currency','prefix': '','digits': 0})+'</td>'+
                  '</tr>';
                });
              });
              // console.log(estimate);
            });
            $('#cost-result-tbody').html($table);
            $('#cost-result').show('slow');
            $('#cost-result-body').collapse('show');
          }
      });
    })
  },
}
</script>
@endpush
