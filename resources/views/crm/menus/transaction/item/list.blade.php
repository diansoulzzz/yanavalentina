@extends('crm.layouts.default')
@section('title', 'Home')
@section('content')
<div class="m-grid__item m-grid__item--fluid m-wrapper">
  <div>
    <div class="m-subheader">
      <div class="d-flex align-items-center">
        <div class="mr-auto">
          <h4 class="m-subheader__title m-subheader__title--separator m--font-boldest">List Jadwal</h4>
          <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
            <li class="m-nav__item m-nav__item--home">
              <a href="{{url('home')}}" class="m-nav__link m-nav__link--icon">
                <i class="m-nav__link-icon la la-home pr-1" style="width: 20px;"></i><span class="m-nav__link-text">Home</span>
              </a>
            </li>
            <li class="m-nav__separator">></li>
            <li class="m-nav__item">
              <a href="{{url()->current()}}" class="m-nav__link">
              <span class="m-nav__link-text">Jadwal</span>
              </a>
            </li>
          </ul>
        </div>
      </div>
    </div>
    <div class="m-content">
      <div class="row">
        @foreach ($schedules as $key => $schedule)
        <div class="col-sm-4 mb-2 rv">
          <div class="card">
            <div class="card-img-top owl-carousel owl-theme owl-loaded" data-items="1" data-nav="true" data-dots="true">
              <div class="owl-stage-outer">
                <div class="owl-stage">
                  @foreach($schedule->schedule_images as $key => $image)
                  <div class="owl-item">
                    <div class="card">
                      <img src="{{Storage::disk('public')->url($image->photo_url)}}" alt="{{$schedule->name}}">
                    </div>
                  </div>
                  @endforeach
                </div>
              </div>
            </div>

            <div class="card-body">
              <h5 class="card-title">{{$schedule->name}}</h5>
              <p class="card-text">{{$schedule->description}}</p>
              <a href="#" class="btn btn-primary btn-block">Lihat Detail</a>
            </div>
          </div>
        </div>
        @endforeach
        @for ($i = 0; $i < 0; $i++)
        <div class="col-sm-4 mb-2 rv">
          <div class="card">
            <div class="card-body">
              <h5 class="card-title">Special title treatment</h5>
              <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
              <a href="#" class="btn btn-primary">Go somewhere</a>
            </div>
          </div>
        </div>
        @endfor
      </div>
    </div>
  </div>
</div>
@endsection

@push('b-css')
<link href="{{asset('assets/crm/vendors/custom/owlcarousel/assets/owl.carousel.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('assets/crm/vendors/custom/owlcarousel/assets/owl.theme.default.min.css')}}" rel="stylesheet" type="text/css" />
<style>
.owl-carousel{
  position: relative;
}
.owl-dots{
  position: absolute;
  bottom:10%;
  left:25%;
  right:25%;
}

.owl-prev {
  bottom: 45%;
  width: 30px;
  height: 30px;
  position: absolute;
  display: block !important;
  border-radius: 50% !important;
  background-color: white !important;

}
.owl-next {
  bottom: 45%;
  width: 30px;
  height: 30px;
  position: absolute;
  display: block !important;
  border-radius: 50% !important;
  background-color: white !important;
}
.owl-prev i, .owl-next i {
  color: #ccc;
}

.owl-prev, .owl-next {
  transition: visibility 0s, opacity 0.3s ease;
  visibility: hidden;
  opacity: 0;
}

.owl-carousel:hover .owl-nav .owl-prev {
  left: 2%;
  visibility: visible;
  opacity: 1;
}

.owl-carousel:hover .owl-nav .owl-next {
  right: 2%;
  visibility: visible;
  opacity: 1;
}

</style>
@endpush

@push('b-script')
<script src="{{asset('assets/crm/vendors/custom/owlcarousel/owl.carousel.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/crm/vendors/custom/scrollreveal/scrollreveal.js')}}" type="text/javascript"></script>

<script>
  $(document).ready(function() {
    $('.owl-carousel').each(function() {
      $items = 3;
      $items = $(this).data('items');
      $nav = $(this).data('nav');
      $dots = $(this).data('dots');
      $(this).owlCarousel({
        loop: true,
        items : $items,
        dots : $dots,
        margin: 10,
        nav : $nav,
        responsiveClass: true,
        navText : ["<i class='fa fa-chevron-left'></i>","<i class='fa fa-chevron-right'></i>"],
      });
    });
  })
  var animate = {
      delay: 250,
  };
  ScrollReveal().reveal('.rv', animate);
</script>
@endpush
