@extends('crm.layouts.default')
@section('title', 'Home')
@section('content')
<div class="m-grid__item m-grid__item--fluid m-wrapper">
  <div>
    <!-- <div class="m-subheader">
      <div class="d-flex align-items-center">
        <div class="mr-auto">
          <h4 class="m-subheader__title m-subheader__title--separator m--font-boldest">List Jadwal</h4>
          <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
            <li class="m-nav__item m-nav__item--home">
              <a href="{{url('home')}}" class="m-nav__link m-nav__link--icon">
                <i class="m-nav__link-icon la la-home pr-1" style="width: 20px;"></i><span class="m-nav__link-text">Home</span>
              </a>
            </li>
            <li class="m-nav__separator">></li>
            <li class="m-nav__item">
              <a href="{{url()->current()}}" class="m-nav__link">
              <span class="m-nav__link-text">Jadwal</span>
              </a>
            </li>
          </ul>
        </div>
      </div>
    </div> -->
    <div class="m-content">
      <form id="form-filter" method="get" action="{{url()->current()}}">
        <div class="row">
          <div class="col-sm-2">
            <div class="row">
              <div class="col-lg-12 my-3">
                <div class="card">
                  <div class="card-header bg-secondary">
                    <h4>Filter</h4>
                  </div>
                  <ul class="list-group list-group-flush">
                    <li class="list-group-item">
                      <div class="form-group m-form__group">
            						<label for="price-low"><b>Harga</b></label>
            						<input name="p_min" class="form-control m-input m-input--square input-filter" value="{{Request::input('p_min')}}" placeholder="Minimum" type="number">
            						<input name="p_max" class="form-control m-input m-input--square input-filter" value="{{Request::input('p_max')}}"  placeholder="Maximum" type="number">
            					</div>
                    </li>
                    <li class="list-group-item">
                      <div class="form-group m-form__group">
            						<label><b>Jadwal</b>Jadwal</label>
                        <div class="m-checkbox-list">
                          @foreach($schedules as $key => $schedule)
      											<label class="m-checkbox">
        											<input name="s[]" class="input-filter" value="{{$schedule->id}}" type="checkbox" {{(is_array(Request::input('s')) && in_array($schedule->id, Request::input('s')) ? 'checked' : '')}}> {{ucwords($schedule->name)}}
        											<span></span>
      											</label>
                          @endforeach
    										</div>
            					</div>
                    </li>
                    <li class="list-group-item">
                      <div class="form-group m-form__group">
            						<label><b>Kategori</b></label>
                        <div class="m-checkbox-list">
                          @foreach($items_category as $key => $category)
      											<label class="m-checkbox">
        											<input name="c[]" class="input-filter" value="{{$category->id}}" type="checkbox" {{(is_array(Request::input('c')) && in_array($category->id, Request::input('c')) ? 'checked' : '')}}> {{ucwords($category->name)}}
        											<span></span>
      											</label>
                          @endforeach
    										</div>
            					</div>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
          <div class="col-sm-10">
            <div class="row">
              <div class="col-lg-12 my-3">
                <div class="pull-left">
                  <p>Menampilkan {{number_format($items->total())}} produk untuk {{$keyword}} ({{number_format($items->firstItem())}} - {{number_format($items->lastItem())}} dari {{number_format($items->total())}})</p>
                </div>
                <div class="pull-right form-inline">
                  <div class="form-group pr-2">
                   <label for="sort_by">Urutkan dari: </label>
                   <select class="form-control input-filter" id="sort_by" name="sb">
                     <option value="">Sesuai</option>
                     <option value="price_asc" {{Request::input('sb')=='price_asc' ? 'selected' : ''}}>Harga Terendah</option>
                     <option value="price_desc" {{Request::input('sb')=='price_desc' ? 'selected' : ''}}>Harga Tertinggi</option>
                   </select>
                  </div>
                  <div class="btn-group">
                    <button class="btn m-btn m-btn--icon m-btn--icon-only btn-outline-info {{(Request::input('md')=='grid') || (Request::input('md')=='') ? 'active' : ''}}" name="md" type="submit" value="grid" id="grid">
                      <i class="fa fa-th-large"></i>
                    </button>
                    <button class="btn m-btn m-btn--icon m-btn--icon-only btn-outline-info {{Request::input('md')=='list' ? 'active' : 'grid-group-item'}}" name="md" type="submit" value="list" id="list">
      								<i class="fa fa-th-list"></i>
      							</button>
                  </div>
                </div>
              </div>
            </div>
            <div id="products" class="row view-group">
              @foreach ($items as $key => $item)
              <a class="item col-xs-3 col-lg-3 {{Request::input('md')=='list' ? 'list-group-item' : 'grid-group-item'}}" style="text-decoration: none !important" href="{{url('p/'.$item->link_url)}}">
                <div class="thumbnail card">
                  <div class="img-event">
                    <img class="group list-group-image img-fluid" src="{{ Storage::disk('public')->url($item->item_images_primary->photo_url) }}" alt="" />
                  </div>
                  <div class="caption card-body">
                    <h4 class="group card-title inner list-group-item-heading text-info">{{$item->name}}</h4>
                    <div class="row">
                      <div class="col-xs-12 col-md-6">
                        <p class="lead text-primary">Rp {{number_format($item->price)}}</p>
                      </div>
                      <!-- <div class="col-xs-12 col-md-6">
                        <a class="btn btn-success" href="">Add to cart</a>
                      </div> -->
                    </div>
                  </div>
                </div>
              </a>
              @endforeach
            </div>
            <div class="row">
              <div class="col-lg-12 my-3">
                <div class="pull-right">
                  {{ $items->appends(Request::all())->links() }}
                </div>
              </div>
            </div>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
@endsection

@push('b-css')
<link href="{{asset('assets/crm/vendors/custom/owlcarousel/assets/owl.carousel.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('assets/crm/vendors/custom/owlcarousel/assets/owl.theme.default.min.css')}}" rel="stylesheet" type="text/css" />
<style>
.owl-carousel{
  position: relative;
}
.owl-dots{
  position: absolute;
  bottom:10%;
  left:25%;
  right:25%;
}

.owl-prev {
  bottom: 45%;
  width: 30px;
  height: 30px;
  position: absolute;
  display: block !important;
  border-radius: 50% !important;
  background-color: white !important;

}
.owl-next {
  bottom: 45%;
  width: 30px;
  height: 30px;
  position: absolute;
  display: block !important;
  border-radius: 50% !important;
  background-color: white !important;
}
.owl-prev i, .owl-next i {
  color: #ccc;
}

.owl-prev, .owl-next {
  transition: visibility 0s, opacity 0.3s ease;
  visibility: hidden;
  opacity: 0;
}

.owl-carousel:hover .owl-nav .owl-prev {
  left: 2%;
  visibility: visible;
  opacity: 1;
}

.owl-carousel:hover .owl-nav .owl-next {
  right: 2%;
  visibility: visible;
  opacity: 1;
}


.view-group {
    display: -ms-flexbox;
    display: flex;
    -ms-flex-direction: row;
    flex-direction: row;
    padding-left: 0;
    margin-bottom: 0;
}
.thumbnail
{
    border: none;
    margin-bottom: 30px;
    padding: 0px;
    -webkit-border-radius: 0px;
    -moz-border-radius: 0px;
    border-radius: 0px;
}

.item.list-group-item
{
    float: none;
    width: 100%;
    background-color: #fff;
    margin-bottom: 30px;
    -ms-flex: 0 0 100%;
    flex: 0 0 100%;
    max-width: 100%;
    padding: 0 1rem;
    border: 0;
}
.item.list-group-item .img-event {
    float: left;
    width: 30%;
}

.item.list-group-item .list-group-image
{
    margin-right: 10px;
}
.item.list-group-item .thumbnail
{
    margin-bottom: 0px;
    display: inline-block;
}
.item.list-group-item .caption
{
    float: left;
    width: 70%;
    margin: 0;
}

.item.list-group-item:before, .item.list-group-item:after
{
    display: table;
    content: " ";
}

.item.list-group-item:after
{
    clear: both;
}
</style>
@endpush

@push('b-script')
<script src="{{asset('assets/crm/vendors/custom/owlcarousel/owl.carousel.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/crm/vendors/custom/scrollreveal/scrollreveal.js')}}" type="text/javascript"></script>

<script>
  $(document).ready(function() {
    $('.owl-carousel').each(function() {
      $items = 3;
      $items = $(this).data('items');
      $nav = $(this).data('nav');
      $dots = $(this).data('dots');
      $(this).owlCarousel({
        loop: true,
        items : $items,
        dots : $dots,
        margin: 10,
        nav : $nav,
        responsiveClass: true,
        navText : ["<i class='fa fa-chevron-left'></i>","<i class='fa fa-chevron-right'></i>"],
      });
    });
  })
  var animate = {
      delay: 250,
  };
  ScrollReveal().reveal('.rv', animate);
  // $('#list').click(function(event){
  //   event.preventDefault();
  //   $('#list').addClass('active');
  //   $('#grid').removeClass('active');
  //   $('#products .item').addClass('list-group-item');
  // });
  // $('#grid').click(function(event){
  //   event.preventDefault();
  //   $('#list').removeClass('active');
  //   $('#grid').addClass('active');
  //   $('#products .item').removeClass('list-group-item');
  //   $('#products .item').addClass('grid-group-item');
  // });

  $('.input-filter').on('change',function(){
    // alert('');
    // window.history.pushState("asd", "Title", "/new-url");
    $('#form-filter').submit();
  });

  // $('button.input-filter').on('click',function(){
  //   // alert('');
  //   // window.history.pushState("asd", "Title", "/new-url");
  //   $('#form-filter').submit();
  // });
</script>
@endpush
