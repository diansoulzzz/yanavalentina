@extends('crm.layouts.default')
@section('title', 'Cart')
@section('content')
<div class="m-grid__item m-grid__item--fluid m-wrapper">
  <div>
    <div class="m-subheader">
      <div class="d-flex align-items-center">
        <div class="mr-auto">
          <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
            <li class="m-nav__item m-nav__item--home">
              <a href="{{url('home')}}" class="m-nav__link m-nav__link--icon">
                <i class="m-nav__link-icon la la-home pr-1" style="width: 20px;"></i><span class="m-nav__link-text">Home</span>
              </a>
            </li>
            <li class="m-nav__separator">></li>
            <li class="m-nav__item">
              <a href="{{url()->current()}}" class="m-nav__link">
                <span class="m-nav__link-text text-limit">Cart</span>
              </a>
            </li>
          </ul>
        </div>
      </div>
    </div>
    <div class="m-content">
      @if (session('status'))
        <div class="alert {{session('status')['alert']}} alert-dismissible fade show" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
            {!! session('status')['message'] !!}
        </div>
      @endif
      <form id="modal-delivery-form" method="post" action="{{url('cart')}}">
        @csrf
        <div class="row">
          @if ($carts_exists)
            <div class="col-sm-8 pb-4">
              <div class="m-portlet m-portlet--mobile">
          			<div class="m-portlet__head">
          				<div class="m-portlet__head-caption">
          					<div class="m-portlet__head-title">
                      <span class="m-portlet__head-icon">
          							<i class="flaticon-business"></i>
          						</span>
          						<h3 class="m-portlet__head-text">
          							Total {{$carts->count()}} Barang di keranjang
          						</h3>
          					</div>
          				</div>
          			</div>
          			<div class="m-portlet__body">
                  @foreach ($carts as $key => $cart)
                  <div class="row">
                    <div class="col-6">
                      <img class="m--marginless m--img-centered s-img-40" src="{{ Storage::disk('public')->url($cart->item->item_images_primary->photo_url) }}" alt=""/>
                      <p class="text-left">{{$cart->item->name}}</p>
                    </div>
                      <div class="col-3">
                        <div class="form-group inline text-center">
                          <input type="number" step="1" min="0" data-subtotal="{{$cart->item->price*$cart->qty}}" data-subtotal-index={{'.subtotal-'.$key}} style="width: 50px;" class="form-control cart-qty spinner" data-item-price="{{$cart->item->price}}" value="{{$cart->qty}}" name="qty[{{$cart->id}}]" placeholder="0" type="text" required>
                           <!-- step="1" min="0" max="10"  -->
                        </div>
                      </div>
                    <div class="col-3">
                      <p class="text-right cart-subtotal {{'subtotal-'.$key}}">Rp {{number_format($cart->item->price*$cart->qty)}}</p>
                    </div>
                  </div>
                  @endforeach
          			</div>
          		</div>
            </div>
            <div class="col-sm-4 pb-4">
              <div class="m-portlet m-portlet--mobile">
          			<div class="m-portlet__head">
          				<div class="m-portlet__head-caption">
          					<div class="m-portlet__head-title">
                      <span class="m-portlet__head-icon">
          							<i class="flaticon-truck"></i>
          						</span>
          						<h3 class="m-portlet__head-text">
          							Pengiriman
          						</h3>
          					</div>
          				</div>
          			</div>
          			<div class="m-portlet__body clearfix">
                  <div class="summary-group">
                    <div class="summary-left">
                      <h6 class="my-0">Pengiriman Ke </h6>
                      <small class="text-muted" id="cart-address"  style="word-wrap: break-word;" data-address="{{$carts->first()->users_address->id}}">{{$carts->first()->users_address->address}}</small>
                    </div>
                    <div class="summary-right text-right">
                       <a href="{{url()->current()}}" class="btn" data-toggle="modal" data-target="#modal-delivery">Ubah</a>
                    </div>
                  </div>
                  <div class="summary-group">
                    <hr/>
                    <div class="summary-full">
                      <h6 class="my-0 pb-2">Courier Pengiriman </h6>
                      <select name="courier_cost" class="form-control m-select2" id="cart-courier" required>
                        <option value="">Kurir pengiriman</option>
                      </select>
                      <div id="select2-errors"></div>
                    </div>
                  </div>
          			</div>
                <hr />
          			<div class="m-portlet__head clearfix">
          				<div class="m-portlet__head-caption">
          					<div class="m-portlet__head-title">
                      <span class="m-portlet__head-icon">
          							<i class="flaticon-notes"></i>
          						</span>
          						<h3 class="m-portlet__head-text">
          							Ringkasan pesanan
          						</h3>
          					</div>
          				</div>
          			</div>
          			<div class="m-portlet__body">
                  <div class="summary-group">
                    <div class="summary-left">
                      <h6 class="my-0">Subtotal <small class="text-muted">(Dari <b class="summary-total-qty">{{$carts->qty}}</b> Barang)</small></h6>
                      <!-- <small class="text-muted">Dari <b class="summary-total-qty">{{$carts->qty}}</b> Barang</small> -->
                    </div>
                    <div class="summary-right text-right">
                       <span id="cart-subtotal-cost" data-subtotal="{{$carts->summary}}" class="text-muted">Rp {{number_format($carts->summary)}}</span>
                    </div>
                  </div>
                  <div class="summary-cost">
                    <div class="summary-group">
                      <div class="summary-left">
                        <h6 class="my-0">Biaya pengiriman</h6>
                        <!-- <small class="text-muted">Dari <b class="summary-total-qty">{{$carts->qty}}</b> Barang</small> -->
                      </div>
                      <div class="summary-right text-right">
                         <span id="cart-courier-cost" data-courier="0" class="text-muted">0</span>
                      </div>
                    </div>
                    <div class="summary-group font-weight-bold">
                      <hr/>
                      <div class="summary-left">
                        <h6 class="my-0">Total tagihan</h6>
                      </div>
                      <div class="summary-right text-right">
                         <span id="cart-total-cost" data-total="0" class="text-info">0</span>
                      </div>
                    </div>
                  </div>
                  <div class="summary-group">
                    <hr/>
                    <button type="submit" class="btn btn-primary btn-block">Check Out</button>
                  </div>
          			</div>
          		</div>
            </div>
          @else
            <div class="col-sm-12 pb-4">
              <div class="text-center">
                <p>Keranjang Belanja Anda Masih Kosong</p>
                <a class="btn btn-primary" href="{{url('home')}}">Belanja Sekarang</a>
              </div>
            </div>
          @endif
        </div>
      </form>
    </div>
  </div>
</div>

<div class="modal fade" id="modal-delivery" role="dialog" aria-labelledby="" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <form id="modal-delivery-form" class="m-form m-form--fit m-form--label-align-right" method="post" action="{{url('cart/address')}}">
        @csrf
        <input type="hidden" name="eid" value="{{Auth::id()}}"/>
        <div class="modal-header">
          <h5 class="modal-title" id="modal-delivery-header">Pilih Alamat Pengiriman</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="m-scrollable m-scroller ps ps--active-y" data-scrollbar-shown="true" data-scrollable="true" data-height="500" style="height: 200px; overflow: hidden;">
            <div class="form-group row">
              <div class="col-lg-12">
                <div class="m-radio-list" id="modal-delivery-radio"></div>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <a href="{{url('member/profile/address')}}" class="btn btn-secondary mr-auto">Tambah Alamat Baru</a>
          <!-- <button type="button" class="btn btn-secondary mr-auto" data-dismiss="modal">Tambah Alamat Baru</button> -->
          <button type="submit" class="btn btn-primary">Gunakan Alamat Ini</button>
        </div>
      </form>
    </div>
  </div>
</div>
@endsection

@push('b-css')
<link href="{{asset('assets/crm/vendors/custom/owlcarousel/assets/owl.carousel.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('assets/crm/vendors/custom/owlcarousel/assets/owl.theme.default.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('assets/crm/vendors/custom/inputspinner/number.css')}}" rel="stylesheet" type="text/css" />
<style>
div.summary-cost {
  display:none;
}
div.summary-group {
  clear:both;
  height: auto;
  padding-top: 10px !important;
  /* padding-bottom: : 20px !important; */
}
div.m-portlet__body {
  padding: 0rem 2.2rem !important;
  padding-bottom: 2.2rem !important;
}
div.summary-group .summary-left {
  width:80%;
  float:left;
}
div.summary-group .summary-full {
  width:100%;
  /* float:left; */
}
div.summary-group .summary-right {
  width:20%;
  float:left;
}
.no-search .select2-search {
    display:none
}

span.select2-container {
  display: block;
}
.owl-carousel{
  position: relative;
}
.owl-dots{
  position: absolute;
  bottom:10%;
  left:25%;
  right:25%;
}

.owl-prev {
  bottom: 45%;
  width: 30px;
  height: 30px;
  position: absolute;
  display: block !important;
  border-radius: 50% !important;
  background-color: white !important;

}
.owl-next {
  bottom: 45%;
  width: 30px;
  height: 30px;
  position: absolute;
  display: block !important;
  border-radius: 50% !important;
  background-color: white !important;
}
.owl-prev i, .owl-next i {
  color: #ccc;
}

.owl-prev, .owl-next {
  transition: visibility 0s, opacity 0.3s ease;
  visibility: hidden;
  opacity: 0;
}

.owl-carousel:hover .owl-nav .owl-prev {
  left: 2%;
  visibility: visible;
  opacity: 1;
}

.owl-carousel:hover .owl-nav .owl-next {
  right: 2%;
  visibility: visible;
  opacity: 1;
}
.text-limit {
  display: block !important;
  max-width: 250px !important;
  overflow: hidden;
  white-space: nowrap;
  text-overflow: ellipsis;
}
.text-limit:hover {
  max-width: 100% !important;
}

.pull-bottom {
  position: absolute;
  bottom: 5%;
  padding-right:30px;
  width:100%;
}

@media only screen and (max-width: 600px) {
    .pull-bottom {
      position: relative;
      bottom: 0;
      padding-right:0;
    }
}
</style>
@endpush

@push('b-script')
<script src="{{asset('assets/crm/vendors/custom/owlcarousel/owl.carousel.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/crm/vendors/custom/scrollreveal/scrollreveal.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/crm/vendors/custom/inputspinner/number.js')}}" type="text/javascript"></script>
<script type="text/javascript">
$(function() {
    thisform.init();
}), thisform = {
  init: function()
  {
    thisform.s_init();
    thisform.c_init();
    thisform.m_init();
    $(".cart-qty").on("change paste keyup", function() {
      $index = $(this).data('subtotal-index');
      $subtotal = $(this).data('item-price')*$(this).val();
      $old_subtotal = $(this).data('subtotal');

      $(this).data('subtotal',$subtotal);
      $($index).html('Rp '+Inputmask.format($subtotal, {'alias': 'currency','prefix': '','digits': 0}));

      $cart_old_subtotal = $('#cart-subtotal-cost').data('subtotal');
      $cart_subtotal = $cart_old_subtotal-$old_subtotal+$subtotal;
      $('#cart-subtotal-cost').html('Rp '+Inputmask.format($cart_subtotal, {'alias': 'currency','prefix': '','digits': 0}));
      $('#cart-subtotal-cost').data('subtotal',$cart_subtotal);

      $total = $('#cart-subtotal-cost').data('subtotal')+$('#cart-courier-cost').data('courier');
      $('#cart-total-cost').html('Rp '+Inputmask.format($total, {'alias': 'currency','prefix': '','digits': 0}));
      // $cartsubtotal_old = $('#cart-subtotal-cost').data('subtotal');
       // $(this).parents().find($index).html();
       // $($(this).data('subtotal-index')).html($subtotal);
       // console.log($(this).parents().find('.cart-subtotal').html());
       // $('.cart-subtotal').parents().closest().html(Inputmask.format($subtotal, {'alias': 'currency','prefix': '','digits': 0}));
    });
  },
  c_init : function () {
    var s = $("#cart-courier");
    s.select2({
        placeholder: "Kurir Pengiriman",
        allowClear: false,
        dropdownCssClass : 'no-search',
        ajax: {
            url: "{{url('cart/courier')}}",
            delay: 250,
            data: function(e) {
                return {
                    q: e.term,
                    page: e.page
                }
            },
            processResults: function(e, t) {
                return t.page = t.page || 1, {
                    results: e.items,
                    pagination: {
                        more: 30 * t.page < e.total_count
                    }
                }
            },
            cache: !0
        },
        escapeMarkup: function(e) {
            return e
        },
        // minimumInputLength: 3,
        // minimumInputLength: 1,
        minimumResultsForSearch: -1,
        templateResult: function(e) {
          if (e.loading) return e.text;
          var t = "<div class='select2-result-repository clearfix'><div class='select2-result-repository__meta'><div class='select2-result-repository__title'>" + e.name + "</div>";
          return e.name && (t += "<div class='select2-result-repository__description'> <p class='float-left'>Estimasi : " + e.cost.etd + "</p><p class='float-right'>Rp "+ Inputmask.format(e.cost.value, {'alias': 'currency','prefix': '','digits': 0}) +"</p>"), t += " </div></div>";
        },
        templateSelection: function(e) {
          if ((e.id=="") || (e.name==null)) return e.text;
          return e.code+' ('+e.cost.etd+') Rp '+Inputmask.format(e.cost.value, {'alias': 'currency','prefix': '','digits': 0});
        },
    }),
    s.on('change', function(e){
      s.trigger('input');
    });
    s.on('select2:select', function (event) {
      $('.summary-cost').css({
        "opacity":"0",
        "display":"block",
      }).show().animate({opacity:1});
      var e = event.params.data;
      $('#cart-courier-cost').html('Rp '+Inputmask.format(e.cost.value, {'alias': 'currency','prefix': '','digits': 0}));
      $('#cart-courier-cost').data('courier',e.cost.value);
      $total = $('#cart-subtotal-cost').data('subtotal')+e.cost.value;
      $('#cart-total-cost').html('Rp '+Inputmask.format($total, {'alias': 'currency','prefix': '','digits': 0}));
      // console.log(data);
    });
  },
  s_init: function()
  {
    $('.spinner').each(function () {
        $(this).number();
    });
  },
  m_init: function (){
    var m = $("#modal-delivery");
    m.on("shown.bs.modal", function() {
      // $('#modal-delivery-radio').html('');
      mApp.block("#modal-delivery .modal-content", {
        overlayColor: "#000000",
        type: "loader",
        state: "primary",
        message: "Processing..."
      });
      $.ajax({
        url: "{{url('cart/address')}}",
        type: "GET",
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success : function(data) {
          mApp.unblock("#modal-delivery .modal-content");
          // console.log(data);
          $result = data['data'];
          $address = $result['address'];
          // console.log($address);
          $address_radio = '';
          $.each($address, function (index, address){
            $checked = '';
            $address_current = $('#cart-address').data('address');
            if ($address_current==address['id']){
              $checked='checked';
            }
            $address_radio+=
            '<div class="border border-secondary p-3 mb-3" style="word-wrap: break-word;">'+
              '<label class="m-radio m-radio--state-success">'+
                '<input name="users_address_id" value="'+address['id']+'" type="radio" '+$checked+'>'+
                  '<b>'+address['person']+'</b></br>'+
                  address['address']+'</br>'+
                  address['province_name']+', '+address['city_name']+', '+address['subdistrict_name']+
                '<span style="top: 40%;bottom: 40%;"></span>'+
              '</label>'+
            '</div>';
          });
          $('#modal-delivery-radio').html($address_radio);
        }
      });
    }).on("hidden.bs.modal", function() {

    });
  },
}
</script>
@endpush
