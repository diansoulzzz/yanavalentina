@extends('crm.layouts.default')
@section('title', 'Cart')
@section('content')
<div class="m-grid__item m-grid__item--fluid m-wrapper">
  <div>
    <div class="m-subheader">
      <div class="d-flex align-items-center">
        <div class="mr-auto">
          <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
            <li class="m-nav__item m-nav__item--home">
              <a href="{{url('home')}}" class="m-nav__link m-nav__link--icon">
                <i class="m-nav__link-icon la la-home pr-1" style="width: 20px;"></i><span class="m-nav__link-text">Home</span>
              </a>
            </li>
            <li class="m-nav__separator">></li>
            <li class="m-nav__item">
              <a href="{{url()->current()}}" class="m-nav__link">
                <span class="m-nav__link-text text-limit">Cart</span>
              </a>
            </li>
          </ul>
        </div>
      </div>
    </div>
    <div class="m-content">
      @if (session('status'))
        <div class="alert {{session('status')['alert']}} alert-dismissible fade show" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
            {!! session('status')['message'] !!}
        </div>
      @endif
<<<<<<< HEAD
      <form id="modal-delivery-form" method="post" action="{{url('cart')}}">
        @csrf
        <div class="row">
=======
      <div class="row">
        @if ($carts_exists)
>>>>>>> origin/master
          <div class="col-sm-8 pb-4">
            <div class="m-portlet m-portlet--mobile">
        			<div class="m-portlet__head">
        				<div class="m-portlet__head-caption">
        					<div class="m-portlet__head-title">
                    <span class="m-portlet__head-icon">
        							<i class="flaticon-business"></i>
        						</span>
        						<h3 class="m-portlet__head-text">
        							Total {{$carts->count()}} Barang di keranjang
        						</h3>
        					</div>
        				</div>
        			</div>
        			<div class="m-portlet__body">
                @foreach ($carts as $key => $cart)
                <div class="row">
<<<<<<< HEAD
                  <div class="col-6">
                    <img class="m--marginless m--img-centered s-img-40" src="{{ Storage::disk('public')->url($cart->item->item_images_primary->photo_url) }}" alt=""/>
                    <p class="text-left">{{$cart->item->name}}</p>
                  </div>
                    <div class="col-3">
                      <div class="form-group inline text-center">
                        <input type="number" style="width: 50px;" class="form-control spinner" value="{{$cart->qty}}" step="1" min="0" max="10" name="qty" placeholder="0" type="text" required>
                      </div>
                    </div>
                  <div class="col-3">
                    <p class="text-right">Rp {{number_format($cart->item->price*$cart->qty)}}</p>
                  </div>
                </div>
                @endforeach
        			</div>
        		</div>
          </div>
          <div class="col-sm-4 pb-4">
            <div class="m-portlet m-portlet--mobile">
=======
                  <div class="col-4">
                    <img class="m--marginless m--img-centered s-img-40" src="{{ Storage::disk('public')->url($cart->item->item_images_primary->photo_url) }}" alt=""/>
                    <p class="text-left">{{$cart->item->name}}</p>
                  </div>
                  <div class="col-4">
        						<div class="input-group m-input-group m-input-group--air btn-counter">
        							<div class="input-group-prepend">
                        <button class="btn btn-danger minus" type="button">-</button>
                      </div>
        							<input class="form-control m-input counter inputmask numeric" name="qty" placeholder="0" type="text" value="{{$cart->qty}}" required>
        							<div class="input-group-append">
                        <button class="btn btn-success plus" type="button">+</button>
                      </div>
        						</div>
                  </div>
                  <div class="col-4">
                    <p class="text-right">Rp {{number_format($cart->item->price*$cart->qty)}}</p>
                  </div>
                </div>
                @endforeach
        			</div>
        		</div>
          </div>
          <div class="col-sm-4 pb-4">
            <div class="m-portlet m-portlet--mobile">
        			<div class="m-portlet__head">
        				<div class="m-portlet__head-caption">
        					<div class="m-portlet__head-title">
                    <span class="m-portlet__head-icon">
        							<i class="flaticon-truck"></i>
        						</span>
        						<h3 class="m-portlet__head-text">
        							Pengiriman
        						</h3>
        					</div>
        				</div>
        			</div>
        			<div class="m-portlet__body">
                <div class="summary-group">
                  <div class="summary-left">
                    <h6 class="my-0">Pengiriman Ke </h6>
                    <small class="text-muted">{{$carts->first()->users_address->address}}</small>
                  </div>
                  <div class="summary-right text-right">
                     <a href="{{url()->current()}}" class="btn">Ubah</a>
                  </div>
                </div>
        			</div>
              <hr/>
>>>>>>> origin/master
        			<div class="m-portlet__head">
        				<div class="m-portlet__head-caption">
        					<div class="m-portlet__head-title">
                    <span class="m-portlet__head-icon">
<<<<<<< HEAD
        							<i class="flaticon-truck"></i>
        						</span>
        						<h3 class="m-portlet__head-text">
        							Pengiriman
=======
        							<i class="flaticon-notes"></i>
        						</span>
        						<h3 class="m-portlet__head-text">
        							Ringkasan pesanan
>>>>>>> origin/master
        						</h3>
        					</div>
        				</div>
        			</div>
<<<<<<< HEAD
        			<div class="m-portlet__body clearfix">
                <div class="summary-group">
                  <div class="summary-left">
                    <h6 class="my-0">Pengiriman Ke </h6>
                    <small class="text-muted" id="cart-address"  style="word-wrap: break-word;" data-address="{{$carts->first()->users_address->id}}">{{$carts->first()->users_address->address}}</small>
                  </div>
                  <div class="summary-right text-right">
                     <a href="{{url()->current()}}" class="btn" data-toggle="modal" data-target="#modal-delivery">Ubah</a>
                  </div>
                </div>
                <hr />
                <div class="summary-group">
                  <div class="summary-left">
                    <h6 class="my-0">Courier Pengiriman </h6>
                    <select></select>
                  </div>
                </div>
        			</div>
        			<div class="m-portlet__head clearfix">
        				<div class="m-portlet__head-caption">
        					<div class="m-portlet__head-title">
                    <span class="m-portlet__head-icon">
        							<i class="flaticon-notes"></i>
        						</span>
        						<h3 class="m-portlet__head-text">
        							Ringkasan pesanan
        						</h3>
        					</div>
        				</div>
        			</div>
        			<div class="m-portlet__body">
                <div class="summary-group">
                  <div class="summary-left">
                    <h6 class="my-0">Total Pembelian</h6>
                    <small class="text-muted">Dari <b class="summary-total-qty">{{$carts->qty}}</b> Barang</small>
                  </div>
                  <div class="summary-right text-right">
                     <span class="text-muted">Rp {{number_format($carts->summary)}}</span>
                  </div>
                </div>
                <div class="summary-group">
                  <hr/>
                  <button type="submit" class="btn btn-primary btn-block">Check Out</button>
=======
        			<div class="m-portlet__body">
                <div class="summary-group">
                  <div class="summary-left">
                    <h6 class="my-0">Total Pembelian</h6>
                    <small class="text-muted">Dari <b class="summary-total-qty">{{$carts->qty}}</b> Barang</small>
                  </div>
                  <div class="summary-right text-right">
                     <span class="text-muted">Rp {{number_format($carts->summary)}}</span>
                  </div>
                </div>
                <div class="summary-group">
                  <hr/>
                  <button class="btn btn-primary btn-block">Check Out</button>
>>>>>>> origin/master
                </div>
        			</div>
        		</div>
          </div>
<<<<<<< HEAD
        </div>
      </form>
    </div>
  </div>
</div>

<div class="modal fade" id="modal-delivery" role="dialog" aria-labelledby="" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <form id="modal-delivery-form" class="m-form m-form--fit m-form--label-align-right" method="post" action="{{url('cart/address')}}">
        @csrf
        <input type="hidden" name="eid" value="{{Auth::id()}}"/>
        <div class="modal-header">
          <h5 class="modal-title" id="modal-delivery-header">Pilih Alamat Pengiriman</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="m-scrollable m-scroller ps ps--active-y" data-scrollbar-shown="true" data-scrollable="true" data-height="500" style="height: 200px; overflow: hidden;">
            <div class="form-group row">
              <div class="col-lg-12">
                <div class="m-radio-list" id="modal-delivery-radio"></div>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary mr-auto" data-dismiss="modal">Tambah Alamat Baru</button>
          <button type="submit" class="btn btn-primary">Gunakan Alamat Ini</button>
        </div>
      </form>
=======
        @else
          <div class="col-sm-12 pb-4">
            <div class="text-center">
              <p>Keranjang Belanja Anda Masih Kosong</p>
              <a class="btn btn-primary" href="{{url('home')}}">Belanja Sekarang</a>
            </div>
          </div>
        @endif
      </div>
>>>>>>> origin/master
    </div>
  </div>
</div>
@endsection

@push('b-css')
<link href="{{asset('assets/crm/vendors/custom/owlcarousel/assets/owl.carousel.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('assets/crm/vendors/custom/owlcarousel/assets/owl.theme.default.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('assets/crm/vendors/custom/inputspinner/number.css')}}" rel="stylesheet" type="text/css" />
<style>
div.summary-group {
  clear:both;
  height: 50px;
}
div.summary-group .summary-left {
  width:80%;
  float:left;
}
div.summary-group .summary-right {
  width:20%;
  float:left;
}

span.select2-container {
  display: block;
}
.owl-carousel{
  position: relative;
}
.owl-dots{
  position: absolute;
  bottom:10%;
  left:25%;
  right:25%;
}

.owl-prev {
  bottom: 45%;
  width: 30px;
  height: 30px;
  position: absolute;
  display: block !important;
  border-radius: 50% !important;
  background-color: white !important;

}
.owl-next {
  bottom: 45%;
  width: 30px;
  height: 30px;
  position: absolute;
  display: block !important;
  border-radius: 50% !important;
  background-color: white !important;
}
.owl-prev i, .owl-next i {
  color: #ccc;
}

.owl-prev, .owl-next {
  transition: visibility 0s, opacity 0.3s ease;
  visibility: hidden;
  opacity: 0;
}

.owl-carousel:hover .owl-nav .owl-prev {
  left: 2%;
  visibility: visible;
  opacity: 1;
}

.owl-carousel:hover .owl-nav .owl-next {
  right: 2%;
  visibility: visible;
  opacity: 1;
}
.text-limit {
  display: block !important;
  max-width: 250px !important;
  overflow: hidden;
  white-space: nowrap;
  text-overflow: ellipsis;
}
.text-limit:hover {
  max-width: 100% !important;
}

.pull-bottom {
  position: absolute;
  bottom: 5%;
  padding-right:30px;
  width:100%;
}

@media only screen and (max-width: 600px) {
    .pull-bottom {
      position: relative;
      bottom: 0;
      padding-right:0;
    }
}
</style>
@endpush

@push('b-script')
<script src="{{asset('assets/crm/vendors/custom/owlcarousel/owl.carousel.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/crm/vendors/custom/scrollreveal/scrollreveal.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/crm/vendors/custom/inputspinner/number.js')}}" type="text/javascript"></script>
<script type="text/javascript">
$(function() {
    thisform.init();
}), thisform = {
  init: function()
  {
    thisform.s_init();
    thisform.c_init();
    thisform.m_init();
  },
  c_init : function () {
    var s = $("#cost-subdistrict");
    s.select2({
        placeholder: "Cari Kota atau Kecamatan",
        allowClear: true,
        ajax: {
            url: "{{url('member/profile/address/country')}}",
            delay: 250,
            data: function(e) {
                return {
                    q: e.term,
                    page: e.page
                }
            },
            processResults: function(e, t) {
                return t.page = t.page || 1, {
                    results: e.items,
                    pagination: {
                        more: 30 * t.page < e.total_count
                    }
                }
            },
            cache: !0
        },
        escapeMarkup: function(e) {
            return e
        },
        minimumInputLength: 3,
        templateResult: function(e) {
            if (e.loading) return e.text;
            var t = "<div class='select2-result-repository clearfix'><div class='select2-result-repository__meta'><div class='select2-result-repository__title'>" + e.province_name +', '+ e.city_name + "</div>";
            return e.name && (t += "<div class='select2-result-repository__description'>" + e.name + "</div>"), t += " </div></div>"
        },
        templateSelection: function(e) {
            if ((e.id=="") || (e.province_name==null) || (e.city_name==null) || (e.name==null)) return e.text;
            return e.province_name +', '+ e.city_name +', ' + e.name
        },
    }), s.on('change', function(){
      s.trigger('input');
    });
    @isset($user->users_addresses_primary->subdistrict)
      var newOption = new Option('{{$user->users_addresses_primary->province_name}}' + ', ' + '{{$user->users_addresses_primary->city_name}}'  + ', ' + '{{$user->users_addresses_primary->subdistrict_name}}', '{{$user->users_addresses_primary->subdistrict_id}}', true, true);
      s.append(newOption).trigger('change');
      s.trigger({
          type: 'select2:select',
          params: {
              data: '{{$user->users_addresses_primary->subdistrict_id}}'
          }
      });
    @endif
  },
  s_init: function()
  {
    $('.spinner').each(function () {
        $(this).number();
    });
  },
  m_init: function (){
    var m = $("#modal-delivery");
    m.on("shown.bs.modal", function() {
      // $('#modal-delivery-radio').html('');
      mApp.block("#modal-delivery .modal-content", {
        overlayColor: "#000000",
        type: "loader",
        state: "primary",
        message: "Processing..."
      });
      $.ajax({
        url: "{{url('cart/address')}}",
        type: "GET",
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success : function(data) {
          mApp.unblock("#modal-delivery .modal-content");
          // console.log(data);
          $result = data['data'];
          $address = $result['address'];
          // console.log($address);
          $address_radio = '';
          $.each($address, function (index, address){
            $checked = '';
            $address_current = $('#cart-address').data('address');
            if ($address_current==address['id']){
              $checked='checked';
            }
            $address_radio+=
            '<div class="border border-secondary p-3 mb-3" style="word-wrap: break-word;">'+
              '<label class="m-radio m-radio--state-success">'+
                '<input name="users_address_id" value="'+address['id']+'" type="radio" '+$checked+'>'+
                  '<b>'+address['person']+'</b></br>'+
                  address['address']+'</br>'+
                  address['province_name']+', '+address['city_name']+', '+address['subdistrict_name']+
                '<span style="top: 40%;bottom: 40%;"></span>'+
              '</label>'+
            '</div>';
          });
          $('#modal-delivery-radio').html($address_radio);
        }
      });
    }).on("hidden.bs.modal", function() {

    });
  },
}
</script>
@endpush
