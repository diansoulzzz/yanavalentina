@extends('crm.layouts.default')
@section('title', 'Dashboard')
@section('content')
<div class="m-grid__item m-grid__item--fluid m-wrapper">
  <div class="m-subheader ">
    <div class="d-flex align-items-center">
      <div class="mr-auto">
        <h3 class="m-subheader__title ">
          Dashboard
        </h3>
      </div>
      <div>
        <span class="m-subheader__daterange" id="m_dashboard_daterangepicker">
          <span class="m-subheader__daterange-label">
            <span class="m-subheader__daterange-title"></span>
            <span class="m-subheader__daterange-date m--font-brand"></span>
          </span>
          <a href="#" class="btn btn-sm btn-brand m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill">
            <i class="la la-angle-down"></i>
          </a>
        </span>
      </div>
    </div>
  </div>
  <div class="m-content">
    <div class="row">
      <div class="col-xl-12">

        <div class="m-portlet " id="m_portlet">
          <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
              <div class="m-portlet__head-title">
                <span class="m-portlet__head-icon">
                  <i class="flaticon-map-location"></i>
                </span>
                <h3 class="m-portlet__head-text">
                  Calendar
                </h3>
              </div>
            </div>
            <div class="m-portlet__head-tools">
              <ul class="m-portlet__nav">
                <li class="m-portlet__nav-item">
                  <a href="#" class="btn btn-accent m-btn m-btn--custom m-btn--icon m-btn--pill m-btn--air">
                    <span>
                      <i class="la la-plus"></i>
                      <span>
                        Add Event
                      </span>
                    </span>
                  </a>
                </li>
              </ul>
            </div>
          </div>
          <div class="m-portlet__body">
            <div id="m_calendar"></div>
          </div>
        </div>

      </div>
    </div>
  </div>
</div>
@endsection
@push('b-css')
<link href="{{asset('assets/crm/vendors/custom/fullcalendar/fullcalendar.bundle.min.css')}}" rel="stylesheet" type="text/css" />
@endpush
@push('b-script')
<script src="{{asset('assets/crm/vendors/custom/fullcalendar/fullcalendar.bundle.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/crm/app/js/dashboard.js')}}" type="text/javascript"></script>
@endpush
