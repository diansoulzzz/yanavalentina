@extends('crm.layouts.default')
@section('title', 'Jadwal')
@section('content')
<div class="m-grid__item m-grid__item--fluid m-wrapper">
  <div class="m-subheader ">
    <div class="d-flex align-items-center">
      <div class="mr-auto">
        <h3 class="m-subheader__title ">
          Jadwal
        </h3>
      </div>
    </div>
  </div>
  <div class="m-content" id="m-form-content">
    @if (session('status'))
      <div class="alert {{session('status')['alert']}} alert-dismissible fade show" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
          {!! session('status')['message'] !!}
      </div>
    @endif
    <div class="row">
      <div class="col-xl-12">
        <div class="m-portlet m-portlet--tab">
    			<div class="m-portlet__head">
    				<div class="m-portlet__head-caption">
    					<div class="m-portlet__head-title">
    						<span class="m-portlet__head-icon m--hide">
    						<i class="la la-gear"></i>
    						</span>
    						<h3 class="m-portlet__head-text">
    							Informasi Jadwal
    						</h3>
    					</div>
    				</div>
    			</div>
    			<form id="v-form" class="m-form m-form--fit m-form--label-align-right" method="post" enctype="multipart/form-data" action="{{url()->current()}}">
            @csrf
            @if(isset($schedule))
            <input type="hidden" name="eid" value="{{$schedule->eid}}"/>
            @endif
    				<div class="m-portlet__body">
              <div class="form-group m-form__group row">
                <label class="col-3 col-form-label text-left" for="form-input-name">
                  Gambar Jadwal<br />
                  <span class="m-form__help">Isi Gambar Minimal 1 - Maximal 4</span>
                </label>
                <div class="col-9">
        					<div class="m-dropzone dropzone m-dropzone--primary" id="m-dropzone">
        						<div class="m-dropzone__msg dz-message needsclick">
        						    <h3 class="m-dropzone__msg-title">Jatuhkan gambar di sini atau klik untuk mengunggah.</h3>
        						    <span class="m-dropzone__msg-desc">Maximal 4 gambar</span>
        						</div>
        					</div>
                </div>
              </div>
    					<div class="form-group m-form__group row">
    						<label class="col-3 col-form-label text-left" for="form-input-name">
                  Nama Jadwal<br />
                  <span class="m-form__help">Tulis nama jadwal yang sesuai.</span>
                </label>
    						<div class="col-9">
    							<input class="form-control m-input" id="form-input-name" name="name" type="text" value="{{old('name', isset($schedule->name) ? $schedule->name : '')}}" placeholder="Masukan Nama Jadwal" data-parsley-required="true" data-parsley-required-message="Nama Wajib diisi">
                  <!-- <span class="m-form__help">We'll never share your email with anyone else.</span> -->
    						</div>
    					</div>
    					<div class="form-group m-form__group row">
    						<label class="col-3 col-form-label text-left" for="form-input-description">
                  Deskripsi Jadwal<br />
                  <span class="m-form__help">Deskipsi jadwal.</span>
                </label>
    						<div class="col-9">
    							<textarea id="form-input-description" name="description" class="form-control m-input" placeholder="Masukan Deskripsi Jadwal" data-parsley-required="true" data-parsley-required-message="Deskripsi wajib diisi">{{old('description', isset($schedule->description) ? $schedule->description : '')}}</textarea>
    						</div>
    					</div>
              <div class="form-group m-form__group row">
                <label class="col-3 col-form-label text-left" for="form-input-start_date">
                  Tanggal Mulai<br />
                  <span class="m-form__help">Tanggal mulai sesuai dengan periode.</span>
                </label>
                <div class="col-9">
                  <div class="input-group" id="date-start">
                    <input type='text' id="form-input-start_date" name="start_date" class="form-control m-input bootstrap-datepicker" value="{{old('start_date', isset($schedule->start_date) ? Carbon\Carbon::parse($schedule->start_date)->format('d/m/Y')  : '')}}" readonly placeholder="Masukan Tanggal Mulai Jadwal" data-parsley-required="true" data-parsley-required-message="Tanggal Mulai wajib di isi" data-parsley-errors-container="#date-start-error" />
                    <div class="input-group-append">
                      <span class="input-group-text"><i class="la la-calendar-check-o"></i></span>
                    </div>
                  </div>
                  <div id="date-start-error"></div>
                </div>
              </div>
              <div class="form-group m-form__group row">
                <label class="col-3 col-form-label text-left" for="form-input-end_date">
                  Tanggal Selesai<br />
                  <span class="m-form__help">Tanggal selesai sesuai dengan periode.</span>
                </label>
                <div class="col-9">
                  <div class="input-group" id="date-end">
                    <input type='text' id="form-input-end_date" name="end_date" class="form-control m-input bootstrap-datepicker" value="{{old('end_date', isset($schedule->end_date) ? Carbon\Carbon::parse($schedule->end_date)->format('d/m/Y') : '')}}" readonly placeholder="Masukan Tanggal Selesai Jadwal" data-parsley-required="true" data-parsley-required-message="Tanggal Akhir wajib di isi" data-parsley-errors-container="#date-end-error" />
                    <div class="input-group-append">
                      <span class="input-group-text"><i class="la la-calendar-check-o"></i></span>
                    </div>
                  </div>
                  <div id="date-end-error"></div>
                </div>
              </div>
    					<div class="form-group m-form__group row">
    						<label class="col-3 col-form-label text-left" for="form-input-publish">
                  Publish<br />
                  <span class="m-form__help">Publish.</span>
                </label>
    						<div class="col-9">
                  <label class="m-checkbox m-checkbox--state-success">
  									<input class="form-control m-input" id="form-input-publish" name="publish" type="checkbox" {{ old('publish',( isset($schedule->publish) ? ($schedule->publish ? 'checked' : '') : '') ) ? 'checked' : '' }} data-parsley-required="false"> Tampil dihalaman?
  									<span></span>
									</label>
    						</div>
    					</div>
    					<!-- <div class="form-group m-form__group row">
    						<label class="col-3 col-form-label text-left" for="form-input-status">
                  Status<br />
                  <span class="m-form__help">Status adalah periode.</span>
                </label>
    						<div class="col-9">
                  <label class="m-checkbox m-checkbox--state-success">
  									<input class="form-control m-input" id="form-input-status" name="status" type="checkbox" value="{{old('status', isset($schedule->name) ? $schedule->status : '')}}" placeholder="Masukan Nama Jadwal" data-parsley-required="true" data-parsley-required-message="Nama Wajib diisi"> Dibuka
  									<span></span>
									</label>
    						</div>
    					</div> -->
            </div>
    				<div class="m-portlet__foot m-portlet__foot--fit">
    					<div class="m-form__actions text-right">
    						<a href="{{url('master/schedule/list')}}" class="mb-1 btn btn-secondary">Cancel</a>
    						<button type="submit" class="mb-1 btn btn-info">Simpan & Daftar Baru</button>
    						<button type="submit" class="mb-1 btn btn-primary">Simpan</button>
    					</div>
    				</div>
    			</form>
    		</div>
      </div>
    </div>
  </div>
</div>
@endsection

@push('b-css')
<link href="{{asset('assets/crm/vendors/custom/cropper/cropper.min.css')}}" rel="stylesheet" type="text/css" />
<style>
.dz-details{
  display:none !important;
}
</style>
@endpush

@push('b-script')
<script src="{{asset('assets/crm/vendors/custom/cropper/cropper.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/crm/vendors/custom/parsley/parsley.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/crm/vendors/custom/jquery.serializeJSON/jquery.serializejson.min.js')}}"></script>
<script>
var DropzoneS = {
  wrapper : '',
  init: function() {
    Dropzone.options.mDropzone = {
      url: '{{url()->current()}}',
      headers :{'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
      dataType: 'text',
      autoProcessQueue: false,
      uploadMultiple: true,
      parallelUploads: 4,
      maxFiles: 4,
      acceptedFiles : "image/*",
      maxFilesize: 3,
      method: "post",
      paramName: "photo_url",
      addRemoveLinks: false,
      // addRemoveLinks: true,
      accept: function(e, o) {
          o();
      },
      init: function() {
        DropzoneS.wrapper = this;
        @isset($schedule)
          @foreach ($schedule->schedule_images as $image)
            var file = {
              name: "{{$image->photo_url}}",
              size: "{{Storage::disk('public')->size($image->photo_url)}}",
              status: "old",
              accepted: true,
              filename : "{{$image->photo_url}}"
            };
            DropzoneS.wrapper.emit("addedfile", file);
            DropzoneS.wrapper.emit("thumbnail", file, "{{asset(Storage::disk('public')->url($image->photo_url))}}");
            DropzoneS.wrapper.emit("complete", file);
            DropzoneS.wrapper.files.push(file);
            $checkbox =
            '<div style="margin-top:10px;">'+
              '<div class="btn-group btn-group-sm btn-group-toggle" role="group">'+
                '<label class="btn btn-success checkbox-radio {{!$image->primary ? :"active"}}">'+
                  '<input type="radio" name="photo_primary" value="'+file.name+'" autocomplete="off" {{!$image->primary ? :"checked"}}><i class="la la-star"></i>'+
                '</label>'+
                '<button type="button" data-file-name="'+file.name+'" class="btn btn-danger btn-image-remove"><i class="la la-trash-o"></i></button>'+
              '</div>'+
            '</div>';
            $element = Dropzone.createElement($checkbox);
            file.previewElement.appendChild($element);
          @endforeach
        @endisset

        this.on("addedfile", function(file) {
          // console.log(file);
          var count = this.files.length;
          if (count > (this.options.maxFiles)) {
              this.removeFile(file);
              return false;
          }
          if (this.files.length) {
              var _i, _len;
              for (_i = 0, _len = this.files.length; _i < _len - 1; _i++) {
                  if(this.files[_i].name === file.name && this.files[_i].size === file.size) {
                    swal("Failed", "Please input different file name", "error");
                    this.removeFile(file);
                  }
              }
          }
          $ischecked=true;
          // console.log($('#m-dropzone label.checkbox-radio'));
          if ($('#m-dropzone label.checkbox-radio').hasClass('active')) {
            $ischecked=false;
          }
          $test1 =
          '<div style="margin-top:10px;">'+
            '<div class="btn-group btn-group-sm btn-group-toggle" role="group">'+
              '<label class="btn btn-success checkbox-radio '+($ischecked?'active':'')+'">'+
                '<input type="radio" name="photo_primary" value="'+file.name+'" autocomplete="off" '+($ischecked?'checked':'')+'><i class="la la-star"></i>'+
              '</label>'+
              '<button type="button" data-file-name="'+file.name+'" class="btn btn-danger btn-image-remove"><i class="la la-trash-o"></i></button>'+
            '</div>'+
          '</div>';
          $element = Dropzone.createElement($test1);
          file.previewElement.appendChild($element);
        }),
        this.on("sendingmultiple", function(file, xhr, formData) {
          $("html, body").animate({ scrollTop: 0 }, "slow");
          var form_data = JSON.stringify($("#v-form").serializeJSON());
          formData.append('form',form_data);
        }),
        this.on("thumbnail", function(file, dataUrl) {

        }),
        this.on("removedfile",function(file){
          // if ($.inArray('dz-success', file.previewElement.classList) != -1)
          // {
          //   $('<input>').attr({
          //       type: 'hidden',
          //       id: 'removed',
          //       name: 'removed[]',
          //       value: file.filename,
          //   }).appendTo('form');
          // }
        }),
        this.on("successmultiple", function(files, response) {
          window.location.href = response;
          // console.log(response);
          // $message =
          // '<div class="alert alert-success alert-dismissible fade show" role="alert">'+
          //   '<button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>'+
          //   response+
          // '</div>';
          // $('#m-form-content').prepend($message);
        })
        this.on("error", function(response, xhr) {
          // console.log(xhr);
          // var errorsHtml= '';
          // $.each($.parseJSON(response.xhr.responseText), function( key, value ) {
          //   errorsHtml += value[0] + '<br/>';
          // });
          // return false;
        });
      }
    }
  }
};
DropzoneS.init();
$(function() {
    thisform.init();
}), thisform = {
  init: function()
  {
    thisform.c_init();
    thisform.r_init();
    thisform.v_init();
  },
  v_init : function (){
    var i = $("#v-form");
    i.parsley().on("form:validated", function() {

    }).on("field:validated", function(i) {

    }).on("form:submit",function(){
      // console.log(DropzoneS.wrapper.files);
      $isZone = false;
      $.each(DropzoneS.wrapper.files, function(index, file) {
        if (file.status != 'old') {
          $isZone = true;
        }
      });
      // console.log($isPost);
      if($isZone)
      {
        DropzoneS.wrapper.processQueue();
        return false;
      };
      // alert('');
    }), window.Parsley.on("field:validate", function() {

    }), window.Parsley.on("field:error", function() {

    });
  },
  r_init : function() {
    $('#m-dropzone').on('click','button.btn-image-remove', function(e){
      if ($(this).prev("label.checkbox-radio").hasClass('active')){
        $(this).parents().find('label.checkbox-radio').not('.active').first().click();
      }
      $removeFileName = $(this).data('file-name');
      $removeFile = '';
      $.each(DropzoneS.wrapper.files, function(index, file) {
        if (file.name == $removeFileName){
          if (file.status == 'old') {
            $('<input>').attr({
              type: 'hidden',
              name: 'removed[]',
              value: file.filename,
            }).appendTo("#v-form");
          }
          $removeFile=file;
        }
      });
      DropzoneS.wrapper.removeFile($removeFile);
    });
    $('#m-dropzone').on('click','label.checkbox-radio', function(e){
      $("label.checkbox-radio").removeClass("active");
      $(this).addClass("active");
    });
  },
  c_init: function() {
    var t;
    t = mUtil.isRTL() ? {
        leftArrow: '<i class="la la-angle-right"></i>',
        rightArrow: '<i class="la la-angle-left"></i>'
    } : {
        leftArrow: '<i class="la la-angle-left"></i>',
        rightArrow: '<i class="la la-angle-right"></i>'
    };
    $(".bootstrap-datepicker").datepicker({
        rtl: mUtil.isRTL(),
        format: 'dd/mm/yyyy',
        todayHighlight: !0,
        templates: t
    });
  },
}
</script>
@endpush
