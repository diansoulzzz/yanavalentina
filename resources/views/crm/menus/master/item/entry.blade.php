@extends('crm.layouts.default')
@section('title', 'Barang')
@section('content')
<div class="m-grid__item m-grid__item--fluid m-wrapper">
  <div class="m-subheader ">
    <div class="d-flex align-items-center">
      <div class="mr-auto">
        <h3 class="m-subheader__title ">
          Barang
        </h3>
      </div>
    </div>
  </div>
  <div class="m-content" id="m-form-content">
    @if (session('status'))
      <div class="alert {{session('status')['alert']}} alert-dismissible fade show" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
          {!! session('status')['message'] !!}
      </div>
    @endif
    <div class="row">
      <div class="col-xl-12">
        <div class="m-portlet m-portlet--tab">
    			<div class="m-portlet__head">
    				<div class="m-portlet__head-caption">
    					<div class="m-portlet__head-title">
    						<span class="m-portlet__head-icon m--hide">
    						<i class="la la-gear"></i>
    						</span>
    						<h3 class="m-portlet__head-text">
    							Informasi Barang
    						</h3>
    					</div>
    				</div>
    			</div>
    			<form id="v-form" class="m-form m-form--fit m-form--label-align-right" method="post" enctype="multipart/form-data" action="{{url()->current()}}">
            @csrf
            @if(isset($item))
            <input type="hidden" name="eid" value="{{$item->eid}}"/>
            @endif
    				<div class="m-portlet__body">
              <div class="form-group m-form__group row">
                <label class="col-3 col-form-label text-left" for="form-input-name">
                  Gambar Barang<br />
                  <span class="m-form__help">Isi Gambar Minimal 1 - Maximal 4</span>
                </label>
                <div class="col-9">
        					<div class="m-dropzone dropzone m-dropzone--primary" id="m-dropzone">
        						<div class="m-dropzone__msg dz-message needsclick">
        						    <h3 class="m-dropzone__msg-title">Jatuhkan gambar di sini atau klik untuk mengunggah.</h3>
        						    <span class="m-dropzone__msg-desc">Maximal 4 gambar</span>
        						</div>
        					</div>
                </div>
              </div>
    					<div class="form-group m-form__group row">
    						<label class="col-3 col-form-label text-left" for="form-input-name">
                  Nama Barang<br />
                  <span class="m-form__help">Tulis nama barang yang sesuai.</span>
                </label>
    						<div class="col-9">
    							<input class="form-control m-input" id="form-input-name" name="name" type="text" value="{{old('name', isset($item->name) ? $item->name : '')}}" placeholder="Masukan Nama Barang" data-parsley-required="true" data-parsley-required-message="Nama Wajib diisi">
    						</div>
    					</div>
    					<div class="form-group m-form__group row">
    						<label class="col-3 col-form-label text-left" for="form-input-description">
                  Deskripsi Barang<br />
                  <span class="m-form__help">Deskipsi barang.</span>
                </label>
    						<div class="col-9">
    							<textarea id="form-input-description" name="description" class="form-control m-input" placeholder="Masukan Deskripsi Barang" data-parsley-required="true" data-parsley-required-message="Deskripsi wajib diisi">{{old('description', isset($item->description) ? $item->description : '')}}</textarea>
    						</div>
    					</div>
    					<div class="form-group m-form__group row">
    						<label class="col-3 col-form-label text-left" for="form-input-schedule_id">
                  Jadwal<br />
                  <span class="m-form__help">Jadwal.</span>
                </label>
    						<div class="col-9">
                  <select class="form-control m-select2"  id="schedule_select" name="schedule_id" data-parsley-required="true" data-parsley-errors-container="#schedule_id-errors" data-parsley-required-message="Jadwal wajib di isi">
                    <option value="">Jadwal Barang</option>
                  </select>
                  <div id="schedule_id-errors"></div>
    						</div>
    					</div>
    					<div class="form-group m-form__group row">
    						<label class="col-3 col-form-label text-left" for="form-input-item_category_id">
                  Kategori<br />
                  <span class="m-form__help">Kategori.</span>
                </label>
    						<div class="col-9">
                  <select class="form-control m-select2"  id="category_select" name="item_category_id" data-parsley-required="true" data-parsley-errors-container="#item_category_id-errors" data-parsley-required-message="Kategori wajib di isi">
                    <option value="">Kategori Barang</option>
                  </select>
                  <div id="item_category_id-errors"></div>
    						</div>
    					</div>
    					<div class="form-group m-form__group row">
    						<label class="col-3 col-form-label text-left" for="form-input-stock">
                  Stok Barang<br />
                  <span class="m-form__help">Stok barang.</span>
                </label>
    						<div class="col-9">
    							<input class="form-control m-input" id="form-input-stock" name="stock" type="text" value="{{old('stock', isset($item->stock) ? $item->stock : '0')}}" placeholder="Masukan Stok Barang" data-parsley-required="true" data-parsley-type="number" data-parsley-type-message="Inputan wajib angka" data-parsley-required-message="Stok Wajib diisi">
    						</div>
    					</div>
    					<div class="form-group m-form__group row">
    						<label class="col-3 col-form-label text-left" for="form-input-price">
                  Harga Jual<br />
                  <span class="m-form__help">Harga jual barang.</span>
                </label>
    						<div class="col-9">
    							<input class="form-control m-input" id="form-input-price" name="price" type="text" value="{{old('price', isset($item->price) ? $item->price : '0')}}" placeholder="Masukan Jual Barang" data-parsley-required="true" data-parsley-type="number" data-parsley-type-message="Inputan wajib angka" data-parsley-required-message="Harga jual Wajib diisi">
    						</div>
    					</div>
    					<div class="form-group m-form__group row">
    						<label class="col-3 col-form-label text-left" for="form-input-price_buy">
                  Harga Beli<br />
                  <span class="m-form__help">Harga beli barang.</span>
                </label>
    						<div class="col-9">
    							<input class="form-control m-input" id="form-input-price_buy" name="price_buy" type="text" value="{{old('price_buy', isset($item->price_buy) ? $item->price_buy : '0')}}" placeholder="Masukan Beli Barang" data-parsley-required="true" data-parsley-type="number" data-parsley-type-message="Inputan wajib angka" data-parsley-required-message="Harga beli Wajib diisi">
    						</div>
    					</div>
    					<div class="form-group m-form__group row">
    						<label class="col-3 col-form-label text-left" for="form-input-weight">
                  Berat Barang (gram)<br />
                  <span class="m-form__help">Maximum 50 (Kg) atau 50000 (gram) </span>
                </label>
    						<div class="col-9">
    							<input class="form-control m-input" id="form-input-weight" name="weight" type="text" value="{{old('weight', isset($item->weight) ? $item->weight : '0')}}" placeholder="Masukan Harga Berat" data-parsley-required="true" data-parsley-type="number" data-parsley-type-message="Inputan wajib angka" data-parsley-required-message="Berat Wajib diisi">
    						</div>
    					</div>
    					<div class="form-group m-form__group row">
    						<label class="col-3 col-form-label text-left" for="form-input-long">
                  Panjang Barang<br />
                  <span class="m-form__help">Panjang barang.</span>
                </label>
    						<div class="col-9">
    							<input class="form-control m-input" id="form-input-long" name="long" type="text" value="{{old('long', isset($item->long) ? $item->long : '0')}}" placeholder="Masukan Panjang Berat" data-parsley-required="true" data-parsley-type="number" data-parsley-type-message="Inputan wajib angka" data-parsley-required-message="Panjang Wajib diisi">
    						</div>
    					</div>
    					<div class="form-group m-form__group row">
    						<label class="col-3 col-form-label text-left" for="form-input-width">
                  Lebar Barang<br />
                  <span class="m-form__help">Lebar barang.</span>
                </label>
    						<div class="col-9">
    							<input class="form-control m-input" id="form-input-width" name="width" type="text" value="{{old('width', isset($item->width) ? $item->width : '0')}}" placeholder="Masukan Harga Berat" data-parsley-required="true" data-parsley-type="number" data-parsley-type-message="Inputan wajib angka" data-parsley-required-message="Lebar Wajib diisi">
    						</div>
    					</div>
    					<div class="form-group m-form__group row">
    						<label class="col-3 col-form-label text-left" for="form-input-height">
                  Tinggi Barang<br />
                  <span class="m-form__help">Tinggi barang.</span>
                </label>
    						<div class="col-9">
    							<input class="form-control m-input" id="form-input-height" name="height" type="text" value="{{old('height', isset($item->height) ? $item->height : '0')}}" placeholder="Masukan Tinggi Berat" data-parsley-required="true" data-parsley-type="number" data-parsley-type-message="Inputan wajib angka" data-parsley-required-message="Tinggi Wajib diisi" >
    						</div>
    					</div>
    					<div class="form-group m-form__group row">
    						<label class="col-3 col-form-label text-left" for="form-input-publish">
                  Publish<br />
                  <span class="m-form__help">Publish.</span>
                </label>
    						<div class="col-9">
                  <label class="m-checkbox m-checkbox--state-success">
  									<input class="form-control m-input" id="form-input-publish" name="publish" type="checkbox" {{ old('publish',( isset($item->publish) ? ($item->publish ? 'checked' : '') : '') ) ? 'checked' : '' }} data-parsley-required="false"> Tampil dihalaman?
  									<span></span>
									</label>
    						</div>
    					</div>
            </div>
    				<div class="m-portlet__foot m-portlet__foot--fit">
    					<div class="m-form__actions text-right">
    						<a href="{{url('master/item/list')}}" class="mb-1 btn btn-secondary">Cancel</a>
    						<button type="submit" class="mb-1 btn btn-info">Simpan & Daftar Baru</button>
    						<button type="submit" class="mb-1 btn btn-primary">Simpan</button>
    					</div>
    				</div>
    			</form>
    		</div>
      </div>
    </div>
  </div>
</div>
@endsection

@push('b-css')
<link href="{{asset('assets/crm/vendors/custom/cropper/cropper.min.css')}}" rel="stylesheet" type="text/css" />
<style>
.dz-details{
  display:none !important;
}
</style>
@endpush

@push('b-script')
<script src="{{asset('assets/crm/vendors/custom/cropper/cropper.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/crm/vendors/custom/parsley/parsley.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/crm/vendors/custom/jquery.serializeJSON/jquery.serializejson.min.js')}}"></script>
<script>
var DropzoneS = {
  wrapper : '',
  init: function() {
    Dropzone.options.mDropzone = {
      url: '{{url()->current()}}',
      headers :{'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
      dataType: 'text',
      autoProcessQueue: false,
      uploadMultiple: true,
      parallelUploads: 4,
      maxFiles: 4,
      acceptedFiles : "image/*",
      maxFilesize: 3,
      method: "post",
      paramName: "photo_url",
      addRemoveLinks: false,
      // addRemoveLinks: true,
      accept: function(e, o) {
          o();
      },
      init: function() {
        DropzoneS.wrapper = this;
        @isset($item)
          @foreach ($item->item_images as $image)
            var file = {
              name: "{{$image->photo_url}}",
              size: "{{Storage::disk('public')->size($image->photo_url)}}",
              status: "old",
              accepted: true,
              filename : "{{$image->photo_url}}"
            };
            DropzoneS.wrapper.emit("addedfile", file);
            DropzoneS.wrapper.emit("thumbnail", file, "{{asset(Storage::disk('public')->url($image->photo_url))}}");
            DropzoneS.wrapper.emit("complete", file);
            DropzoneS.wrapper.files.push(file);
            $checkbox =
            '<div style="margin-top:10px;">'+
              '<div class="btn-group btn-group-sm btn-group-toggle" role="group">'+
                '<label class="btn btn-success checkbox-radio {{!$image->primary ? :"active"}}">'+
                  '<input type="radio" name="photo_primary" value="'+file.name+'" autocomplete="off" {{!$image->primary ? :"checked"}}><i class="la la-star"></i>'+
                '</label>'+
                '<button type="button" data-file-name="'+file.name+'" class="btn btn-danger btn-image-remove"><i class="la la-trash-o"></i></button>'+
              '</div>'+
            '</div>';
            $element = Dropzone.createElement($checkbox);
            file.previewElement.appendChild($element);
          @endforeach
        @endisset

        this.on("addedfile", function(file) {
          // console.log(file);
          var count = this.files.length;
          if (count > (this.options.maxFiles)) {
              this.removeFile(file);
              return false;
          }
          if (this.files.length) {
              var _i, _len;
              for (_i = 0, _len = this.files.length; _i < _len - 1; _i++) {
                  if(this.files[_i].name === file.name && this.files[_i].size === file.size) {
                    swal("Failed", "Please input different file name", "error");
                    this.removeFile(file);
                  }
              }
          }
          $ischecked=true;
          // console.log($('#m-dropzone label.checkbox-radio'));
          if ($('#m-dropzone label.checkbox-radio').hasClass('active')) {
            $ischecked=false;
          }
          $test1 =
          '<div style="margin-top:10px;">'+
            '<div class="btn-group btn-group-sm btn-group-toggle" role="group">'+
              '<label class="btn btn-success checkbox-radio '+($ischecked?'active':'')+'">'+
                '<input type="radio" name="photo_primary" value="'+file.name+'" autocomplete="off" '+($ischecked?'checked':'')+'><i class="la la-star"></i>'+
              '</label>'+
              '<button type="button" data-file-name="'+file.name+'" class="btn btn-danger btn-image-remove"><i class="la la-trash-o"></i></button>'+
            '</div>'+
          '</div>';
          $element = Dropzone.createElement($test1);
          file.previewElement.appendChild($element);
        }),
        this.on("sendingmultiple", function(file, xhr, formData) {
          $("html, body").animate({ scrollTop: 0 }, "slow");
          var form_data = JSON.stringify($("#v-form").serializeJSON());
          formData.append('form',form_data);
        }),
        this.on("thumbnail", function(file, dataUrl) {

        }),
        this.on("removedfile",function(file){
          // if ($.inArray('dz-success', file.previewElement.classList) != -1)
          // {
          //   $('<input>').attr({
          //       type: 'hidden',
          //       id: 'removed',
          //       name: 'removed[]',
          //       value: file.filename,
          //   }).appendTo('form');
          // }
        }),
        this.on("successmultiple", function(files, response) {
          // console.log(response);
          // return;
          window.location.href = response;
          // console.log(response);
          // $message =
          // '<div class="alert alert-success alert-dismissible fade show" role="alert">'+
          //   '<button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>'+
          //   response+
          // '</div>';
          // $('#m-form-content').prepend($message);
        })
        this.on("error", function(response, xhr) {
          // console.log(xhr);
          // var errorsHtml= '';
          // $.each($.parseJSON(response.xhr.responseText), function( key, value ) {
          //   errorsHtml += value[0] + '<br/>';
          // });
          // return false;
        });
      }
    }
  }
};
DropzoneS.init();
$(function() {
    thisform.init();
}), thisform = {
  init: function()
  {
    thisform.c_init();
    thisform.r_init();
    thisform.v_init();
    thisform.s_init();
  },
  v_init : function (){
    var i = $("#v-form");
    i.parsley().on("form:validated", function() {

    }).on("field:validated", function(i) {

    }).on("form:submit",function(){
      // console.log(DropzoneS.wrapper.files);
      $isZone = false;
      $.each(DropzoneS.wrapper.files, function(index, file) {
        if (file.status != 'old') {
          $isZone = true;
        }
      });
      // console.log($isPost);
      if($isZone)
      {
        DropzoneS.wrapper.processQueue();
        return false;
      };
      // alert('');
    }), window.Parsley.on("field:validate", function() {

    }), window.Parsley.on("field:error", function() {

    });
  },
  r_init : function() {
    $('#m-dropzone').on('click','button.btn-image-remove', function(e){
      if ($(this).prev("label.checkbox-radio").hasClass('active')){
        $(this).parents().find('label.checkbox-radio').not('.active').first().click();
      }
      $removeFileName = $(this).data('file-name');
      $removeFile = '';
      $.each(DropzoneS.wrapper.files, function(index, file) {
        if (file.name == $removeFileName){
          if (file.status == 'old') {
            $('<input>').attr({
              type: 'hidden',
              name: 'removed[]',
              value: file.filename,
            }).appendTo("#v-form");
          }
          $removeFile=file;
        }
      });
      DropzoneS.wrapper.removeFile($removeFile);
    });
    $('#m-dropzone').on('click','label.checkbox-radio', function(e){
      $("label.checkbox-radio").removeClass("active");
      $(this).addClass("active");
    });
  },
  c_init: function() {
    var t;
    t = mUtil.isRTL() ? {
        leftArrow: '<i class="la la-angle-right"></i>',
        rightArrow: '<i class="la la-angle-left"></i>'
    } : {
        leftArrow: '<i class="la la-angle-left"></i>',
        rightArrow: '<i class="la la-angle-right"></i>'
    };
    $(".bootstrap-datepicker").datepicker({
        rtl: mUtil.isRTL(),
        format: 'dd/mm/yyyy',
        todayHighlight: !0,
        templates: t
    });
  },
  s_init : function () {
    var s1 = $("#schedule_select");
    s1.select2({
        placeholder: "Cari Jadwal",
        allowClear: true,
        ajax: {
            url: "{{url('master/item/schedule')}}",
            delay: 250,
            data: function(e) {
                return {
                    q: e.term,
                    page: e.page
                }
            },
            processResults: function(e, t) {
                return t.page = t.page || 1, {
                    results: e.items,
                    pagination: {
                        more: 30 * t.page < e.total_count
                    }
                }
            },
            cache: !0
        },
        escapeMarkup: function(e) {
            return e
        },
        minimumInputLength: 3,
        templateResult: function(e) {
          if (e.loading) return e.text;
          var t = "<div class='select2-result-repository clearfix'><div class='select2-result-repository__meta'><div class='select2-result-repository__title'>" + e.name+ "</div>";
          return e.name && (t += "<div class='select2-result-repository__description'>" + e.description + "</div>"), t += " </div></div>"
        },
        templateSelection: function(e) {
          if ((e.id=="") || (e.name==null)) return e.text;
          return e.name +', '+ e.description
        },
    }), s1.on('change', function(e){
      // console.log(e);
      s1.trigger('input');
    });
    @isset($item)
      var newOption = new Option('{{$item->schedule->name}}' + '{{$item->schedule->description}}', '{{$item->schedule->id}}', true, true);
      s1.append(newOption).trigger('change');
      s1.trigger({
          type: 'select2:select',
          params: {
              data: '{{$item->schedule->id}}'
          }
      });
    @endif
    var s2 = $("#category_select");
    s2.select2({
        placeholder: "Cari Kategori",
        allowClear: true,
        ajax: {
            url: "{{url('master/item/category')}}",
            delay: 250,
            data: function(e) {
                return {
                    q: e.term,
                    page: e.page
                }
            },
            processResults: function(e, t) {
                return t.page = t.page || 1, {
                    results: e.items,
                    pagination: {
                        more: 30 * t.page < e.total_count
                    }
                }
            },
            cache: !0
        },
        escapeMarkup: function(e) {
            return e
        },
        minimumInputLength: 3,
        templateResult: function(e) {
          if (e.loading) return e.text;
          var t = "<div class='select2-result-repository clearfix'><div class='select2-result-repository__meta'><div class='select2-result-repository__title'>" + e.name+ "</div>";
          return e.name && (t += ""), t += " </div></div>"
        },
        templateSelection: function(e) {
          if ((e.id=="") || (e.name==null)) return e.text;
          return e.name
        },
    }), s2.on('change', function(){
      s2.trigger('input');
    });
    @isset($item)
      var newOption = new Option('{{$item->item_category->name}}' , '{{$item->item_category->id}}', true, true);
      s2.append(newOption).trigger('change');
      s2.trigger({
          type: 'select2:select',
          params: {
              data: '{{$item->item_category->id}}'
          }
      });
    @endif
  },
}
</script>
@endpush
