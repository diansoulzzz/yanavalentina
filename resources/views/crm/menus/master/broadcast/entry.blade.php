@extends('crm.layouts.default')
@section('title', 'Broadcast')
@section('content')
<div class="m-grid__item m-grid__item--fluid m-wrapper">
  <div class="m-subheader ">
    <div class="d-flex align-items-center">
      <div class="mr-auto">
        <h3 class="m-subheader__title ">
          Broadcast
        </h3>
      </div>
    </div>
  </div>
  <div class="m-content" id="m-form-content">
    @if (session('status'))
      <div class="alert {{session('status')['alert']}} alert-dismissible fade show" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
          {!! session('status')['message'] !!}
      </div>
    @endif
    <div class="row">
      <div class="col-xl-12">
        <div class="m-portlet m-portlet--tab">
    			<div class="m-portlet__head">
    				<div class="m-portlet__head-caption">
    					<div class="m-portlet__head-title">
    						<span class="m-portlet__head-icon m--hide">
    						<i class="la la-gear"></i>
    						</span>
    						<h3 class="m-portlet__head-text">
    							Informasi Broadcast
    						</h3>
    					</div>
    				</div>
    			</div>
    			<form id="v-form" class="m-form m-form--fit m-form--label-align-right" method="post" enctype="multipart/form-data" action="{{url()->current()}}">
            @csrf
            @if(isset($model))
            <input type="hidden" name="eid" value="{{$model->eid}}"/>
            @endif
    				<div class="m-portlet__body">
              <div class="form-group m-form__group row">
                <label class="col-3 col-form-label text-left" for="form-input-subject">
                  Judul<br />
                  <span class="m-form__help">Tulis judul broadcast.</span>
                </label>
                <div class="col-9">
                  <input class="form-control m-input" id="form-input-subject" name="subject" type="text" value="{{old('subject', isset($model->subject) ? $model->subject : '')}}" placeholder="Masukan Judul Broadcast" data-parsley-required="true" data-parsley-required-message="Judul Wajib diisi">
                </div>
              </div>
              <div class="form-group m-form__group row">
                <label class="col-3 col-form-label text-left" for="users_select">
                  User<br />
                  <span class="m-form__help">Masukan user yang akan di email.</span>
                </label>
                <div class="col-9">
                  <select class="form-control m-select2" multiple="multiple" id="users_select" name="users_select[]" data-parsley-required="true" data-parsley-errors-container="#schedule_id-errors" data-parsley-required-message="User wajib di isi">
                    <option value="">Cari User</option>
                  </select>
                  <div id="users_select-errors"></div>
                </div>
              </div>
        			<div class="form-group m-form__group row">
    						<label class="col-3 col-form-label text-left" for="form-broadcast">
                  Broadcast<br />
                  <span class="m-form__help">Masukan Messages</span>
                </label>
        				<div class="col-lg-9">
                  <textarea name="message" class="summernote"id="form-broadcast">{!! old('message', isset($model->message) ? $model->message : '') !!}</textarea>
        					<!-- <div class="summernote"id="form-broadcast"></div> -->
        				</div>
        			</div>
    					<div class="form-group m-form__group row">
    						<label class="col-3 col-form-label text-left" for="form-input-publish">
                  Publish<br />
                  <span class="m-form__help">Publish.</span>
                </label>
    						<div class="col-9">
                  <label class="m-checkbox m-checkbox--state-success">
  									<input class="form-control m-input" id="form-input-publish" name="publish" type="checkbox" {{ old('publish',( isset($model->publish) ? ($model->publish ? 'checked' : '') : '') ) ? 'checked' : '' }} data-parsley-required="false"> Langsung Emai?
  									<span></span>
									</label>
    						</div>
    					</div>
            </div>
    				<div class="m-portlet__foot m-portlet__foot--fit">
    					<div class="m-form__actions text-right">
    						<a href="{{url('master/item/list')}}" class="mb-1 btn btn-secondary">Cancel</a>
    						<button type="submit" class="mb-1 btn btn-info">Simpan & Daftar Baru</button>
    						<button type="submit" class="mb-1 btn btn-primary">Simpan</button>
    					</div>
    				</div>
    			</form>
    		</div>
      </div>
    </div>
  </div>
</div>
@endsection

@push('b-css')
<link href="{{asset('assets/crm/vendors/custom/cropper/cropper.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('assets/crm/vendors/custom/dropify/css/dropify.min.css')}}" rel="stylesheet" type="text/css" />
<style>
.dz-details{
  display:none !important;
}
</style>
@endpush

@push('b-script')
<script src="{{asset('assets/crm/vendors/custom/cropper/cropper.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/crm/vendors/custom/dropify/js/dropify.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/crm/vendors/custom/parsley/parsley.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/crm/vendors/custom/jquery.serializeJSON/jquery.serializejson.min.js')}}"></script>
<script>
$(function() {
    thisform.init();
}), thisform = {
  init: function()
  {
    thisform.c_init();
    thisform.n_init();
    thisform.s_init();
    thisform.v_init();
  },
  v_init : function (){
    var i = $("#v-form");
    i.parsley().on("form:validated", function() {

    }).on("field:validated", function(i) {

    }).on("form:submit",function(){

    }), window.Parsley.on("field:validate", function() {

    }), window.Parsley.on("field:error", function() {

    });
  },
  n_init : function() {
    $(".summernote").summernote({
        tabsize: 2,
        height: 150,
        callbacks: {
          onImageUpload: function(image) {
              uploadImage(image[0]);
          },
          // onMediaDelete : function ($target, $editable) {
          //   console.log($target.attr('src'));   // get image url
          // }
        }
    });
    function uploadImage(image) {
      var data = new FormData();
      data.append("image", image);
      $.ajax({
          url: "{{url('broadcast/upload')}}",
          cache: false,
          contentType: false,
          processData: false,
          data: data,
          type: "POST",
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
          success: function(url) {
            $url = url;
            var image = $('<img>').attr('src', $url);
            $(".summernote").summernote("insertNode", image[0]);
          },
          error: function(data) {
              console.log(data);
          }
      });
    }
  },
  s_init : function () {
    var s1 = $("#users_select");
    s1.select2({
        placeholder: "Cari User",
        // allowClear: true,
        // tags: true,
        ajax: {
            url: "{{url('broadcast/users')}}",
            delay: 250,
            data: function(e) {
                return {
                    q: e.term,
                    page: e.page
                }
            },
            processResults: function(e, t) {
                return t.page = t.page || 1, {
                    results: e.items,
                    pagination: {
                        more: 30 * t.page < e.total_count
                    }
                }
            },
            cache: !0
        },
        escapeMarkup: function(e) {
            return e
        },
        // minimumInputLength: 3,
        templateResult: function(e) {
          if (e.loading) return e.text;
          var t = "<div class='select2-result-repository clearfix'><div class='select2-result-repository__meta'><div class='select2-result-repository__title'>" + e.name+ "</div>";
          return e.name && (t += "<div class='select2-result-repository__description'>" + e.email + "</div>"), t += " </div></div>"
        },
        templateSelection: function(e) {
          if ((e.id=="") || (e.name==null)) return e.text;
          return e.email;
        },
    }), s1.on('change', function(e){
      // console.log(e);
      s1.trigger('input');
    });
    
  },
  c_init: function() {
    var t;
    t = mUtil.isRTL() ? {
        leftArrow: '<i class="la la-angle-right"></i>',
        rightArrow: '<i class="la la-angle-left"></i>'
    } : {
        leftArrow: '<i class="la la-angle-left"></i>',
        rightArrow: '<i class="la la-angle-right"></i>'
    };
    $(".bootstrap-datepicker").datepicker({
        rtl: mUtil.isRTL(),
        format: 'dd/mm/yyyy',
        todayHighlight: !0,
        templates: t
    });
  },
}
</script>
@endpush
