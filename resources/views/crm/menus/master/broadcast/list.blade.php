@extends('crm.layouts.default')
@section('title', 'Data Barang Kategori')
@section('content')
<div class="m-grid__item m-grid__item--fluid m-wrapper">
  <div class="m-content">
    <div class="m-portlet m-portlet--mobile">
      <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
          <div class="m-portlet__head-title">
            <h3 class="m-portlet__head-text">
              Lihat Data Barang Kategori
            </h3>
          </div>
        </div>
        <div class="m-portlet__head-tools">
          <ul class="m-portlet__nav">
            <li class="m-portlet__nav-item"></li>
            <li class="m-portlet__nav-item m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push" m-dropdown-toggle="hover" aria-expanded="true">
              <a href="#" class="m-portlet__nav-link m-dropdown__toggle dropdown-toggle btn btn--sm m-btn--pill btn-secondary m-btn m-btn--label-brand">
                Export
              </a>
              <div class="m-dropdown__wrapper" style="z-index: 101;">
                <span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust" style="left: auto; right: 36px;"></span>
                <div class="m-dropdown__inner">
                  <div class="m-dropdown__body">
                    <div class="m-dropdown__content">
                      <ul class="m-nav">
                        <li class="m-nav__section m-nav__section--first">
                          <span class="m-nav__section-text">Export Tools</span>
                        </li>
                        <li class="m-nav__item">
                          <a href="#" class="m-nav__link" id="export_print">
                            <i class="m-nav__link-icon la la-print"></i>
                            <span class="m-nav__link-text">Print</span>
                          </a>
                        </li>
                        <li class="m-nav__item">
                          <a href="#" class="m-nav__link" id="export_copy">
                            <i class="m-nav__link-icon la la-copy"></i>
                            <span class="m-nav__link-text">Copy</span>
                          </a>
                        </li>
                        <li class="m-nav__item">
                          <a href="#" class="m-nav__link" id="export_excel">
                            <i class="m-nav__link-icon la la-file-excel-o"></i>
                            <span class="m-nav__link-text">Excel</span>
                          </a>
                        </li>
                        <li class="m-nav__item">
                          <a href="#" class="m-nav__link" id="export_csv">
                            <i class="m-nav__link-icon la la-file-text-o"></i>
                            <span class="m-nav__link-text">CSV</span>
                          </a>
                        </li>
                        <li class="m-nav__item">
                          <a href="#" class="m-nav__link" id="export_pdf">
                            <i class="m-nav__link-icon la la-file-pdf-o"></i>
                            <span class="m-nav__link-text">PDF</span>
                          </a>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            </li>
          </ul>
        </div>
      </div>
      <div class="m-portlet__body">
        @if (session('error'))
          <div class="form-group m-form__group m--margin-top-10">
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
              {{ session('error') }}
            </div>
          </div>
        @endif
        @if (session('success'))
          <div class="form-group m-form__group m--margin-top-10">
            <div class="alert alert-success alert-dismissible fade show" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
              {{ session('success') }}
            </div>
          </div>
        @endif
        <table class="table table-striped- table-bordered table-hover table-checkable" id="data_table">
          <thead>
            <tr>
              <th>No</th>
              <th>Nama</th>
              <th>Publish</th>
              <th>Aksi</th>
            </tr>
          </thead>
          <tfoot>
            <tr>
              <th>#</th>
              <th>Nama</th>
              <th>Publish</th>
              <th>Aksi</th>
            </tr>
          </tfoot>
        </table>
      </div>
    </div>
  </div>
</div>
@endsection
@push('a-css')
<link href="{{asset('assets/crm/vendors/custom/datatables/datatables.bundle.css')}}" rel="stylesheet" type="text/css" />
@endpush
@push('b-script')
<script src="{{asset('assets/crm/vendors/custom/datatables/datatables.bundle.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/crm/demo/default/custom/crud/datatables/extensions/buttons.js')}}" type="text/javascript"></script>
<script>
$(function() {
    thisform.init();
}), thisform = {
  init: function()
  {
    thisform.d_init();
  },
  e_init: function()
  {
    $(".schedule-delete").on("click", function(e) {
      e.preventDefault();
      $link=$(this).attr('href');
      swal({
        title: "Yakin Hapus?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes",
        cancelButtonText: "No",
        }).then((result) => {
          if (result.value) {
            location.href = $link;
          }
        });
    });
  },
  d_init: function() {
    var t;
    t = $("#data_table").DataTable({
        "lengthMenu": [ [10, 25, 50, 100, -1], [10, 25, 50, 100,"All"]],
        responsive: !0,
        buttons: ["print", "copyHtml5", "excelHtml5", "csvHtml5", "pdfHtml5"],
        processing: !0,
        serverSide: !0,
        ajax: {
            url: "{{url('master/item-category/datatable')}}",
            type: "POST",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
        },
        "drawCallback": function( settings ) {
          $('#data_table tbody').on('click', 'td.sorting_1', function () {
            thisform.e_init();
          });
          thisform.e_init();
        },
        columns: [{
            data: "id", name:"item_category.id"
        },{
            data: "name", name:"item_category.name"
        },{
            data: "publish", name:"item_category.publish"
        },{
            data: "action", name:"action"
        }],
    }), $("#export_print").on("click", function(e) {
        e.preventDefault(), t.button(0).trigger()
    }), $("#export_copy").on("click", function(e) {
        e.preventDefault(), t.button(1).trigger()
    }), $("#export_excel").on("click", function(e) {
        e.preventDefault(), t.button(2).trigger()
    }), $("#export_csv").on("click", function(e) {
        e.preventDefault(), t.button(3).trigger()
    }), $("#export_pdf").on("click", function(e) {
        e.preventDefault(), t.button(4).trigger()
    });
  },
};
</script>
@endpush
