<html>
  <head>
    <style>
    .page-break-after {
        page-break-after: always;
    }
    .page-break-inside {
        page-break-inside: always;
    }
    .descriptors-align {
        width: 50%;
        display: inline-block;
    }

    .row:after {
      content: "";
      display: table;
      clear: both;
    }
    .column {
      float: left;
    }
    .column-2 {
      width: 50%;
    }
    .column-4 {
      width: 25%;
    }
    </style>
  </head>
  <body>
    <div class="row">
      @foreach ($users as $user)
        <div class="column column-4" style="background-color:#aaa;">
          <h2>Column 1</h2>
          <p>Some text..</p>
        </div>
        <div class="column column-4" style="background-color:#bbb;">
          <h2>Column 2</h2>
          <p>Some text..</p>
        </div>
        <div class="page-break-inside"></div>
        <h1>{{$user->code}}</h1>
        <h1>{{$user->name}}</h1>
      @endforeach
    </div>
  </body>
</html>
