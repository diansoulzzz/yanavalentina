<html>
  <head>
    <style>
    .page-break-after {
        page-break-after: always;
    }
    .page-break-inside {
        page-break-inside: always;
    }
    .descriptors-align {
        width: 50%;
        display: inline-block;
    }
    .row:after {
      content: "";
      display: table;
      clear: both;
      overflow: auto;
    }
    .column {
      float: left;
      margin-left: 1%;
      margin-right: 1%;
    }
    .column-wrapper {
      width: 48%;
    }
    .column-2 {
      width: 48%;
      display:inline-block;
    }
    /* .column-3 {
      width: 30%;
      display:inline-block;
    } */
    .column-4 {
      width: 24%;
      display:inline-block;
    }
    .container {
      width: 100%;
      position: relative;
    }
    .bg-pink {
      background-color: rgb(236, 85, 244);
    }
    .bg-blue {
      background-color : rgb(104, 197, 233);
    }
    .bg-red {
      background-color : rgb(214, 78, 58);
    }
    .bg-yellow {
      background-color : rgb(202, 214, 58);
    }
    .bg-green {
      background-color : rgb(61, 214, 58);
    }
    .bg-black {
      background-color : rgb(68, 69, 59);
    }
    </style>
  </head>
  <body>
    <div class="container">
      @foreach ($users as $user)
      <div class="row">
        <div class="column column-wrapper bg-pink">
          <div class="column-2">
            Logo
          </div>
          <div class="row">
            <div class="column-2">
              Code :
            </div>
            <div class="column-2">
              {{$user->code}}
            </div>
          </div>
          <div class="row">
            <div class="column-2">
              Nama :
            </div>
            <div class="column-2">
              {{$user->name}}
            </div>
          </div>
          <div class="row">
            <div class="column-4">
              Alamat :
            </div>
            <div class="column-4">
              @if (!blank($user->users_addresses_primary))
                {{$user->users_addresses_primary->address}}
              @endif
              aaaaaaaaaaaaaaaaaa
              aasdasd
              adsasd
              aasdasd
            </div>
          </div>
        </div>
      </div>
      @endforeach
    </div>
  </body>
</html>
