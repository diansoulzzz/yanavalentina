<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  <style>
  .text-centered {
    text-align: center;
  }
  .text-bold {
    font-weight: bold;
  }
  .text-lowercase {
      text-transform: lowercase;
  }
  .text-uppercase {
      text-transform: uppercase;
  }
  .text-capitalize {
      text-transform: capitalize;
  }
  .text-font-14 {
      font-size: 14pt;
  }
  .text-font-13 {
      font-size: 13pt;
  }
  .text-font-12 {
      font-size: 12pt;
  }
  .text-font-11 {
      font-size: 11pt;
  }
  .text-font-10 {
      font-size: 10pt;
  }
  .text-font-9 {
      font-size: 9pt;
  }
  .text-font-8 {
      font-size: 8pt;
  }
  </style>
</head>

<h4>Penerima :</h4>
<span class="text-font-8">No Member : {{$user->code}}</span><br />
<span class="text-bold text-font-10">Nama : {{$user->name}}</span><br />
<span class="text-bold text-font-10">No Hp : {{$user->phone_number}}</span> <br /><br />
<span class="text-bold text-font-10">Alamat : @if (!blank($user->users_addresses_primary)) {{$user->users_addresses_primary->address}} @endif</span> <br />
@if(false)
<span class="text-font-12">Kota/Provinsi :
  @if (isset($user->users_addresses_primary->subdistrict))
    @if (!blank($user->users_addresses_primary->subdistrict))
      {{$user->users_addresses_primary->province_name}}, {{$user->users_addresses_primary->city_name}}, {{$user->users_addresses_primary->subdistrict_name}}
    @else
      Belum diisi
    @endif
  @else
    Belum diisi
  @endif
</span>
@endif
<h4>Pengirim :</h4>
<span class="text-font-8">Yana Valentina</span>
<span class="text-font-8">+6283 - 83222 - 2214</span>
