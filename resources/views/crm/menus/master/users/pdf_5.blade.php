<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    @include('crm.menus.master.users.bootstrap')
    <style>
    .col-print-1 {width:8%;  float:left;}
    .col-print-2 {width:16%; float:left;}
    .col-print-3 {width:25%; float:left;}
    .col-print-4 {width:33%; float:left;}
    .col-print-5 {width:42%; float:left;}
    .col-print-6 {width:50%; float:left;}
    .col-print-7 {width:58%; float:left;}
    .col-print-8 {width:66%; float:left;}
    .col-print-9 {width:75%; float:left;}
    .col-print-10{width:83%; float:left;}
    .col-print-11{width:92%; float:left;}
    .col-print-12{width:100%; float:left;}

    .row {
        padding: 0px !important;
        margin: 0px !important;
    }

    .row > div {
        margin: 0px !important;
        padding: 0px !important;
    }

    .row .col {
        margin: 0px !important;
        padding: 0px !important;
    }
    </style>
  </head>
  <body>
    <div class="container" style="background-color:green">
      @foreach ($users as $user)
      <div class="row">
        <div class="col col-xs-6" style="background-color:pink">
          <div class="row">
            <div class="col col-xs-6" style="background-color:yellow">
              A
            </div>
            <div class="col col-xs-6" style="background-color:silver">
              B
            </div>
          </div>
        </div>
      </div>
      @endforeach
      <table class="table table-bordered">
        <thead>
          <tr>
            <th scope="col">#</th>
            <th scope="col">First</th>
            <th scope="col">Last</th>
            <th scope="col">Handle</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <th scope="row">1</th>
            <td>Mark</td>
            <td>Otto</td>
            <td>@mdo</td>
          </tr>
          <tr>
            <th scope="row">2</th>
            <td>Jacob</td>
            <td>Thornton</td>
            <td>@fat</td>
          </tr>
          <tr>
            <th scope="row">3</th>
            <td colspan="2">Larry the Bird</td>
            <td>@twitter</td>
          </tr>
        </tbody>
      </table>
    </div>
  </body>
</html>
