@extends('crm.layouts.default')
@section('title', 'Home')
@section('content')
<div class="m-grid__item m-grid__item--fluid m-wrapper">
  <div class="m-content">
    <div class="row">
      <div class="col-xl-3 col-lg-4">
        <div class="m-portlet m-portlet--full-height  ">
          <div class="m-portlet__body">
            <div class="m-card-profile">
              @if (session('success_photo'))
              <div class="form-group m-form__group">
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
                  {{ session('success_photo') }}
                </div>
              </div>
              @endif
              @if (session('error_photo'))
              <div class="form-group m-form__group">
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
                    @foreach (session('error_photo')->all() as $message)
                        <li>{{$message}}</li>
                    @endforeach
                </div>
              </div>
              @endif
              <div class="m-card-profile__pic mb-2" style="border: 4px solid #efefef">
                <div class="m-card-profile__pic-wrapper" >
                  <img style="max-height:150px;max-width:200px;height:120px;width:120px;" src="{{ ($users->photo_url != '' ? Storage::disk('public')->url($users->photo_url) : asset('assets/logo/logo-rd.png')) }}" alt=""/>
                </div>
                <div class="pl-3 pr-3">
                  <form id="photo_form" method="post" enctype="multipart/form-data" action="{{url('master/users/edit/photo')}}">
                    @csrf
                    <input type="hidden" name="eid" value="{{$users->eid}}"/>
                    <label class="btn btn-info btn-block">
                        Pilih Foto <input type="file" name="photo_image" id="photo_image" accept="image/x-png,image/gif,image/jpeg" style="display: none;">
                    </label>
                  </form>
                  <p class="text-left mt-2">
                    Ekstensi file yang diperbolehkan : .jpg .jpeg .png </br>
                    Ukuran file yang diperbolehkan : 100 x 100 pixel
                  </p>
                </div>
              </div>
              <div class="m-card-profile__details">
                <span class="m-card-profile__name">{{$users->name}}</span>
                <a href="#" class="m-card-profile__email m-link">{{$users->email}}</a>
              </div>
            </div>
            <ul class="m-nav m-nav--hover-bg m-portlet-fit--sides">
              <li class="m-nav__separator m-nav__separator--fit"></li>
              <li class="m-nav__item">
                <a href="{{url('master/users/edit/account#profile')}}" class="m-nav__link">
                <i class="m-nav__link-icon flaticon-lock-1"></i>
                  <span class="m-nav__link-text">Ubah Sandi</span>
                </a>
              </li>
              @if (Auth::check())
                @if (Auth::user()->role==3)
                @endif
              @endif
            </ul>
            @if (Auth::check())
              @if (Auth::user()->role==3)
              <div class="m-portlet__body-separator"></div>
              <div class="m-widget1 m-widget1--paddingless">
                <div class="m-widget1__item">
                  <div class="row m-row--no-padding align-items-center">
                    <div class="col">
                      <h3 class="m-widget1__title">Member Profit</h3>
                      <span class="m-widget1__desc">Awerage Weekly Profit</span>
                    </div>
                    <div class="col m--align-right">
                      <span class="m-widget1__number m--font-brand">+$17,800</span>
                    </div>
                  </div>
                </div>
                <div class="m-widget1__item">
                  <div class="row m-row--no-padding align-items-center">
                    <div class="col">
                      <h3 class="m-widget1__title">Orders</h3>
                      <span class="m-widget1__desc">Weekly Customer Orders</span>
                    </div>
                    <div class="col m--align-right">
                      <span class="m-widget1__number m--font-danger">+1,800</span>
                    </div>
                  </div>
                </div>
                <div class="m-widget1__item">
                  <div class="row m-row--no-padding align-items-center">
                    <div class="col">
                      <h3 class="m-widget1__title">Issue Reports</h3>
                      <span class="m-widget1__desc">System bugs and issues</span>
                    </div>
                    <div class="col m--align-right">
                      <span class="m-widget1__number m--font-success">-27,49%</span>
                    </div>
                  </div>
                </div>
              </div>
              @endif
            @endif
          </div>
        </div>
      </div>
      @php
        $tab=0;
        if(Request::is('master/users/edit/address/*'))
        {
          $tab=1;
        } else if(Request::is('master/users/edit/account/*'))
        {
          $tab=2;
        } else
        {
          $tab=0;
        }
      @endphp
      <div class="col-xl-9 col-lg-8" id="profile">
        <div class="m-portlet m-portlet--full-height m-portlet--tabs  ">
          <div class="m-portlet__head">
            <div class="m-portlet__head-tools">
              <ul class="nav nav-tabs m-tabs m-tabs-line   m-tabs-line--left m-tabs-line--primary" role="tablist">
                <li class="nav-item m-tabs__item">
                  <a class="nav-link m-tabs__link {{ ($tab==0) ? 'active' : '' }}" href="{{url('master/users/edit/'.$users->eid.'#profile')}}" role="tab">
                    <i class="flaticon-share m--hide"></i>
                    Biodata
                  </a>
                </li>
                <li class="nav-item m-tabs__item">
                  <a class="nav-link m-tabs__link {{ ($tab==1) ? 'active' : '' }}" href="{{url('master/users/edit/address/'.$users->eid.'#profile')}}" role="tab">
                    Alamat
                  </a>
                </li>
                <li class="nav-item m-tabs__item">
                  <a class="nav-link m-tabs__link {{ ($tab==2) ? 'active' : '' }}" href="{{url('master/users/edit/account/'.$users->eid.'#profile')}}" role="tab">
                    Akun
                  </a>
                </li>
              </ul>
            </div>
            @if (Auth::check())
              @if (Auth::user()->role==3)
                <div class="m-portlet__head-tools">
                  <ul class="m-portlet__nav">
                    <li class="m-portlet__nav-item m-portlet__nav-item--last">
                      <div class="m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push" m-dropdown-toggle="hover" aria-expanded="true">
                        <a href="#" class="m-portlet__nav-link btn btn-lg btn-secondary  m-btn m-btn--icon m-btn--icon-only m-btn--pill  m-dropdown__toggle">
                          <i class="la la-gear"></i>
                        </a>
                        <div class="m-dropdown__wrapper">
                          <span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"></span>
                          <div class="m-dropdown__inner">
                            <div class="m-dropdown__body">
                              <div class="m-dropdown__content">
                                <ul class="m-nav">
                                  <li class="m-nav__section m-nav__section--first">
                                    <span class="m-nav__section-text">Quick Actions</span>
                                  </li>
                                  <li class="m-nav__item">
                                    <a href="#" class="m-nav__link">
                                    <i class="m-nav__link-icon flaticon-share"></i>
                                    <span class="m-nav__link-text">Create Post</span>
                                    </a>
                                  </li>
                                  <li class="m-nav__item">
                                    <a href="#" class="m-nav__link">
                                    <i class="m-nav__link-icon flaticon-chat-1"></i>
                                    <span class="m-nav__link-text">Send Messages</span>
                                    </a>
                                  </li>
                                  <li class="m-nav__item">
                                    <a href="#" class="m-nav__link">
                                    <i class="m-nav__link-icon flaticon-multimedia-2"></i>
                                    <span class="m-nav__link-text">Upload File</span>
                                    </a>
                                  </li>
                                  <li class="m-nav__section">
                                    <span class="m-nav__section-text">Useful Links</span>
                                  </li>
                                  <li class="m-nav__item">
                                    <a href="#" class="m-nav__link">
                                    <i class="m-nav__link-icon flaticon-info"></i>
                                    <span class="m-nav__link-text">FAQ</span>
                                    </a>
                                  </li>
                                  <li class="m-nav__item">
                                    <a href="#" class="m-nav__link">
                                    <i class="m-nav__link-icon flaticon-lifebuoy"></i>
                                    <span class="m-nav__link-text">Support</span>
                                    </a>
                                  </li>
                                  <li class="m-nav__separator m-nav__separator--fit m--hide">
                                  </li>
                                  <li class="m-nav__item m--hide">
                                    <a href="#" class="btn btn-outline-danger m-btn m-btn--pill m-btn--wide btn-sm">Submit</a>
                                  </li>
                                </ul>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </li>
                  </ul>
                </div>
              @endif
            @endif
          </div>
          <div class="tab-content">
            @if($tab==0)
              <div class="tab-pane active">
                <form class="m-form m-form--fit m-form--label-align-right" method="post" action="{{url()->current().'#profile'}}">
                  @csrf
                  <input type="hidden" name="eid" value="{{$users->eid}}"/>
                  <div class="m-portlet__body">
                    @if (session('error'))
                      <div class="form-group m-form__group m--margin-top-10">
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                          <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
    										  {{ session('error') }}
      			            </div>
                      </div>
    			          @endif
                    @if (session('success'))
                      <div class="form-group m-form__group m--margin-top-10">
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                          <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
    										  {{ session('success') }}
      			            </div>
                      </div>
    			          @endif
    			          @if ($errors->any())
                      <div class="form-group m-form__group m--margin-top-10">
      			            <div class="alert alert-danger alert-dismissible fade show" role="alert">
      			              <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
      			                <ul>
      			                    @foreach ($errors->all() as $message)
      			                        <li>{{$message}}</li>
      			                    @endforeach
      			                </ul>
      			            </div>
                      </div>
    			          @endif
                    <div class="form-group m-form__group row">
                      <div class="col ml-auto">
                        <h3 class="m-form__section">Biodata Diri</h3>
                      </div>
                    </div>
                    <div class="form-group m-form__group row">
                      <label for="example-text-input" class="col-3 col-form-label">Kode Member</label>
                      <div class="col-9">
                        <input class="form-control m-input" type="text" name="code" value="{{old('code',$users->code)}}" disabled>
                      </div>
                    </div>
                    <div class="form-group m-form__group row">
                      <label for="example-text-input" class="col-3 col-form-label">Nama Lengkap</label>
                      <div class="col-9">
                        <input class="form-control m-input" type="text" name="name" value="{{old('name',$users->name)}}">
                      </div>
                    </div>
                    <div class="form-group m-form__group row">
                      <label for="example-text-input" class="col-3 col-form-label">Jenis Kelamin</label>
                      <div class="col-9">
    										<div class="m-radio-inline">
    											<label class="m-radio">
    												<input type="radio" name="gender" value="pria" {{old('gender',$users->gender)=='pria' ? 'checked' : '' }}>
    												Pria
    												<span></span>
    											</label>
    											<label class="m-radio">
    												<input type="radio" name="gender" value="wanita" {{old('gender',$users->gender)=='wanita' ? 'checked' : '' }}>
    												Wanita
    												<span></span>
    											</label>
    										</div>
                      </div>
                    </div>
                    <div class="form-group m-form__group row">
                      <label class="col-3 col-form-label">Tanggal Lahir</label>
                      <div class="col-9">
              					<div class="input-group date" >
              						<input type="text" class="form-control m-input bootstrap-datepicker" name="birthday" value="{{old('code',Carbon\Carbon::parse($users->birthday)->format('d/m/Y'))}}"/>
              						<div class="input-group-append">
              							<span class="input-group-text">
              								<i class="la la-calendar"></i>
              							</span>
              						</div>
              					</div>
                      </div>
                    </div>
                    <div class="m-form__seperator m-form__seperator--dashed m-form__seperator--space-2x"></div>
                    <div class="form-group m-form__group row">
                      <div class="col ml-auto">
                        <h3 class="m-form__section">Kontak</h3>
                      </div>
                    </div>
                    <div class="form-group m-form__group row">
                      <label for="example-text-input" class="col-3 col-form-label">Email</label>
                      <div class="col-9">
                        <input class="form-control m-input" type="text" name="email" value="{{$users->email}}" disabled>
                        <span class="m-form__help">
                          @if (!$users->verified)
                          <span class="m-badge m-badge--warning m-badge--wide mt-1"><i class="fa fa-exclamation-triangle"></i> Belum diverifikasi</span>
                          <a href="{{url('master/users/edit/sendverify/'.$users->eid)}}" class="btn btn-sm m-btn--pill btn-outline-warning mt-1">Kirim Verifikasi</a>
                          @else
                          <span class="m-badge m-badge--success m-badge--wide mt-1"><i class="fa fa-check"></i> Terverifikasi</span>
                          @endif
                          <!-- <a href="{{url('sendmail')}}" class="btn btn-sm m-btn--pill btn-outline-info mt-1">Ubah</a> -->
                        </span>
                      </div>
                    </div>
                    <div class="form-group m-form__group row">
                      <label for="example-text-input" class="col-3 col-form-label">Nomor Telepon</label>
                      <div class="col-9">
                        <input class="form-control m-input" type="text" value="{{$users->phone_number}}" name="phone_number" disabled>
                        <!-- <span class="m-form__help">
                          <a href="" class="btn btn-sm m-btn--pill btn-outline-info mt-1">Ubah</a>
                        </span> -->
                      </div>
                    </div>
                  </div>
                  <div class="m-portlet__foot m-portlet__foot--fit">
                    <div class="m-form__actions">
                      <div class="row">
                        <div class="col-12 ml-auto">
                          <button type="submit" class="pull-right btn btn-accent m-btn m-btn--air m-btn--custom">Simpan</button>
                        </div>
                      </div>
                    </div>
                  </div>
                </form>
              </div>
            @endif
            @if($tab==1)
              <div class="tab-pane active">
                <div class="m-portlet__head">
                  <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                      <button class="btn btn-accent m-btn m-btn--air m-btn--custom" data-toggle="modal" data-target="#modal-address">
                        Tambah Alamat Baru
                      </button>
                      <div class="modal fade" id="modal-address" role="dialog" aria-labelledby="" aria-hidden="true">
                        <div class="modal-dialog modal-lg" role="document">
                          <div class="modal-content">
                            <form id="modal-address-form" class="m-form m-form--fit m-form--label-align-right" method="post" action="{{url()->current().'#profile'}}">
                              @csrf
                              <input type="hidden" name="eid" value="{{$users->eid}}"/>
                              <input type="hidden" id="modal-address-eua" name="eua"/>
                              <div class="modal-header">
                                <h5 class="modal-title" id="modal-address-header">Tambah Alamat Baru</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <span aria-hidden="true">&times;</span>
                                </button>
                              </div>
                              <div class="modal-body">
                                <div class="form-group">
                                  <label for="recipient-name" class="form-control-label">Alamat Pengiriman:</label>
                                  <textarea class="form-control" name="address" id="modal-address-address" data-parsley-required="true" data-parsley-required-message="Alamat Pengiriman wajib di isi"></textarea>
                                </div>
                      					<div class="form-group row">
                      						<div class="col-lg-6">
                      							<label>Nama Penerima:</label>
                      							<input type="text" class="form-control m-input" id="modal-address-person" name="person" data-parsley-required="true" data-parsley-required-message="Nama Penerima wajib di isi">
                      							<span class="m-form__help">Tuan Budi</span>
                      						</div>
                      						<div class="col-lg-6">
                      							<label class="">Nomor HP:</label>
                      							<input type="text" class="form-control m-input" id="modal-address-no_tel" name="no_tel" data-parsley-required="true" data-parsley-type="digits" data-parsley-type-message="Nomor HP tidak valid" data-parsley-required-message="Nomor HP wajib di isi">
                      							<span class="m-form__help">Contoh: 081234567890</span>
                      						</div>
                      					</div>
                      					<div class="form-group row">
                      						<div class="col-lg-9">
                      							<label>Kota atau Kecamatan:</label>
                                    <select class="form-control m-select2" id="modal-address-country" name="subdistrict" data-parsley-required="true" data-parsley-errors-container="#select2-errors" data-parsley-required-message="Kota atau Kecamatan wajib di isi">
                    							    <option value="">Cari Kota atau Kecamatan</option>
                                    </select>
                                    <div id="select2-errors"></div>
                      						</div>
                      						<div class="col-lg-3">
			                              <label class="form-control-label">Set Sebagai Utama:</label>
                                    <div class="m-checkbox-inline">
                                      <label class="m-checkbox">
                                        <input type="checkbox" name="primary" id="modal-address-primary"> Ya
                                        <span></span>
                                      </label>
                                    </div>
                      						</div>
                      					</div>
                              </div>
                              <div class="modal-footer">
                                <div class="mx-auto btn-group">
                                  <button type="button" class="btn btn-secondary mr-1" data-dismiss="modal" style="width: 150px;">Batal</button>
                                  <button type="submit" id="modal-address-button-submit" class="btn btn-primary ml-1" style="width: 150px;">Tambah Alamat</button>
                                </div>
                              </div>
                            </form>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="m-portlet__head-tools">
                    <ul class="m-portlet__nav">
                      <li class="m-portlet__nav-item"></li>
                      <li class="m-portlet__nav-item m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push" m-dropdown-toggle="hover" aria-expanded="true">
                        <a href="#" class="m-portlet__nav-link m-dropdown__toggle dropdown-toggle btn btn--sm m-btn--pill btn-secondary m-btn m-btn--label-brand">
                          Export
                        </a>
                        <div class="m-dropdown__wrapper" style="z-index: 101;">
                          <span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust" style="left: auto; right: 36px;"></span>
                          <div class="m-dropdown__inner">
                            <div class="m-dropdown__body">
                              <div class="m-dropdown__content">
                                <ul class="m-nav">
                                  <li class="m-nav__section m-nav__section--first">
                                    <span class="m-nav__section-text">Export Tools</span>
                                  </li>
                                  <li class="m-nav__item">
                                    <a href="#" class="m-nav__link" id="export_print">
                                      <i class="m-nav__link-icon la la-print"></i>
                                      <span class="m-nav__link-text">Print</span>
                                    </a>
                                  </li>
                                  <li class="m-nav__item">
                                    <a href="#" class="m-nav__link" id="export_copy">
                                      <i class="m-nav__link-icon la la-copy"></i>
                                      <span class="m-nav__link-text">Copy</span>
                                    </a>
                                  </li>
                                  <li class="m-nav__item">
                                    <a href="#" class="m-nav__link" id="export_excel">
                                      <i class="m-nav__link-icon la la-file-excel-o"></i>
                                      <span class="m-nav__link-text">Excel</span>
                                    </a>
                                  </li>
                                  <li class="m-nav__item">
                                    <a href="#" class="m-nav__link" id="export_csv">
                                      <i class="m-nav__link-icon la la-file-text-o"></i>
                                      <span class="m-nav__link-text">CSV</span>
                                    </a>
                                  </li>
                                  <li class="m-nav__item">
                                    <a href="#" class="m-nav__link" id="export_pdf">
                                      <i class="m-nav__link-icon la la-file-pdf-o"></i>
                                      <span class="m-nav__link-text">PDF</span>
                                    </a>
                                  </li>
                                </ul>
                              </div>
                            </div>
                          </div>
                        </div>
                      </li>
                    </ul>
                  </div>
                </div>
                <div class="m-portlet__body">
                  @if (session('error'))
                    <div class="form-group m-form__group m--margin-top-10">
                      <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
                        {{ session('error') }}
                      </div>
                    </div>
                  @endif
                  @if (session('success'))
                    <div class="form-group m-form__group m--margin-top-10">
                      <div class="alert alert-success alert-dismissible fade show" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
                        {{ session('success') }}
                      </div>
                    </div>
                  @endif
                  @if ($errors->any())
                    <div class="form-group m-form__group m--margin-top-10">
                      <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
                          <ul>
                              @foreach ($errors->all() as $message)
                                  <li>{{$message}}</li>
                              @endforeach
                          </ul>
                      </div>
                    </div>
                  @endif
                  <table class="table table-striped- table-bordered table-hover table-checkable" id="data_table">
                    <thead>
                      <tr>
                        <th>Utama</th>
                        <th>Penerima</th>
                        <th>Telepon</th>
                        <th>Alamat</th>
                        <th>Daerah</th>
                        <th>Aksi</th>
                      </tr>
                    </thead>
                  </table>
                </div>
              </div>
            @endif
            @if($tab==2)
              <div class="tab-pane active">
                <form class="m-form m-form--fit m-form--label-align-right" method="post" action="{{url()->current().'#profile'}}">
                  @csrf
                  <input type="hidden" name="eid" value="{{$users->eid}}"/>
                  <div class="m-portlet__body">
                    @if (session('success'))
                      <div class="form-group m-form__group m--margin-top-10">
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                          <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
    										  {{ session('success') }}
      			            </div>
                      </div>
    			          @endif
    			          @if ($errors->any())
                      <div class="form-group m-form__group m--margin-top-10">
      			            <div class="alert alert-danger alert-dismissible fade show" role="alert">
      			              <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
      			                <ul>
      			                    @foreach ($errors->all() as $message)
      			                        <li>{{$message}}</li>
      			                    @endforeach
      			                </ul>
      			            </div>
                      </div>
    			          @endif
                    <div class="form-group m-form__group row">
                      <div class="col ml-auto">
                        <h3 class="m-form__section">Ubah Kata Sandi</h3>
                      </div>
                    </div>
                    <div class="form-group m-form__group row">
                      <label for="example-text-input" class="col-3 col-form-label">Kata Sandi Lama</label>
                      <div class="col-9">
                        <input class="form-control m-input" type="password" name="old_password">
                      </div>
                    </div>
                    <div class="form-group m-form__group row">
                      <label for="example-text-input" class="col-3 col-form-label">Kata Sandi Baru</label>
                      <div class="col-9">
                        <input class="form-control m-input" type="password" name="password">
                      </div>
                    </div>
                    <div class="form-group m-form__group row">
                      <label for="example-text-input" class="col-3 col-form-label">Konfirmasi Kata Sandi Baru</label>
                      <div class="col-9">
                        <input class="form-control m-input" type="password" name="password_confirmation">
                      </div>
                    </div>
                  </div>
                  <div class="m-portlet__foot m-portlet__foot--fit">
                    <div class="m-form__actions">
                      <div class="row">
                        <div class="col-12 ml-auto">
                          <button type="submit" class="pull-right btn btn-accent m-btn m-btn--air m-btn--custom">Simpan</button>
                        </div>
                      </div>
                    </div>
                  </div>
                </form>
              </div>
            @endif
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@push('a-css')
<link href="{{asset('assets/crm/vendors/custom/datatables/datatables.bundle.css')}}" rel="stylesheet" type="text/css" />
<style>
input.parsley-success,
select.parsley-success,
textarea.parsley-success {
  border-color: green;
}

input.parsley-error,
select.parsley-error,
textarea.parsley-error {
  border-color: red;
}

.parsley-errors-list {
  margin: 4px 0 3px;
  padding: 0;
  list-style-type: none;
  font-size: 0.9em;
  line-height: 0.9em;
  opacity: 0;
  transition: all .3s ease-in;
  -o-transition: all .3s ease-in;
  -moz-transition: all .3s ease-in;
  -webkit-transition: all .3s ease-in;
  color:red;
}

.parsley-errors-list.filled {
  opacity: 1;
}

.parsley-error + span.select2 span.select2-selection--single {
  border-color: red;
}
.parsley-success + span.select2 span.select2-selection--single {
  border-color: green;
}

</style>
@endpush
@push('b-script')
<script src="{{asset('assets/crm/vendors/custom/datatables/datatables.bundle.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/crm/demo/default/custom/crud/datatables/extensions/buttons.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/crm/vendors/custom/parsley/parsley.js')}}" type="text/javascript"></script>
<script>
$(function() {
    thisform.init();
}), thisform = {
  init: function()
  {
    thisform.p_init();
    thisform.c_init();
    @if($tab==1)
    thisform.d_init();
    thisform.m_init();
    @endif
  },
  p_init: function()
  {
    $('#photo_image').on('change',function(){
      $(this).parents('form').submit();
    });
  },
  c_init: function() {
    var t;
    t = mUtil.isRTL() ? {
        leftArrow: '<i class="la la-angle-right"></i>',
        rightArrow: '<i class="la la-angle-left"></i>'
    } : {
        leftArrow: '<i class="la la-angle-left"></i>',
        rightArrow: '<i class="la la-angle-right"></i>'
    };
    $(".bootstrap-datepicker").datepicker({
        rtl: mUtil.isRTL(),
        format: 'dd/mm/yyyy',
        todayHighlight: !0,
        templates: t
    });
  },
  d_init: function() {
    var t;
    t = $("#data_table").DataTable({
        "lengthMenu": [ [10, 25, 50, 100, -1], [10, 25, 50, 100,"All"]],
        responsive: !0,
        buttons: ["print", "copyHtml5", "excelHtml5", "csvHtml5", "pdfHtml5"],
        processing: !0,
        serverSide: !0,
        ajax: {
            url: "{{url('master/users/edit/address/datatable')}}",
            type: "POST",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                eid : '{{$users->eid}}',
            },
        },
        columns: [{
            data: "primary", name:"users_address.primary"
        },{
            data: "person", name:"users_address.person"
        },{
            data: "telephone", name:"users_address.telephone"
        },{
            data: "address", name:"users_address.address"
        },{
            data: "area", name:"area"
        },{
            data: "action", name:"action"
        }],
        "drawCallback": function( settings ) {
          $('#data_table tbody').on('click', 'td.sorting_1', function () {
            thisform.e_init();
          });
          thisform.e_init();
        },
    }), $("#export_print").on("click", function(e) {
        e.preventDefault(), t.button(0).trigger()
    }), $("#export_copy").on("click", function(e) {
        e.preventDefault(), t.button(1).trigger()
    }), $("#export_excel").on("click", function(e) {
        e.preventDefault(), t.button(2).trigger()
    }), $("#export_csv").on("click", function(e) {
        e.preventDefault(), t.button(3).trigger()
    }), $("#export_pdf").on("click", function(e) {
        e.preventDefault(), t.button(4).trigger()
    });
  },
  m_init: function (){
    var i = $("#modal-address-form");
    var m = $("#modal-address");
    i.parsley().on("form:validated", function() {

    }).on("field:validated", function(i) {

    }).on("form:submit",function(){
      // $(".masked_input").inputmask('remove');
    }), window.Parsley.on("field:validate", function() {

    }), window.Parsley.on("field:error", function() {

    });
    m.on("shown.bs.modal", function() {
      thisform.s_init();
    }).on("hidden.bs.modal", function() {
      i.parsley().reset();
      $("#modal-address-country").val([]);
    });;

  },
  e_init: function (){
    $(".address-delete").on("click", function(e) {
      e.preventDefault();
      $link=$(this).attr('href');
      swal({
        title: "Yakin Hapus?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes",
        cancelButtonText: "No",
        }).then((result) => {
          if (result.value) {
            location.href = $link;
          }
        });
    });
    $(".address-edit").on("click", function(e) {
      $eua = $(this).data('address-eua');
      $.ajax({
          url: "{{url('master/users/edit/address/datatable/detail')}}",
          type: "POST",
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
          data: {
              eid : '{{$users->eid}}',
              eua : $eua,
          },
          success : function(data) {
            $("#modal-address-header").text('Ubah Alamat');
            $("#modal-address-button-submit").text('Ubah Alamat');
            $("#modal-address-address").val(data.address);
            $("#modal-address-person").val(data.person);
            $("#modal-address-eua").val(data.eid);
            $("#modal-address-no_tel").val(data.telephone);
            $("#modal-address-primary").prop('checked', data.primary);
            $("#modal-address").modal('show');
            var subdistrict = $('#modal-address-country');
            if (data.subdistrict_id==null)
            {
              data.subdistrict_id='';
            }
            var newOption = new Option(data.province_name +', '+ data.city_name +', ' + data.subdistrict_name, data.subdistrict_id, true, true);
            subdistrict.append(newOption).trigger('change');
            subdistrict.trigger({
                type: 'select2:select',
                params: {
                    data: data
                }
            });
         }
      });
    });
  },
  s_init : function () {
    var s = $("#modal-address-country");
    s.select2({
        placeholder: "Cari Kota atau Kecamatan",
        allowClear: true,
        ajax: {
            url: "{{url('master/users/edit/address/country')}}",
            // dataType: "json",
            delay: 250,
            data: function(e) {
                return {
                    q: e.term,
                    page: e.page
                }
            },
            processResults: function(e, t) {
                return t.page = t.page || 1, {
                    results: e.items,
                    pagination: {
                        more: 30 * t.page < e.total_count
                    }
                }
            },
            cache: !0
        },
        escapeMarkup: function(e) {
            return e
        },
        minimumInputLength: 3,
        templateResult: function(e) {
            if (e.loading) return e.text;
            var t = "<div class='select2-result-repository clearfix'><div class='select2-result-repository__meta'><div class='select2-result-repository__title'>" + e.province_name +', '+ e.city_name + "</div>";
            return e.name && (t += "<div class='select2-result-repository__description'>" + e.name + "</div>"), t += " </div></div>"
        },
        templateSelection: function(e) {
            // console.log(e);
            if ((e.id=="") || (e.province_name==null) || (e.city_name==null) || (e.name==null)) return e.text;
            return e.province_name +', '+ e.city_name +', ' + e.name
        },
        // initSelection: function(element, callback) {
        //
        // },
    }), s.on('change', function(){
      s.trigger('input');
    });
  },
};
</script>
@endpush
