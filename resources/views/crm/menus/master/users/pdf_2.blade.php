<html>
  <head>
    <style>
    .page-break-after {
        page-break-after: always;
    }
    .page-break-inside {
        page-break-inside: always;
    }
    .descriptors-align {
        width: 50%;
        display: inline-block;
    }
    .column-count-2 {
      column-count: 2;
    }
    .column-count-3 {
      column-count: 3;
    }
    .solid {
      column-rule-style: solid;
    }
    .column {
      float: left;
      width: 50%;
    }
    .row:after {
      content: "";
      display: table;
      clear: both;
    }
    .container {

    }
    </style>
  </head>
  <body>
    @foreach ($users as $user)
      <div class="row">
        <div class="column" style="background-color:#aaa;">
          <div class="row">
            <div class="column" style="background-color:#aaa;">
              <h2>Column 1</h2>
              <p>Some text..</p>
            </div>
            <div class="column" style="background-color:#bbb;">
              <h2>Column 2</h2>
              <p>Some text..</p>
            </div>
          </div>
        </div>
        <div class="column" style="background-color:#bbb;">
          <h2>Column 2</h2>
          <p>Some text..</p>
        </div>
      </div>
      <h1>{{$user->code}}</h1>
      <h1>{{$user->name}}</h1>
    @endforeach
  </body>
</html>
