@extends('crm.layouts.default')
@section('title', 'Barang Kategori')
@section('content')
<div class="m-grid__item m-grid__item--fluid m-wrapper">
  <div class="m-subheader ">
    <div class="d-flex align-items-center">
      <div class="mr-auto">
        <h3 class="m-subheader__title ">
          Barang Kategori
        </h3>
      </div>
    </div>
  </div>
  <div class="m-content" id="m-form-content">
    @if (session('status'))
      <div class="alert {{session('status')['alert']}} alert-dismissible fade show" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
          {!! session('status')['message'] !!}
      </div>
    @endif
    <div class="row">
      <div class="col-xl-12">
        <div class="m-portlet m-portlet--tab">
    			<div class="m-portlet__head">
    				<div class="m-portlet__head-caption">
    					<div class="m-portlet__head-title">
    						<span class="m-portlet__head-icon m--hide">
    						<i class="la la-gear"></i>
    						</span>
    						<h3 class="m-portlet__head-text">
    							Informasi Barang Kategori
    						</h3>
    					</div>
    				</div>
    			</div>
    			<form id="v-form" class="m-form m-form--fit m-form--label-align-right" method="post" enctype="multipart/form-data" action="{{url()->current()}}">
            @csrf
            @if(isset($item))
            <input type="hidden" name="eid" value="{{$item->eid}}"/>
            @endif
    				<div class="m-portlet__body">
    					<div class="form-group m-form__group row">
    						<label class="col-3 col-form-label text-left" for="form-input-name">
                  Icon Gambar Kategori<br />
                  <span class="m-form__help">Gambar Kategori.</span>
                </label>
    						<div class="col-9">
    							<input type="file" name="photo_url" class="dropify" data-default-file="{{old('photo_url', isset($item->photo_url) ? Storage::disk('public')->url($item->photo_url) : '')}}" />
    						</div>
    					</div>
    					<div class="form-group m-form__group row">
    						<label class="col-3 col-form-label text-left" for="form-input-name">
                  Nama Kategori<br />
                  <span class="m-form__help">Tulis nama barang yang sesuai.</span>
                </label>
    						<div class="col-9">
    							<input class="form-control m-input" id="form-input-name" name="name" type="text" value="{{old('name', isset($item->name) ? $item->name : '')}}" placeholder="Masukan Nama Kategori" data-parsley-required="true" data-parsley-required-message="Nama Wajib diisi">
    						</div>
    					</div>
    					<div class="form-group m-form__group row">
    						<label class="col-3 col-form-label text-left" for="form-input-publish">
                  Publish<br />
                  <span class="m-form__help">Publish.</span>
                </label>
    						<div class="col-9">
                  <label class="m-checkbox m-checkbox--state-success">
  									<input class="form-control m-input" id="form-input-publish" name="publish" type="checkbox" {{ old('publish',( isset($item->publish) ? ($item->publish ? 'checked' : '') : '') ) ? 'checked' : '' }} data-parsley-required="false"> Tampil dihalaman?
  									<span></span>
									</label>
    						</div>
    					</div>
            </div>
    				<div class="m-portlet__foot m-portlet__foot--fit">
    					<div class="m-form__actions text-right">
    						<a href="{{url('master/item/list')}}" class="mb-1 btn btn-secondary">Cancel</a>
    						<button type="submit" class="mb-1 btn btn-info">Simpan & Daftar Baru</button>
    						<button type="submit" class="mb-1 btn btn-primary">Simpan</button>
    					</div>
    				</div>
    			</form>
    		</div>
      </div>
    </div>
  </div>
</div>
@endsection

@push('b-css')
<link href="{{asset('assets/crm/vendors/custom/cropper/cropper.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('assets/crm/vendors/custom/dropify/css/dropify.min.css')}}" rel="stylesheet" type="text/css" />
<style>
.dz-details{
  display:none !important;
}
</style>
@endpush

@push('b-script')
<script src="{{asset('assets/crm/vendors/custom/cropper/cropper.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/crm/vendors/custom/dropify/js/dropify.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/crm/vendors/custom/parsley/parsley.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/crm/vendors/custom/jquery.serializeJSON/jquery.serializejson.min.js')}}"></script>
<script>
$(function() {
    thisform.init();
}), thisform = {
  init: function()
  {
    thisform.c_init();
    thisform.d_init();
    thisform.v_init();
  },
  v_init : function (){
    var i = $("#v-form");
    i.parsley().on("form:validated", function() {

    }).on("field:validated", function(i) {

    }).on("form:submit",function(){

    }), window.Parsley.on("field:validate", function() {

    }), window.Parsley.on("field:error", function() {

    });
  },
  d_init : function() {
    $('.dropify').dropify();
  },
  c_init: function() {
    var t;
    t = mUtil.isRTL() ? {
        leftArrow: '<i class="la la-angle-right"></i>',
        rightArrow: '<i class="la la-angle-left"></i>'
    } : {
        leftArrow: '<i class="la la-angle-left"></i>',
        rightArrow: '<i class="la la-angle-right"></i>'
    };
    $(".bootstrap-datepicker").datepicker({
        rtl: mUtil.isRTL(),
        format: 'dd/mm/yyyy',
        todayHighlight: !0,
        templates: t
    });
  },
}
</script>
@endpush
