@extends('crm.layouts.default')
@section('title', 'Kas')
@section('content')
<div class="m-grid__item m-grid__item--fluid m-wrapper">
  <div class="m-subheader ">
    <div class="d-flex align-items-center">
      <div class="mr-auto">
        <h3 class="m-subheader__title ">
          Input Kas
        </h3>
      </div>
    </div>
  </div>
  <div class="m-content" id="m-form-content">
    @if (session('status'))
      <div class="alert {{session('status')['alert']}} alert-dismissible fade show" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
          {!! session('status')['message'] !!}
      </div>
    @endif
    <div class="row">
      <div class="col-xl-6">
        <div class="m-portlet m-portlet--tab">
    			<div class="m-portlet__head">
    				<div class="m-portlet__head-caption">
    					<div class="m-portlet__head-title">
    						<span class="m-portlet__head-icon m--hide">
    						<i class="la la-gear"></i>
    						</span>
    						<h3 class="m-portlet__head-text">
    							Informasi Kas
    						</h3>
    					</div>
    				</div>
    			</div>
    			<form id="v-form" class="m-form m-form--fit m-form--label-align-right" method="post" enctype="multipart/form-data" action="{{url()->current()}}">
            @csrf
            @if(isset($model))
            <input type="hidden" name="eid" value="{{$model->eid}}"/>
            @endif
    				<div class="m-portlet__body">
              <div class="form-group m-form__group row">
                <label class="col-3 col-form-label text-left" for="form-input-receipt_number">
                  No Transaksi<br />
                  <span class="m-form__help">No Transaksi.</span>
                </label>
                <div class="col-9">
                  <input class="form-control m-input" id="form-input-receipt_number" readonly name="receipt_number" type="text" value="{{old('receipt_number', isset($model->receipt_number) ? $model->receipt_number : '')}}">
                </div>
              </div>
    					<div class="form-group m-form__group row">
    						<label class="col-3 col-form-label text-left" for="form-input-schedule_id">
                  Jadwal<br />
                  <span class="m-form__help">Jadwal.</span>
                </label>
    						<div class="col-9">
                  <select class="form-control m-select2"  id="schedule_select" name="schedule_id" data-parsley-required="true" data-parsley-errors-container="#schedule_id-errors" data-parsley-required-message="Jadwal wajib di isi" readonly>
                    <option value="">Jadwal</option>
                  </select>
                  <div id="schedule_id-errors"></div>
    						</div>
    					</div>
              <div class="form-group m-form__group row">
                <label class="col-3 col-form-label text-left" for="form-input-name">
                  Jenis<br />
                  <span class="m-form__help">Masukan nominal.</span>
                </label>
                <div class="col-9">
                  <div class="m-radio-inline">
                    <label class="m-radio">
                      <input type="radio" name="type" value="debet" {{(old('type', isset($model->type) ? $model->type : '')) =='debet' ? 'checked' : ''}}>Debet
                      <span></span>
                    </label>
                    <label class="m-radio">
                      <input type="radio" name="type" value="credit" {{(old('type', isset($model->type) ? $model->type : '')) =='credit' ? 'checked' : ''}}>Kredit
                      <span></span>
                    </label>
                  </div>
                </div>
              </div>
              <div class="form-group m-form__group row">
                <label class="col-3 col-form-label text-left" for="form-input-amount">
                  Jumlah<br />
                  <span class="m-form__help">Masukan nominal.</span>
                </label>
                <div class="col-9">
                  <input class="form-control m-input" id="form-input-amount" name="amount" type="text" value="{{old('amount', isset($model->amount) ? $model->amount : '0')}}" placeholder="Masukan Nominal Kas" data-parsley-required="true" data-parsley-required-message="Nominal Wajib diisi">
                </div>
              </div>
        			<div class="form-group m-form__group row">
    						<label class="col-3 col-form-label text-left" for="form-input-description">
                  Keterangan<br />
                  <span class="m-form__help">Masukan keterangan.</span>
                </label>
        				<div class="col-lg-9">
                  <textarea id="form-input-description" name="note" class="form-control m-input" placeholder="Masukan Keterangan Kas" data-parsley-required="false">{{old('note', isset($model->note) ? $model->note : '')}}</textarea>
        				</div>
        			</div>
            </div>
    				<div class="m-portlet__foot m-portlet__foot--fit">
    					<div class="m-form__actions text-right">
    						<a href="{{url('master/item/list')}}" class="mb-1 btn btn-secondary">Cancel</a>
    						<button type="submit" name="saveandnew" class="mb-1 btn btn-info">Simpan & Daftar Baru</button>
    						<button type="submit" name="saveonly" class="mb-1 btn btn-primary">Simpan</button>
    					</div>
    				</div>
    			</form>
    		</div>
      </div>
      <div class="col-xl-6">
        <div class="m-portlet m-portlet--tab">
          <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
              <div class="m-portlet__head-title">
                <span class="m-portlet__head-icon m--hide">
                <i class="la la-gear"></i>
                </span>
                <h3 class="m-portlet__head-text">
                  Detail Kas Jadwal
                </h3>
              </div>
            </div>
          </div>

          <div class="m-portlet__body">
            {!! $datatable->table() !!}
          </div>

        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@push('b-css')
<link href="{{asset('assets/crm/vendors/custom/cropper/cropper.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('assets/crm/vendors/custom/dropify/css/dropify.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('assets/crm/vendors/custom/datatables/datatables.bundle.css')}}" rel="stylesheet" type="text/css" />
<style>
.dz-details{
  display:none !important;
}
</style>
@endpush

@push('b-script')
<script src="{{asset('assets/crm/vendors/custom/cropper/cropper.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/crm/vendors/custom/dropify/js/dropify.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/crm/vendors/custom/parsley/parsley.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/crm/vendors/custom/jquery.serializeJSON/jquery.serializejson.min.js')}}"></script>
<script src="{{asset('assets/crm/vendors/custom/datatables/datatables.bundle.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/crm/demo/default/custom/crud/datatables/extensions/buttons.js')}}" type="text/javascript"></script>
<script>
$(function() {
    thisform.init();
}), thisform = {
  init: function()
  {
    thisform.c_init();
    thisform.s_init();
    thisform.v_init();
  },
  v_init : function (){
    var i = $("#v-form");
    i.parsley().on("form:validated", function() {

    }).on("field:validated", function(i) {

    }).on("form:submit",function(){

    }), window.Parsley.on("field:validate", function() {

    }), window.Parsley.on("field:error", function() {

    });
  },
  c_init: function() {
    var t;
    t = mUtil.isRTL() ? {
        leftArrow: '<i class="la la-angle-right"></i>',
        rightArrow: '<i class="la la-angle-left"></i>'
    } : {
        leftArrow: '<i class="la la-angle-left"></i>',
        rightArrow: '<i class="la la-angle-right"></i>'
    };
    $(".bootstrap-datepicker").datepicker({
        rtl: mUtil.isRTL(),
        format: 'dd/mm/yyyy',
        todayHighlight: !0,
        templates: t
    });
  },
  s_init : function () {
    var s1 = $("#schedule_select");
    s1.select2({
        placeholder: "Cari Jadwal",
        allowClear: true,
        ajax: {
            url: "{{url('master/item/schedule')}}",
            delay: 250,
            data: function(e) {
                return {
                    q: e.term,
                    page: e.page
                }
            },
            processResults: function(e, t) {
                return t.page = t.page || 1, {
                    results: e.items,
                    pagination: {
                        more: 30 * t.page < e.total_count
                    }
                }
            },
            cache: !0
        },
        escapeMarkup: function(e) {
            return e
        },
        minimumInputLength: 3,
        templateResult: function(e) {
          if (e.loading) return e.text;
          var t = "<div class='select2-result-repository clearfix'><div class='select2-result-repository__meta'><div class='select2-result-repository__title'>" + e.name+ "</div>";
          return e.name && (t += "<div class='select2-result-repository__description'>" + e.description + "</div>"), t += " </div></div>"
        },
        templateSelection: function(e) {
          if ((e.id=="") || (e.name==null)) return e.text;
          return e.name +', '+ e.description
        },
    }), s1.on('change', function(e){
      // console.log(e);
      s1.trigger('input');
    });
    @isset($schedule)
      var newOption = new Option('{{$schedule->name}}' + '{{$schedule->description}}', '{{$schedule->id}}', true, true);
      s1.append(newOption).trigger('change');
      s1.trigger({
          type: 'select2:select',
          params: {
              data: '{{$schedule->id}}'
          }
      });
    @endif
  },
}
</script>
{!! $datatable->scripts() !!}
@endpush
