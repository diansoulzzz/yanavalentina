@extends('crm.layouts.default')
@section('title', 'Home')
@section('content')
<div class="m-grid__item m-grid__item--fluid m-wrapper">
  <div>
    <div class="m-subheader">
      @if (session('status'))
        <div class="alert {{session('status')['alert']}} alert-dismissible fade show" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
            {!! session('status')['message'] !!}
        </div>
      @endif
      <div class="d-flex align-items-center">
        <div class="mr-auto">
          <p class="m--font-boldest mb-0">New Arrival</p>
        </div>
        <!-- <div>
          <a href="{{url('p/latest')}}" class="btn btn-sm">
            Lihat Semua
          </a>
        </div> -->
      </div>
    </div>
    <div class="m-content pt-1">
      <div class="row">
        <div class="col-xl-12">
          <div class="owl-carousel owl-theme owl-loaded" data-items="4" data-nav="true" data-dots="false" data-autoplay="false" data-autoplaytimeout="0" data-autoplayhoverpause="false">
            <div class="owl-stage-outer">
              <div class="owl-stage">
                @foreach($items as $key => $item)
                <div class="owl-item">
                  <div class="card">
                    <a href="{{'p/'.$item->link_url}}" style="width:auto;height:100px;">
                      <img src="{{Storage::disk('public')->url($item->item_images_primary->photo_url)}}" alt="{{$item->name}}">
                    </a>
                  </div>
                </div>
                @endforeach
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div>
    <div class="m-subheader pt-0">
      <div class="d-flex align-items-center">
        <div class="mr-auto">
          <p class="m--font-boldest mb-0">Jadwal</p>
        </div>
        <div>
          <a href="{{url('s/list')}}" class="btn btn-sm">
            Lihat Semua
          </a>
        </div>
      </div>
    </div>
    <div class="m-content pt-1">
      <div class="row">
        <div class="col-xl-12">
          <div class="owl-carousel owl-theme owl-loaded" data-items="1" data-nav="true" data-dots="true" data-autoplay="true" data-autoplaytimeout="5000" data-autoplayhoverpause="true">
            <div class="owl-stage-outer">
              <div class="owl-stage">
                @foreach($schedules as $key => $schedule)
                <div class="owl-item">
                  <div class="card">
                    <a href="{{'s/'.$schedule->link_url}}" style="width:auto;height:400px;">
                      <img src="{{Storage::disk('public')->url($schedule->schedule_images[0]->photo_url)}}" alt="{{$schedule->name}}">
                    </a>
                  </div>
                </div>
                @endforeach
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div>
    <div class="m-subheader pt-0">
      <div class="d-flex align-items-center">
        <div class="mr-auto">
          <p class="m--font-boldest mb-0">Terakhir Dilihat</p>
        </div>
        <!-- <div>
          <a href="{{url('p/history')}}" class="btn btn-sm">
            Lihat Semua
          </a>
        </div> -->
      </div>
    </div>
    <div class="m-content pt-1">
      <div class="row">
        <div class="col-xl-12">
          <div class="owl-carousel owl-theme owl-loaded" data-items="4" data-nav="true" data-dots="false" data-autoplay="false" data-autoplaytimeout="0" data-autoplayhoverpause="false">
            <div class="owl-stage-outer">
              <div class="owl-stage">
                @foreach($items as $key => $item)
                <div class="owl-item">
                  <div class="card">
                    <a href="{{'p/'.$item->link_url}}">
                      <img style="width:auto;height:100px;" src="{{Storage::disk('public')->url($item->item_images_primary->photo_url)}}" alt="{{$item->name}}">
                    </a>
                  </div>
                </div>
                @endforeach
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@push('b-css')
<link href="{{asset('assets/crm/vendors/custom/owlcarousel/assets/owl.carousel.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('assets/crm/vendors/custom/owlcarousel/assets/owl.theme.default.min.css')}}" rel="stylesheet" type="text/css" />
<style>
.owl-carousel{
  position: relative;
}
.owl-dots{
  position: absolute;
  top:90%;
  left:25%;
  right:25%;
}

.owl-prev {
  top: 45%;
  width: 30px;
  height: 30px;
  position: absolute;
  /* left :-10%; */
  display: block !important;
  border-radius: 50% !important;
  background-color: white !important;

}
.owl-next {
  top: 45%;
  width: 30px;
  height: 30px;
  /* right : -10%; */
  position: absolute;
  display: block !important;
  border-radius: 50% !important;
  background-color: white !important;
}
.owl-prev i, .owl-next i {
  /* transform : scale(1,6); */
  color: #ccc;
}

.owl-prev, .owl-next {
  /* transition: ease 0.3s; */
  transition: visibility 0s, opacity 0.3s ease;
  visibility: hidden;
  opacity: 0;
  /* opacity: 0 !important; */
}

.owl-carousel:hover .owl-nav .owl-prev {
  /* opacity: 1 !important; */
  left: 2%;
  visibility: visible;
  opacity: 1;
}

.owl-carousel:hover .owl-nav .owl-next {
  /* opacity: 1 !important; */
  right: 2%;
  visibility: visible;
  opacity: 1;
}

</style>
@endpush

@push('b-script')
<script src="{{asset('assets/crm/vendors/custom/owlcarousel/owl.carousel.js')}}" type="text/javascript"></script>

<script>
  $(document).ready(function() {
    $('.owl-carousel').each(function() {
      $items = 3;
      $items = $(this).data('items');
      $nav = $(this).data('nav');
      $dots = $(this).data('dots');
      $autoplay = $(this).data('autoplay');
      $autoplayTimeout = $(this).data('autoplaytimeout');
      $autoplayHoverPause = $(this).data('autoplayhoverpause');
      $(this).owlCarousel({
        loop: true,
        items : $items,
        stagePadding: 80,
        dots : $dots,
        margin: 10,
        nav : $nav,
        autoplay : $autoplay,
        autoplayTimeout: $autoplayTimeout,
        autoplayHoverPause : $autoplayHoverPause,
        navText : ["<i class='fa fa-chevron-left'></i>","<i class='fa fa-chevron-right'></i>"],
        responsiveClass: true,
        responsive: {
          0 : {
              items : 1,
              nav : $nav,
              stagePadding: 0,
          },
          // 600 : {
          //     items : $items,
          //     nav : false
          // },
          1000 : {
              items : $items,
              nav : true,
              loop : true
          }
        },
      });
    });
    // $('.owl-carousel').owlCarousel({
    //   loop: true,
    //   items : 4,
    //   dots : false,
    //   margin: 10,
    //   nav : true,
    //   responsiveClass: true,
    //   navText : ["<i class='fa fa-chevron-left'></i>","<i class='fa fa-chevron-right'></i>"],
    // })
  })
</script>
@endpush
