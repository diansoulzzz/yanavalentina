@extends('crm.layouts.default')
@section('title', 'Data Promosi')
@section('content')
<div class="m-grid__item m-grid__item--fluid m-wrapper">
  <div class="m-content">
    <div class="m-portlet m-portlet--mobile">
      <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
          <div class="m-portlet__head-title">
            <h3 class="m-portlet__head-text">
              Lihat Data Promosi
            </h3>
          </div>
        </div>
      </div>
      <div class="m-portlet__body">
        @if (session('error'))
          <div class="form-group m-form__group m--margin-top-10">
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
              {{ session('error') }}
            </div>
          </div>
        @endif
        @if (session('success'))
          <div class="form-group m-form__group m--margin-top-10">
            <div class="alert alert-success alert-dismissible fade show" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
              {{ session('success') }}
            </div>
          </div>
        @endif
        {!! $datatable->table() !!}
      </div>
    </div>
  </div>
</div>
@endsection
@push('a-css')
<link href="{{asset('assets/crm/vendors/custom/datatables/datatables.bundle.css')}}" rel="stylesheet" type="text/css" />
@endpush
@push('b-script')
<script src="{{asset('assets/crm/vendors/custom/datatables/datatables.bundle.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/crm/demo/default/custom/crud/datatables/extensions/buttons.js')}}" type="text/javascript"></script>
<script>
$(function() {
    thisform.init();
}), thisform = {
  init: function()
  {
  },
  e_init: function()
  {
    $(".cms-promotion-delete").on("click", function(e) {
      e.preventDefault();
      $link=$(this).attr('href');
      swal({
        title: "Yakin Hapus?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes",
        cancelButtonText: "No",
        }).then((result) => {
          if (result.value) {
            location.href = $link;
          }
        });
    });
  },
};
</script>
{!! $datatable->scripts() !!}
@endpush
