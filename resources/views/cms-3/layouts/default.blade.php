<!DOCTYPE html>
<html lang="en">
  <head>
      <title>@yield('title') | Yana Valentina</title>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <meta name="description" content="">
      <meta name="author" content="">
      <meta name="csrf-token" content="{{ csrf_token() }}">
      @stack('a-css')
      @include('cms.includes.css')
      @stack('b-css')
  </head>
  <body>
    <div class="site-wrapper">
      @yield('content')
    </div>
    @stack('a-script')
    @include('cms.includes.script')
    @stack('b-script')
  </body>
</html>
