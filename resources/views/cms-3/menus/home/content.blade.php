@extends('cms.layouts.default')
@section('title', 'Home')
@section('content')
<div class="site-wrapper-inner bg-full" >
  <div class="masthead clearfix inner">
    <!-- <div class="inner">
    </div> -->
  </div>
  <div class="cover-container">
    <div class="inner cover">
      <div class="row mb-5">
        <div class="col-md-4">
          <img src="{{asset('assets/logo/logo-wt.png')}}" style="width:150px;height:150px">
          <h1 class="nopadding wild-font font-lg" style="margin-top:-20px !important;">Yana valentina</h1>
          <p style="margin-top:-20px !important;"><small>Branded Bag's</small></p>
          <hr class="mb-3"/>
          <div class="row px-4 pb-5">
            <div class="col-sm-4">
              <a href="#" role="button">About Us</a>
            </div>
            <div class="col-sm-4">
              <a href="#" role="button">Contact Us</a>
            </div>
            <div class="col-sm-4">
              <a href="#" role="button">Why Us</a>
            </div>
          </div>
          <div class="row" style="padding-top:100px">
            <p class="ml-auto col-pink">Want to see my new collection?</p>
            <div class="col-sm-6 ml-auto">
              <a class="btn btn-pink" href="{{url('register')}}" role="button">Daftar</a>
              <a class="btn btn-pink" href="{{url('login')}}" role="button">Masuk</a>
            </div>
          </div>
        </div>
        <div class="col-md-8">
          <div class="row mb-1">
            <div class="col-md-12">
              <!-- <h3 align="right">Make your colorful day's</h3> -->
              <!-- <h3 align="right">with YANA VALENTINA collection</h3> -->
              <h3 class="text-right" >Make your colorful day's</h3>
              <h3 class="text-right" style="margin-top:-20px !important;">with <span class="wild-font font-lg">Yana valentina</span> collection</h3>
              <!-- <h3 class="nopadding wild-font font-lg" style="margin-top:-20px !important;">Yana valentina</h3> -->
            </div>
          </div>
          <div class="row pt-3">
            <div class="col-md-10 ml-auto">
              <div class="swiper-container" style="height:350px; border-style: solid; border-color: white; border-radius: 5px; border-width: 8px;">
                <div class="swiper-wrapper">
                  <div class="swiper-slide" style="background-image:url({{asset('assets/cms/img/bag-brand-01.jpg')}}); background-size: 100% 100%; background-repeat: no-repeat; background-position: center;"></div>
                  <div class="swiper-slide" style="background-image:url({{asset('assets/cms/img/bag-brand2.jpg')}}); background-size: 100% 100%; background-repeat: no-repeat; background-position: center;"></div>
                </div>
                <div class="swiper-pagination"></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="mastfoot">
      <div class="inner">
        <p class="float-right">@2018 <a href="https://ashland.co.id">Ashland</a>.</p>
      </div>
    </div>
  </div>
</div>
@endsection

@push('a-css')
<style>
.box {
    box-sizing: border-box;
    width: 100px;
    height: 100px;
}
.mh-50 {
    max-height: 50vh;
}
.clip {
    overflow: hidden;
}
.swiper-container {
    width: 100%;
    height: 100%;
    margin-left: auto;
    margin-right: auto;
}
.swiper-slide {
    text-align: center;
    font-size: 18px;
    background: #fff;
    /* Center slide text vertically */
    display: -webkit-box;
    display: -ms-flexbox;
    display: -webkit-flex;
    display: flex;
    -webkit-box-pack: center;
    -ms-flex-pack: center;
    -webkit-justify-content: center;
    justify-content: center;
    -webkit-box-align: center;
    -ms-flex-align: center;
    -webkit-align-items: center;
    align-items: center;
}
</style>
@endpush
@push('b-script')
<script>
var swiper = new Swiper('.swiper-container', {
    direction: 'vertical',
    slidesPerView: 1,
    spaceBetween: 30,
    mousewheel: true,
    pagination: {
      el: '.swiper-pagination',
      clickable: true,
    },
});
// var vswiper = new Swiper('.v-swiper-container', {
//     direction: 'vertical',
//     speed: 2000,
//     autoplay: {
//         delay: 2000,
//     },
//     loop: true,
//     pagination: {
//         el: '.swiper-pagination',
//         clickable: true,
//     },
// });
</script>
@endpush
