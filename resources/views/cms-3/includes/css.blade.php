<link href="{{asset('node_modules/bootstrap/dist/css/bootstrap.min.css')}}" rel="stylesheet">
<link href="{{asset('assets/cms/css/style.css')}}" rel="stylesheet">
<link href="{{asset('assets/font/font-stylesheet.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('node_modules/swiper/dist/css/swiper.min.css')}}" rel="stylesheet" type="text/css" />
<link rel="shortcut icon" href="{{asset('assets/logo/favicon.ico')}}" />
