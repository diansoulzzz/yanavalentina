<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Yana Valentina</title>
  <link rel="shortcut icon" href="{{asset('assets/logo/favicon.ico')}}" type="image/x-icon"/>
  <link rel="stylesheet" href="{{asset('node_modules/bootstrap/dist/css/bootstrap.min.css')}}"/>
  <link href="{{asset('assets/crm/vendors/custom/owlcarousel/assets/owl.carousel.css')}}" rel="stylesheet" type="text/css" />
  <link href="{{asset('assets/crm/vendors/custom/owlcarousel/assets/owl.theme.default.min.css')}}" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" href="{{asset('node_modules/swiper/dist/css/swiper.min.css')}}" type="text/css"/>
  <style>
  body {
    background: rgb(240, 153, 172) no-repeat center center fixed;
    -webkit-background-size: cover;
    -moz-background-size: cover;
    background-size: cover;
    -o-background-size: cover;
  }
  body, wrapper {
     min-height:100vh;
  }
  .bg-t-pink {
    background-color : rgb(240, 153, 172) !important;
  }
  .navbar .divider-vertical {
    height: auto;
    margin:10px 0px 10px 0px;px;
    border-right: 1px solid #ffffff;
    border-left: 1px solid #f2f2f2;
  }
  .navbar-inverse .divider-vertical {
    border-right-color: #222222;
    border-left-color: #111111;
  }
  @media (max-width: 767px) {
    .navbar-collapse .nav > .divider-vertical {
      display: none;
     }
  }
  .btn-col-pink {
    color: #FFFFFF;
    background-color: #FF5891;
    border-color: #FF586E;
  }
  .btn-col-pink:hover,
  .btn-col-pink:focus,
  .btn-col-pink:active,
  .btn-col-pink.active,
  .open .dropdown-toggle.btn-col-pink {
    color: #FFFFFF;
    background-color: #FF586E;
    border-color: #FF586E;
  }
  .btn-col-pink:active,
  .btn-col-pink.active,
  .open .dropdown-toggle.btn-col-pink {
    background-image: none;
  }
  .btn-col-pink.disabled,
  .btn-col-pink[disabled],
  fieldset[disabled] .btn-col-pink,
  .btn-col-pink.disabled:hover,
  .btn-col-pink[disabled]:hover,
  fieldset[disabled] .btn-col-pink:hover,
  .btn-col-pink.disabled:focus,
  .btn-col-pink[disabled]:focus,
  fieldset[disabled] .btn-col-pink:focus,
  .btn-col-pink.disabled:active,
  .btn-col-pink[disabled]:active,
  fieldset[disabled] .btn-col-pink:active,
  .btn-col-pink.disabled.active,
  .btn-col-pink[disabled].active,
  fieldset[disabled] .btn-col-pink.active {
    background-color: #FF5891;
    border-color: #FF586E;
  }
  .btn-col-pink .badge {
    color: #FF5891;
    background-color: #FFFFFF;
  }
  .swiper-slogan{
    padding-left: 100px;
    padding-right: 100px;
  }
  .font-25 {
    font-size: 25px;
  }

  .owl-carousel{
    position: relative;
  }
  .owl-dots{
    position: absolute;
    top:90%;
    left:25%;
    right:25%;
  }

  .owl-prev {
    top: 30%;
    width: 30px;
    height: 30px;
    position: absolute;
    display: block !important;
    border-radius: 50% !important;
    background-color: white !important;

  }
  .owl-next {
    top: 30%;
    width: 30px;
    height: 30px;
    position: absolute;
    display: block !important;
    border-radius: 50% !important;
    background-color: white !important;
  }
  .owl-theme .owl-nav {
    margin-top:0px !important;
  }
  .owl-prev i, .owl-next i {
    color: #ccc;
  }
  .owl-prev, .owl-next {
    transition: visibility 0s, opacity 0.3s ease;
    visibility: hidden;
    opacity: 0;
  }
  .owl-carousel:hover .owl-nav .owl-prev {
    left: 1%;
    visibility: visible;
    opacity: 1;
  }
  .owl-carousel:hover .owl-nav .owl-next {
    right: 1%;
    visibility: visible;
    opacity: 1;
  }
  /* .absolute{
    position: relative;
    height:65%;
    top:130px;
    right: 7%;
    left: 35%;
  } */
  /* Custom, iPhone Retina */
  @media only screen and (min-width : 320px) {
    .swiper-container {
      width: 100%;
      height: 250px;
      margin-left: auto;
      margin-right: auto;
      margin-bottom: 15px;
    }
    .swiper-slide {
      text-align: center;
      font-size: 18px;
      background: #fff;
      display: -webkit-box;
      display: -ms-flexbox;
      display: -webkit-flex;
      display: flex;
      -webkit-box-pack: center;
      -ms-flex-pack: center;
      -webkit-justify-content: center;
      justify-content: center;
      -webkit-box-align: center;
      -ms-flex-align: center;
      -webkit-align-items: center;
      align-items: center;
    }
    .owl-height-max {
      height: 150px !important;
    }
    .navbar {
      background-image: -webkit-linear-gradient(left, rgba(255,255,255,0) 0, rgba(255,255,255,0.4) 100%);
      background-image: -moz-linear-gradient(to right, rgba(255,255,255,0) 0, rgba(255,255,255,0.4) 100%);
      background-image: linear-gradient(to right, rgba(255,255,255,0.0) 0, rgba(255,255,255,0.3) 100%);
      background-position: 50% 50%;
      -webkit-background-origin: padding-box;
      background-origin: padding-box;
      -webkit-background-clip: border-box;
      background-clip: border-box;
      -webkit-background-size: auto auto;
      background-size: auto auto;
    }
    .navbar li a {
      font-size: 20px;
      color: rgb(255, 255, 255) !important;
    }
    .navbar .nav-item {
      padding: 5px 10px 5px 10px;
    }
    .navbar ul.nav-left {
      /* margin-left: 0px; */
      /* margin-left: 100px; */
    }
    .navbar .nav-grouping {
      margin-right: 5px;
    }
    .img-logo {

    }

    .linear-color {
      background: linear-gradient(to bottom, rgb(240, 153, 172) 0%,rgb(240, 153, 172) 70%,#000000 30%,white 30%,white 100%) !important;
    }
  }
  /* Extra Small Devices, Phones */
  @media only screen and (min-width : 480px) {

  }
  /* Small Devices, Tablets */
  @media only screen and (min-width : 768px) {

  }
  /* Medium Devices, Desktops */
  @media only screen and (min-width : 992px) {
    .swiper-container {
      /* width: 650px;
      height: 250px; */
      width:75%;
      height:250px;
      margin-left: 0 !important;
      /* margin-right: 200px !important; */
      margin-bottom: 15px;
    }
    .swiper-slide {
      text-align: center;
      font-size: 18px;
      background: #fff;
      display: -webkit-box;
      display: -ms-flexbox;
      display: -webkit-flex;
      display: flex;
      -webkit-box-pack: center;
      -ms-flex-pack: center;
      -webkit-justify-content: center;
      justify-content: center;
      -webkit-box-align: center;
      -ms-flex-align: center;
      -webkit-align-items: center;
      align-items: center;
    }
    .owl-height-max {
      /* height: 100% !important; */
      height: 150px !important;
    }
    .navbar {
      background-image: -webkit-linear-gradient(left, rgba(255,255,255,0) 0, rgba(255,255,255,0.4) 100%);
      background-image: -moz-linear-gradient(to right, rgba(255,255,255,0) 0, rgba(255,255,255,0.4) 100%);
      background-image: linear-gradient(to right, rgba(255,255,255,0.0) 0, rgba(255,255,255,0.3) 100%);
      background-position: 50% 50%;
      -webkit-background-origin: padding-box;
      background-origin: padding-box;
      -webkit-background-clip: border-box;
      background-clip: border-box;
      -webkit-background-size: auto auto;
      background-size: auto auto;
    }
    .navbar li a {
      font-size: 20px;
      color: rgb(255, 255, 255) !important;
    }
    .navbar .nav-item {
      padding: 10px 10px 10px 10px;
    }
    .navbar .nav-item.grouping {
      padding: 0 20px 0 20px !important;
    }
    .navbar ul.nav-left {
      margin-left: auto;
      margin-right: 20px;
    }
    .navbar .nav-grouping {
      margin-right: 200px;
    }
    .img-logo {
       max-height:200px !important;
       margin-left: auto;
    }
    .linear-color {
      margin-top: -70px !important;
      background: linear-gradient(to bottom, rgb(240, 153, 172) 0%,rgb(240, 153, 172) 70%,#000000 30%,white 30%,white 100%) !important;
    }
  }
  /* Large Devices, Wide Screens */
  @media only screen and (min-width : 1200px) {

  }
  </style>
</head>
<body>
  <wrapper class="d-flex flex-column">
    <header>
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-3 my-0">
            <img class="img-fluid d-flex justify-content-center img-logo" src="{{asset('assets/cms/images/yana-valentina-logo-white.png')}}" alt="">
          </div>
          <!-- <div class="col-lg-2 my-0"></div> -->
          <div class="col-lg-9 my-4 px-0">
            <nav class="navbar navbar-expand-lg navbar-light">
              <button class="navbar-toggler" style="background-color:white !important;" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon" style="background-color:white !important;"></span>
              </button>
              <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="nav navbar-nav nav-left">
                  <li class="nav-item active">
                    <a class="nav-link" href="#">ABOUT US</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="#">CONTACT US</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="#">WHY US</a>
                  </li>
                </ul>
              </div>
              <div class="nav-grouping ml-auto">
                <p class="mb-1 text-white">Want to see my new collection?</p>
                <div class="row">
                  <a class="col-5 btn btn-col-pink mx-auto" href="{{url('register')}}">Register</a>
                  <a class="col-5 btn btn-col-pink mx-auto" href="{{url('login')}}">Login</a>
                </div>
              </div>
            </nav>
          </div>
        </div>
        <div class="row linear-color">
          <div class="col-lg-5 font-25 pb-3 my-auto">
            <p class="text-right m-1 text-danger">Make your day colorful</p>
            <p class="text-right m-1 text-danger">with <b>Yanavalentina</b> collection.</p>
          </div>
          <div class="col-lg-7">
            <div class="swiper-container">
              <div class="swiper-wrapper">
                @foreach ($promotions as $key => $promotion)
                  <div class="swiper-slide" style="background-image: url({{Storage::disk('public')->url($promotion->photo_url)}}); background-size: 100% 100%; background-repeat: no-repeat; background-position: center center;"></div>
                @endforeach
              </div>
              <div class="swiper-pagination"></div>
            </div>
          </div>
        </div>
      </div>
    </header>
    <div class="container-fluid flex-fill bg-white d-flex flex-column">
      <div class="row py-3 bg-t-pink flex-fill">
        <div class="col-lg-12 my-auto ">
          <div class="owl-carousel owl-theme owl-loaded owl-height-max" data-items="4" data-nav="true" data-dots="false" data-autoplay="true" data-autoplaytimeout="5000" data-autoplayhoverpause="true">
            <div class="owl-stage-outer owl-height-max">
              <div class="owl-stage owl-height-max">
                @foreach ($items as $key => $item)
                <div class="owl-item owl-height-max">
                  <div class="card owl-height-max">
                    <a href="{{'p/'.$item->link_url}}" class="owl-height-max">
                      <div class="owl-height-max" style="background: url({{Storage::disk('public')->url($item->item_images_primary->photo_url)}}) no-repeat;background-size: 100%;"></div>
                    </a>
                  </div>
                </div>
                @endforeach
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- <div class="absolute">
      <div class="swiper-container">
        <div class="swiper-wrapper">
          @foreach ($promotions as $key => $promotion)
            <div class="swiper-slide" style="background-image: url({{Storage::disk('public')->url($promotion->photo_url)}}); background-size: 100% 100%; background-repeat: no-repeat; background-position: center center; height: 334px ; margin-bottom: 30px;"></div>
          @endforeach
        </div>
        <div class="swiper-pagination"></div>
      </div>
    </div> -->
    <footer class="footer font-small" style="background-color:rgb(237, 137, 159)">
      <div class="footer-copyright text-center my-1">
        <p class="text-white">
          © 2018 Copyright: Ashland
        </p>
      </div>
    </footer>
  </wrapper>
  <script src="{{asset('node_modules/jquery/dist/jquery.slim.js')}}"></script>
  <script src="{{asset('node_modules/jquery/dist/jquery.js')}}"></script>
  <script src="{{asset('node_modules/tether/dist/js/tether.min.js')}}"></script>
  <script src="{{asset('node_modules/swiper/dist/js/swiper.min.js')}}"></script>
  <script src="{{asset('node_modules/bootstrap/dist/js/bootstrap.min.js')}}"></script>
  <script src="{{asset('assets/crm/vendors/custom/owlcarousel/owl.carousel.js')}}" type="text/javascript"></script>
  <!-- <script src="{{asset('assets/cms/js/main.js')}}"></script> -->
  <script>
    $(document).ready(function() {
      var swiper = new Swiper('.swiper-container', {
        direction: 'vertical',
        slidesPerView: 1,
        spaceBetween: 30,
        mousewheel: true,
        pagination: {
          el: '.swiper-pagination',
          clickable: true,
        },
      });

      $('.owl-carousel').each(function() {
        $items = 3;
        $items = $(this).data('items');
        $nav = $(this).data('nav');
        $dots = $(this).data('dots');
        $autoplay = $(this).data('autoplay');
        $autoplayTimeout = $(this).data('autoplaytimeout');
        $autoplayHoverPause = $(this).data('autoplayhoverpause');
        $(this).owlCarousel({
          loop: true,
          items : $items,
          stagePadding: 80,
          dots : $dots,
          margin: 10,
          nav : $nav,
          autoplay : $autoplay,
          autoplayTimeout: $autoplayTimeout,
          autoplayHoverPause : $autoplayHoverPause,
          navText : ["<i><</i>","<i>></i>"],
          responsiveClass: true,
          responsive: {
            0 : {
                items : 1,
                nav : $nav,
                stagePadding: 0,
            },
            1000 : {
                items : $items,
                nav : true,
                loop : true
            }
          },
        });
      });
    })
  </script>
</body>
</html>
