<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Yana Valentina</title>
  <link rel="shortcut icon" href="{{asset('assets/logo/favicon.ico')}}" type="image/x-icon">
  <!--build:css css/styles.min.css-->
  <link rel="stylesheet" href="{{asset('assets/cms/css/styles.css')}}">
  <link rel="stylesheet" href="{{asset('assets/cms/lib/bootstrap/dist/css/bootstrap.min.css')}}" />
  <!--endbuild-->
</head>

<body>
  <div class="site-wrapper">
    <div class="yv-body-bg">
      <div class="yv-master-body">
        <div class="container-fluid yv-master-head">
          <div class="row">
            <div class="col-md-3">
              <div class="yv-logo-master">
                <img src="{{asset('assets/cms/images/yana-valentina-logo-white.png')}}" alt="">
              </div>
            </div>
            <div class="col-md-9">
              <div class="yv-master-menu">
                <div class="row">
                  <div class="col-md-7">
                    <div class="yv-menu">
                      <a href="javascript:void(0);">About Us</a>
                      <a href="javascript:void(0);">Contact Us</a>
                      <a href="javascript:void(0);">Why Us</a>
                    </div>
                  </div>
                  <div class="col-md-5">
                    <div class="yv-option-menu">
                      <p>Want to see my new collection ?</p>
                      <a href="{{url('register')}}">Register</a>
                      <a href="{{url('login')}}">Login</a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="container-fluid yv-master-slogan">
          <div class="row">
            <div class="col-md-4">
              <div class="yv-slogan">
                <p>Make your colorfull day's</p>
                <p>with <b>YANA VALENTINA</b> collection</p>
              </div>
            </div>
            <div class="col-md-8">
              <div class="yv-master-poster">
                @foreach ($promotions as $key => $promotion)
                <img src="{{Storage::disk('public')->url($promotion->photo_url)}}" alt="">
                @endforeach
              </div>
            </div>
          </div>
        </div>
        <div class="container">
          <div class="row">
            <div class="MultiCarousel" data-items="1,3,5,6" data-slide="1" id="MultiCarousel" data-interval="1000">
              <div class="MultiCarousel-inner">
                @foreach ($items as $key => $item)
                <div class="item">
                  <div>
                    <a href="{{'p/'.$item->link_url}}">
                      <img style="max-width:100px; height:80px !important;" src="{{Storage::disk('public')->url($item->item_images_primary->photo_url)}}" alt="{{$item->name}}">
                    </a>
                  </div>
                </div>
                @endforeach
              </div>
              <button class="btn btn-primary leftLst">
                <</button> <button class="btn btn-primary rightLst">>
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!--build:js js/main.min.js -->
  <script src="{{asset('assets/cms/lib/jquery/dist/jquery.js')}}"></script>
  <script src="{{asset('assets/cms/lib/bootstrap/dist/js/bootstrap.min.js')}}"></script>
  <script src="{{asset('assets/cms/js/main.js')}}"></script>
  <!-- endbuild -->
</body>

</html>
