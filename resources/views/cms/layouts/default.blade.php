<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Yana Valentina</title>
  <link rel="shortcut icon" href="{{asset('assets/logo/favicon.ico')}}" type="image/x-icon">
  <!--build:css css/styles.min.css-->
  <link rel="stylesheet" href="{{asset('assets/cms/css/styles.min.css')}}">
  <!--endbuild-->
</head>
<body>
  <div id="bg">
    <img src="{{asset('assets/cms/images/yana-valentina-background.jpg')}}" alt="">
  </div>
  <div class="main-content">
    <nav class="navbar navbar-default">
      <div class="container">
        <div class="navbar-header">
          <a class="navbar-brand" href="#">
            <img src="{{asset('assets/cms/images/yana-valentina-logo-white.png')}}" alt="">
          </a>
        </div>
        <ul class="nav navbar-nav navbar-right">
          <li class="li-md-menu">
            <div class="md-menu">
              <a href="#">About Us</a>
              <a href="#">Contact Us</a>
              <a href="#">Why Us</a>
            </div>
          </li>
          <li><a href="#">ABOUT US</a></li>
          <li><a href="#">CONTACT US</a></li>
          <li><a href="#">WHY US</a></li>
          <li class="li-in-up">
            <div class="in-up">
              <p>Want to see my new collection ?</p>
              <a href="{{url('register')}}">Register</a>
              <a href="{{url('login')}}">Login</a>
            </div>
          </li>
        </ul>
      </div>
    </nav>
    <div class="headline-content">
      <div class="container">
        <div class="row">
          <div class="col-md-5">
            <div class="head-tagline text-right">
              <p>Make your colorfull day's <br> with <strong>YANA VALENTINA</strong> collection</p>
            </div>
          </div>
          <div class="col-md-7">
            <div class="headline-slide">
              <div class="swiper-container swiper-container-vertical">
                <div class="swiper-wrapper">
                  @foreach ($promotions as $key => $promotion)
                    <div class="swiper-slide" style="background-image: url({{Storage::disk('public')->url($promotion->photo_url)}}); background-size: 100% 100%; background-repeat: no-repeat; background-position: center center; height: 334px; margin-bottom: 30px;"></div>
                  @endforeach
                </div>
                <div class="swiper-pagination"></div>
              </div>
            </div>
            <div class="head-tagline-md text-right">
              <p>Make your colorfull day's <br> with <strong>YANA VALENTINA</strong> collection</p>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="slide-content">
      <div class="container">
        <div class="slick-item">
          @foreach ($items as $key => $item)
            <div><a href="{{'p/'.$item->link_url}}"><img style="width:auto; height:80px !important;" src="{{Storage::disk('public')->url($item->item_images_primary->photo_url)}}" alt="{{$item->name}}"></a></div>
          @endforeach
        </div>
      </div>
    </div>
  </div>
  <script src="{{asset('assets/cms/js/main.min.js')}}"></script>
</body>

</html>
