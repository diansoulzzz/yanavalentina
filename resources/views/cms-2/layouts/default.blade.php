<!DOCTYPE html>
<html lang="en" id="top" class="no-js">
  <head>
      <title>@yield('title') | Yana Valentina</title>
      <meta name="csrf-token" content="{{ csrf_token() }}">
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <meta name="viewport" content="width=device-width, initial-scale=1" />
      <meta name="description" content="Ultimo is a responsive Magento theme, extremely customizable, easy to use. Great as a starting point for your custom projects and suitable for every type of store." />
      <meta name="keywords" content="magento, theme, themes, magento premium theme, magento template, responsive theme" />
      <meta name="robots" content="INDEX,FOLLOW" />
      @stack('a-css')
      @include('cms-2.includes.css')
      @stack('b-css')
  </head>
  <body class="cms-index-index responsive cms-home">
     <div id="root-wrapper">
        <div class="wrapper">
           <noscript>
              <div class="global-site-notice noscript">
                 <div class="notice-inner">
                    <p>
                       <strong>JavaScript seems to be disabled in your browser.</strong><br />
                       You must have JavaScript enabled in your browser to utilize the functionality of this website.
                    </p>
                 </div>
              </div>
           </noscript>
           <div class="page">
              @include('cms-2.includes.header')
              @yield('content')
              @include('cms-2.includes.footer')

              @stack('a-script')
              @include('cms-2.includes.script')
              @stack('b-script')
           </div>
        </div>
     </div>
  </body>
</html>
