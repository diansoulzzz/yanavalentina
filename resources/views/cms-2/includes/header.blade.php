<div id="header-container" class="header-container header-mobile" style="display:none;">
  <div class="header-container2">
     <div class="header-container3">
        <div class="header-m-container">
           <div class="header-m-primary-container">
              <div class="header-m-primary header container">
                 <div class="inner-container">
                    <div class="logo-wrapper--mobile">
                       <a class="logo logo--mobile" href="index.html" title="Yanavalentina">
                         <img src="{{asset('assets/logo/logo-pk.png')}}" style="width:150px;height:150px" alt="Yanavalentina" />
                         <span class="color-pink wild-font font-lg">
                           <p>Yanavalentina.</br><small>Branded bags.</small></p>
                         </span>
                       </a>
                    </div>
                    <div class="clearer after-mobile-logo"></div>
                    <div class="skip-links-wrapper skip-links--2">
                       <a href="#header-nav" class="skip-link skip-nav">
                         <span class="icon ic ic-menu"></span>
                         <span class="label">Menu</span>
                       </a>
                       <a href="#header-account" class="skip-link skip-account">
                         <span class="icon ic ic-user"></span>
                         <span class="label">Account</span>
                       </a>
                       <div id="header-nav" class="skip-content skip-content--style">
                          <div id="nav-marker-mobile"></div>
                       </div>
                       <div id="account-links-marker-mobile"></div>
                       <div id="header-account" class="account-links top-links links-wrapper-separators-left skip-content skip-content--style">
                          <ul class="links">
                            <li style="margin-left:2px">
                              <a title="View Details" class="button" title="Masuk" href="{{url('login')}}">Masuk</a>
                            </li>
                            <li style="margin-left:2px">
                              <a title="View Details" class="button" title="Daftar" href="{{url('register')}}">Daftar</a>
                            </li>
                          </ul>
                       </div>
                       <div class="skip-links-clearer clearer"></div>
                    </div>
                 </div>
              </div>
           </div>
        </div>
        <div class="header-primary-container">
           <div class="header-primary header container">
              <div class="inner-container">
                 <div class="hp-blocks-holder">
                    <div class="hp-block left-column grid12-4">
                       <div class="item">
                          <div class="logo-wrapper logo-wrapper--regular">
                             <h1 class="logo logo--regular" style="vertical-align: middle!important; ">
                               <strong>Yanavalentina</strong>
                               <a href="{{url('')}}" title="Yanavalentina">
                                 <div class="box color-pink">
                                    <img src="{{asset('assets/logo/logo-pk.png')}}" style="width:120px;height:120px;" alt="Yanavalentina" />
                                    <span class="color-pink wild-font font-xl">
                                      <p>Yanavalentina.</br><small>Branded bags.</small></p>
                                    </span>
                                  </div>
                               </a>
                          </div>
                       </div>
                    </div>
                    <div class="hp-block central-column grid12-4">
                       <div class="item">
                          <div id="search-marker-regular"></div>
                       </div>
                    </div>
                    <div class="hp-block right-column grid12-4">
                       <div class="item">
                          <div id="user-menu-wrapper-regular">
                             <div id="user-menu" class="user-menu">
                                <div id="mini-cart-marker-regular"></div>
                                <div id="mini-compare-marker-regular"></div>
                                <div id="account-links-marker-regular"></div>
                             </div>
                          </div>
                       </div>
                    </div>
                 </div>
              </div>
           </div>
        </div>
        <div class="nav-container skip-content sticky-container">
           <div class="nav container clearer">
              <div class="inner-container">
                 <div class="navi-wrapper">
                    <ul class="nav-holders-wrapper">
                       <li id="nav-holder1" class="nav-item level0 level-top nav-holder"></li>
                       <li id="nav-holder2" class="nav-item level0 level-top nav-holder"></li>
                       <li id="nav-holder3" class="nav-item level0 level-top nav-holder"></li>
                    </ul>
                    <div class="mobnav-trigger menu-trigger">
                       <div class="menu-trigger-inner">
                          <span class="trigger-icon"><span class="line"></span><span class="line"></span><span class="line"></span></span>
                          <span class="label">Menu</span>
                       </div>
                    </div>
                    <div id="nav-marker-regular"></div>
                    <nav id="mainmenu" class="navi nav-regular opt-fx-fade-inout opt-sb0 opt-sob opt-hide480 with-bullets">
                       <ul>
                          <li class="nav-item nav-item--home level0 level-top active">
                             <a class="level-top" href="index.html"><span>Home</span></a>
                          </li>
                          <li class="nav-item level0 level-top">
                             <a class="level-top" href="about-us-page.html" title="About Us"><span>About Us</span></a>
                          </li>
                          <li class="nav-item level0 level-top">
                             <a class="level-top" href="about-us-page.html" title="About Us"><span>Why Us</span></a>
                          </li>
                       </ul>
                    </nav>
                    <div class="nav-border-bottom"></div>
                 </div>
              </div>
           </div>
        </div>
     </div>
  </div>
</div>
