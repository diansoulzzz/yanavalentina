<script type="text/javascript" src="{{asset('assets/cms-2/media/js/script.min.js')}}"></script>

<script type="text/javascript">
   //<![CDATA[
   optionalZipCountries = ["HK","IE","MO","PA"];
   //]]>
</script>
<script type="text/javascript">//<![CDATA[
   var Translator = new Translate([]);
   //]]>
</script>


<script type="text/javascript">
   //<![CDATA[

       var gridItemsEqualHeightApplied = false;
   function setGridItemsEqualHeight($)
   {
     var $list = $('.category-products-grid');
     var $listItems = $list.children();

     var centered = $list.hasClass('centered');
     var gridItemMaxHeight = 0;
     $listItems.each(function() {

       $(this).css("height", "auto"); 			var $object = $(this).find('.actions');

             if (centered)
       {
         var objectWidth = $object.width();
         var availableWidth = $(this).width();
         var space = availableWidth - objectWidth;
         var leftOffset = space / 2;
         $object.css("padding-left", leftOffset + "px"); 			}

             var bottomOffset = parseInt($(this).css("padding-top"));
       if (centered) bottomOffset += 10;
       $object.css("bottom", bottomOffset + "px");

             if ($object.is(":visible"))
       {
                 var objectHeight = $object.height();
         $(this).css("padding-bottom", (objectHeight + bottomOffset) + "px");
       }


       gridItemMaxHeight = Math.max(gridItemMaxHeight, $(this).height());
     });

     //Apply max height
     $listItems.css("height", gridItemMaxHeight + "px");
     gridItemsEqualHeightApplied = true;

   }



   jQuery(function($) {

          // Drop-down
          var ddBlockSelector = '.dropdown';
          var ddOpenTimeout;
          var dMenuPosTimeout;
          var DD_DELAY_IN = 200;
          var DD_DELAY_OUT = 0;
          var DD_ANIMATION_IN = 0;
          var DD_ANIMATION_OUT = 0;

          $(document).on('mouseenter touchstart', ddBlockSelector, function(e) {

              var dd = $(this);
              var ddHeading = dd.children('.dropdown-heading');
              var ddContent = dd.children('.dropdown-content');

              // If dd is not opened yet (or not initialized yet)
              var isDdOpened = dd.data('ddOpened');
              if (isDdOpened === false || isDdOpened === undefined)
              {
                  // Clear old position of dd menu
                  ddContent.css("left", "");
                  ddContent.css("right", "");

                  // Show dd menu
                  clearTimeout(ddOpenTimeout);
                  ddOpenTimeout = setTimeout(function() {

                      dd.addClass('open');

                      // Set dd open flag
                      dd.data('ddOpened', true);

                  }, DD_DELAY_IN);

                  ddContent.stop(true, true).delay(DD_DELAY_IN).fadeIn(DD_ANIMATION_IN, "easeOutCubic");

                  // Set new position of dd menu.
                  // This code is delayed the same amount of time as dd animation.
                  clearTimeout(dMenuPosTimeout);
                  dMenuPosTimeout = setTimeout(function() {

                      if (ddContent.offset().left < 0)
                      {
                          var space = dd.offset().left; // Space available on the left of dd
                          ddContent.css("left", (-1)*space);
                          ddContent.css("right", "auto");
                      }

                  }, DD_DELAY_IN);

              } // end: dd is not opened yet

          }).on('mouseleave', ddBlockSelector, function(e) {

              var dd = $(this);
              var ddContent = dd.children('.dropdown-content');

              clearTimeout(ddOpenTimeout); // Clear, to close dd on mouseleave
              ddContent.stop(true, true).delay(DD_DELAY_OUT).fadeOut(DD_ANIMATION_OUT, "easeInCubic");
              if (ddContent.is(":hidden"))
              {
                  ddContent.hide();
              }
              dd.removeClass('open');

              // Clear dd open flag
              dd.data('ddOpened', false);

              // After hiding, clear the click event flag
              dd.data('ddClickIntercepted', false);

          }).on('click', ddBlockSelector, function(e) {

              var dd = $(this);
              var ddHeading = dd.children('.dropdown-heading');
              var ddContent = dd.children('.dropdown-content');

              // Only if the heading was clicked
              if ($.contains(ddHeading[0], e.target) || ddHeading.is(e.target))
              {
                  // Only after the first click already happened, the second click can close the dropdown
                  if (dd.data('ddClickIntercepted'))
                  {
                      if (dd.hasClass('open'))
                      {
                          clearTimeout(ddOpenTimeout); // Clear, to close dd on mouseleave
                          ddContent.stop(true, true).delay(DD_DELAY_OUT).fadeOut(DD_ANIMATION_OUT, "easeInCubic");
                          if (ddContent.is(":hidden"))
                          {
                              ddContent.hide();
                          }
                          dd.removeClass('open');

                          // Clear dd open flag
                          dd.data('ddOpened', false);

                          // After hiding, clear the click event flag
                          dd.data('ddClickIntercepted', false);
                      }
                  }
                  else
                  {
                      // Set the click event flag
                      dd.data('ddClickIntercepted', true);
                  }
              }

          });



         var windowScroll_t;
     $(window).scroll(function(){

       clearTimeout(windowScroll_t);
       windowScroll_t = setTimeout(function() {

         if ($(this).scrollTop() > 100)
         {
           $('#scroll-to-top').fadeIn();
         }
         else
         {
           $('#scroll-to-top').fadeOut();
         }

       }, 500);

     });

     $('#scroll-to-top').click(function(){
       $("html, body").animate({scrollTop: 0}, 600, "easeOutCubic");
       return false;
     });




       var startHeight;
       var bpad;
       $('.category-products-grid').on('mouseenter', '.item', function() {

                             if ($(window).width() >= 320)
           {

                       if (gridItemsEqualHeightApplied === false)
             {
               return false;
             }

           startHeight = $(this).height();
           $(this).css("height", "auto"); //Release height
           $(this).find(".display-onhover").fadeIn(400, "easeOutCubic"); //Show elements visible on hover
           var h2 = $(this).height();

                     ////////////////////////////////////////////////////////////////
           var addtocartHeight = 0;
           var addtolinksHeight = 0;


                       var addtolinksEl = $(this).find('.add-to-links');
             if (addtolinksEl.hasClass("addto-onimage") == false)
               addtolinksHeight = addtolinksEl.innerHeight(); //.height();

                       var h3 = h2 + addtocartHeight + addtolinksHeight;
             var diff = 0;
             if (h3 < startHeight)
             {
               $(this).height(startHeight);
             }
             else
             {
               $(this).height(h3); 							diff = h3 - startHeight;
             }
                     ////////////////////////////////////////////////////////////////

           $(this).css("margin-bottom", "-" + diff + "px");
                   }
       }).on('mouseleave', '.item', function() {

                           if ($(window).width() >= 320)
           {

           //Clean up
           $(this).find(".display-onhover").stop(true).hide();
           $(this).css("margin-bottom", "");

                                 $(this).height(startHeight);

                   }
       });




         $('.products-grid, .products-list').on('mouseenter', '.product-image-wrapper', function() {
       $(this).find(".alt-img").fadeIn(400, "easeOutCubic");
     }).on('mouseleave', '.product-image-wrapper', function() {
       $(this).find(".alt-img").stop(true).fadeOut(400, "easeOutCubic");
     });



         $('.fade-on-hover').on('mouseenter', function() {
       $(this).animate({opacity: 0.75}, 300, 'easeInOutCubic');
     }).on('mouseleave', function() {
       $(this).stop(true).animate({opacity: 1}, 300, 'easeInOutCubic');
     });



         var dResize = {

       winWidth : 0
       , winHeight : 0
       , windowResizeTimeout : null

       , init : function()
       {
         dResize.winWidth = $(window).width();
         dResize.winHeight = $(window).height();
         dResize.windowResizeTimeout;

         $(window).on('resize', function(e) {
           clearTimeout(dResize.windowResizeTimeout);
           dResize.windowResizeTimeout = setTimeout(function() {
             dResize.onEventResize(e);
           }, 50);
         });
       }

       , onEventResize : function(e)
       {
         //Prevent from executing the code in IE when the window wasn't actually resized
         var winNewWidth = $(window).width();
         var winNewHeight = $(window).height();

         //Code in this condition will be executed only if window was actually resized
         if (dResize.winWidth != winNewWidth || dResize.winHeight != winNewHeight)
         {
           //Trigger deferred resize event
           $(window).trigger("themeResize", e);

           //Additional code executed on deferred resize
           dResize.onEventDeferredResize();
         }

         //Update window size variables
         dResize.winWidth = winNewWidth;
         dResize.winHeight = winNewHeight;
       }

       , onEventDeferredResize : function() //Additional code, execute after window was actually resized
       {
         //Products grid: equal height of items
                   setGridItemsEqualHeight($);

       }

     }; //end: dResize

     dResize.init();



   });

   jQuery(window).load(function(){
               setGridItemsEqualHeight(jQuery);
   });
</script>
<script type="text/javascript">
   //<![CDATA[

       var theHeaderContainer = jQuery('#header-container');


           var smartHeaderSettings = {
               cartBlockSelector: '#mini-cart'
               , dropdownBlockClass: 'dropdown'
           };
           theHeaderContainer.smartheader(smartHeaderSettings);


       jQuery(function($) {


               //Skip Links
               var skipContents = $('.skip-content');
               var skipLinks = $('.skip-link');
               skipLinks.on('click', function (e) {
                   e.preventDefault();

                   var self = $(this);
                   var target = self.attr('href');

                   //Get target element
                   var elem = $(target);

                   //Check if stub is open
                   var isSkipContentOpen = elem.hasClass('skip-active') ? 1 : 0;

                   //Hide all stubs
                   skipLinks.removeClass('skip-active');
                   skipContents.removeClass('skip-active');

                   //Toggle stubs
                   if (isSkipContentOpen) {
                       self.removeClass('skip-active');
                   } else {
                       self.addClass('skip-active');
                       elem.addClass('skip-active');
                   }
               });



               var stickyHeaderSettings = {
                   stickyThreshold: 960                , cartBlockSelector: '#mini-cart'
               };
               theHeaderContainer.stickyheader(stickyHeaderSettings);


       }); //end: on document ready

   //]]>
</script>
<script type="text/javascript">
   //<![CDATA[

       //Expose the header container
       var jsHeaderContainerObject = document.getElementById("header-container");
       if (jsHeaderContainerObject.style.display == 'none')
       {
           jsHeaderContainerObject.style.display = "block";
           jsHeaderContainerObject.classList.add("js-shown"); ///
       }

   //]]>
</script>
<!--
<script type="text/javascript">
   //<![CDATA[
       var newsletterSubscriberFormDetail = new VarienForm('newsletter-validate-detail');
       new Varien.searchForm('newsletter-validate-detail', 'newsletter', 'Enter your email address');
   //]]>
</script>


<script type="text/javascript">
   //<![CDATA[
       var searchForm = new Varien.searchForm('search_mini_form', 'search', 'Search entire store here...');
       searchForm.initAutocomplete('catalogsearch/ajax/suggest/index.html', 'search_autocomplete');
   //]]>
</script> -->

<script type="text/javascript">
   //<![CDATA[
       jQuery(function($) {

           var owl = $('#itemslider-featured-b4535c7fdfcdb78d8361e55e3f345f02');
           owl.owlCarousel({

                       lazyLoad: true,

                       itemsCustom: [ [0, 1], [480, 2], [768, 1], [960, 2] ],
               responsiveRefreshRate: 50,

                       slideSpeed: 200,

                       paginationSpeed: 500,


                       autoPlay: 4000,

                       stopOnHover: true,

                       rewindNav: true,
               rewindSpeed: 600,

                       pagination: false,

               navigation: true,
               navigationText: false

           }); //end: owl

       });
   //]]>
</script>

<script type="text/javascript">
   //<![CDATA[

       var topMenuContainer = jQuery('#mainmenu');
       var topMenuSettings = {
           mobileMenuThreshold: 960        , isVerticalLayout: false
           , mode: 0                , initVerticalMenuCollapsed: true        , outermostContainer: jQuery('.hp-blocks-holder')        , fullWidthDdContainer: jQuery('.hp-blocks-holder')    };
       var theTopMenu = topMenuContainer.ultramegamenu(topMenuSettings).data("infortis-ultramegamenu");
       theTopMenu.enableDropdowns();


           jQuery(function($) {

               var itemsList = topMenuContainer.children('ul');

               // Non-clickable links
               itemsList.on('click', '.no-click', function(e) {
                   e.preventDefault();
               });

           }); //end: on document ready

           jQuery(window).on("load", function() {

               var menubar = topMenuContainer;
               var isTouchDevice = ('ontouchstart' in window) || (navigator.msMaxTouchPoints > 0);
               if (isTouchDevice)
               {
                   menubar.on('click', 'a', function(e) {

                       var link = jQuery(this);
                       if (!menubar.hasClass('nav-mobile') && link.parent().hasClass('nav-item--parent'))
                       {
                           if (!link.hasClass('ready'))
                           {
                               e.preventDefault();
                               menubar.find('.ready').removeClass('ready');
                               link.parents('li').children('a').addClass('ready');
                           }
                       }

                   }); //end: on click
               } //end: if isTouchDevice

           }); //end: on load


   //]]>
</script>
