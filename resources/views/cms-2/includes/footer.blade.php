<div class="footer-container">
   <div class="footer-container2">
      <div class="footer-container3">
         <div class="footer-bottom-container section-container">
            <div class="footer-bottom footer container">
               <div class="inner-container">
                  <div class="clearer">
                     <!-- item-left -->
                     <div class="item a-right">
                        <div class="footer-copyright"><strong>Copyright &copy; 2018 Ashland</strong>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <a id="scroll-to-top" class="ic ic-up" href="#top"></a>
      </div>
   </div>
</div>
