@extends('cms-2.layouts.default')
@section('title', 'Home')
@section('content')
<div class="main-container col1-layout">
   <div class="main-top-container"></div>
   <div class="main container">
      <div class="inner-container">
         <div class="preface">
            <div class="slideshow-wrapper-additional" >
               <div class="slideshow-wrapper-outer">
                  <div class="slideshow-wrapper col-sm-9 no-gutter  slider-arrows2 slider-pagination2 pagination-pos-over-bottom-centered">
                     <div id="slideshow-276fd886786923f5e5b18bb85dce7cc4" class="slideshow owl-carousel">
                        <div class="item" >
                           <a href="#" title="Click to see all features">
                              <div class="ban">
                                 <img class="image" src="{{asset('assets/cms-2/media/wysiwyg/infortis/slideshow/a01.jpg')}}" alt="Customizable Magento Theme" />
                                 <div class="cap cap-top-right cap-push-down-10 cap-push-left-10 cap-no-bg cap-text-bg cap-text-bg-light-1">
                                    <h2 class="text" style="font-size: 36px;" data-animate-in="fadeInUp delay-0-5">Make your colorful day</h2>
                                    <p class="text" style="font-size: 18px;" data-animate-in="fadeInUp delay-1">with Yanavalentina</p>
                                 </div>
                              </div>
                           </a>
                        </div>
                        <div class="item">
                           <a href="#" title="Click to see all features">
                              <div class="ban ban-effect-1">
                                 <img class="image" src="{{asset('assets/cms-2/media/wysiwyg/infortis/slideshow/a02.jpg')}}" alt="Responsive Magento Theme" />
                                 <div class="cap cap-bottom-left cap-push-up-10 cap-push-right-10 cap-no-bg cap-text-bg cap-text-bg-dark-2">
                                    <h2 class="text" style="font-size: 36px;" data-animate-in="fadeInRight">Make your colorful day</h2>
                                    <p class="text" style="font-size: 18px;" data-animate-in="fadeInRight delay-0-5">with Yanavalentina</p>
                                 </div>
                              </div>
                           </a>
                        </div>
                        <div class="item">
                           <div class="ban ban-effect-1">
                              <img class="image" src="{{asset('assets/cms-2/media/wysiwyg/infortis/slideshow/a03.jpg')}}" alt="Sample Slide" />
                              <div class="cap cap-bottom cap-no-bg cap-push-up-10 cap-push-right-10">
                                 <a href="dress7.html" title="Click to see the product">
                                    <div class="feature feature-icon-hover indent indent-size-xl">
                                       <span class="ic ic-2x ic-heart-o ib ib-size-xl ib-ef-3 ib-ef-3a" data-animate-in="fadeInDown delay-1"></span>
                                       <h3 class="no-margin" style="color: #fff; font-size: 35px; line-height: 80px;" data-animate-in="fadeInDown">Super Promo</h3>
                                    </div>
                                 </a>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div id="slideshow-banners-276fd886786923f5e5b18bb85dce7cc4" class="slideshow-banners col-sm-3 no-right-gutter hidden-xs">
                     <div class="row row-bottom-gutter-half">
                        <div class="col-md-12 small-banner">
                           <a href="#" title="#">
                              <div class="ban ban-effect-3">
                                 <img class="image" src="{{asset('assets/cms-2/media/wysiwyg/infortis/slideshow/banners/a01.jpg')}}" alt="Banner" />
                                 <div class="cap cap-bottom-right cap-no-bg cap-text-bg cap-text-bg-light-1 hidden-sm" style="background-color: rgba(91, 210, 236, 0.85);">
                                    <h4 class="text">Right 1</h4>
                                 </div>
                              </div>
                           </a>
                        </div>
                        <div class="col-md-12 small-banner">
                           <a href="#">
                              <div class="ban ban-effect-3">
                                 <img class="image" src="{{asset('assets/cms-2/media/wysiwyg/infortis/slideshow/banners/a02.jpg')}}" alt="Banner" />
                                 <div class="cap cap-bottom-right cap-no-bg cap-text-bg cap-text-bg-dark-1 hidden-sm" style="background-color: rgba(245, 88, 86, 0.85);">
                                    <h4 class="text">Right 2</h4>
                                 </div>
                              </div>
                           </a>
                        </div>
                        <div class="col-md-12 small-banner">
                           <div class="ban ban-effect-3">
                              <img class="image" src="{{asset('assets/cms-2/media/wysiwyg/infortis/slideshow/banners/a03.jpg')}}" alt="Banner" />
                              <div class="cap cap-bottom-right cap-no-bg cap-text-bg cap-text-bg-light-1 hidden-sm" style="background-color: rgba(135, 195, 30, 0.85);">
                                 <h4 class="text">Right 3</h4>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div id="page-columns" class="columns">
            <div class="column-main">
               <div class="std">
                  <br/>
                  <strong class="section-title padding-right">Our Featured Products</strong>
                   <!-- slider-pagination1 -->
                  <div class="itemslider-wrapper slider-arrows1 slider-arrows1-pos-top-right">
                     <div id="itemslider-featured-4ad9e655957839bc161d16ef617fe17f" class="itemslider itemslider-responsive products-grid centered">
                        <div class="item">
                           <div class="product-image-wrapper" style="max-width:196px;">
                              <a href="top1.html" title="Product with Variants" class="product-image">
                              <img class="lazyOwl" data-src="http://ultimo.infortis-themes.com/demo/media/catalog/product/cache/1/small_image/196x/040ec09b1e35df139433887a97daa66f/3/b/3b.jpg" alt="Product with Variants" />
                              <img class="alt-img" src="../media/catalog/product/cache/1/small_image/196x/040ec09b1e35df139433887a97daa66f/3/b/3bb.jpg" alt="Product with Variants" />
                              </a>
                              <ul class="add-to-links clearer addto-links-icons addto-onimage visible-onhover">
                                 <li><a class="link-wishlist"
                                    href="wishlist/index/add/product/35/form_key/XlJE1UQDDgAjqV67/index.html"
                                    title="Add to Wishlist">
                                    <span class="2 icon ib ic ic-heart"></span>
                                    </a>
                                 </li>
                                 <li><a class="link-compare"
                                    href="catalog/product_compare/add/product/35/uenc/aHR0cDovL3VsdGltby5pbmZvcnRpcy10aGVtZXMuY29tL2RlbW8vZGVmYXVsdC8%2c/form_key/XlJE1UQDDgAjqV67/index.html"
                                    title="Add to Compare">
                                    <span class="2 icon ib ic ic-compare"></span>
                                    </a>
                                 </li>
                              </ul>
                           </div>
                           <!-- end: product-image-wrapper -->
                           <strong class="product-name"><a href="top1.html" title="Product with Variants">Product with Variants</a></strong>
                           <div class="ratings">
                              <div class="rating-box">
                                 <div class="rating" style="width:80%"></div>
                              </div>
                              <span class="amount">2 Review(s)</span>
                           </div>
                           <div class="price-box">
                              <span class="regular-price" id="product-price-354ad9e655957839bc161d16ef617fe17f">
                              <span class="price">$40.00</span>                                    </span>
                           </div>
                           <div class="actions">
                           </div>
                        </div>
                        <div class="item">
                           <div class="product-image-wrapper" style="max-width:196px;">
                              <a href="top9.html" title="alt" class="product-image">
                              <img class="lazyOwl" data-src="http://ultimo.infortis-themes.com/demo/media/catalog/product/cache/1/small_image/196x/040ec09b1e35df139433887a97daa66f/1/_/1_4.jpg" alt="alt" />
                              <img class="alt-img" src="../media/catalog/product/cache/1/small_image/196x/040ec09b1e35df139433887a97daa66f/1/_/1_4.jpg" alt="Configurable Product" />
                              </a>
                              <ul class="add-to-links clearer addto-links-icons addto-onimage visible-onhover">
                                 <li><a class="link-wishlist"
                                    href="wishlist/index/add/product/81/form_key/XlJE1UQDDgAjqV67/index.html"
                                    title="Add to Wishlist">
                                    <span class="2 icon ib ic ic-heart"></span>
                                    </a>
                                 </li>
                                 <li><a class="link-compare"
                                    href="catalog/product_compare/add/product/81/uenc/aHR0cDovL3VsdGltby5pbmZvcnRpcy10aGVtZXMuY29tL2RlbW8vZGVmYXVsdC8%2c/form_key/XlJE1UQDDgAjqV67/index.html"
                                    title="Add to Compare">
                                    <span class="2 icon ib ic ic-compare"></span>
                                    </a>
                                 </li>
                              </ul>
                           </div>
                           <!-- end: product-image-wrapper -->
                           <strong class="product-name"><a href="top9.html" title="Configurable Product">Configurable Product</a></strong>
                           <div class="ratings">
                              <div class="rating-box">
                                 <div class="rating" style="width:100%"></div>
                              </div>
                              <span class="amount">1 Review(s)</span>
                           </div>
                           <div class="price-box">
                              <span class="regular-price" id="product-price-814ad9e655957839bc161d16ef617fe17f">
                              <span class="price">$100.00</span>                                    </span>
                           </div>
                           <div class="actions">
                           </div>
                        </div>
                        <div class="item">
                           <div class="product-image-wrapper" style="max-width:196px;">
                              <a href="top3.html" title="Product Example" class="product-image">
                              <img class="lazyOwl" data-src="http://ultimo.infortis-themes.com/demo/media/catalog/product/cache/1/small_image/196x/040ec09b1e35df139433887a97daa66f/2/b/2b_2.jpg" alt="Product Example" />
                              <img class="alt-img" src="../media/catalog/product/cache/1/small_image/196x/040ec09b1e35df139433887a97daa66f/9/_/9_1.jpg" alt="Product Example" />
                              </a>
                              <ul class="add-to-links clearer addto-links-icons addto-onimage visible-onhover">
                                 <li><a class="link-wishlist"
                                    href="wishlist/index/add/product/37/form_key/XlJE1UQDDgAjqV67/index.html"
                                    title="Add to Wishlist">
                                    <span class="2 icon ib ic ic-heart"></span>
                                    </a>
                                 </li>
                                 <li><a class="link-compare"
                                    href="catalog/product_compare/add/product/37/uenc/aHR0cDovL3VsdGltby5pbmZvcnRpcy10aGVtZXMuY29tL2RlbW8vZGVmYXVsdC8%2c/form_key/XlJE1UQDDgAjqV67/index.html"
                                    title="Add to Compare">
                                    <span class="2 icon ib ic ic-compare"></span>
                                    </a>
                                 </li>
                              </ul>
                           </div>
                           <!-- end: product-image-wrapper -->
                           <strong class="product-name"><a href="top3.html" title="Product Example">Product Example</a></strong>
                           <div class="ratings">
                              <div class="rating-box">
                                 <div class="rating" style="width:80%"></div>
                              </div>
                              <span class="amount">1 Review(s)</span>
                           </div>
                           <div class="price-box">
                              <span class="regular-price" id="product-price-374ad9e655957839bc161d16ef617fe17f">
                              <span class="price">$99.00</span>                                    </span>
                           </div>
                           <div class="actions">
                           </div>
                        </div>
                        <div class="item">
                           <div class="product-image-wrapper" style="max-width:196px;">
                              <a href="top4.html" title="New Hot Top" class="product-image">
                              <img class="lazyOwl" data-src="http://ultimo.infortis-themes.com/demo/media/catalog/product/cache/1/small_image/196x/040ec09b1e35df139433887a97daa66f/4/_/4_3.jpg" alt="New Hot Top" />
                              <img class="alt-img" src="../media/catalog/product/cache/1/small_image/196x/040ec09b1e35df139433887a97daa66f/4/c/4c.jpg" alt="New Hot Top" />
                              </a>
                              <ul class="add-to-links clearer addto-links-icons addto-onimage visible-onhover">
                                 <li><a class="link-wishlist"
                                    href="wishlist/index/add/product/39/form_key/XlJE1UQDDgAjqV67/index.html"
                                    title="Add to Wishlist">
                                    <span class="2 icon ib ic ic-heart"></span>
                                    </a>
                                 </li>
                                 <li><a class="link-compare"
                                    href="catalog/product_compare/add/product/39/uenc/aHR0cDovL3VsdGltby5pbmZvcnRpcy10aGVtZXMuY29tL2RlbW8vZGVmYXVsdC8%2c/form_key/XlJE1UQDDgAjqV67/index.html"
                                    title="Add to Compare">
                                    <span class="2 icon ib ic ic-compare"></span>
                                    </a>
                                 </li>
                              </ul>
                           </div>
                           <!-- end: product-image-wrapper -->
                           <strong class="product-name"><a href="top4.html" title="New Hot Top">New Hot Top</a></strong>
                           <div class="ratings">
                              <div class="rating-box">
                                 <div class="rating" style="width:100%"></div>
                              </div>
                              <span class="amount">2 Review(s)</span>
                           </div>
                           <div class="price-box">
                              <span class="regular-price" id="product-price-394ad9e655957839bc161d16ef617fe17f">
                              <span class="price">$80.00</span>                                    </span>
                           </div>
                           <div class="actions">
                           </div>
                        </div>
                        <div class="item">
                           <div class="product-image-wrapper" style="max-width:196px;">
                              <a href="top5.html" title="Another Sample Top" class="product-image">
                              <img class="lazyOwl" data-src="http://ultimo.infortis-themes.com/demo/media/catalog/product/cache/1/small_image/196x/040ec09b1e35df139433887a97daa66f/6/_/6.jpg" alt="Another Sample Top" />
                              </a>
                              <ul class="add-to-links clearer addto-links-icons addto-onimage visible-onhover">
                                 <li><a class="link-wishlist"
                                    href="wishlist/index/add/product/40/form_key/XlJE1UQDDgAjqV67/index.html"
                                    title="Add to Wishlist">
                                    <span class="2 icon ib ic ic-heart"></span>
                                    </a>
                                 </li>
                                 <li><a class="link-compare"
                                    href="catalog/product_compare/add/product/40/uenc/aHR0cDovL3VsdGltby5pbmZvcnRpcy10aGVtZXMuY29tL2RlbW8vZGVmYXVsdC8%2c/form_key/XlJE1UQDDgAjqV67/index.html"
                                    title="Add to Compare">
                                    <span class="2 icon ib ic ic-compare"></span>
                                    </a>
                                 </li>
                              </ul>
                           </div>
                           <!-- end: product-image-wrapper -->
                           <strong class="product-name"><a href="top5.html" title="Another Sample Top">Another Sample Top</a></strong>
                           <div class="ratings">
                              <div class="rating-box">
                                 <div class="rating" style="width:80%"></div>
                              </div>
                              <span class="amount">1 Review(s)</span>
                           </div>
                           <div class="price-box">
                              <span class="regular-price" id="product-price-404ad9e655957839bc161d16ef617fe17f">
                              <span class="price">$110.00</span>                                    </span>
                           </div>
                           <div class="actions">
                           </div>
                        </div>
                        <div class="item">
                           <div class="product-image-wrapper" style="max-width:196px;">
                              <a href="top6.html" title="Next Sample Product" class="product-image">
                              <img class="lazyOwl" data-src="http://ultimo.infortis-themes.com/demo/media/catalog/product/cache/1/small_image/196x/040ec09b1e35df139433887a97daa66f/5/_/5.jpg" alt="Next Sample Product" />
                              </a>
                              <ul class="add-to-links clearer addto-links-icons addto-onimage visible-onhover">
                                 <li><a class="link-wishlist"
                                    href="wishlist/index/add/product/41/form_key/XlJE1UQDDgAjqV67/index.html"
                                    title="Add to Wishlist">
                                    <span class="2 icon ib ic ic-heart"></span>
                                    </a>
                                 </li>
                                 <li><a class="link-compare"
                                    href="catalog/product_compare/add/product/41/uenc/aHR0cDovL3VsdGltby5pbmZvcnRpcy10aGVtZXMuY29tL2RlbW8vZGVmYXVsdC8%2c/form_key/XlJE1UQDDgAjqV67/index.html"
                                    title="Add to Compare">
                                    <span class="2 icon ib ic ic-compare"></span>
                                    </a>
                                 </li>
                              </ul>
                           </div>
                           <!-- end: product-image-wrapper -->
                           <strong class="product-name"><a href="top6.html" title="Next Sample Product">Next Sample Product</a></strong>
                           <div class="ratings">
                              <div class="rating-box">
                                 <div class="rating" style="width:60%"></div>
                              </div>
                              <span class="amount">1 Review(s)</span>
                           </div>
                           <div class="price-box">
                              <span class="regular-price" id="product-price-414ad9e655957839bc161d16ef617fe17f">
                              <span class="price">$110.00</span>                                    </span>
                           </div>
                           <div class="actions">
                           </div>
                        </div>
                        <div class="item">
                           <div class="product-image-wrapper" style="max-width:196px;">
                              <a href="top7.html" title="Some Other Product" class="product-image">
                              <img class="lazyOwl" data-src="http://ultimo.infortis-themes.com/demo/media/catalog/product/cache/1/small_image/196x/040ec09b1e35df139433887a97daa66f/7/b/7b.jpg" alt="Some Other Product" />
                              </a>
                              <ul class="add-to-links clearer addto-links-icons addto-onimage visible-onhover">
                                 <li><a class="link-wishlist"
                                    href="wishlist/index/add/product/42/form_key/XlJE1UQDDgAjqV67/index.html"
                                    title="Add to Wishlist">
                                    <span class="2 icon ib ic ic-heart"></span>
                                    </a>
                                 </li>
                                 <li><a class="link-compare"
                                    href="catalog/product_compare/add/product/42/uenc/aHR0cDovL3VsdGltby5pbmZvcnRpcy10aGVtZXMuY29tL2RlbW8vZGVmYXVsdC8%2c/form_key/XlJE1UQDDgAjqV67/index.html"
                                    title="Add to Compare">
                                    <span class="2 icon ib ic ic-compare"></span>
                                    </a>
                                 </li>
                              </ul>
                           </div>
                           <!-- end: product-image-wrapper -->
                           <strong class="product-name"><a href="top7.html" title="Some Other Product">Some Other Product</a></strong>
                           <div class="ratings">
                              <div class="rating-box">
                                 <div class="rating" style="width:100%"></div>
                              </div>
                              <span class="amount">1 Review(s)</span>
                           </div>
                           <div class="price-box">
                              <span class="regular-price" id="product-price-424ad9e655957839bc161d16ef617fe17f">
                              <span class="price">$200.00</span>                                    </span>
                           </div>
                           <div class="actions">
                           </div>
                        </div>
                        <div class="item">
                           <div class="product-image-wrapper" style="max-width:196px;">
                              <a href="top8.html" title="Simple Woman Top" class="product-image">
                              <img class="lazyOwl" data-src="http://ultimo.infortis-themes.com/demo/media/catalog/product/cache/1/small_image/196x/040ec09b1e35df139433887a97daa66f/8/_/8_1.jpg" alt="Simple Woman Top" />
                              </a>
                              <ul class="add-to-links clearer addto-links-icons addto-onimage visible-onhover">
                                 <li><a class="link-wishlist"
                                    href="wishlist/index/add/product/43/form_key/XlJE1UQDDgAjqV67/index.html"
                                    title="Add to Wishlist">
                                    <span class="2 icon ib ic ic-heart"></span>
                                    </a>
                                 </li>
                                 <li><a class="link-compare"
                                    href="catalog/product_compare/add/product/43/uenc/aHR0cDovL3VsdGltby5pbmZvcnRpcy10aGVtZXMuY29tL2RlbW8vZGVmYXVsdC8%2c/form_key/XlJE1UQDDgAjqV67/index.html"
                                    title="Add to Compare">
                                    <span class="2 icon ib ic ic-compare"></span>
                                    </a>
                                 </li>
                              </ul>
                           </div>
                           <!-- end: product-image-wrapper -->
                           <strong class="product-name"><a href="top8.html" title="Simple Woman Top">Simple Woman Top</a></strong>
                           <div class="ratings">
                              <div class="rating-box">
                                 <div class="rating" style="width:100%"></div>
                              </div>
                              <span class="amount">1 Review(s)</span>
                           </div>
                           <div class="price-box">
                              <span class="regular-price" id="product-price-434ad9e655957839bc161d16ef617fe17f">
                              <span class="price">$39.50</span>                                    </span>
                           </div>
                           <div class="actions">
                           </div>
                        </div>
                     </div>
                     <!-- end: itemslider -->
                  </div>
               </div>
            </div>
         </div>
         <div class="postscript"></div>
      </div>
   </div>
   <div class="main-bottom-container"></div>
</div>
@endsection

@push('a-css')

@endpush

@push('b-script')
<script type="text/javascript">
   //<![CDATA[
       jQuery(function($) {

           var owl = $('#slideshow-276fd886786923f5e5b18bb85dce7cc4');
           owl.owlCarousel({

               navigationText: false
               , addClassActive: true

                       , singleItem: true

                               , transitionStyle: 'fadeUp'


                       , slideSpeed: 500
                       , paginationSpeed: 500
                       , autoPlay: 10000
                       , stopOnHover: true

                       , rewindNav: true
               , rewindSpeed: 600

                       , pagination: true

                       , navigation: true


               , beforeInit: function() {
                   var firstSlide = owl.find('.item:eq(0)');
                   firstSlide.find('[data-animate-in]').each(function() {
                       $(this).addClass($(this).data('animate-in') + ' animated');
                   });
               }

                           , afterMove: function() {
                   owl.find('.owl-item.active [data-animate-in]').each(function() {
                       $(this).addClass($(this).data('animate-in') + ' animated');
                   });
               }

               , beforeMove: function() {
                   owl.find('.owl-item.active [data-animate-in]').each(function() {
                       $(this).removeClass($(this).data('animate-in') + ' animated');
                   });
               }

                       , afterInit: function() {
                   owl.parent().parent().children('#slideshow-banners-276fd886786923f5e5b18bb85dce7cc4').addClass('_show');
               }

           }); // end: owl

       });
   //]]>
</script>

<script type="text/javascript">
   //<![CDATA[
       jQuery(function($) {

           var owl = $('#itemslider-featured-4ad9e655957839bc161d16ef617fe17f');
           owl.owlCarousel({

                       lazyLoad: true,

                       itemsCustom: [ [0, 1], [320, 2], [480, 3], [768, 4], [960, 5], [1280, 6] ],
               responsiveRefreshRate: 50,

                       slideSpeed: 200,

                       paginationSpeed: 500,

                       scrollPerPage: true,


                       stopOnHover: true,

                       rewindNav: true,
               rewindSpeed: 600,

                       pagination: true,

               navigation: true,
               navigationText: false

           }); //end: owl

       });
   //]]>
</script>

<script type="text/javascript">
   //<![CDATA[
       jQuery(function($) {

           var owl = $('#itemslider-featured-5f71739b4c83e563b86c6709478ffa57');
           owl.owlCarousel({

                       lazyLoad: true,

                       itemsCustom: [ [0, 1], [320, 2], [480, 3], [768, 2], [960, 2], [1280, 3] ],
               responsiveRefreshRate: 50,

                       slideSpeed: 200,

                       paginationSpeed: 500,

                       scrollPerPage: true,

                       autoPlay: 4000,

                       stopOnHover: true,

                       rewindNav: true,
               rewindSpeed: 600,

                       pagination: false,

               navigation: true,
               navigationText: false

           }); //end: owl

       });
   //]]>
</script>

<script type="text/javascript">
   //<![CDATA[
       jQuery(function($) {

           var owl = $('#itemslider-featured-3384e854b691b155139ffed34be9b2cf');
           owl.owlCarousel({

                       lazyLoad: true,

                       itemsCustom: [ [0, 1], [320, 2], [480, 3], [768, 2], [960, 2], [1280, 3] ],
               responsiveRefreshRate: 50,

                       slideSpeed: 200,

                       paginationSpeed: 500,

                       scrollPerPage: true,

                       autoPlay: 4000,

                       stopOnHover: true,

                       rewindNav: true,
               rewindSpeed: 600,

                       pagination: false,

               navigation: true,
               navigationText: false

           }); //end: owl

       });
   //]]>
</script>

<script type="text/javascript">
   //<![CDATA[
       jQuery(function($) {

           var owl = $('#itemslider-new');
           owl.owlCarousel({

                       lazyLoad: true,

                       itemsCustom: [ [0, 1], [320, 2], [480, 3], [768, 4], [960, 5], [1280, 6] ],
               responsiveRefreshRate: 50,

                       slideSpeed: 200,

                       paginationSpeed: 500,



                       stopOnHover: true,

                       rewindNav: true,
               rewindSpeed: 600,

                       pagination: true,

               navigation: true,
               navigationText: false

           }); //end: owl

       });
   //]]>
</script>

<script type="text/javascript">
   //<![CDATA[
     jQuery(function($) {

       var owl = $('#itemslider-brands-07b648afe672ff342f9fc3309b28b216');
       owl.owlCarousel({

             lazyLoad: true,

             itemsCustom: [ [0, 1], [320, 2], [480, 2], [768, 3], [960, 4], [1280, 5] ],
         responsiveRefreshRate: 50,

             slideSpeed: 200,

             paginationSpeed: 500,


             autoPlay: 6000,

             stopOnHover: true,

             rewindNav: true,
         rewindSpeed: 600,

             pagination: true,
         paginationSpeed: 600,

         navigation: true,
         navigationText: false

       }); //end: owl

     });
   //]]>
</script>
@endpush
