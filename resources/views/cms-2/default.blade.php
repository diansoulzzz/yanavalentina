<!DOCTYPE html>
            <html lang="en" id="top" class="no-js">
               <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
               <head>
                  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                  <title>Ultimo - Responsive Magento Theme</title>
                  <meta name="viewport" content="width=device-width, initial-scale=1" />
                  <meta name="description" content="Ultimo is a responsive Magento theme, extremely customizable, easy to use. Great as a starting point for your custom projects and suitable for every type of store." />
                  <meta name="keywords" content="magento, theme, themes, magento premium theme, magento template, responsive theme" />
                  <meta name="robots" content="INDEX,FOLLOW" />
                  <link rel="icon" href="http://ultimo.infortis-themes.com/demo/skin/frontend/ultimo/default/favicon.ico" type="image/x-icon" />
                  <link rel="shortcut icon" href="http://ultimo.infortis-themes.com/demo/skin/frontend/ultimo/default/favicon.ico" type="image/x-icon" />
                  <link rel="stylesheet" type="text/css" href="{{asset('assets/cms-2/media/css/6fd5ddf81581de0588bfb8ca14bc18a2.css')}}" media="all" />
                  <link rel="stylesheet" type="text/css" href="{{asset('assets/cms-2/media/f6e62d1da07bb38772c4bacfcef59296.css')}}" media="print" />
                  <script type="text/javascript" src="{{asset('assets/cms-2/media/js/1859c18e57ce1293df1cd2db41fc26d6.js')}}"></script>
                  <link href="rss/catalog/new/store_id/1/index.html" title="New Products" rel="alternate" type="application/rss+xml" />
                  <link href="rss/catalog/special/store_id/1/cid/0/index.html" title="Special Products" rel="alternate" type="application/rss+xml" />

                  <script type="text/javascript">
                     //<![CDATA[
                     Mage.Cookies.path     = '/demo';
                     Mage.Cookies.domain   = '.ultimo.infortis-themes.com';
                     //]]>
                  </script>
                  <script type="text/javascript">
                     //<![CDATA[
                     optionalZipCountries = ["HK","IE","MO","PA"];
                     //]]>
                  </script>
                  <style type="text/css">
                     .footer-container2
                     {
                     background-image: url(../media/wysiwyg/infortis/ultimo/_patterns/default/1.png);
                     }
                  </style>
                  <script type="text/javascript">//<![CDATA[
                     var Translator = new Translate([]);
                     //]]>
                  </script>
                  <link href='http://fonts.googleapis.com/css?family=Bitter&amp;subset=latin' rel='stylesheet' type='text/css' />
               </head>
               <body class=" cms-index-index responsive cms-home ">
                  <div id="root-wrapper">
                     <div class="wrapper">
                        <noscript>
                           <div class="global-site-notice noscript">
                              <div class="notice-inner">
                                 <p>
                                    <strong>JavaScript seems to be disabled in your browser.</strong><br />
                                    You must have JavaScript enabled in your browser to utilize the functionality of this website.
                                 </p>
                              </div>
                           </div>
                        </noscript>
                        <div class="page">
                           <div id="header-container" class="header-container header-mobile" style="display:none;">
                              <div class="header-container2">
                                 <div class="header-container3">
                                    <div class="header-m-container">
                                       <div class="header-m-top-container">
                                          <div class="header-m-top header container clearer">
                                             <div class="inner-container">
                                                <div class="widget widget-static-block">
                                                   <div class="item item-left">
                                                      <div class="links-wrapper-separators">
                                                         <ul class="links">
                                                            <li>
                                                               <a href="http://ultimo.infortis-themes.com/demo/select-demo/" title="See more demos">All demos</a>
                                                            </li>
                                                            <li class="hide-below-480">
                                                               <a href="http://themeforest.net/item/ultimo-fluid-responsive-magento-theme/3231798?ref=infortis" title="Click to buy this theme">Buy me!</a>
                                                            </li>
                                                         </ul>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div id="lang-switcher-wrapper-mobile" class="item item-right">
                                                   <div class="lang-switcher dropdown">
                                                      <a href="#" class="dropdown-heading cover">
                                                      <span>
                                                      <span class="label dropdown-icon flag"
                                                         style="background-image:url(../skin/frontend/ultimo/default/images/flags/default.png)">&nbsp;</span>
                                                      <span class="value">Demo1</span>
                                                      <span class="caret"></span>
                                                      </span>
                                                      </a>
                                                      <ul class="dropdown-content left-hand">
                                                         <li class="current"><span class="label dropdown-icon" style="background-image:url(../skin/frontend/ultimo/default/images/flags/default.png);">&nbsp;</span>Demo1</li>
                                                         <li><a href="http://ultimo.infortis-themes.com/demo/default_de/?___from_store=default"><span class="label dropdown-icon" style="background-image:url(../skin/frontend/ultimo/default/images/flags/default_de.png);">&nbsp;</span>German</a></li>
                                                      </ul>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="header-m-primary-container">
                                          <div class="header-m-primary header container">
                                             <div class="inner-container">
                                                <!-- Mobile logo -->
                                                <div class="logo-wrapper--mobile">
                                                   <a class="logo logo--mobile" href="index.html" title="Responsive Magento Theme">
                                                   <img src="../skin/frontend/ultimo/default/images/logo-1.png" alt="Responsive Magento Theme" />
                                                   </a>
                                                </div>
                                                <div class="clearer after-mobile-logo"></div>
                                                <!-- Skip links -->
                                                <div class="skip-links-wrapper skip-links--5">
                                                   <a href="#header-nav" class="skip-link skip-nav">
                                                   <span class="icon ic ic-menu"></span>
                                                   <span class="label">Menu</span>
                                                   </a>
                                                   <a href="#header-search" class="skip-link skip-search">
                                                   <span class="icon ic ic-search"></span>
                                                   <span class="label">Search</span>
                                                   </a>
                                                   <a href="#header-account" class="skip-link skip-account">
                                                   <span class="icon ic ic-user"></span>
                                                   <span class="label">Account</span>
                                                   </a>
                                                   <a href="#header-compare" class="skip-link skip-compare">
                                                   <span class="icon ic ic-compare"></span>
                                                   <span class="label">Compare</span>
                                                   </a>
                                                   <div id="mini-cart-marker-mobile"></div>
                                                   <div id="mini-cart" class="mini-cart dropdown is-empty">
                                                      <a href="#header-cart" class="mini-cart-heading dropdown-heading cover skip-link skip-cart">
                                                      <span>
                                                      <span class="icon ic ic-cart"></span>
                                                      <span class="label">Cart</span>
                                                      <span class="caret"></span>
                                                      </span>
                                                      </a> <!-- end: heading -->
                                                      <div id="header-cart" class="mini-cart-content dropdown-content left-hand block block block-cart skip-content skip-content--style">
                                                         <div class="block-content-inner">
                                                            <div class="empty">You have no items in your shopping cart.</div>
                                                         </div>
                                                         <!-- end: inner block -->
                                                      </div>
                                                      <!-- end: dropdown-content -->
                                                   </div>
                                                   <div id="header-nav" class="skip-content skip-content--style">
                                                      <div id="nav-marker-mobile"></div>
                                                   </div>
                                                   <div id="search-marker-mobile"></div>
                                                   <div id="header-search" class="skip-content skip-content--style">
                                                      <div class="search-wrapper">
                                                         <form id="search_mini_form" action="http://ultimo.infortis-themes.com/demo/default/catalogsearch/result/" method="get">
                                                            <div class="form-search">
                                                               <label for="search">Search:</label>
                                                               <input id="search" type="text" name="q" value="" class="input-text" maxlength="128" />
                                                               <button type="submit" title="Search" class="button"><span><span>Search</span></span></button>
                                                               <div id="search_autocomplete" class="search-autocomplete"></div>
                                                               <script type="text/javascript">
                                                                  //<![CDATA[
                                                                      var searchForm = new Varien.searchForm('search_mini_form', 'search', 'Search entire store here...');
                                                                      searchForm.initAutocomplete('catalogsearch/ajax/suggest/index.html', 'search_autocomplete');
                                                                  //]]>
                                                               </script>
                                                            </div>
                                                         </form>
                                                      </div>
                                                   </div>
                                                   <div id="account-links-marker-mobile"></div>
                                                   <div id="header-account" class="account-links top-links links-wrapper-separators-left skip-content skip-content--style">
                                                      <ul class="links">
                                                         <li class="first" ><a href="customer/account/index.html" title="Account" >Account</a></li>
                                                         <li ><a href="wishlist/index.html" title="Wishlist" >Wishlist</a></li>
                                                         <li class=" last" ><a href="customer/account/login/index.html" title="Log In" >Log In</a></li>
                                                      </ul>
                                                   </div>
                                                   <div id="mini-compare-marker-mobile"></div>
                                                   <div id="mini-compare" class="mini-compare dropdown is-empty">
                                                      <div class="mini-compare-heading dropdown-heading cover skip-link skip-compare" title="You have no items to compare.">
                                                         <span>
                                                         <span class="icon ic ic-compare"></span>
                                                         <span class="label">Compare</span>
                                                         <span class="caret"></span>
                                                         </span>
                                                      </div>
                                                      <div id="header-compare" class="mini-compare-content dropdown-content left-hand block skip-content skip-content--style">
                                                         <div class="empty">You have no items to compare.</div>
                                                      </div>
                                                      <!-- end: dropdown-content -->
                                                   </div>
                                                   <div class="skip-links-clearer clearer"></div>
                                                </div>
                                                <!-- end: skip-links-wrapper -->
                                             </div>
                                             <!-- end: inner-container -->
                                          </div>
                                          <!-- end: header-m-primary -->
                                       </div>
                                    </div>
                                    <div class="header-top-container">
                                       <div class="header-top header container clearer">
                                          <div class="inner-container">
                                             <div class="left-column">
                                                <div class="item item-left block_header_top_left">
                                                   <div class="hide-below-960" title="You can put here a phone number or some additional help info"><span class="ic ic-lg ic-phone"></span> Call +001 555 801</div>
                                                </div>
                                                <div class="item item-left block_header_top_left2">
                                                   <div class="links-wrapper-separators">
                                                      <ul class="links">
                                                         <li class="first">
                                                            <a href="http://ultimo.infortis-themes.com/demo/select-demo/" title="See more demos">All demos</a>
                                                         </li>
                                                         <li class="hide-below-768">
                                                            <a href="ultimo-responsive-magento-theme/index.html" title="See the list of all features">Features</a>
                                                         </li>
                                                         <li class="last hide-below-480">
                                                            <a href="http://themeforest.net/item/ultimo-fluid-responsive-magento-theme/3231798?ref=infortis" title="Click to buy this theme">Buy me!</a>
                                                         </li>
                                                      </ul>
                                                   </div>
                                                </div>
                                                <div class="item item-left hide-below-960">
                                                   <p class="welcome-msg">Welcome msg! </p>
                                                </div>
                                             </div>
                                             <!-- end: left column -->
                                             <div class="right-column">
                                                <div class="item item-right item-interface">
                                                   <div id="lang-switcher-wrapper-regular">
                                                      <div class="lang-switcher dropdown">
                                                         <a href="#" class="dropdown-heading cover">
                                                         <span>
                                                         <span class="label dropdown-icon flag"
                                                            style="background-image:url(../skin/frontend/ultimo/default/images/flags/default.png)">&nbsp;</span>
                                                         <span class="value">Demo1</span>
                                                         <span class="caret"></span>
                                                         </span>
                                                         </a>
                                                         <ul class="dropdown-content left-hand">
                                                            <li class="current"><span class="label dropdown-icon" style="background-image:url(../skin/frontend/ultimo/default/images/flags/default.png);">&nbsp;</span>Demo1</li>
                                                            <li><a href="http://ultimo.infortis-themes.com/demo/default_de/?___from_store=default"><span class="label dropdown-icon" style="background-image:url(../skin/frontend/ultimo/default/images/flags/default_de.png);">&nbsp;</span>German</a></li>
                                                         </ul>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <!-- end: right column -->
                                          </div>
                                          <!-- end: inner-container -->
                                       </div>
                                       <!-- end: header-top -->
                                    </div>
                                    <!-- end: header-top-container -->
                                    <div class="header-primary-container">
                                       <div class="header-primary header container">
                                          <div class="inner-container">
                                             <div class="hp-blocks-holder">
                                                <!-- Left column -->
                                                <div class="hp-block left-column grid12-4">
                                                   <div class="item">
                                                      <div class="logo-wrapper logo-wrapper--regular">
                                                         <h1 class="logo logo--regular"><strong>Responsive Magento Theme</strong><a href="index.html" title="Responsive Magento Theme"><img src="../skin/frontend/ultimo/default/images/logo-1.png" alt="Responsive Magento Theme" /></a></h1>
                                                      </div>
                                                   </div>
                                                </div>
                                                <!-- end: left column -->
                                                <!-- Central column -->
                                                <div class="hp-block central-column grid12-4">
                                                   <div class="item">
                                                      <div id="search-marker-regular"></div>
                                                   </div>
                                                </div>
                                                <!-- end: central column -->
                                                <!-- Right column -->
                                                <div class="hp-block right-column grid12-4">
                                                   <div class="item">
                                                      <div id="user-menu-wrapper-regular">
                                                         <div id="user-menu" class="user-menu">
                                                            <div id="mini-cart-marker-regular"></div>
                                                            <div id="mini-compare-marker-regular"></div>
                                                            <div id="account-links-marker-regular"></div>
                                                         </div>
                                                         <!-- end: user-menu -->
                                                      </div>
                                                   </div>
                                                </div>
                                                <!-- end: right column -->
                                             </div>
                                             <!-- end: hp-blocks-holder -->
                                          </div>
                                          <!-- end: inner-container -->
                                       </div>
                                       <!-- end: header-primary -->
                                    </div>
                                    <!-- end: header-primary-container -->
                                    <div class="nav-container skip-content sticky-container">
                                       <div class="nav container clearer">
                                          <div class="inner-container">
                                             <div class="navi-wrapper">
                                                <ul class="nav-holders-wrapper">
                                                   <li id="nav-holder1" class="nav-item level0 level-top nav-holder"></li>
                                                   <li id="nav-holder2" class="nav-item level0 level-top nav-holder"></li>
                                                   <li id="nav-holder3" class="nav-item level0 level-top nav-holder"></li>
                                                </ul>
                                                <div class="mobnav-trigger menu-trigger">
                                                   <div class="menu-trigger-inner">
                                                      <span class="trigger-icon"><span class="line"></span><span class="line"></span><span class="line"></span></span>
                                                      <span class="label">Menu</span>
                                                   </div>
                                                </div>
                                                <div id="nav-marker-regular"></div>
                                                <nav id="mainmenu" class="navi nav-regular opt-fx-fade-inout opt-sb0 opt-sob opt-hide480 with-bullets">
                                                   <ul>
                                                      <li class="nav-item nav-item--home level0 level-top active">
                                                         <a class="level-top" href="index.html"><span>Home</span></a>
                                                      </li>
                                                      <li class="nav-item level0 nav-1 level-top first nav-item--parent mega parent">
                                                         <a href="women.html" class="level-top"><span>Women</span><span class="caret"></span></a><span class="opener"></span>
                                                         <div class="nav-panel--dropdown nav-panel" style="width:595px;">
                                                            <div class="nav-panel-inner">
                                                               <div class="nav-block--center grid12-4">
                                                                  <ul class="level0 nav-submenu nav-submenu--mega dd-itemgrid dd-itemgrid-1col">
                                                                     <li class="nav-item level1 nav-1-1 first"><a href="women/sunglasses.html"><span>Sunglasses</span></a></li>
                                                                     <li class="nav-item level1 nav-1-2 nav-item--only-subcategories parent">
                                                                        <a href="women/shirts.html"><span>Shirts &amp; Tops</span><span class="caret"></span></a><span class="opener"></span>
                                                                        <ul class="level1 nav-submenu nav-panel">
                                                                           <li class="nav-item level2 nav-1-2-1 first classic"><a href="women/shirts/size-s.html"><span>Size - Small</span></a></li>
                                                                           <li class="nav-item level2 nav-1-2-2 classic"><a href="women/shirts/size-m.html"><span>Size - Medium</span></a></li>
                                                                           <li class="nav-item level2 nav-1-2-3 last classic"><a href="women/shirts/size-l.html"><span>Size - Large</span></a></li>
                                                                        </ul>
                                                                     </li>
                                                                     <li class="nav-item level1 nav-1-3"><a href="women/designer-tops.html"><span>Designer Tops</span></a></li>
                                                                     <li class="nav-item level1 nav-1-4"><a href="women/blouses.html"><span>Blouses</span></a></li>
                                                                     <li class="nav-item level1 nav-1-5"><a href="women/socks-tights.html"><span>Socks &amp; Tights</span></a></li>
                                                                     <li class="nav-item level1 nav-1-6"><a href="women/going-out.html"><span>Going Out</span></a></li>
                                                                     <li class="nav-item level1 nav-1-7 last"><a href="women/party.html"><span>Party</span></a></li>
                                                                  </ul>
                                                               </div>
                                                               <div class="nav-block nav-block--right std grid12-8">
                                                                  <a href="dress7.html">
                                                                     <div class="ban ban-effect-3 ban-caption-fade-out">
                                                                        <img class="image" src="../media/wysiwyg/infortis/other/menu/01.jpg" alt="Sample custom content of the category block" />
                                                                        <div class="cap" style="background-color: rgba(0,0,0, 0.2);">
                                                                           <div class="cap cap-center-vertically cap-center-horizontally cap-no-bg">
                                                                              <span class="ic ic-gift ic-6x animated infinite pulse"></span>
                                                                           </div>
                                                                        </div>
                                                                     </div>
                                                                  </a>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </li>
                                                      <li class="nav-item level0 nav-2 level-top nav-item--parent mega parent">
                                                         <a href="fashion.html" class="level-top"><span>Fashion<span class="cat-label cat-label-label1 pin-bottom">New</span></span><span class="caret"></span></a><span class="opener"></span>
                                                         <div class="nav-panel--dropdown nav-panel full-width">
                                                            <div class="nav-panel-inner">
                                                               <div class="nav-block--center grid12-8">
                                                                  <ul class="level0 nav-submenu nav-submenu--mega dd-itemgrid dd-itemgrid-4col">
                                                                     <li class="nav-item level1 nav-2-1 first nav-item--only-subcategories parent">
                                                                        <a href="fashion/tops.html"><span>Tops<span class="cat-label cat-label-label2">Hot!</span></span><span class="caret"></span></a><span class="opener"></span>
                                                                        <ul class="level1 nav-submenu nav-panel">
                                                                           <li class="nav-item level2 nav-2-1-1 first nav-item--parent classic nav-item--only-subcategories parent">
                                                                              <a href="fashion/tops/casual-tops.html"><span>Casual Tops</span><span class="caret"></span></a><span class="opener"></span>
                                                                              <ul class="level2 nav-submenu nav-panel--dropdown nav-panel">
                                                                                 <li class="nav-item level3 nav-2-1-1-1 first classic"><a href="fashion/tops/casual-tops/modest.html"><span>Modest</span></a></li>
                                                                                 <li class="nav-item level3 nav-2-1-1-2 classic"><a href="fashion/tops/casual-tops/distinguished.html"><span>Distinguished</span></a></li>
                                                                                 <li class="nav-item level3 nav-2-1-1-3 last nav-item--parent classic nav-item--only-subcategories parent">
                                                                                    <a href="fashion/tops/casual-tops/collections.html"><span>Collections</span><span class="caret"></span></a><span class="opener"></span>
                                                                                    <ul class="level3 nav-submenu nav-panel--dropdown nav-panel">
                                                                                       <li class="nav-item level4 nav-2-1-1-3-1 first classic"><a href="fashion/tops/casual-tops/collections/designer-style.html"><span>Designer Style</span></a></li>
                                                                                       <li class="nav-item level4 nav-2-1-1-3-2 last classic"><a href="fashion/tops/casual-tops/collections/paris-style.html"><span>Paris Style</span></a></li>
                                                                                    </ul>
                                                                                 </li>
                                                                              </ul>
                                                                           </li>
                                                                           <li class="nav-item level2 nav-2-1-2 nav-item--parent mega nav-item--only-blocks parent">
                                                                              <a href="fashion/tops/shirts-blouses.html"><span>Shirts</span><span class="caret"></span></a><span class="opener"></span>
                                                                              <div class="nav-panel--dropdown nav-panel" style="width:290px;">
                                                                                 <div class="nav-panel-inner">
                                                                                    <div class="nav-block nav-block--top std grid-full">
                                                                                       <h4 style="margin-bottom:0;">Sample brands:</h4>
                                                                                       <span class="section-line"></span>
                                                                                       <div style="text-align:center;">
                                                                                          <br/>
                                                                                          <a href="fashionc3a4.html?manufacturer=24" class="fade-on-hover" style="display:block;">
                                                                                          <img src="../media/wysiwyg/infortis/brands/bluelogo.png" alt="Brand1" />
                                                                                          </a>
                                                                                          <br/><br/>
                                                                                          <a href="fashiona039.html?manufacturer=23" class="fade-on-hover" style="display:block;">
                                                                                          <img src="../media/wysiwyg/infortis/brands/logofashion.png" alt="Brand2" />
                                                                                          </a>
                                                                                          <br/><br/>
                                                                                          <a href="fashion6e6c.html?manufacturer=25" class="fade-on-hover" style="display:block;">
                                                                                          <img src="../media/wysiwyg/infortis/brands/brandisimi.png" alt="Brand3" />
                                                                                          </a>
                                                                                          <br/>
                                                                                       </div>
                                                                                    </div>
                                                                                 </div>
                                                                              </div>
                                                                           </li>
                                                                           <li class="nav-item level2 nav-2-1-3 classic"><a href="fashion/tops/tunics.html"><span>Tunics</span></a></li>
                                                                           <li class="nav-item level2 nav-2-1-4 last classic"><a href="fashion/tops/vests.html"><span>Vests</span></a></li>
                                                                        </ul>
                                                                     </li>
                                                                     <li class="nav-item level1 nav-2-2 nav-item--only-subcategories parent">
                                                                        <a href="fashion/bags-purses.html"><span>Bags<span class="cat-label cat-label-label2">Hot!</span></span><span class="caret"></span></a><span class="opener"></span>
                                                                        <ul class="level1 nav-submenu nav-panel">
                                                                           <li class="nav-item level2 nav-2-2-5 first classic"><a href="fashion/bags-purses/designer-bags.html"><span>Designer Bags</span></a></li>
                                                                           <li class="nav-item level2 nav-2-2-6 classic"><a href="fashion/bags-purses/handbags.html"><span>Handbags</span></a></li>
                                                                           <li class="nav-item level2 nav-2-2-7 classic"><a href="fashion/bags-purses/purses.html"><span>Purses</span></a></li>
                                                                           <li class="nav-item level2 nav-2-2-8 last classic"><a href="fashion/bags-purses/shoulder-bags.html"><span>Shoulder Bags</span></a></li>
                                                                        </ul>
                                                                     </li>
                                                                     <li class="nav-item level1 nav-2-3 nav-item--only-subcategories parent">
                                                                        <a href="fashion/shoes.html"><span>Shoes<span class="cat-label cat-label-label1">New</span></span><span class="caret"></span></a><span class="opener"></span>
                                                                        <ul class="level1 nav-submenu nav-panel">
                                                                           <li class="nav-item level2 nav-2-3-9 first classic"><a href="fashion/shoes/flat-shoes.html"><span>Flat Shoes</span></a></li>
                                                                           <li class="nav-item level2 nav-2-3-10 classic"><a href="fashion/shoes/flat-sandals.html"><span>Flat Sandals</span></a></li>
                                                                           <li class="nav-item level2 nav-2-3-11 classic"><a href="fashion/shoes/boots.html"><span>Boots</span></a></li>
                                                                           <li class="nav-item level2 nav-2-3-12 last classic"><a href="fashion/shoes/heels.html"><span>Heels</span></a></li>
                                                                        </ul>
                                                                     </li>
                                                                     <li class="nav-item level1 nav-2-4 nav-item--only-subcategories parent">
                                                                        <a href="fashion/jewelry.html"><span>Jewelry</span><span class="caret"></span></a><span class="opener"></span>
                                                                        <ul class="level1 nav-submenu nav-panel">
                                                                           <li class="nav-item level2 nav-2-4-13 first classic"><a href="fashion/jewelry/bracelets.html"><span>Bracelets</span></a></li>
                                                                           <li class="nav-item level2 nav-2-4-14 classic"><a href="fashion/jewelry/necklaces-pendants.html"><span>Necklaces &amp; Pendants</span></a></li>
                                                                           <li class="nav-item level2 nav-2-4-15 last classic"><a href="fashion/jewelry/pins-brooches.html"><span>Pins &amp; Brooches</span></a></li>
                                                                        </ul>
                                                                     </li>
                                                                     <li class="nav-item level1 nav-2-5 nav-item--only-subcategories parent">
                                                                        <a href="fashion/dresses.html"><span>Dresses</span><span class="caret"></span></a><span class="opener"></span>
                                                                        <ul class="level1 nav-submenu nav-panel">
                                                                           <li class="nav-item level2 nav-2-5-16 first classic"><a href="fashion/dresses/casual-dresses.html"><span>Casual Dresses</span></a></li>
                                                                           <li class="nav-item level2 nav-2-5-17 classic"><a href="fashion/dresses/evening.html"><span>Evening</span></a></li>
                                                                           <li class="nav-item level2 nav-2-5-18 classic"><a href="fashion/dresses/designer.html"><span>Designer</span></a></li>
                                                                           <li class="nav-item level2 nav-2-5-19 last classic"><a href="fashion/dresses/party.html"><span>Party</span></a></li>
                                                                        </ul>
                                                                     </li>
                                                                     <li class="nav-item level1 nav-2-6 nav-item--only-subcategories parent">
                                                                        <a href="fashion/lingerie.html"><span>Lingerie</span><span class="caret"></span></a><span class="opener"></span>
                                                                        <ul class="level1 nav-submenu nav-panel">
                                                                           <li class="nav-item level2 nav-2-6-20 first classic"><a href="fashion/lingerie/bras.html"><span>Bras</span></a></li>
                                                                           <li class="nav-item level2 nav-2-6-21 classic"><a href="fashion/lingerie/bodies.html"><span>Bodies</span></a></li>
                                                                           <li class="nav-item level2 nav-2-6-22 classic"><a href="fashion/lingerie/lingerie-sets.html"><span>Lingerie Sets</span></a></li>
                                                                           <li class="nav-item level2 nav-2-6-23 last classic"><a href="fashion/lingerie/shapewear.html"><span>Shapewear</span></a></li>
                                                                        </ul>
                                                                     </li>
                                                                     <li class="nav-item level1 nav-2-7 nav-item--only-subcategories parent">
                                                                        <a href="fashion/coats-jackets.html"><span>Jackets</span><span class="caret"></span></a><span class="opener"></span>
                                                                        <ul class="level1 nav-submenu nav-panel">
                                                                           <li class="nav-item level2 nav-2-7-24 first classic"><a href="fashion/coats-jackets/coats.html"><span>Coats</span></a></li>
                                                                           <li class="nav-item level2 nav-2-7-25 classic"><a href="fashion/coats-jackets/jackets.html"><span>Jackets</span></a></li>
                                                                           <li class="nav-item level2 nav-2-7-26 classic"><a href="fashion/coats-jackets/leather-jackets.html"><span>Leather Jackets</span></a></li>
                                                                           <li class="nav-item level2 nav-2-7-27 last classic"><a href="fashion/coats-jackets/blazers.html"><span>Blazers</span></a></li>
                                                                        </ul>
                                                                     </li>
                                                                     <li class="nav-item level1 nav-2-8 last nav-item--only-subcategories parent">
                                                                        <a href="fashion/swimwear.html"><span>Swimwear</span><span class="caret"></span></a><span class="opener"></span>
                                                                        <ul class="level1 nav-submenu nav-panel">
                                                                           <li class="nav-item level2 nav-2-8-28 first classic"><a href="fashion/swimwear/swimsuits.html"><span>Swimsuits</span></a></li>
                                                                           <li class="nav-item level2 nav-2-8-29 classic"><a href="fashion/swimwear/beach-clothing.html"><span>Beach Clothing</span></a></li>
                                                                           <li class="nav-item level2 nav-2-8-30 last classic"><a href="fashion/swimwear/bikinis.html"><span>Bikinis</span></a></li>
                                                                        </ul>
                                                                     </li>
                                                                  </ul>
                                                               </div>
                                                               <div class="nav-block nav-block--right std grid12-4">
                                                                  <div class="hide-in-desktop-menu"><br/></div>
                                                                  <strong class="section-title padding-right">Recommended Bags</strong>
                                                                  <div class="itemslider-wrapper slider-arrows1 slider-arrows1-pos-top-right slider-pagination1">
                                                                     <div id="itemslider-featured-b4535c7fdfcdb78d8361e55e3f345f02" class="itemslider itemslider-responsive products-grid size-s centered">
                                                                        <div class="item">
                                                                           <div class="product-image-wrapper" style="max-width:110px;">
                                                                              <a href="bag5.html" title="Skyblue Mini Bag" class="product-image">
                                                                              <img class="lazyOwl" data-src="http://ultimo.infortis-themes.com/demo/media/catalog/product/cache/1/small_image/110x/040ec09b1e35df139433887a97daa66f/9/9/996093-0100_1.jpg" alt="Skyblue Mini Bag" />
                                                                              </a>
                                                                              <ul class="add-to-links clearer addto-links-icons addto-onimage visible-onhover">
                                                                                 <li><a class="link-wishlist"
                                                                                    href="wishlist/index/add/product/53/form_key/ETmRbvXu8yyuzdLp/index.html"
                                                                                    title="Add to Wishlist">
                                                                                    <span class="2 icon ib ic ic-heart"></span>
                                                                                    </a>
                                                                                 </li>
                                                                                 <li><a class="link-compare"
                                                                                    href="catalog/product_compare/add/product/53/uenc/aHR0cDovL3VsdGltby5pbmZvcnRpcy10aGVtZXMuY29tL2R/form_key/ETmRbvXu8yyuzdLp/index.html"
                                                                                    title="Add to Compare">
                                                                                    <span class="2 icon ib ic ic-compare"></span>
                                                                                    </a>
                                                                                 </li>
                                                                              </ul>
                                                                           </div>
                                                                           <!-- end: product-image-wrapper -->
                                                                           <strong class="product-name"><a href="bag5.html" title="Skyblue Mini Bag">Skyblue Mini Bag</a></strong>
                                                                           <div class="price-box">
                                                                              <span class="regular-price" id="product-price-53b4535c7fdfcdb78d8361e55e3f345f02">
                                                                              <span class="price">$105.00</span>                                    </span>
                                                                           </div>
                                                                           <div class="actions">
                                                                           </div>
                                                                        </div>
                                                                        <div class="item">
                                                                           <div class="product-image-wrapper" style="max-width:110px;">
                                                                              <a href="bag6.html" title="Union Jack Purse" class="product-image">
                                                                              <img class="lazyOwl" data-src="http://ultimo.infortis-themes.com/demo/media/catalog/product/cache/1/small_image/110x/040ec09b1e35df139433887a97daa66f/2/1/216840-0129_1.jpg" alt="Union Jack Purse" />
                                                                              </a>
                                                                              <ul class="add-to-links clearer addto-links-icons addto-onimage visible-onhover">
                                                                                 <li><a class="link-wishlist"
                                                                                    href="wishlist/index/add/product/54/form_key/ETmRbvXu8yyuzdLp/index.html"
                                                                                    title="Add to Wishlist">
                                                                                    <span class="2 icon ib ic ic-heart"></span>
                                                                                    </a>
                                                                                 </li>
                                                                                 <li><a class="link-compare"
                                                                                    href="catalog/product_compare/add/product/54/uenc/aHR0cDovL3VsdGltby5pbmZvcnRpcy10aGVtZXMuY29tL2R/form_key/ETmRbvXu8yyuzdLp/index.html"
                                                                                    title="Add to Compare">
                                                                                    <span class="2 icon ib ic ic-compare"></span>
                                                                                    </a>
                                                                                 </li>
                                                                              </ul>
                                                                           </div>
                                                                           <!-- end: product-image-wrapper -->
                                                                           <strong class="product-name"><a href="bag6.html" title="Union Jack Purse">Union Jack Purse</a></strong>
                                                                           <div class="price-box">
                                                                              <span class="regular-price" id="product-price-54b4535c7fdfcdb78d8361e55e3f345f02">
                                                                              <span class="price">$105.00</span>                                    </span>
                                                                           </div>
                                                                           <div class="actions">
                                                                           </div>
                                                                        </div>
                                                                        <div class="item">
                                                                           <div class="product-image-wrapper" style="max-width:110px;">
                                                                              <a href="bag7.html" title="Red Travel Handbag" class="product-image">
                                                                              <img class="lazyOwl" data-src="http://ultimo.infortis-themes.com/demo/media/catalog/product/cache/1/small_image/110x/040ec09b1e35df139433887a97daa66f/2/7/271669-0054_1.jpg" alt="Red Travel Handbag" />
                                                                              </a>
                                                                              <ul class="add-to-links clearer addto-links-icons addto-onimage visible-onhover">
                                                                                 <li><a class="link-wishlist"
                                                                                    href="wishlist/index/add/product/55/form_key/ETmRbvXu8yyuzdLp/index.html"
                                                                                    title="Add to Wishlist">
                                                                                    <span class="2 icon ib ic ic-heart"></span>
                                                                                    </a>
                                                                                 </li>
                                                                                 <li><a class="link-compare"
                                                                                    href="catalog/product_compare/add/product/55/uenc/aHR0cDovL3VsdGltby5pbmZvcnRpcy10aGVtZXMuY29tL2R/form_key/ETmRbvXu8yyuzdLp/index.html"
                                                                                    title="Add to Compare">
                                                                                    <span class="2 icon ib ic ic-compare"></span>
                                                                                    </a>
                                                                                 </li>
                                                                              </ul>
                                                                           </div>
                                                                           <!-- end: product-image-wrapper -->
                                                                           <strong class="product-name"><a href="bag7.html" title="Red Travel Handbag">Red Travel Handbag</a></strong>
                                                                           <div class="price-box">
                                                                              <span class="regular-price" id="product-price-55b4535c7fdfcdb78d8361e55e3f345f02">
                                                                              <span class="price">$120.55</span>                                    </span>
                                                                           </div>
                                                                           <div class="actions">
                                                                           </div>
                                                                        </div>
                                                                        <div class="item">
                                                                           <div class="product-image-wrapper" style="max-width:110px;">
                                                                              <a href="bag8.html" title="Orange Shopper" class="product-image">
                                                                              <img class="lazyOwl" data-src="http://ultimo.infortis-themes.com/demo/media/catalog/product/cache/1/small_image/110x/040ec09b1e35df139433887a97daa66f/6/7/677188-0067_1.jpg" alt="Orange Shopper" />
                                                                              </a>
                                                                              <ul class="add-to-links clearer addto-links-icons addto-onimage visible-onhover">
                                                                                 <li><a class="link-wishlist"
                                                                                    href="wishlist/index/add/product/56/form_key/ETmRbvXu8yyuzdLp/index.html"
                                                                                    title="Add to Wishlist">
                                                                                    <span class="2 icon ib ic ic-heart"></span>
                                                                                    </a>
                                                                                 </li>
                                                                                 <li><a class="link-compare"
                                                                                    href="catalog/product_compare/add/product/56/uenc/aHR0cDovL3VsdGltby5pbmZvcnRpcy10aGVtZXMuY29tL2R/form_key/ETmRbvXu8yyuzdLp/index.html"
                                                                                    title="Add to Compare">
                                                                                    <span class="2 icon ib ic ic-compare"></span>
                                                                                    </a>
                                                                                 </li>
                                                                              </ul>
                                                                           </div>
                                                                           <!-- end: product-image-wrapper -->
                                                                           <strong class="product-name"><a href="bag8.html" title="Orange Shopper">Orange Shopper</a></strong>
                                                                           <div class="price-box">
                                                                              <span class="regular-price" id="product-price-56b4535c7fdfcdb78d8361e55e3f345f02">
                                                                              <span class="price">$120.55</span>                                    </span>
                                                                           </div>
                                                                           <div class="actions">
                                                                           </div>
                                                                        </div>
                                                                     </div>
                                                                     <!-- end: itemslider -->
                                                                  </div>
                                                                  <!-- end: itemslider-wrapper -->
                                                                  <script type="text/javascript">
                                                                     //<![CDATA[
                                                                         jQuery(function($) {

                                                                             var owl = $('#itemslider-featured-b4535c7fdfcdb78d8361e55e3f345f02');
                                                                             owl.owlCarousel({

                                                                                         lazyLoad: true,

                                                                                         itemsCustom: [ [0, 1], [480, 2], [768, 1], [960, 2] ],
                                                                                 responsiveRefreshRate: 50,

                                                                                         slideSpeed: 200,

                                                                                         paginationSpeed: 500,


                                                                                         autoPlay: 4000,

                                                                                         stopOnHover: true,

                                                                                         rewindNav: true,
                                                                                 rewindSpeed: 600,

                                                                                         pagination: false,

                                                                                 navigation: true,
                                                                                 navigationText: false

                                                                             }); //end: owl

                                                                         });
                                                                     //]]>
                                                                  </script>
                                                               </div>
                                                               <div class="nav-block nav-block--bottom std grid-full">
                                                                  <div class="clearer hide-below-480" style="background-color: #eee; padding: 10px 15px;">
                                                                     <ul class="links">
                                                                        <li class="label hide-in-mobile-menu">Featured Categories:</li>
                                                                        <li class="first">
                                                                           <a href="fashion/tops.html">Casual Tops</a>
                                                                        </li>
                                                                        <li>
                                                                           <a href="#">Dresses</a>
                                                                        </li>
                                                                        <li class="last">
                                                                           <a href="#">Shirts & Blouses</a>
                                                                        </li>
                                                                     </ul>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </li>
                                                      <li class="nav-item level0 nav-3 level-top nav-item--parent mega parent">
                                                         <a href="electronics.html" class="level-top"><span>Electronics</span><span class="caret"></span></a><span class="opener"></span>
                                                         <div class="nav-panel--dropdown nav-panel" style="width:870px;">
                                                            <div class="nav-panel-inner">
                                                               <div class="nav-block nav-block--top std grid-full">
                                                                  <div class="links-wrapper-separators">
                                                                     <ul class="links">
                                                                        <li class="label hidden-xs hidden-sm">Recommended:</li>
                                                                        <li>
                                                                           <a href="electronics/smartphones/phone3.html">Configurable</a>
                                                                        </li>
                                                                        <li>
                                                                           <a href="#">Galaxy S3</a>
                                                                        </li>
                                                                        <li>
                                                                           <a href="#">Galaxy S4</a>
                                                                        </li>
                                                                        <li>
                                                                           <a href="#">iPhone 5</a>
                                                                        </li>
                                                                     </ul>
                                                                  </div>
                                                                  <div class="links-wrapper-separators pull-right hide-in-mobile-menu hidden-xs hidden-sm">
                                                                     <ul class="links">
                                                                        <li>
                                                                           <a href="#">Add</a>
                                                                        </li>
                                                                        <li>
                                                                           <a href="#">Custom</a>
                                                                        </li>
                                                                        <li>
                                                                           <a href="#">Links</a>
                                                                        </li>
                                                                     </ul>
                                                                  </div>
                                                                  <span class="section-line"></span>
                                                               </div>
                                                               <div class="nav-block nav-block--left std grid12-4">
                                                                  <a class="hidden-xs" href="electronics/phone-accessories.html">
                                                                     <div class="ban ban-caption-hinge-left">
                                                                        <img class="image" src="../media/wysiwyg/infortis/other/menu/02.jpg" alt="Sample custom content of the category block" />
                                                                        <div class="cap" style="background-color: #ffc60f;">
                                                                           <div class="cap cap-center-vertically cap-center-horizontally cap-no-bg">
                                                                              <span class="ic ic-star ic-4x animated infinite pulse margin-bottom"></span>
                                                                              <h3 class="no-margin">Go to category</h3>
                                                                           </div>
                                                                        </div>
                                                                     </div>
                                                                  </a>
                                                                  <br/>
                                                                  <h3>Featured Category</h3>
                                                                  <p>This is a custom block ready to display any content. You can add blocks to any category in the catalog...</p>
                                                                  <a class="button" href="#">View Category &nbsp;<span class="ic ic-arrow-right"></span></a>
                                                               </div>
                                                               <div class="nav-block--center grid12-8">
                                                                  <ul class="level0 nav-submenu nav-submenu--mega dd-itemgrid dd-itemgrid-3col">
                                                                     <li class="nav-item level1 nav-3-1 first nav-item--only-subcategories parent">
                                                                        <div class="nav-block nav-block--top std">
                                                                           <a href="electronics/smartphones.html">
                                                                              <div class="ban ban-caption-hinge-up">
                                                                                 <span class="image ic ic-3x ic-smartphones ib ib-hover ib-square ib-size-xxl" style="background-color: #333;"></span>
                                                                                 <div class="cap">
                                                                                    <div class="cap cap-center-vertically cap-center-horizontally cap-no-bg">
                                                                                       <h4 class="no-margin">Phones & Tablets</h4>
                                                                                    </div>
                                                                                 </div>
                                                                              </div>
                                                                           </a>
                                                                        </div>
                                                                        <a href="electronics/smartphones.html"><span>Phones &amp; Tablets</span><span class="caret"></span></a><span class="opener"></span>
                                                                        <ul class="level1 nav-submenu nav-panel">
                                                                           <li class="nav-item level2 nav-3-1-1 first classic"><a href="electronics/smartphones/sample.html"><span>Sample<span class="cat-label cat-label-label1">New</span></span></a></li>
                                                                           <li class="nav-item level2 nav-3-1-2 classic"><a href="electronics/smartphones/samsung.html"><span>Samsung</span></a></li>
                                                                           <li class="nav-item level2 nav-3-1-3 simple nav-item--only-subcategories parent">
                                                                              <a href="electronics/smartphones/apple.html"><span>Apple</span><span class="caret"></span></a><span class="opener"></span>
                                                                              <ul class="level2 nav-submenu nav-panel">
                                                                                 <li class="nav-item level3 nav-3-1-3-1 first classic"><a href="electronics/smartphones/apple/iphone-3gs.html"><span>iPhone 3GS</span></a></li>
                                                                                 <li class="nav-item level3 nav-3-1-3-2 classic"><a href="electronics/smartphones/apple/iphone-4.html"><span>iPhone 4</span></a></li>
                                                                                 <li class="nav-item level3 nav-3-1-3-3 classic"><a href="electronics/smartphones/apple/iphone-4s.html"><span>iPhone 4s</span></a></li>
                                                                                 <li class="nav-item level3 nav-3-1-3-4 classic"><a href="electronics/smartphones/apple/iphone-5.html"><span>iPhone 5</span></a></li>
                                                                                 <li class="nav-item level3 nav-3-1-3-5 classic"><a href="electronics/smartphones/apple/ipad-retina.html"><span>iPad Retina</span></a></li>
                                                                                 <li class="nav-item level3 nav-3-1-3-6 classic"><a href="electronics/smartphones/apple/ipad-mini.html"><span>iPad Mini</span></a></li>
                                                                                 <li class="nav-item level3 nav-3-1-3-7 last classic"><a href="electronics/smartphones/apple/ipad-air.html"><span>iPad Air</span></a></li>
                                                                              </ul>
                                                                           </li>
                                                                           <li class="nav-item level2 nav-3-1-4 classic"><a href="electronics/smartphones/blackberry.html"><span>BlackBerry</span></a></li>
                                                                           <li class="nav-item level2 nav-3-1-5 classic"><a href="electronics/smartphones/motorola.html"><span>Motorola</span></a></li>
                                                                           <li class="nav-item level2 nav-3-1-6 classic"><a href="electronics/smartphones/nokia.html"><span>Nokia</span></a></li>
                                                                           <li class="nav-item level2 nav-3-1-7 classic"><a href="electronics/smartphones/htc.html"><span>HTC</span></a></li>
                                                                           <li class="nav-item level2 nav-3-1-8 classic"><a href="electronics/smartphones/huawei.html"><span>Huawei</span></a></li>
                                                                           <li class="nav-item level2 nav-3-1-9 classic"><a href="electronics/smartphones/google.html"><span>Google</span></a></li>
                                                                           <li class="nav-item level2 nav-3-1-10 last classic"><a href="electronics/smartphones/sony.html"><span>Sony</span></a></li>
                                                                        </ul>
                                                                     </li>
                                                                     <li class="nav-item level1 nav-3-2 nav-item--only-subcategories parent">
                                                                        <div class="nav-block nav-block--top std">
                                                                           <a href="electronics/phone-accessories.html">
                                                                              <div class="ban ban-caption-hinge-up">
                                                                                 <span class="image ic ic-3x ic-callcenter ib ib-hover ib-square ib-size-xxl" style="background-color: #333;"></span>
                                                                                 <div class="cap">
                                                                                    <div class="cap cap-center-vertically cap-center-horizontally cap-no-bg">
                                                                                       <h4 class="no-margin">Accessories</h4>
                                                                                    </div>
                                                                                 </div>
                                                                              </div>
                                                                           </a>
                                                                        </div>
                                                                        <a href="electronics/phone-accessories.html"><span>Accessories</span><span class="caret"></span></a><span class="opener"></span>
                                                                        <ul class="level1 nav-submenu nav-panel">
                                                                           <li class="nav-item level2 nav-3-2-11 first classic"><a href="electronics/phone-accessories/headsets.html"><span>Headsets</span></a></li>
                                                                           <li class="nav-item level2 nav-3-2-12 classic"><a href="electronics/phone-accessories/bluetooth-wireless.html"><span>Bluetooth Wireless</span></a></li>
                                                                           <li class="nav-item level2 nav-3-2-13 classic"><a href="electronics/phone-accessories/earphones.html"><span>Earphones</span></a></li>
                                                                           <li class="nav-item level2 nav-3-2-14 classic"><a href="electronics/phone-accessories/batteries.html"><span>Batteries</span></a></li>
                                                                           <li class="nav-item level2 nav-3-2-15 classic"><a href="electronics/phone-accessories/screen-protectors.html"><span>Screen Protectors</span></a></li>
                                                                           <li class="nav-item level2 nav-3-2-16 classic"><a href="electronics/phone-accessories/memory-cards.html"><span>Memory Cards</span></a></li>
                                                                           <li class="nav-item level2 nav-3-2-17 classic"><a href="electronics/phone-accessories/cables-adapters.html"><span>Cables &amp; Adapters</span></a></li>
                                                                           <li class="nav-item level2 nav-3-2-18 classic"><a href="electronics/phone-accessories/cleaning-cloth.html"><span>Cleaning Cloth</span></a></li>
                                                                           <li class="nav-item level2 nav-3-2-19 classic"><a href="electronics/phone-accessories/chargers.html"><span>Chargers</span></a></li>
                                                                           <li class="nav-item level2 nav-3-2-20 classic"><a href="electronics/phone-accessories/cases.html"><span>Cases</span></a></li>
                                                                           <li class="nav-item level2 nav-3-2-21 classic"><a href="electronics/phone-accessories/covers.html"><span>Covers</span></a></li>
                                                                           <li class="nav-item level2 nav-3-2-22 last classic"><a href="electronics/phone-accessories/skins.html"><span>Skins</span></a></li>
                                                                        </ul>
                                                                     </li>
                                                                     <li class="nav-item level1 nav-3-3 last nav-item--only-subcategories parent">
                                                                        <div class="nav-block nav-block--top std">
                                                                           <a href="electronics/cameras.html">
                                                                              <div class="ban ban-caption-hinge-up">
                                                                                 <span class="image ic ic-3x ic-gift ib ib-hover ib-square ib-size-xxl" style="background-color: #333;"></span>
                                                                                 <div class="cap">
                                                                                    <div class="cap cap-center-vertically cap-center-horizontally cap-no-bg">
                                                                                       <h4 class="no-margin">Cameras</h4>
                                                                                    </div>
                                                                                 </div>
                                                                              </div>
                                                                           </a>
                                                                        </div>
                                                                        <a href="electronics/cameras.html"><span>Cameras</span><span class="caret"></span></a><span class="opener"></span>
                                                                        <ul class="level1 nav-submenu nav-panel">
                                                                           <li class="nav-item level2 nav-3-3-23 first classic"><a href="electronics/cameras/digital-cameras.html"><span>Digital Cameras</span></a></li>
                                                                           <li class="nav-item level2 nav-3-3-24 classic"><a href="electronics/cameras/camcorders.html"><span>Camcorders</span></a></li>
                                                                           <li class="nav-item level2 nav-3-3-25 classic"><a href="electronics/cameras/lenses.html"><span>Lenses</span></a></li>
                                                                           <li class="nav-item level2 nav-3-3-26 classic"><a href="electronics/cameras/filters.html"><span>Filters</span></a></li>
                                                                           <li class="nav-item level2 nav-3-3-27 last classic"><a href="electronics/cameras/tripods.html"><span>Tripods</span></a></li>
                                                                        </ul>
                                                                     </li>
                                                                  </ul>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </li>
                                                      <li class="nav-item level0 nav-4 level-top last nav-item--parent classic nav-item--only-subcategories parent">
                                                         <a href="downloadable.html" class="level-top"><span>Digital</span><span class="caret"></span></a><span class="opener"></span>
                                                         <ul class="level0 nav-submenu nav-panel--dropdown nav-panel">
                                                            <li class="nav-item level1 nav-4-1 first nav-item--parent classic nav-item--only-subcategories parent">
                                                               <a href="downloadable/ebooks.html"><span>eBooks</span><span class="caret"></span></a><span class="opener"></span>
                                                               <ul class="level1 nav-submenu nav-panel--dropdown nav-panel">
                                                                  <li class="nav-item level2 nav-4-1-1 first classic"><a href="downloadable/ebooks/design-theory.html"><span>Design Theory</span></a></li>
                                                                  <li class="nav-item level2 nav-4-1-2 classic"><a href="downloadable/ebooks/web-design.html"><span>Web Design</span></a></li>
                                                                  <li class="nav-item level2 nav-4-1-3 classic"><a href="downloadable/ebooks/typography.html"><span>Typography</span></a></li>
                                                                  <li class="nav-item level2 nav-4-1-4 last nav-item--parent classic nav-item--only-subcategories parent">
                                                                     <a href="downloadable/ebooks/magento-tutorials.html"><span>Magento Tutorials</span><span class="caret"></span></a><span class="opener"></span>
                                                                     <ul class="level2 nav-submenu nav-panel--dropdown nav-panel">
                                                                        <li class="nav-item level3 nav-4-1-4-1 first classic"><a href="downloadable/ebooks/magento-tutorials/magento-themes.html"><span>Magento Themes</span></a></li>
                                                                        <li class="nav-item level3 nav-4-1-4-2 last classic"><a href="downloadable/ebooks/magento-tutorials/magento-extensions.html"><span>Magento Extensions</span></a></li>
                                                                     </ul>
                                                                  </li>
                                                               </ul>
                                                            </li>
                                                            <li class="nav-item level1 nav-4-2 simple nav-item--only-subcategories parent">
                                                               <a href="downloadable/themes.html"><span>Themes</span><span class="caret"></span></a><span class="opener"></span>
                                                               <ul class="level1 nav-submenu nav-panel">
                                                                  <li class="nav-item level2 nav-4-2-5 first classic"><a href="downloadable/themes/wordpress.html"><span>WordPress</span></a></li>
                                                                  <li class="nav-item level2 nav-4-2-6 classic"><a href="downloadable/themes/magento.html"><span>Magento</span></a></li>
                                                                  <li class="nav-item level2 nav-4-2-7 classic"><a href="downloadable/themes/opencart.html"><span>OpenCart</span></a></li>
                                                                  <li class="nav-item level2 nav-4-2-8 classic"><a href="downloadable/themes/prestashop.html"><span>PrestaShop</span></a></li>
                                                                  <li class="nav-item level2 nav-4-2-9 last classic"><a href="downloadable/themes/drupal.html"><span>Drupal</span></a></li>
                                                               </ul>
                                                            </li>
                                                            <li class="nav-item level1 nav-4-3 classic"><a href="downloadable/music.html"><span>Music</span></a></li>
                                                            <li class="nav-item level1 nav-4-4 last classic"><a href="downloadable/software.html"><span>Software</span></a></li>
                                                         </ul>
                                                      </li>
                                                      <li class="nav-item nav-item--parent nav-item--only-blocks level0 level-top parent nav-custom-link right">
                                                         <a class="level-top no-click" href="#"><span>Custom</span><span class="caret"></span></a>
                                                         <span class="opener"></span>
                                                         <div class="nav-panel nav-panel--dropdown full-width">
                                                            <div class="nav-panel-inner">
                                                               <div class="header-nav-dropdown-wrapper nav-block std grid-full clearer">
                                                                  <div class="grid-container-spaced">
                                                                     <div class="grid12-3">
                                                                        <h2>Responsive Magento Theme</h2>
                                                                        <p>Ultimo is a premium Magento theme with advanced admin module. It's extremely customizable and fully responsive. Can be used for every type of store.</p>
                                                                        <h5><a class="go" href="http://themeforest.net/item/ultimo-fluid-responsive-magento-theme/3231798?ref=infortis" style="color:red;">Buy this Magento theme</a></h5>
                                                                     </div>
                                                                     <div class="grid12-3">
                                                                        <a href="ultimo-responsive-magento-theme/index.html">
                                                                        <img src="../media/wysiwyg/infortis/ultimo/menu/custom/01.png" class="fade-on-hover" alt="Magento CMS blocks" />
                                                                        </a>
                                                                        <h4 class="heading">50+ CMS blocks</h4>
                                                                        <p>You can use CMS blocks as content placeholders to display custom content in almost every part of the store. Import sample CMS blocks from the demo.</p>
                                                                        <a href="ultimo-responsive-magento-theme/index.html" class="go">See all features</a>
                                                                     </div>
                                                                     <div class="grid12-3">
                                                                        <a href="ultimo-responsive-magento-theme/index.html">
                                                                        <img src="../media/wysiwyg/infortis/ultimo/menu/custom/02.png" class="fade-on-hover" alt="Magento theme documentation" />
                                                                        </a>
                                                                        <h4 class="heading">190-pages documentation</h4>
                                                                        <p>The best Magento theme documentation on ThemeForest. It also describes selected Magento features which you need to know when starting to work with Magento.</p>
                                                                        <a href="ultimo-responsive-magento-theme/index.html" class="go">See all features</a>
                                                                     </div>
                                                                     <div class="grid12-3">
                                                                        <a href="ultimo-responsive-magento-theme/index.html">
                                                                        <img src="../media/wysiwyg/infortis/ultimo/menu/custom/03.png" class="fade-on-hover" alt="Create Magento sub-themes" />
                                                                        </a>
                                                                        <h4 class="heading">Easy to customize</h4>
                                                                        <p>Use Ultimo as a starting point for your custom projects. Unlike many other themes, Ultimo lets you create multiple custom sub-themes (theme variants) for your clients.</p>
                                                                        <a href="ultimo-responsive-magento-theme/index.html" class="go">See all features</a>
                                                                     </div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </li>
                                                      <li class="nav-item level0 level-top right">
                                                         <a class="level-top" href="about-us-page.html" title="About Us">
                                                         <span>About Us</span>
                                                         </a>
                                                      </li>
                                                   </ul>
                                                </nav>
                                                <div class="nav-border-bottom"></div>
                                             </div>
                                             <!-- end: navi-wrapper -->
                                             <script type="text/javascript">
                                                //<![CDATA[

                                                    var topMenuContainer = jQuery('#mainmenu');
                                                    var topMenuSettings = {
                                                        mobileMenuThreshold: 960        , isVerticalLayout: false
                                                        , mode: 0                , initVerticalMenuCollapsed: true        , outermostContainer: jQuery('.hp-blocks-holder')        , fullWidthDdContainer: jQuery('.hp-blocks-holder')    };
                                                    var theTopMenu = topMenuContainer.ultramegamenu(topMenuSettings).data("infortis-ultramegamenu");
                                                    theTopMenu.enableDropdowns();


                                                        jQuery(function($) {

                                                            var itemsList = topMenuContainer.children('ul');

                                                            // Non-clickable links
                                                            itemsList.on('click', '.no-click', function(e) {
                                                                e.preventDefault();
                                                            });

                                                        }); //end: on document ready

                                                        jQuery(window).on("load", function() {

                                                            var menubar = topMenuContainer;
                                                            var isTouchDevice = ('ontouchstart' in window) || (navigator.msMaxTouchPoints > 0);
                                                            if (isTouchDevice)
                                                            {
                                                                menubar.on('click', 'a', function(e) {

                                                                    var link = jQuery(this);
                                                                    if (!menubar.hasClass('nav-mobile') && link.parent().hasClass('nav-item--parent'))
                                                                    {
                                                                        if (!link.hasClass('ready'))
                                                                        {
                                                                            e.preventDefault();
                                                                            menubar.find('.ready').removeClass('ready');
                                                                            link.parents('li').children('a').addClass('ready');
                                                                        }
                                                                    }

                                                                }); //end: on click
                                                            } //end: if isTouchDevice

                                                        }); //end: on load


                                                //]]>
                                             </script>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <!-- end: header-container3 -->
                              </div>
                              <!-- end: header-container2 -->
                           </div>
                           <!-- end: header-container -->
                           <script type="text/javascript">
                              //<![CDATA[

                                  var theHeaderContainer = jQuery('#header-container');


                                      var smartHeaderSettings = {
                                          cartBlockSelector: '#mini-cart'
                                          , dropdownBlockClass: 'dropdown'
                                      };
                                      theHeaderContainer.smartheader(smartHeaderSettings);


                                  jQuery(function($) {


                                          //Skip Links
                                          var skipContents = $('.skip-content');
                                          var skipLinks = $('.skip-link');
                                          skipLinks.on('click', function (e) {
                                              e.preventDefault();

                                              var self = $(this);
                                              var target = self.attr('href');

                                              //Get target element
                                              var elem = $(target);

                                              //Check if stub is open
                                              var isSkipContentOpen = elem.hasClass('skip-active') ? 1 : 0;

                                              //Hide all stubs
                                              skipLinks.removeClass('skip-active');
                                              skipContents.removeClass('skip-active');

                                              //Toggle stubs
                                              if (isSkipContentOpen) {
                                                  self.removeClass('skip-active');
                                              } else {
                                                  self.addClass('skip-active');
                                                  elem.addClass('skip-active');
                                              }
                                          });



                                          var stickyHeaderSettings = {
                                              stickyThreshold: 960                , cartBlockSelector: '#mini-cart'
                                          };
                                          theHeaderContainer.stickyheader(stickyHeaderSettings);


                                  }); //end: on document ready

                              //]]>
                           </script>
                           <script type="text/javascript">
                              //<![CDATA[

                                  //Expose the header container
                                  var jsHeaderContainerObject = document.getElementById("header-container");
                                  if (jsHeaderContainerObject.style.display == 'none')
                                  {
                                      jsHeaderContainerObject.style.display = "block";
                                      jsHeaderContainerObject.classList.add("js-shown"); ///
                                  }

                              //]]>
                           </script>
                           <div class="main-container col1-layout">
                              <div class="main-top-container"></div>
                              <div class="main container">
                                 <div class="inner-container">
                                    <div class="preface">
                                       <div class="slideshow-wrapper-additional" >
                                          <div class="slideshow-wrapper-outer">
                                             <div class="slideshow-wrapper col-sm-9 no-gutter  slider-arrows2 slider-pagination2 pagination-pos-over-bottom-centered">
                                                <div id="slideshow-276fd886786923f5e5b18bb85dce7cc4" class="slideshow owl-carousel">
                                                   <!-- Slide 1 -->
                                                   <div class="item" >
                                                      <a href="ultimo-responsive-magento-theme/index.html" title="Click to see all features">
                                                         <div class="ban">
                                                            <img class="image" src="../media/wysiwyg/infortis/slideshow/a01.jpg" alt="Customizable Magento Theme" />
                                                            <div class="cap cap-top-right cap-push-down-10 cap-push-left-10 cap-no-bg cap-text-bg cap-text-bg-light-1">
                                                               <h2 class="text" style="font-size: 36px;" data-animate-in="fadeInUp delay-0-5">Customizable Theme</h2>
                                                               <p class="text" style="font-size: 18px;" data-animate-in="fadeInUp delay-1">You can change colors of almost every element</p>
                                                               <p class="text" style="font-size: 18px;" data-animate-in="fadeInUp delay-1-5">You have never seen so many options</p>
                                                            </div>
                                                         </div>
                                                      </a>
                                                   </div>
                                                   <!-- end: slide -->
                                                   <!-- Slide 2 -->
                                                   <div class="item">
                                                      <a href="ultimo-responsive-magento-theme/index.html" title="Click to see all features">
                                                         <div class="ban ban-effect-1">
                                                            <img class="image" src="../media/wysiwyg/infortis/slideshow/a02.jpg" alt="Responsive Magento Theme" />
                                                            <div class="cap cap-bottom-left cap-push-up-10 cap-push-right-10 cap-no-bg cap-text-bg cap-text-bg-dark-2">
                                                               <h2 class="text" style="font-size: 36px;" data-animate-in="fadeInRight">Responsive</h2>
                                                               <p class="text" style="font-size: 18px;" data-animate-in="fadeInRight delay-0-5">This theme can adapt to any mobile screen resolution</p>
                                                            </div>
                                                         </div>
                                                      </a>
                                                   </div>
                                                   <!-- end: slide -->
                                                   <!-- Slide 3 -->
                                                   <div class="item">
                                                      <div class="ban ban-effect-1">
                                                         <img class="image" src="../media/wysiwyg/infortis/slideshow/a03.jpg" alt="Sample Slide" />
                                                         <div class="cap cap-bottom cap-no-bg cap-push-up-10 cap-push-right-10">
                                                            <a href="dress7.html" title="Click to see the product">
                                                               <div class="feature feature-icon-hover indent indent-size-xl">
                                                                  <span class="ic ic-2x ic-heart-o ib ib-size-xl ib-ef-3 ib-ef-3a" data-animate-in="fadeInDown delay-1"></span>
                                                                  <h3 class="no-margin" style="color: #fff; font-size: 35px; line-height: 80px;" data-animate-in="fadeInDown">Super Promo</h3>
                                                               </div>
                                                            </a>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <!-- end: slide -->
                                                   <!-- Slide 4 -->
                                                   <div class="item">
                                                      <a href="http://themeforest.net/item/ultimo-fluid-responsive-magento-theme/3231798?ref=infortis" title="Go to ThemeForest marketplace to buy this theme">
                                                         <div class="ban ban-effect-fade-out">
                                                            <img class="image" src="../media/wysiwyg/infortis/slideshow/a04.png" alt="Buy Ultimo theme" />
                                                         </div>
                                                      </a>
                                                   </div>
                                                   <!-- end: slide -->
                                                </div>
                                             </div>
                                             <div id="slideshow-banners-276fd886786923f5e5b18bb85dce7cc4" class="slideshow-banners col-sm-3 no-right-gutter hidden-xs">
                                                <div class="row row-bottom-gutter-half">
                                                   <!-- Banner 1 -->
                                                   <div class="col-md-12 small-banner">
                                                      <a href="http://themeforest.net/item/ultimo-fluid-responsive-magento-theme/3231798?ref=infortis" title="Go to ThemeForest marketplace to buy this theme">
                                                         <div class="ban ban-effect-3">
                                                            <img class="image" src="../media/wysiwyg/infortis/slideshow/banners/a01.jpg" alt="Banner" />
                                                            <div class="cap cap-bottom-right cap-no-bg cap-text-bg cap-text-bg-light-1 hidden-sm" style="background-color: rgba(91, 210, 236, 0.85);">
                                                               <h4 class="text">Buy This Theme</h4>
                                                            </div>
                                                         </div>
                                                      </a>
                                                   </div>
                                                   <!-- Banner 2 -->
                                                   <div class="col-md-12 small-banner">
                                                      <a href="http://ultimo.infortis-themes.com/demo/select-demo/">
                                                         <div class="ban ban-effect-3">
                                                            <img class="image" src="../media/wysiwyg/infortis/slideshow/banners/a02.jpg" alt="Banner" />
                                                            <div class="cap cap-bottom-right cap-no-bg cap-text-bg cap-text-bg-dark-1 hidden-sm" style="background-color: rgba(245, 88, 86, 0.85);">
                                                               <h4 class="text">See All Demos</h4>
                                                            </div>
                                                         </div>
                                                      </a>
                                                   </div>
                                                   <!-- Banner 3 -->
                                                   <div class="col-md-12 small-banner">
                                                      <div class="ban ban-effect-3">
                                                         <img class="image" src="../media/wysiwyg/infortis/slideshow/banners/a03.jpg" alt="Banner" />
                                                         <div class="cap cap-bottom-right cap-no-bg cap-text-bg cap-text-bg-light-1 hidden-sm" style="background-color: rgba(135, 195, 30, 0.85);">
                                                            <h4 class="text">Fully Responsive</h4>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <!-- end: row -->
                                             </div>
                                          </div>
                                       </div>
                                       <script type="text/javascript">
                                          //<![CDATA[
                                              jQuery(function($) {

                                                  var owl = $('#slideshow-276fd886786923f5e5b18bb85dce7cc4');
                                                  owl.owlCarousel({

                                                      navigationText: false
                                                      , addClassActive: true

                                                              , singleItem: true

                                                                      , transitionStyle: 'fadeUp'


                                                              , slideSpeed: 500
                                                              , paginationSpeed: 500
                                                              , autoPlay: 10000
                                                              , stopOnHover: true

                                                              , rewindNav: true
                                                      , rewindSpeed: 600

                                                              , pagination: true

                                                              , navigation: true


                                                      , beforeInit: function() {
                                                          var firstSlide = owl.find('.item:eq(0)');
                                                          firstSlide.find('[data-animate-in]').each(function() {
                                                              $(this).addClass($(this).data('animate-in') + ' animated');
                                                          });
                                                      }

                                                                  , afterMove: function() {
                                                          owl.find('.owl-item.active [data-animate-in]').each(function() {
                                                              $(this).addClass($(this).data('animate-in') + ' animated');
                                                          });
                                                      }

                                                      , beforeMove: function() {
                                                          owl.find('.owl-item.active [data-animate-in]').each(function() {
                                                              $(this).removeClass($(this).data('animate-in') + ' animated');
                                                          });
                                                      }

                                                              , afterInit: function() {
                                                          owl.parent().parent().children('#slideshow-banners-276fd886786923f5e5b18bb85dce7cc4').addClass('_show');
                                                      }

                                                  }); // end: owl

                                              });
                                          //]]>
                                       </script>
                                    </div>
                                    <div id="page-columns" class="columns">
                                       <div class="column-main">
                                          <div class="std">
                                             <br/>
                                             <br/>
                                             <br/>
                                             <div class="row row-bottom-gutter">
                                                <div class="col-md-4">
                                                   <div class="feature feature-icon-hover indent indent-size-xl">
                                                      <span class="ic ic-2x ic-paint ib ib-size-xl ib-ef-1 ib-ef-1a"></span>
                                                      <h6 class="above-heading">Customizable design</h6>
                                                      <h3>Unlimited Colors</h3>
                                                      <p>You have never seen so many options! Change colors of dozens of elements, apply textures, upload background images...</p>
                                                      <a href="ultimo-responsive-magento-theme/index.html" class="go">See all features</a>
                                                   </div>
                                                </div>
                                                <div class="col-md-4">
                                                   <div class="feature feature-icon-hover indent indent-size-xl">
                                                      <span class="ic ic-2x ic-smartphones ib ib-size-xl ib-ef-1 ib-ef-1a"></span>
                                                      <h6 class="above-heading">12-column grid</h6>
                                                      <h3>Responsive Layout</h3>
                                                      <p>Ultimo can be displayed on any screen. It is based on fluid grid system. If screen is resized, layout will be automatically adjusted...</p>
                                                      <a href="ultimo-responsive-magento-theme/index.html" class="go">See all features</a>
                                                   </div>
                                                </div>
                                                <div class="col-md-4">
                                                   <div class="feature feature-icon-hover indent indent-size-xl">
                                                      <span class="ic ic-2x ic-menu ib ib-size-xl ib-ef-1 ib-ef-1a"></span>
                                                      <h6 class="above-heading">Customizable drop-down menu</h6>
                                                      <h3>Mega Menu</h3>
                                                      <p>Two styles: wide mega menu or classic drop-down menu. You can add any custom content to any category in the catalog...</p>
                                                      <a href="ultimo-responsive-magento-theme/index.html" class="go">See all features</a>
                                                   </div>
                                                </div>
                                             </div>
                                             <br/>
                                             <strong class="section-title padding-right">Our Featured Products</strong>
                                             <div class="itemslider-wrapper slider-arrows1 slider-arrows1-pos-top-right slider-pagination1">
                                                <div id="itemslider-featured-4ad9e655957839bc161d16ef617fe17f" class="itemslider itemslider-responsive products-grid centered">
                                                   <div class="item">
                                                      <div class="product-image-wrapper" style="max-width:196px;">
                                                         <a href="top1.html" title="Product with Variants" class="product-image">
                                                         <img class="lazyOwl" data-src="http://ultimo.infortis-themes.com/demo/media/catalog/product/cache/1/small_image/196x/040ec09b1e35df139433887a97daa66f/3/b/3b.jpg" alt="Product with Variants" />
                                                         <img class="alt-img" src="../media/catalog/product/cache/1/small_image/196x/040ec09b1e35df139433887a97daa66f/3/b/3bb.jpg" alt="Product with Variants" />
                                                         </a>
                                                         <ul class="add-to-links clearer addto-links-icons addto-onimage visible-onhover">
                                                            <li><a class="link-wishlist"
                                                               href="wishlist/index/add/product/35/form_key/XlJE1UQDDgAjqV67/index.html"
                                                               title="Add to Wishlist">
                                                               <span class="2 icon ib ic ic-heart"></span>
                                                               </a>
                                                            </li>
                                                            <li><a class="link-compare"
                                                               href="catalog/product_compare/add/product/35/uenc/aHR0cDovL3VsdGltby5pbmZvcnRpcy10aGVtZXMuY29tL2RlbW8vZGVmYXVsdC8%2c/form_key/XlJE1UQDDgAjqV67/index.html"
                                                               title="Add to Compare">
                                                               <span class="2 icon ib ic ic-compare"></span>
                                                               </a>
                                                            </li>
                                                         </ul>
                                                      </div>
                                                      <!-- end: product-image-wrapper -->
                                                      <strong class="product-name"><a href="top1.html" title="Product with Variants">Product with Variants</a></strong>
                                                      <div class="ratings">
                                                         <div class="rating-box">
                                                            <div class="rating" style="width:80%"></div>
                                                         </div>
                                                         <span class="amount">2 Review(s)</span>
                                                      </div>
                                                      <div class="price-box">
                                                         <span class="regular-price" id="product-price-354ad9e655957839bc161d16ef617fe17f">
                                                         <span class="price">$40.00</span>                                    </span>
                                                      </div>
                                                      <div class="actions">
                                                      </div>
                                                   </div>
                                                   <div class="item">
                                                      <div class="product-image-wrapper" style="max-width:196px;">
                                                         <a href="top9.html" title="alt" class="product-image">
                                                         <img class="lazyOwl" data-src="http://ultimo.infortis-themes.com/demo/media/catalog/product/cache/1/small_image/196x/040ec09b1e35df139433887a97daa66f/1/_/1_4.jpg" alt="alt" />
                                                         <img class="alt-img" src="../media/catalog/product/cache/1/small_image/196x/040ec09b1e35df139433887a97daa66f/1/_/1_4.jpg" alt="Configurable Product" />
                                                         </a>
                                                         <ul class="add-to-links clearer addto-links-icons addto-onimage visible-onhover">
                                                            <li><a class="link-wishlist"
                                                               href="wishlist/index/add/product/81/form_key/XlJE1UQDDgAjqV67/index.html"
                                                               title="Add to Wishlist">
                                                               <span class="2 icon ib ic ic-heart"></span>
                                                               </a>
                                                            </li>
                                                            <li><a class="link-compare"
                                                               href="catalog/product_compare/add/product/81/uenc/aHR0cDovL3VsdGltby5pbmZvcnRpcy10aGVtZXMuY29tL2RlbW8vZGVmYXVsdC8%2c/form_key/XlJE1UQDDgAjqV67/index.html"
                                                               title="Add to Compare">
                                                               <span class="2 icon ib ic ic-compare"></span>
                                                               </a>
                                                            </li>
                                                         </ul>
                                                      </div>
                                                      <!-- end: product-image-wrapper -->
                                                      <strong class="product-name"><a href="top9.html" title="Configurable Product">Configurable Product</a></strong>
                                                      <div class="ratings">
                                                         <div class="rating-box">
                                                            <div class="rating" style="width:100%"></div>
                                                         </div>
                                                         <span class="amount">1 Review(s)</span>
                                                      </div>
                                                      <div class="price-box">
                                                         <span class="regular-price" id="product-price-814ad9e655957839bc161d16ef617fe17f">
                                                         <span class="price">$100.00</span>                                    </span>
                                                      </div>
                                                      <div class="actions">
                                                      </div>
                                                   </div>
                                                   <div class="item">
                                                      <div class="product-image-wrapper" style="max-width:196px;">
                                                         <a href="top3.html" title="Product Example" class="product-image">
                                                         <img class="lazyOwl" data-src="http://ultimo.infortis-themes.com/demo/media/catalog/product/cache/1/small_image/196x/040ec09b1e35df139433887a97daa66f/2/b/2b_2.jpg" alt="Product Example" />
                                                         <img class="alt-img" src="../media/catalog/product/cache/1/small_image/196x/040ec09b1e35df139433887a97daa66f/9/_/9_1.jpg" alt="Product Example" />
                                                         </a>
                                                         <ul class="add-to-links clearer addto-links-icons addto-onimage visible-onhover">
                                                            <li><a class="link-wishlist"
                                                               href="wishlist/index/add/product/37/form_key/XlJE1UQDDgAjqV67/index.html"
                                                               title="Add to Wishlist">
                                                               <span class="2 icon ib ic ic-heart"></span>
                                                               </a>
                                                            </li>
                                                            <li><a class="link-compare"
                                                               href="catalog/product_compare/add/product/37/uenc/aHR0cDovL3VsdGltby5pbmZvcnRpcy10aGVtZXMuY29tL2RlbW8vZGVmYXVsdC8%2c/form_key/XlJE1UQDDgAjqV67/index.html"
                                                               title="Add to Compare">
                                                               <span class="2 icon ib ic ic-compare"></span>
                                                               </a>
                                                            </li>
                                                         </ul>
                                                      </div>
                                                      <!-- end: product-image-wrapper -->
                                                      <strong class="product-name"><a href="top3.html" title="Product Example">Product Example</a></strong>
                                                      <div class="ratings">
                                                         <div class="rating-box">
                                                            <div class="rating" style="width:80%"></div>
                                                         </div>
                                                         <span class="amount">1 Review(s)</span>
                                                      </div>
                                                      <div class="price-box">
                                                         <span class="regular-price" id="product-price-374ad9e655957839bc161d16ef617fe17f">
                                                         <span class="price">$99.00</span>                                    </span>
                                                      </div>
                                                      <div class="actions">
                                                      </div>
                                                   </div>
                                                   <div class="item">
                                                      <div class="product-image-wrapper" style="max-width:196px;">
                                                         <a href="top4.html" title="New Hot Top" class="product-image">
                                                         <img class="lazyOwl" data-src="http://ultimo.infortis-themes.com/demo/media/catalog/product/cache/1/small_image/196x/040ec09b1e35df139433887a97daa66f/4/_/4_3.jpg" alt="New Hot Top" />
                                                         <img class="alt-img" src="../media/catalog/product/cache/1/small_image/196x/040ec09b1e35df139433887a97daa66f/4/c/4c.jpg" alt="New Hot Top" />
                                                         </a>
                                                         <ul class="add-to-links clearer addto-links-icons addto-onimage visible-onhover">
                                                            <li><a class="link-wishlist"
                                                               href="wishlist/index/add/product/39/form_key/XlJE1UQDDgAjqV67/index.html"
                                                               title="Add to Wishlist">
                                                               <span class="2 icon ib ic ic-heart"></span>
                                                               </a>
                                                            </li>
                                                            <li><a class="link-compare"
                                                               href="catalog/product_compare/add/product/39/uenc/aHR0cDovL3VsdGltby5pbmZvcnRpcy10aGVtZXMuY29tL2RlbW8vZGVmYXVsdC8%2c/form_key/XlJE1UQDDgAjqV67/index.html"
                                                               title="Add to Compare">
                                                               <span class="2 icon ib ic ic-compare"></span>
                                                               </a>
                                                            </li>
                                                         </ul>
                                                      </div>
                                                      <!-- end: product-image-wrapper -->
                                                      <strong class="product-name"><a href="top4.html" title="New Hot Top">New Hot Top</a></strong>
                                                      <div class="ratings">
                                                         <div class="rating-box">
                                                            <div class="rating" style="width:100%"></div>
                                                         </div>
                                                         <span class="amount">2 Review(s)</span>
                                                      </div>
                                                      <div class="price-box">
                                                         <span class="regular-price" id="product-price-394ad9e655957839bc161d16ef617fe17f">
                                                         <span class="price">$80.00</span>                                    </span>
                                                      </div>
                                                      <div class="actions">
                                                      </div>
                                                   </div>
                                                   <div class="item">
                                                      <div class="product-image-wrapper" style="max-width:196px;">
                                                         <a href="top5.html" title="Another Sample Top" class="product-image">
                                                         <img class="lazyOwl" data-src="http://ultimo.infortis-themes.com/demo/media/catalog/product/cache/1/small_image/196x/040ec09b1e35df139433887a97daa66f/6/_/6.jpg" alt="Another Sample Top" />
                                                         </a>
                                                         <ul class="add-to-links clearer addto-links-icons addto-onimage visible-onhover">
                                                            <li><a class="link-wishlist"
                                                               href="wishlist/index/add/product/40/form_key/XlJE1UQDDgAjqV67/index.html"
                                                               title="Add to Wishlist">
                                                               <span class="2 icon ib ic ic-heart"></span>
                                                               </a>
                                                            </li>
                                                            <li><a class="link-compare"
                                                               href="catalog/product_compare/add/product/40/uenc/aHR0cDovL3VsdGltby5pbmZvcnRpcy10aGVtZXMuY29tL2RlbW8vZGVmYXVsdC8%2c/form_key/XlJE1UQDDgAjqV67/index.html"
                                                               title="Add to Compare">
                                                               <span class="2 icon ib ic ic-compare"></span>
                                                               </a>
                                                            </li>
                                                         </ul>
                                                      </div>
                                                      <!-- end: product-image-wrapper -->
                                                      <strong class="product-name"><a href="top5.html" title="Another Sample Top">Another Sample Top</a></strong>
                                                      <div class="ratings">
                                                         <div class="rating-box">
                                                            <div class="rating" style="width:80%"></div>
                                                         </div>
                                                         <span class="amount">1 Review(s)</span>
                                                      </div>
                                                      <div class="price-box">
                                                         <span class="regular-price" id="product-price-404ad9e655957839bc161d16ef617fe17f">
                                                         <span class="price">$110.00</span>                                    </span>
                                                      </div>
                                                      <div class="actions">
                                                      </div>
                                                   </div>
                                                   <div class="item">
                                                      <div class="product-image-wrapper" style="max-width:196px;">
                                                         <a href="top6.html" title="Next Sample Product" class="product-image">
                                                         <img class="lazyOwl" data-src="http://ultimo.infortis-themes.com/demo/media/catalog/product/cache/1/small_image/196x/040ec09b1e35df139433887a97daa66f/5/_/5.jpg" alt="Next Sample Product" />
                                                         </a>
                                                         <ul class="add-to-links clearer addto-links-icons addto-onimage visible-onhover">
                                                            <li><a class="link-wishlist"
                                                               href="wishlist/index/add/product/41/form_key/XlJE1UQDDgAjqV67/index.html"
                                                               title="Add to Wishlist">
                                                               <span class="2 icon ib ic ic-heart"></span>
                                                               </a>
                                                            </li>
                                                            <li><a class="link-compare"
                                                               href="catalog/product_compare/add/product/41/uenc/aHR0cDovL3VsdGltby5pbmZvcnRpcy10aGVtZXMuY29tL2RlbW8vZGVmYXVsdC8%2c/form_key/XlJE1UQDDgAjqV67/index.html"
                                                               title="Add to Compare">
                                                               <span class="2 icon ib ic ic-compare"></span>
                                                               </a>
                                                            </li>
                                                         </ul>
                                                      </div>
                                                      <!-- end: product-image-wrapper -->
                                                      <strong class="product-name"><a href="top6.html" title="Next Sample Product">Next Sample Product</a></strong>
                                                      <div class="ratings">
                                                         <div class="rating-box">
                                                            <div class="rating" style="width:60%"></div>
                                                         </div>
                                                         <span class="amount">1 Review(s)</span>
                                                      </div>
                                                      <div class="price-box">
                                                         <span class="regular-price" id="product-price-414ad9e655957839bc161d16ef617fe17f">
                                                         <span class="price">$110.00</span>                                    </span>
                                                      </div>
                                                      <div class="actions">
                                                      </div>
                                                   </div>
                                                   <div class="item">
                                                      <div class="product-image-wrapper" style="max-width:196px;">
                                                         <a href="top7.html" title="Some Other Product" class="product-image">
                                                         <img class="lazyOwl" data-src="http://ultimo.infortis-themes.com/demo/media/catalog/product/cache/1/small_image/196x/040ec09b1e35df139433887a97daa66f/7/b/7b.jpg" alt="Some Other Product" />
                                                         </a>
                                                         <ul class="add-to-links clearer addto-links-icons addto-onimage visible-onhover">
                                                            <li><a class="link-wishlist"
                                                               href="wishlist/index/add/product/42/form_key/XlJE1UQDDgAjqV67/index.html"
                                                               title="Add to Wishlist">
                                                               <span class="2 icon ib ic ic-heart"></span>
                                                               </a>
                                                            </li>
                                                            <li><a class="link-compare"
                                                               href="catalog/product_compare/add/product/42/uenc/aHR0cDovL3VsdGltby5pbmZvcnRpcy10aGVtZXMuY29tL2RlbW8vZGVmYXVsdC8%2c/form_key/XlJE1UQDDgAjqV67/index.html"
                                                               title="Add to Compare">
                                                               <span class="2 icon ib ic ic-compare"></span>
                                                               </a>
                                                            </li>
                                                         </ul>
                                                      </div>
                                                      <!-- end: product-image-wrapper -->
                                                      <strong class="product-name"><a href="top7.html" title="Some Other Product">Some Other Product</a></strong>
                                                      <div class="ratings">
                                                         <div class="rating-box">
                                                            <div class="rating" style="width:100%"></div>
                                                         </div>
                                                         <span class="amount">1 Review(s)</span>
                                                      </div>
                                                      <div class="price-box">
                                                         <span class="regular-price" id="product-price-424ad9e655957839bc161d16ef617fe17f">
                                                         <span class="price">$200.00</span>                                    </span>
                                                      </div>
                                                      <div class="actions">
                                                      </div>
                                                   </div>
                                                   <div class="item">
                                                      <div class="product-image-wrapper" style="max-width:196px;">
                                                         <a href="top8.html" title="Simple Woman Top" class="product-image">
                                                         <img class="lazyOwl" data-src="http://ultimo.infortis-themes.com/demo/media/catalog/product/cache/1/small_image/196x/040ec09b1e35df139433887a97daa66f/8/_/8_1.jpg" alt="Simple Woman Top" />
                                                         </a>
                                                         <ul class="add-to-links clearer addto-links-icons addto-onimage visible-onhover">
                                                            <li><a class="link-wishlist"
                                                               href="wishlist/index/add/product/43/form_key/XlJE1UQDDgAjqV67/index.html"
                                                               title="Add to Wishlist">
                                                               <span class="2 icon ib ic ic-heart"></span>
                                                               </a>
                                                            </li>
                                                            <li><a class="link-compare"
                                                               href="catalog/product_compare/add/product/43/uenc/aHR0cDovL3VsdGltby5pbmZvcnRpcy10aGVtZXMuY29tL2RlbW8vZGVmYXVsdC8%2c/form_key/XlJE1UQDDgAjqV67/index.html"
                                                               title="Add to Compare">
                                                               <span class="2 icon ib ic ic-compare"></span>
                                                               </a>
                                                            </li>
                                                         </ul>
                                                      </div>
                                                      <!-- end: product-image-wrapper -->
                                                      <strong class="product-name"><a href="top8.html" title="Simple Woman Top">Simple Woman Top</a></strong>
                                                      <div class="ratings">
                                                         <div class="rating-box">
                                                            <div class="rating" style="width:100%"></div>
                                                         </div>
                                                         <span class="amount">1 Review(s)</span>
                                                      </div>
                                                      <div class="price-box">
                                                         <span class="regular-price" id="product-price-434ad9e655957839bc161d16ef617fe17f">
                                                         <span class="price">$39.50</span>                                    </span>
                                                      </div>
                                                      <div class="actions">
                                                      </div>
                                                   </div>
                                                </div>
                                                <!-- end: itemslider -->
                                             </div>
                                             <!-- end: itemslider-wrapper -->
                                             <script type="text/javascript">
                                                //<![CDATA[
                                                    jQuery(function($) {

                                                        var owl = $('#itemslider-featured-4ad9e655957839bc161d16ef617fe17f');
                                                        owl.owlCarousel({

                                                                    lazyLoad: true,

                                                                    itemsCustom: [ [0, 1], [320, 2], [480, 3], [768, 4], [960, 5], [1280, 6] ],
                                                            responsiveRefreshRate: 50,

                                                                    slideSpeed: 200,

                                                                    paginationSpeed: 500,

                                                                    scrollPerPage: true,


                                                                    stopOnHover: true,

                                                                    rewindNav: true,
                                                            rewindSpeed: 600,

                                                                    pagination: true,

                                                            navigation: true,
                                                            navigationText: false

                                                        }); //end: owl

                                                    });
                                                //]]>
                                             </script>
                                             <br/>
                                             <br/>
                                             <div class="grid12-6 no-left-gutter">
                                                <strong class="section-title padding-right">Recommended Shoes</strong>
                                                <div class="itemslider-wrapper slider-arrows1 slider-arrows1-pos-top-right slider-pagination1">
                                                   <div id="itemslider-featured-5f71739b4c83e563b86c6709478ffa57" class="itemslider itemslider-responsive products-grid size-s centered">
                                                      <div class="item">
                                                         <div class="product-image-wrapper" style="max-width:196px;">
                                                            <a href="shoe1.html" title="Sample Shoe" class="product-image">
                                                            <img class="lazyOwl" data-src="http://ultimo.infortis-themes.com/demo/media/catalog/product/cache/1/small_image/196x/040ec09b1e35df139433887a97daa66f/4/2/428381-0001_1_1.jpg" alt="Sample Shoe" />
                                                            </a>
                                                            <ul class="add-to-links clearer addto-links-icons addto-onimage visible-onhover">
                                                               <li><a class="link-wishlist"
                                                                  href="wishlist/index/add/product/61/form_key/XlJE1UQDDgAjqV67/index.html"
                                                                  title="Add to Wishlist">
                                                                  <span class="2 icon ib ic ic-heart"></span>
                                                                  </a>
                                                               </li>
                                                               <li><a class="link-compare"
                                                                  href="catalog/product_compare/add/product/61/uenc/aHR0cDovL3VsdGltby5pbmZvcnRpcy10aGVtZXMuY29tL2RlbW8vZGVmYXVsdC8%2c/form_key/XlJE1UQDDgAjqV67/index.html"
                                                                  title="Add to Compare">
                                                                  <span class="2 icon ib ic ic-compare"></span>
                                                                  </a>
                                                               </li>
                                                            </ul>
                                                         </div>
                                                         <!-- end: product-image-wrapper -->
                                                         <strong class="product-name"><a href="shoe1.html" title="Sample Shoe">Sample Shoe</a></strong>
                                                         <div class="price-box">
                                                            <span class="regular-price" id="product-price-615f71739b4c83e563b86c6709478ffa57">
                                                            <span class="price">$150.55</span>                                    </span>
                                                         </div>
                                                         <div class="actions">
                                                         </div>
                                                      </div>
                                                      <div class="item">
                                                         <div class="product-image-wrapper" style="max-width:196px;">
                                                            <a href="shoe2.html" title="Every Day Heel" class="product-image">
                                                            <img class="lazyOwl" data-src="http://ultimo.infortis-themes.com/demo/media/catalog/product/cache/1/small_image/196x/040ec09b1e35df139433887a97daa66f/4/2/428374-0023_1_1.jpg" alt="Every Day Heel" />
                                                            </a>
                                                            <ul class="add-to-links clearer addto-links-icons addto-onimage visible-onhover">
                                                               <li><a class="link-wishlist"
                                                                  href="wishlist/index/add/product/62/form_key/XlJE1UQDDgAjqV67/index.html"
                                                                  title="Add to Wishlist">
                                                                  <span class="2 icon ib ic ic-heart"></span>
                                                                  </a>
                                                               </li>
                                                               <li><a class="link-compare"
                                                                  href="catalog/product_compare/add/product/62/uenc/aHR0cDovL3VsdGltby5pbmZvcnRpcy10aGVtZXMuY29tL2RlbW8vZGVmYXVsdC8%2c/form_key/XlJE1UQDDgAjqV67/index.html"
                                                                  title="Add to Compare">
                                                                  <span class="2 icon ib ic ic-compare"></span>
                                                                  </a>
                                                               </li>
                                                            </ul>
                                                         </div>
                                                         <!-- end: product-image-wrapper -->
                                                         <strong class="product-name"><a href="shoe2.html" title="Every Day Heel">Every Day Heel</a></strong>
                                                         <div class="price-box">
                                                            <span class="regular-price" id="product-price-625f71739b4c83e563b86c6709478ffa57">
                                                            <span class="price">$95.30</span>                                    </span>
                                                         </div>
                                                         <div class="actions">
                                                         </div>
                                                      </div>
                                                      <div class="item">
                                                         <div class="product-image-wrapper" style="max-width:196px;">
                                                            <a href="shoe3.html" title="Prestige Lite Heels" class="product-image">
                                                            <img class="lazyOwl" data-src="http://ultimo.infortis-themes.com/demo/media/catalog/product/cache/1/small_image/196x/040ec09b1e35df139433887a97daa66f/4/2/428357-0001_1_1_1.jpg" alt="Prestige Lite Heels" />
                                                            </a>
                                                            <ul class="add-to-links clearer addto-links-icons addto-onimage visible-onhover">
                                                               <li><a class="link-wishlist"
                                                                  href="wishlist/index/add/product/63/form_key/XlJE1UQDDgAjqV67/index.html"
                                                                  title="Add to Wishlist">
                                                                  <span class="2 icon ib ic ic-heart"></span>
                                                                  </a>
                                                               </li>
                                                               <li><a class="link-compare"
                                                                  href="catalog/product_compare/add/product/63/uenc/aHR0cDovL3VsdGltby5pbmZvcnRpcy10aGVtZXMuY29tL2RlbW8vZGVmYXVsdC8%2c/form_key/XlJE1UQDDgAjqV67/index.html"
                                                                  title="Add to Compare">
                                                                  <span class="2 icon ib ic ic-compare"></span>
                                                                  </a>
                                                               </li>
                                                            </ul>
                                                         </div>
                                                         <!-- end: product-image-wrapper -->
                                                         <strong class="product-name"><a href="shoe3.html" title="Prestige Lite Heels">Prestige Lite Heels</a></strong>
                                                         <div class="price-box">
                                                            <span class="regular-price" id="product-price-635f71739b4c83e563b86c6709478ffa57">
                                                            <span class="price">$215.00</span>                                    </span>
                                                         </div>
                                                         <div class="actions">
                                                         </div>
                                                      </div>
                                                      <div class="item">
                                                         <div class="product-image-wrapper" style="max-width:196px;">
                                                            <a href="shoe4.html" title="Metropolis Classy" class="product-image">
                                                            <img class="lazyOwl" data-src="http://ultimo.infortis-themes.com/demo/media/catalog/product/cache/1/small_image/196x/040ec09b1e35df139433887a97daa66f/9/1/917397-0134_1_1_1_1.jpg" alt="Metropolis Classy" />
                                                            </a>
                                                            <ul class="add-to-links clearer addto-links-icons addto-onimage visible-onhover">
                                                               <li><a class="link-wishlist"
                                                                  href="wishlist/index/add/product/64/form_key/XlJE1UQDDgAjqV67/index.html"
                                                                  title="Add to Wishlist">
                                                                  <span class="2 icon ib ic ic-heart"></span>
                                                                  </a>
                                                               </li>
                                                               <li><a class="link-compare"
                                                                  href="catalog/product_compare/add/product/64/uenc/aHR0cDovL3VsdGltby5pbmZvcnRpcy10aGVtZXMuY29tL2RlbW8vZGVmYXVsdC8%2c/form_key/XlJE1UQDDgAjqV67/index.html"
                                                                  title="Add to Compare">
                                                                  <span class="2 icon ib ic ic-compare"></span>
                                                                  </a>
                                                               </li>
                                                            </ul>
                                                         </div>
                                                         <!-- end: product-image-wrapper -->
                                                         <strong class="product-name"><a href="shoe4.html" title="Metropolis Classy">Metropolis Classy</a></strong>
                                                         <div class="price-box">
                                                            <span class="regular-price" id="product-price-645f71739b4c83e563b86c6709478ffa57">
                                                            <span class="price">$199.50</span>                                    </span>
                                                         </div>
                                                         <div class="actions">
                                                         </div>
                                                      </div>
                                                      <div class="item">
                                                         <div class="product-image-wrapper" style="max-width:196px;">
                                                            <a href="shoe5.html" title="Metropolis High Heels" class="product-image">
                                                            <img class="lazyOwl" data-src="http://ultimo.infortis-themes.com/demo/media/catalog/product/cache/1/small_image/196x/040ec09b1e35df139433887a97daa66f/4/2/428446-0067_1_1_1_1_1.jpg" alt="Metropolis High Heels" />
                                                            </a>
                                                            <ul class="add-to-links clearer addto-links-icons addto-onimage visible-onhover">
                                                               <li><a class="link-wishlist"
                                                                  href="wishlist/index/add/product/65/form_key/XlJE1UQDDgAjqV67/index.html"
                                                                  title="Add to Wishlist">
                                                                  <span class="2 icon ib ic ic-heart"></span>
                                                                  </a>
                                                               </li>
                                                               <li><a class="link-compare"
                                                                  href="catalog/product_compare/add/product/65/uenc/aHR0cDovL3VsdGltby5pbmZvcnRpcy10aGVtZXMuY29tL2RlbW8vZGVmYXVsdC8%2c/form_key/XlJE1UQDDgAjqV67/index.html"
                                                                  title="Add to Compare">
                                                                  <span class="2 icon ib ic ic-compare"></span>
                                                                  </a>
                                                               </li>
                                                            </ul>
                                                         </div>
                                                         <!-- end: product-image-wrapper -->
                                                         <strong class="product-name"><a href="shoe5.html" title="Metropolis High Heels">Metropolis High Heels</a></strong>
                                                         <div class="price-box">
                                                            <span class="regular-price" id="product-price-655f71739b4c83e563b86c6709478ffa57">
                                                            <span class="price">$140.00</span>                                    </span>
                                                         </div>
                                                         <div class="actions">
                                                         </div>
                                                      </div>
                                                      <div class="item">
                                                         <div class="product-image-wrapper" style="max-width:196px;">
                                                            <a href="shoe6.html" title="Fire Fenix Shoe" class="product-image">
                                                            <img class="lazyOwl" data-src="http://ultimo.infortis-themes.com/demo/media/catalog/product/cache/1/small_image/196x/040ec09b1e35df139433887a97daa66f/4/2/428507-0066_1_1.jpg" alt="Fire Fenix Shoe" />
                                                            </a>
                                                            <ul class="add-to-links clearer addto-links-icons addto-onimage visible-onhover">
                                                               <li><a class="link-wishlist"
                                                                  href="wishlist/index/add/product/66/form_key/XlJE1UQDDgAjqV67/index.html"
                                                                  title="Add to Wishlist">
                                                                  <span class="2 icon ib ic ic-heart"></span>
                                                                  </a>
                                                               </li>
                                                               <li><a class="link-compare"
                                                                  href="catalog/product_compare/add/product/66/uenc/aHR0cDovL3VsdGltby5pbmZvcnRpcy10aGVtZXMuY29tL2RlbW8vZGVmYXVsdC8%2c/form_key/XlJE1UQDDgAjqV67/index.html"
                                                                  title="Add to Compare">
                                                                  <span class="2 icon ib ic ic-compare"></span>
                                                                  </a>
                                                               </li>
                                                            </ul>
                                                         </div>
                                                         <!-- end: product-image-wrapper -->
                                                         <strong class="product-name"><a href="shoe6.html" title="Fire Fenix Shoe">Fire Fenix Shoe</a></strong>
                                                         <div class="price-box">
                                                            <span class="regular-price" id="product-price-665f71739b4c83e563b86c6709478ffa57">
                                                            <span class="price">$140.00</span>                                    </span>
                                                         </div>
                                                         <div class="actions">
                                                         </div>
                                                      </div>
                                                      <div class="item">
                                                         <div class="product-image-wrapper" style="max-width:196px;">
                                                            <a href="shoe7.html" title="Crimson Wave Heel" class="product-image">
                                                            <img class="lazyOwl" data-src="http://ultimo.infortis-themes.com/demo/media/catalog/product/cache/1/small_image/196x/040ec09b1e35df139433887a97daa66f/9/2/928172-0484_1.jpg" alt="Crimson Wave Heel" />
                                                            </a>
                                                            <ul class="add-to-links clearer addto-links-icons addto-onimage visible-onhover">
                                                               <li><a class="link-wishlist"
                                                                  href="wishlist/index/add/product/67/form_key/XlJE1UQDDgAjqV67/index.html"
                                                                  title="Add to Wishlist">
                                                                  <span class="2 icon ib ic ic-heart"></span>
                                                                  </a>
                                                               </li>
                                                               <li><a class="link-compare"
                                                                  href="catalog/product_compare/add/product/67/uenc/aHR0cDovL3VsdGltby5pbmZvcnRpcy10aGVtZXMuY29tL2RlbW8vZGVmYXVsdC8%2c/form_key/XlJE1UQDDgAjqV67/index.html"
                                                                  title="Add to Compare">
                                                                  <span class="2 icon ib ic ic-compare"></span>
                                                                  </a>
                                                               </li>
                                                            </ul>
                                                         </div>
                                                         <!-- end: product-image-wrapper -->
                                                         <strong class="product-name"><a href="shoe7.html" title="Crimson Wave Heel">Crimson Wave Heel</a></strong>
                                                         <div class="price-box">
                                                            <span class="regular-price" id="product-price-675f71739b4c83e563b86c6709478ffa57">
                                                            <span class="price">$85.50</span>                                    </span>
                                                         </div>
                                                         <div class="actions">
                                                         </div>
                                                      </div>
                                                      <div class="item">
                                                         <div class="product-image-wrapper" style="max-width:196px;">
                                                            <a href="shoe8.html" title="Dark Flower Shoe" class="product-image">
                                                            <img class="lazyOwl" data-src="http://ultimo.infortis-themes.com/demo/media/catalog/product/cache/1/small_image/196x/040ec09b1e35df139433887a97daa66f/4/2/428338-0029_1.jpg" alt="Dark Flower Shoe" />
                                                            </a>
                                                            <ul class="add-to-links clearer addto-links-icons addto-onimage visible-onhover">
                                                               <li><a class="link-wishlist"
                                                                  href="wishlist/index/add/product/68/form_key/XlJE1UQDDgAjqV67/index.html"
                                                                  title="Add to Wishlist">
                                                                  <span class="2 icon ib ic ic-heart"></span>
                                                                  </a>
                                                               </li>
                                                               <li><a class="link-compare"
                                                                  href="catalog/product_compare/add/product/68/uenc/aHR0cDovL3VsdGltby5pbmZvcnRpcy10aGVtZXMuY29tL2RlbW8vZGVmYXVsdC8%2c/form_key/XlJE1UQDDgAjqV67/index.html"
                                                                  title="Add to Compare">
                                                                  <span class="2 icon ib ic ic-compare"></span>
                                                                  </a>
                                                               </li>
                                                            </ul>
                                                         </div>
                                                         <!-- end: product-image-wrapper -->
                                                         <strong class="product-name"><a href="shoe8.html" title="Dark Flower Shoe">Dark Flower Shoe</a></strong>
                                                         <div class="price-box">
                                                            <span class="regular-price" id="product-price-685f71739b4c83e563b86c6709478ffa57">
                                                            <span class="price">$85.50</span>                                    </span>
                                                         </div>
                                                         <div class="actions">
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <!-- end: itemslider -->
                                                </div>
                                                <!-- end: itemslider-wrapper -->
                                                <script type="text/javascript">
                                                   //<![CDATA[
                                                       jQuery(function($) {

                                                           var owl = $('#itemslider-featured-5f71739b4c83e563b86c6709478ffa57');
                                                           owl.owlCarousel({

                                                                       lazyLoad: true,

                                                                       itemsCustom: [ [0, 1], [320, 2], [480, 3], [768, 2], [960, 2], [1280, 3] ],
                                                               responsiveRefreshRate: 50,

                                                                       slideSpeed: 200,

                                                                       paginationSpeed: 500,

                                                                       scrollPerPage: true,

                                                                       autoPlay: 4000,

                                                                       stopOnHover: true,

                                                                       rewindNav: true,
                                                               rewindSpeed: 600,

                                                                       pagination: false,

                                                               navigation: true,
                                                               navigationText: false

                                                           }); //end: owl

                                                       });
                                                   //]]>
                                                </script>
                                             </div>
                                             <div class="grid12-6 no-right-gutter">
                                                <strong class="section-title padding-right">Recommended Bags</strong>
                                                <div class="itemslider-wrapper slider-arrows1 slider-arrows1-pos-top-right slider-pagination1">
                                                   <div id="itemslider-featured-3384e854b691b155139ffed34be9b2cf" class="itemslider itemslider-responsive products-grid size-s centered">
                                                      <div class="item">
                                                         <div class="product-image-wrapper" style="max-width:196px;">
                                                            <a href="bag1.html" title="Sample Bag" class="product-image">
                                                            <img class="lazyOwl" data-src="http://ultimo.infortis-themes.com/demo/media/catalog/product/cache/1/small_image/196x/040ec09b1e35df139433887a97daa66f/8/5/857055-0115_1_2.jpg" alt="Sample Bag" />
                                                            </a>
                                                            <ul class="add-to-links clearer addto-links-icons addto-onimage visible-onhover">
                                                               <li><a class="link-wishlist"
                                                                  href="wishlist/index/add/product/49/form_key/XlJE1UQDDgAjqV67/index.html"
                                                                  title="Add to Wishlist">
                                                                  <span class="2 icon ib ic ic-heart"></span>
                                                                  </a>
                                                               </li>
                                                               <li><a class="link-compare"
                                                                  href="catalog/product_compare/add/product/49/uenc/aHR0cDovL3VsdGltby5pbmZvcnRpcy10aGVtZXMuY29tL2RlbW8vZGVmYXVsdC8%2c/form_key/XlJE1UQDDgAjqV67/index.html"
                                                                  title="Add to Compare">
                                                                  <span class="2 icon ib ic ic-compare"></span>
                                                                  </a>
                                                               </li>
                                                            </ul>
                                                         </div>
                                                         <!-- end: product-image-wrapper -->
                                                         <strong class="product-name"><a href="bag1.html" title="Sample Bag">Sample Bag</a></strong>
                                                         <div class="price-box">
                                                            <span class="regular-price" id="product-price-493384e854b691b155139ffed34be9b2cf">
                                                            <span class="price">$75.95</span>                                    </span>
                                                         </div>
                                                         <div class="actions">
                                                         </div>
                                                      </div>
                                                      <div class="item">
                                                         <div class="product-image-wrapper" style="max-width:196px;">
                                                            <a href="bag2.html" title="Metropolis Small Bag" class="product-image">
                                                            <img class="lazyOwl" data-src="http://ultimo.infortis-themes.com/demo/media/catalog/product/cache/1/small_image/196x/040ec09b1e35df139433887a97daa66f/7/3/734198-0056_1_2.jpg" alt="Metropolis Small Bag" />
                                                            </a>
                                                            <ul class="add-to-links clearer addto-links-icons addto-onimage visible-onhover">
                                                               <li><a class="link-wishlist"
                                                                  href="wishlist/index/add/product/50/form_key/XlJE1UQDDgAjqV67/index.html"
                                                                  title="Add to Wishlist">
                                                                  <span class="2 icon ib ic ic-heart"></span>
                                                                  </a>
                                                               </li>
                                                               <li><a class="link-compare"
                                                                  href="catalog/product_compare/add/product/50/uenc/aHR0cDovL3VsdGltby5pbmZvcnRpcy10aGVtZXMuY29tL2RlbW8vZGVmYXVsdC8%2c/form_key/XlJE1UQDDgAjqV67/index.html"
                                                                  title="Add to Compare">
                                                                  <span class="2 icon ib ic ic-compare"></span>
                                                                  </a>
                                                               </li>
                                                            </ul>
                                                         </div>
                                                         <!-- end: product-image-wrapper -->
                                                         <strong class="product-name"><a href="bag2.html" title="Metropolis Small Bag">Metropolis Small Bag</a></strong>
                                                         <div class="price-box">
                                                            <span class="regular-price" id="product-price-503384e854b691b155139ffed34be9b2cf">
                                                            <span class="price">$55.95</span>                                    </span>
                                                         </div>
                                                         <div class="actions">
                                                         </div>
                                                      </div>
                                                      <div class="item">
                                                         <div class="product-image-wrapper" style="max-width:196px;">
                                                            <a href="bag3.html" title="White Handbag" class="product-image">
                                                            <img class="lazyOwl" data-src="http://ultimo.infortis-themes.com/demo/media/catalog/product/cache/1/small_image/196x/040ec09b1e35df139433887a97daa66f/5/4/546029-0013_1_2.jpg" alt="White Handbag" />
                                                            </a>
                                                            <ul class="add-to-links clearer addto-links-icons addto-onimage visible-onhover">
                                                               <li><a class="link-wishlist"
                                                                  href="wishlist/index/add/product/51/form_key/XlJE1UQDDgAjqV67/index.html"
                                                                  title="Add to Wishlist">
                                                                  <span class="2 icon ib ic ic-heart"></span>
                                                                  </a>
                                                               </li>
                                                               <li><a class="link-compare"
                                                                  href="catalog/product_compare/add/product/51/uenc/aHR0cDovL3VsdGltby5pbmZvcnRpcy10aGVtZXMuY29tL2RlbW8vZGVmYXVsdC8%2c/form_key/XlJE1UQDDgAjqV67/index.html"
                                                                  title="Add to Compare">
                                                                  <span class="2 icon ib ic ic-compare"></span>
                                                                  </a>
                                                               </li>
                                                            </ul>
                                                         </div>
                                                         <!-- end: product-image-wrapper -->
                                                         <strong class="product-name"><a href="bag3.html" title="White Handbag">White Handbag</a></strong>
                                                         <div class="price-box">
                                                            <span class="regular-price" id="product-price-513384e854b691b155139ffed34be9b2cf">
                                                            <span class="price">$99.00</span>                                    </span>
                                                         </div>
                                                         <div class="actions">
                                                         </div>
                                                      </div>
                                                      <div class="item">
                                                         <div class="product-image-wrapper" style="max-width:196px;">
                                                            <a href="bag4.html" title="Marina Shopping Bag" class="product-image">
                                                            <img class="lazyOwl" data-src="http://ultimo.infortis-themes.com/demo/media/catalog/product/cache/1/small_image/196x/040ec09b1e35df139433887a97daa66f/8/7/870044-0032_1_2.jpg" alt="Marina Shopping Bag" />
                                                            </a>
                                                            <ul class="add-to-links clearer addto-links-icons addto-onimage visible-onhover">
                                                               <li><a class="link-wishlist"
                                                                  href="wishlist/index/add/product/52/form_key/XlJE1UQDDgAjqV67/index.html"
                                                                  title="Add to Wishlist">
                                                                  <span class="2 icon ib ic ic-heart"></span>
                                                                  </a>
                                                               </li>
                                                               <li><a class="link-compare"
                                                                  href="catalog/product_compare/add/product/52/uenc/aHR0cDovL3VsdGltby5pbmZvcnRpcy10aGVtZXMuY29tL2RlbW8vZGVmYXVsdC8%2c/form_key/XlJE1UQDDgAjqV67/index.html"
                                                                  title="Add to Compare">
                                                                  <span class="2 icon ib ic ic-compare"></span>
                                                                  </a>
                                                               </li>
                                                            </ul>
                                                         </div>
                                                         <!-- end: product-image-wrapper -->
                                                         <strong class="product-name"><a href="bag4.html" title="Marina Shopping Bag">Marina Shopping Bag</a></strong>
                                                         <div class="price-box">
                                                            <span class="regular-price" id="product-price-523384e854b691b155139ffed34be9b2cf">
                                                            <span class="price">$99.00</span>                                    </span>
                                                         </div>
                                                         <div class="actions">
                                                         </div>
                                                      </div>
                                                      <div class="item">
                                                         <div class="product-image-wrapper" style="max-width:196px;">
                                                            <a href="bag5.html" title="Skyblue Mini Bag" class="product-image">
                                                            <img class="lazyOwl" data-src="http://ultimo.infortis-themes.com/demo/media/catalog/product/cache/1/small_image/196x/040ec09b1e35df139433887a97daa66f/9/9/996093-0100_1.jpg" alt="Skyblue Mini Bag" />
                                                            </a>
                                                            <ul class="add-to-links clearer addto-links-icons addto-onimage visible-onhover">
                                                               <li><a class="link-wishlist"
                                                                  href="wishlist/index/add/product/53/form_key/XlJE1UQDDgAjqV67/index.html"
                                                                  title="Add to Wishlist">
                                                                  <span class="2 icon ib ic ic-heart"></span>
                                                                  </a>
                                                               </li>
                                                               <li><a class="link-compare"
                                                                  href="catalog/product_compare/add/product/53/uenc/aHR0cDovL3VsdGltby5pbmZvcnRpcy10aGVtZXMuY29tL2RlbW8vZGVmYXVsdC8%2c/form_key/XlJE1UQDDgAjqV67/index.html"
                                                                  title="Add to Compare">
                                                                  <span class="2 icon ib ic ic-compare"></span>
                                                                  </a>
                                                               </li>
                                                            </ul>
                                                         </div>
                                                         <!-- end: product-image-wrapper -->
                                                         <strong class="product-name"><a href="bag5.html" title="Skyblue Mini Bag">Skyblue Mini Bag</a></strong>
                                                         <div class="price-box">
                                                            <span class="regular-price" id="product-price-533384e854b691b155139ffed34be9b2cf">
                                                            <span class="price">$105.00</span>                                    </span>
                                                         </div>
                                                         <div class="actions">
                                                         </div>
                                                      </div>
                                                      <div class="item">
                                                         <div class="product-image-wrapper" style="max-width:196px;">
                                                            <a href="bag6.html" title="Union Jack Purse" class="product-image">
                                                            <img class="lazyOwl" data-src="http://ultimo.infortis-themes.com/demo/media/catalog/product/cache/1/small_image/196x/040ec09b1e35df139433887a97daa66f/2/1/216840-0129_1.jpg" alt="Union Jack Purse" />
                                                            </a>
                                                            <ul class="add-to-links clearer addto-links-icons addto-onimage visible-onhover">
                                                               <li><a class="link-wishlist"
                                                                  href="wishlist/index/add/product/54/form_key/XlJE1UQDDgAjqV67/index.html"
                                                                  title="Add to Wishlist">
                                                                  <span class="2 icon ib ic ic-heart"></span>
                                                                  </a>
                                                               </li>
                                                               <li><a class="link-compare"
                                                                  href="catalog/product_compare/add/product/54/uenc/aHR0cDovL3VsdGltby5pbmZvcnRpcy10aGVtZXMuY29tL2RlbW8vZGVmYXVsdC8%2c/form_key/XlJE1UQDDgAjqV67/index.html"
                                                                  title="Add to Compare">
                                                                  <span class="2 icon ib ic ic-compare"></span>
                                                                  </a>
                                                               </li>
                                                            </ul>
                                                         </div>
                                                         <!-- end: product-image-wrapper -->
                                                         <strong class="product-name"><a href="bag6.html" title="Union Jack Purse">Union Jack Purse</a></strong>
                                                         <div class="price-box">
                                                            <span class="regular-price" id="product-price-543384e854b691b155139ffed34be9b2cf">
                                                            <span class="price">$105.00</span>                                    </span>
                                                         </div>
                                                         <div class="actions">
                                                         </div>
                                                      </div>
                                                      <div class="item">
                                                         <div class="product-image-wrapper" style="max-width:196px;">
                                                            <a href="bag7.html" title="Red Travel Handbag" class="product-image">
                                                            <img class="lazyOwl" data-src="http://ultimo.infortis-themes.com/demo/media/catalog/product/cache/1/small_image/196x/040ec09b1e35df139433887a97daa66f/2/7/271669-0054_1.jpg" alt="Red Travel Handbag" />
                                                            </a>
                                                            <ul class="add-to-links clearer addto-links-icons addto-onimage visible-onhover">
                                                               <li><a class="link-wishlist"
                                                                  href="wishlist/index/add/product/55/form_key/XlJE1UQDDgAjqV67/index.html"
                                                                  title="Add to Wishlist">
                                                                  <span class="2 icon ib ic ic-heart"></span>
                                                                  </a>
                                                               </li>
                                                               <li><a class="link-compare"
                                                                  href="catalog/product_compare/add/product/55/uenc/aHR0cDovL3VsdGltby5pbmZvcnRpcy10aGVtZXMuY29tL2RlbW8vZGVmYXVsdC8%2c/form_key/XlJE1UQDDgAjqV67/index.html"
                                                                  title="Add to Compare">
                                                                  <span class="2 icon ib ic ic-compare"></span>
                                                                  </a>
                                                               </li>
                                                            </ul>
                                                         </div>
                                                         <!-- end: product-image-wrapper -->
                                                         <strong class="product-name"><a href="bag7.html" title="Red Travel Handbag">Red Travel Handbag</a></strong>
                                                         <div class="price-box">
                                                            <span class="regular-price" id="product-price-553384e854b691b155139ffed34be9b2cf">
                                                            <span class="price">$120.55</span>                                    </span>
                                                         </div>
                                                         <div class="actions">
                                                         </div>
                                                      </div>
                                                      <div class="item">
                                                         <div class="product-image-wrapper" style="max-width:196px;">
                                                            <a href="bag8.html" title="Orange Shopper" class="product-image">
                                                            <img class="lazyOwl" data-src="http://ultimo.infortis-themes.com/demo/media/catalog/product/cache/1/small_image/196x/040ec09b1e35df139433887a97daa66f/6/7/677188-0067_1.jpg" alt="Orange Shopper" />
                                                            </a>
                                                            <ul class="add-to-links clearer addto-links-icons addto-onimage visible-onhover">
                                                               <li><a class="link-wishlist"
                                                                  href="wishlist/index/add/product/56/form_key/XlJE1UQDDgAjqV67/index.html"
                                                                  title="Add to Wishlist">
                                                                  <span class="2 icon ib ic ic-heart"></span>
                                                                  </a>
                                                               </li>
                                                               <li><a class="link-compare"
                                                                  href="catalog/product_compare/add/product/56/uenc/aHR0cDovL3VsdGltby5pbmZvcnRpcy10aGVtZXMuY29tL2RlbW8vZGVmYXVsdC8%2c/form_key/XlJE1UQDDgAjqV67/index.html"
                                                                  title="Add to Compare">
                                                                  <span class="2 icon ib ic ic-compare"></span>
                                                                  </a>
                                                               </li>
                                                            </ul>
                                                         </div>
                                                         <!-- end: product-image-wrapper -->
                                                         <strong class="product-name"><a href="bag8.html" title="Orange Shopper">Orange Shopper</a></strong>
                                                         <div class="price-box">
                                                            <span class="regular-price" id="product-price-563384e854b691b155139ffed34be9b2cf">
                                                            <span class="price">$120.55</span>                                    </span>
                                                         </div>
                                                         <div class="actions">
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <!-- end: itemslider -->
                                                </div>
                                                <!-- end: itemslider-wrapper -->
                                                <script type="text/javascript">
                                                   //<![CDATA[
                                                       jQuery(function($) {

                                                           var owl = $('#itemslider-featured-3384e854b691b155139ffed34be9b2cf');
                                                           owl.owlCarousel({

                                                                       lazyLoad: true,

                                                                       itemsCustom: [ [0, 1], [320, 2], [480, 3], [768, 2], [960, 2], [1280, 3] ],
                                                               responsiveRefreshRate: 50,

                                                                       slideSpeed: 200,

                                                                       paginationSpeed: 500,

                                                                       scrollPerPage: true,

                                                                       autoPlay: 4000,

                                                                       stopOnHover: true,

                                                                       rewindNav: true,
                                                               rewindSpeed: 600,

                                                                       pagination: false,

                                                               navigation: true,
                                                               navigationText: false

                                                           }); //end: owl

                                                       });
                                                   //]]>
                                                </script>
                                             </div>
                                             <div class="clearer"></div>
                                             <br/>
                                             <br/>
                                             <br/>
                                             <div class="grid-container page-banners">
                                                <div class="grid12-4 no-left-gutter banner">
                                                   <img src="../media/wysiwyg/infortis/ultimo/custom/banners/2a.html" alt="Sample banner" />
                                                   <br/><br/>
                                                </div>
                                                <div class="grid12-4">
                                                   <div class="feature feature-icon-hover indent first">
                                                      <span class="ic ic-char ib" style="background-color: #ddd;">1</span>
                                                      <h4>Responsive Layout</h4>
                                                      <p>Ultimo can be displayed on any screen. <span class="hide-below-1280">It is based on fluid grid system lorem ipsum dolor sit.</span> <span class="hide-below-960">If screen is resized, layout will be automat...</span></p>
                                                   </div>
                                                   <div class="feature feature-icon-hover indent">
                                                      <span class="ic ic-char ib" style="background-color: #ddd;">2</span>
                                                      <h4>Responsive Layout</h4>
                                                      <p>Ultimo can be displayed on any screen. <span class="hide-below-1280">It is based on fluid grid system lorem ipsum dolor sit.</span> <span class="hide-below-960">If screen is resized, layout will be automat...</span></p>
                                                   </div>
                                                   <div class="feature feature-icon-hover indent last">
                                                      <span class="ic ic-char ib" style="background-color: #ddd;">3</span>
                                                      <h4>Responsive Layout</h4>
                                                      <p>Ultimo can be displayed on any screen. <span class="hide-below-1280">It is based on fluid grid system lorem ipsum dolor sit.</span> <span class="hide-below-960">If screen is resized, layout will be automat...</span></p>
                                                   </div>
                                                </div>
                                                <div class="grid12-4 no-right-gutter hide-below-768">
                                                   <div class="page-banners grid-container-spaced">
                                                      <div class="grid12-12 no-gutter banner fade-on-hover">
                                                         <a href="#">
                                                         <img src="../media/wysiwyg/infortis/ultimo/custom/banners/3a.jpg" alt="Sample banner" />
                                                         </a>
                                                      </div>
                                                      <div class="grid12-6 no-left-gutter banner fade-on-hover">
                                                         <a href="#">
                                                         <img src="../media/wysiwyg/infortis/ultimo/custom/banners/3b.jpg" alt="Sample banner" />
                                                         </a>
                                                      </div>
                                                      <div class="grid12-6 no-right-gutter banner fade-on-hover">
                                                         <a href="#">
                                                         <img src="../media/wysiwyg/infortis/ultimo/custom/banners/3c.jpg" alt="Sample banner" />
                                                         </a>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <br/>
                                             <br/>
                                             <strong class="section-title padding-right">New in Store</strong>
                                             <div class="itemslider-wrapper itemslider-new-wrapper slider-arrows1 slider-arrows1-pos-top-right slider-pagination1">
                                                <div id="itemslider-new" class="itemslider itemslider-responsive products-grid centered">
                                                   <div class="item">
                                                      <div class="product-image-wrapper" style="max-width:196px;">
                                                         <a href="phone9.html" title="Grouped Product" class="product-image">
                                                         <img class="lazyOwl" data-src="http://ultimo.infortis-themes.com/demo/media/catalog/product/cache/1/small_image/196x/040ec09b1e35df139433887a97daa66f/p/h/phone10.jpg" alt="Grouped Product" />
                                                         <span class="sticker-wrapper top-left"><span class="sticker new">New</span></span>                </a>
                                                         <ul class="add-to-links clearer addto-links-icons addto-onimage visible-onhover">
                                                            <li><a class="link-wishlist"
                                                               href="wishlist/index/add/product/34/form_key/1hIZq4SOV1EG5k8T/index.html"
                                                               title="Add to Wishlist">
                                                               <span class="2 icon ib ic ic-heart"></span>
                                                               </a>
                                                            </li>
                                                            <li><a class="link-compare"
                                                               href="catalog/product_compare/add/product/34/uenc/aHR0cDovL3VsdGltby5pbmZvcnRpcy10aGVtZXMuY29tL2RlbW8vZGVmYXVsdC8%2c/form_key/1hIZq4SOV1EG5k8T/index.html"
                                                               title="Add to Compare">
                                                               <span class="2 icon ib ic ic-compare"></span>
                                                               </a>
                                                            </li>
                                                         </ul>
                                                      </div>
                                                      <!-- end: product-image-wrapper -->
                                                      <strong class="product-name"><a href="phone9.html" title="Grouped Product">Grouped Product</a></strong>
                                                      <div class="ratings">
                                                         <div class="rating-box">
                                                            <div class="rating" style="width:100%"></div>
                                                         </div>
                                                         <span class="amount">1 Review(s)</span>
                                                      </div>
                                                      <div class="price-box">
                                                         <p class="minimal-price">
                                                            <span class="price-label">Starting at:</span>
                                                            <span class="price" id="product-minimal-price-34-new">
                                                            $45.00                </span>
                                                         </p>
                                                      </div>
                                                      <div class="actions">
                                                      </div>
                                                   </div>
                                                   <div class="item">
                                                      <div class="product-image-wrapper" style="max-width:196px;">
                                                         <a href="phone6.html" title="Big Smartphone" class="product-image">
                                                         <img class="lazyOwl" data-src="http://ultimo.infortis-themes.com/demo/media/catalog/product/cache/1/small_image/196x/040ec09b1e35df139433887a97daa66f/p/h/phone05_2.jpg" alt="Big Smartphone" />
                                                         <span class="sticker-wrapper top-left"><span class="sticker new">New</span></span>                </a>
                                                         <ul class="add-to-links clearer addto-links-icons addto-onimage visible-onhover">
                                                            <li><a class="link-wishlist"
                                                               href="wishlist/index/add/product/31/form_key/1hIZq4SOV1EG5k8T/index.html"
                                                               title="Add to Wishlist">
                                                               <span class="2 icon ib ic ic-heart"></span>
                                                               </a>
                                                            </li>
                                                            <li><a class="link-compare"
                                                               href="catalog/product_compare/add/product/31/uenc/aHR0cDovL3VsdGltby5pbmZvcnRpcy10aGVtZXMuY29tL2RlbW8vZGVmYXVsdC8%2c/form_key/1hIZq4SOV1EG5k8T/index.html"
                                                               title="Add to Compare">
                                                               <span class="2 icon ib ic ic-compare"></span>
                                                               </a>
                                                            </li>
                                                         </ul>
                                                      </div>
                                                      <!-- end: product-image-wrapper -->
                                                      <strong class="product-name"><a href="phone6.html" title="Big Smartphone">Big Smartphone</a></strong>
                                                      <div class="ratings">
                                                         <div class="rating-box">
                                                            <div class="rating" style="width:100%"></div>
                                                         </div>
                                                         <span class="amount">1 Review(s)</span>
                                                      </div>
                                                      <div class="price-box">
                                                         <span class="regular-price" id="product-price-31-new">
                                                         <span class="price">$124.00</span>                                    </span>
                                                      </div>
                                                      <div class="actions">
                                                      </div>
                                                   </div>
                                                   <div class="item">
                                                      <div class="product-image-wrapper" style="max-width:196px;">
                                                         <a href="phone2.html" title="Phone with Variants" class="product-image">
                                                         <img class="lazyOwl" data-src="http://ultimo.infortis-themes.com/demo/media/catalog/product/cache/1/small_image/196x/040ec09b1e35df139433887a97daa66f/p/h/phone03.jpg" alt="Phone with Variants" />
                                                         <img class="alt-img" src="../media/catalog/product/cache/1/small_image/196x/040ec09b1e35df139433887a97daa66f/p/h/phone07a_2.jpg" alt="Phone with Variants" />
                                                         <span class="sticker-wrapper top-left"><span class="sticker new">New</span></span>                </a>
                                                         <ul class="add-to-links clearer addto-links-icons addto-onimage visible-onhover">
                                                            <li><a class="link-wishlist"
                                                               href="wishlist/index/add/product/22/form_key/1hIZq4SOV1EG5k8T/index.html"
                                                               title="Add to Wishlist">
                                                               <span class="2 icon ib ic ic-heart"></span>
                                                               </a>
                                                            </li>
                                                            <li><a class="link-compare"
                                                               href="catalog/product_compare/add/product/22/uenc/aHR0cDovL3VsdGltby5pbmZvcnRpcy10aGVtZXMuY29tL2RlbW8vZGVmYXVsdC8%2c/form_key/1hIZq4SOV1EG5k8T/index.html"
                                                               title="Add to Compare">
                                                               <span class="2 icon ib ic ic-compare"></span>
                                                               </a>
                                                            </li>
                                                         </ul>
                                                      </div>
                                                      <!-- end: product-image-wrapper -->
                                                      <strong class="product-name"><a href="phone2.html" title="Phone with Variants">Phone with Variants</a></strong>
                                                      <div class="ratings">
                                                         <div class="rating-box">
                                                            <div class="rating" style="width:80%"></div>
                                                         </div>
                                                         <span class="amount">1 Review(s)</span>
                                                      </div>
                                                      <div class="price-box">
                                                         <span class="regular-price" id="product-price-22-new">
                                                         <span class="price">$750.00</span>                                    </span>
                                                      </div>
                                                      <div class="actions">
                                                      </div>
                                                   </div>
                                                   <div class="item">
                                                      <div class="product-image-wrapper" style="max-width:196px;">
                                                         <a href="phone1.html" title="The New Phone" class="product-image">
                                                         <img class="lazyOwl" data-src="http://ultimo.infortis-themes.com/demo/media/catalog/product/cache/1/small_image/196x/040ec09b1e35df139433887a97daa66f/p/h/phone02.jpg" alt="The New Phone" />
                                                         <img class="alt-img" src="../media/catalog/product/cache/1/small_image/196x/040ec09b1e35df139433887a97daa66f/p/h/phone09_1.jpg" alt="The New Phone" />
                                                         <span class="sticker-wrapper top-left"><span class="sticker new">New</span></span><span class="sticker-wrapper top-right"><span class="sticker sale">Sale</span></span>                </a>
                                                         <ul class="add-to-links clearer addto-links-icons addto-onimage visible-onhover">
                                                            <li><a class="link-wishlist"
                                                               href="wishlist/index/add/product/21/form_key/1hIZq4SOV1EG5k8T/index.html"
                                                               title="Add to Wishlist">
                                                               <span class="2 icon ib ic ic-heart"></span>
                                                               </a>
                                                            </li>
                                                            <li><a class="link-compare"
                                                               href="catalog/product_compare/add/product/21/uenc/aHR0cDovL3VsdGltby5pbmZvcnRpcy10aGVtZXMuY29tL2RlbW8vZGVmYXVsdC8%2c/form_key/1hIZq4SOV1EG5k8T/index.html"
                                                               title="Add to Compare">
                                                               <span class="2 icon ib ic ic-compare"></span>
                                                               </a>
                                                            </li>
                                                         </ul>
                                                      </div>
                                                      <!-- end: product-image-wrapper -->
                                                      <strong class="product-name"><a href="phone1.html" title="The New Phone">The New Phone</a></strong>
                                                      <div class="ratings">
                                                         <div class="rating-box">
                                                            <div class="rating" style="width:100%"></div>
                                                         </div>
                                                         <span class="amount">1 Review(s)</span>
                                                      </div>
                                                      <div class="price-box">
                                                         <p class="old-price">
                                                            <span class="price-label">Regular Price:</span>
                                                            <span class="price" id="old-price-21-new">
                                                            $800.00                </span>
                                                         </p>
                                                         <p class="special-price">
                                                            <span class="price-label">Special Price</span>
                                                            <span class="price" id="product-price-21-new">
                                                            $750.00                </span>
                                                         </p>
                                                      </div>
                                                      <div class="actions">
                                                      </div>
                                                   </div>
                                                   <div class="item">
                                                      <div class="product-image-wrapper" style="max-width:196px;">
                                                         <a href="phone8.html" title="Tier Pricing Product" class="product-image">
                                                         <img class="lazyOwl" data-src="http://ultimo.infortis-themes.com/demo/media/catalog/product/cache/1/small_image/196x/040ec09b1e35df139433887a97daa66f/p/h/phone08_2.jpg" alt="Tier Pricing Product" />
                                                         <span class="sticker-wrapper top-left"><span class="sticker new">New</span></span>                </a>
                                                         <ul class="add-to-links clearer addto-links-icons addto-onimage visible-onhover">
                                                            <li><a class="link-wishlist"
                                                               href="wishlist/index/add/product/33/form_key/1hIZq4SOV1EG5k8T/index.html"
                                                               title="Add to Wishlist">
                                                               <span class="2 icon ib ic ic-heart"></span>
                                                               </a>
                                                            </li>
                                                            <li><a class="link-compare"
                                                               href="catalog/product_compare/add/product/33/uenc/aHR0cDovL3VsdGltby5pbmZvcnRpcy10aGVtZXMuY29tL2RlbW8vZGVmYXVsdC8%2c/form_key/1hIZq4SOV1EG5k8T/index.html"
                                                               title="Add to Compare">
                                                               <span class="2 icon ib ic ic-compare"></span>
                                                               </a>
                                                            </li>
                                                         </ul>
                                                      </div>
                                                      <!-- end: product-image-wrapper -->
                                                      <strong class="product-name"><a href="phone8.html" title="Tier Pricing Product">Tier Pricing Product</a></strong>
                                                      <div class="ratings">
                                                         <div class="rating-box">
                                                            <div class="rating" style="width:100%"></div>
                                                         </div>
                                                         <span class="amount">1 Review(s)</span>
                                                      </div>
                                                      <div class="price-box">
                                                         <span class="regular-price" id="product-price-33-new">
                                                         <span class="price">$500.00</span>                                    </span>
                                                         <a href="phone8.html" class="minimal-price-link">
                                                         <span class="label">As low as:</span>
                                                         <span class="price" id="product-minimal-price-33-new">
                                                         $400.00            </span>
                                                         </a>
                                                      </div>
                                                      <div class="actions">
                                                      </div>
                                                   </div>
                                                   <div class="item">
                                                      <div class="product-image-wrapper" style="max-width:196px;">
                                                         <a href="phone7.html" title="Modern Cell Phone" class="product-image">
                                                         <img class="lazyOwl" data-src="http://ultimo.infortis-themes.com/demo/media/catalog/product/cache/1/small_image/196x/040ec09b1e35df139433887a97daa66f/p/h/phone07.jpg" alt="Modern Cell Phone" />
                                                         <span class="sticker-wrapper top-left"><span class="sticker new">New</span></span>                </a>
                                                         <ul class="add-to-links clearer addto-links-icons addto-onimage visible-onhover">
                                                            <li><a class="link-wishlist"
                                                               href="wishlist/index/add/product/32/form_key/1hIZq4SOV1EG5k8T/index.html"
                                                               title="Add to Wishlist">
                                                               <span class="2 icon ib ic ic-heart"></span>
                                                               </a>
                                                            </li>
                                                            <li><a class="link-compare"
                                                               href="catalog/product_compare/add/product/32/uenc/aHR0cDovL3VsdGltby5pbmZvcnRpcy10aGVtZXMuY29tL2RlbW8vZGVmYXVsdC8%2c/form_key/1hIZq4SOV1EG5k8T/index.html"
                                                               title="Add to Compare">
                                                               <span class="2 icon ib ic ic-compare"></span>
                                                               </a>
                                                            </li>
                                                         </ul>
                                                      </div>
                                                      <!-- end: product-image-wrapper -->
                                                      <strong class="product-name"><a href="phone7.html" title="Modern Cell Phone">Modern Cell Phone</a></strong>
                                                      <div class="ratings">
                                                         <div class="rating-box">
                                                            <div class="rating" style="width:100%"></div>
                                                         </div>
                                                         <span class="amount">1 Review(s)</span>
                                                      </div>
                                                      <div class="price-box">
                                                         <span class="regular-price" id="product-price-32-new">
                                                         <span class="price">$999.00</span>                                    </span>
                                                      </div>
                                                      <div class="actions">
                                                      </div>
                                                   </div>
                                                   <div class="item">
                                                      <div class="product-image-wrapper" style="max-width:196px;">
                                                         <a href="phone5.html" title="Deep Blue Phone" class="product-image">
                                                         <img class="lazyOwl" data-src="http://ultimo.infortis-themes.com/demo/media/catalog/product/cache/1/small_image/196x/040ec09b1e35df139433887a97daa66f/p/h/phone04_1.jpg" alt="Deep Blue Phone" />
                                                         <img class="alt-img" src="../media/catalog/product/cache/1/small_image/196x/040ec09b1e35df139433887a97daa66f/p/h/phone01a_2.jpg" alt="Deep Blue Phone" />
                                                         <span class="sticker-wrapper top-left"><span class="sticker new">New</span></span>                </a>
                                                         <ul class="add-to-links clearer addto-links-icons addto-onimage visible-onhover">
                                                            <li><a class="link-wishlist"
                                                               href="wishlist/index/add/product/30/form_key/1hIZq4SOV1EG5k8T/index.html"
                                                               title="Add to Wishlist">
                                                               <span class="2 icon ib ic ic-heart"></span>
                                                               </a>
                                                            </li>
                                                            <li><a class="link-compare"
                                                               href="catalog/product_compare/add/product/30/uenc/aHR0cDovL3VsdGltby5pbmZvcnRpcy10aGVtZXMuY29tL2RlbW8vZGVmYXVsdC8%2c/form_key/1hIZq4SOV1EG5k8T/index.html"
                                                               title="Add to Compare">
                                                               <span class="2 icon ib ic ic-compare"></span>
                                                               </a>
                                                            </li>
                                                         </ul>
                                                      </div>
                                                      <!-- end: product-image-wrapper -->
                                                      <strong class="product-name"><a href="phone5.html" title="Deep Blue Phone">Deep Blue Phone</a></strong>
                                                      <div class="ratings">
                                                         <div class="rating-box">
                                                            <div class="rating" style="width:100%"></div>
                                                         </div>
                                                         <span class="amount">1 Review(s)</span>
                                                      </div>
                                                      <div class="price-box">
                                                         <span class="regular-price" id="product-price-30-new">
                                                         <span class="price">$400.00</span>                                    </span>
                                                      </div>
                                                      <div class="actions">
                                                      </div>
                                                   </div>
                                                   <div class="item">
                                                      <div class="product-image-wrapper" style="max-width:196px;">
                                                         <a href="phone3.html" title="Configurable Phone" class="product-image">
                                                         <img class="lazyOwl" data-src="http://ultimo.infortis-themes.com/demo/media/catalog/product/cache/1/small_image/196x/040ec09b1e35df139433887a97daa66f/p/h/phone01_3.jpg" alt="Configurable Phone" />
                                                         <img class="alt-img" src="../media/catalog/product/cache/1/small_image/196x/040ec09b1e35df139433887a97daa66f/p/h/phone08_5.jpg" alt="Configurable Phone" />
                                                         <span class="sticker-wrapper top-left"><span class="sticker new">New</span></span>                </a>
                                                         <ul class="add-to-links clearer addto-links-icons addto-onimage visible-onhover">
                                                            <li><a class="link-wishlist"
                                                               href="wishlist/index/add/product/24/form_key/1hIZq4SOV1EG5k8T/index.html"
                                                               title="Add to Wishlist">
                                                               <span class="2 icon ib ic ic-heart"></span>
                                                               </a>
                                                            </li>
                                                            <li><a class="link-compare"
                                                               href="catalog/product_compare/add/product/24/uenc/aHR0cDovL3VsdGltby5pbmZvcnRpcy10aGVtZXMuY29tL2RlbW8vZGVmYXVsdC8%2c/form_key/1hIZq4SOV1EG5k8T/index.html"
                                                               title="Add to Compare">
                                                               <span class="2 icon ib ic ic-compare"></span>
                                                               </a>
                                                            </li>
                                                         </ul>
                                                      </div>
                                                      <!-- end: product-image-wrapper -->
                                                      <strong class="product-name"><a href="phone3.html" title="Configurable Phone">Configurable Phone</a></strong>
                                                      <div class="ratings">
                                                         <div class="rating-box">
                                                            <div class="rating" style="width:100%"></div>
                                                         </div>
                                                         <span class="amount">1 Review(s)</span>
                                                      </div>
                                                      <div class="price-box">
                                                         <span class="regular-price" id="product-price-24-new">
                                                         <span class="price">$700.00</span>                                    </span>
                                                      </div>
                                                      <div class="actions">
                                                      </div>
                                                   </div>
                                                   <div class="item">
                                                      <div class="product-image-wrapper" style="max-width:196px;">
                                                         <a href="phone4.html" title="Simple Phone" class="product-image">
                                                         <img class="lazyOwl" data-src="http://ultimo.infortis-themes.com/demo/media/catalog/product/cache/1/small_image/196x/040ec09b1e35df139433887a97daa66f/p/h/phone06_2.jpg" alt="Simple Phone" />
                                                         <span class="sticker-wrapper top-left"><span class="sticker new">New</span></span>                </a>
                                                         <ul class="add-to-links clearer addto-links-icons addto-onimage visible-onhover">
                                                            <li><a class="link-wishlist"
                                                               href="wishlist/index/add/product/29/form_key/1hIZq4SOV1EG5k8T/index.html"
                                                               title="Add to Wishlist">
                                                               <span class="2 icon ib ic ic-heart"></span>
                                                               </a>
                                                            </li>
                                                            <li><a class="link-compare"
                                                               href="catalog/product_compare/add/product/29/uenc/aHR0cDovL3VsdGltby5pbmZvcnRpcy10aGVtZXMuY29tL2RlbW8vZGVmYXVsdC8%2c/form_key/1hIZq4SOV1EG5k8T/index.html"
                                                               title="Add to Compare">
                                                               <span class="2 icon ib ic ic-compare"></span>
                                                               </a>
                                                            </li>
                                                         </ul>
                                                      </div>
                                                      <!-- end: product-image-wrapper -->
                                                      <strong class="product-name"><a href="phone4.html" title="Simple Phone">Simple Phone</a></strong>
                                                      <div class="ratings">
                                                         <div class="rating-box">
                                                            <div class="rating" style="width:100%"></div>
                                                         </div>
                                                         <span class="amount">1 Review(s)</span>
                                                      </div>
                                                      <div class="price-box">
                                                         <span class="regular-price" id="product-price-29-new">
                                                         <span class="price">$699.00</span>                                    </span>
                                                      </div>
                                                      <div class="actions">
                                                      </div>
                                                   </div>
                                                </div>
                                                <!-- end: itemslider -->
                                             </div>
                                             <!-- end: itemslider-wrapper -->
                                             <script type="text/javascript">
                                                //<![CDATA[
                                                    jQuery(function($) {

                                                        var owl = $('#itemslider-new');
                                                        owl.owlCarousel({

                                                                    lazyLoad: true,

                                                                    itemsCustom: [ [0, 1], [320, 2], [480, 3], [768, 4], [960, 5], [1280, 6] ],
                                                            responsiveRefreshRate: 50,

                                                                    slideSpeed: 200,

                                                                    paginationSpeed: 500,



                                                                    stopOnHover: true,

                                                                    rewindNav: true,
                                                            rewindSpeed: 600,

                                                                    pagination: true,

                                                            navigation: true,
                                                            navigationText: false

                                                        }); //end: owl

                                                    });
                                                //]]>
                                             </script>
                                             <br/>
                                             <br/>
                                             <span class="section-title">Sample Custom Banners</span>
                                             <br />
                                             <div class="nested-container">
                                                <!-- First banner has a link which adds a product to the cart (link was copied from product's button "Add to cart") -->
                                                <div class="page-banners grid-container">
                                                   <div class="grid12-3 banner fade-on-hover hide-below-768">
                                                      <a href="checkout/cart/add/uenc/aHR0cDovL2xvY2FsaG9zdC5jb20vbTE3MDItMDF1bHRpbW8vZGVmYXVsdC8%2c/product/30/index.html" title="Click to add sample product to your cart">
                                                      <img class="fade-on-hover" src="../media/wysiwyg/infortis/ultimo/custom/banners/1a.png" alt="Add product to cart" />
                                                      </a>
                                                   </div>
                                                   <div class="grid12-3 banner fade-on-hover hide-below-768">
                                                      <a href="checkout/cart/index.html" title="Click to go to cart page">
                                                      <img class="fade-on-hover" src="../media/wysiwyg/infortis/ultimo/custom/banners/1b.png" alt="Go to Magento cart page" />
                                                      </a>
                                                   </div>
                                                   <div class="grid12-6 banner">
                                                      <img class="fade-on-hover" src="../media/wysiwyg/infortis/ultimo/custom/banners/1c.png" alt="Magento discount code" title="Use discount code/coupon on cart page" />
                                                   </div>
                                                </div>
                                             </div>
                                             <br/>
                                             <br/>
                                             <br/>
                                             <h3 class="section-title padding-right">Our Brands</h3>
                                             <div class="itemslider-wrapper brand-slider-wrapper slider-arrows1 slider-arrows1-pos-top-right slider-pagination1 slider-pagination1-centered">
                                                <div id="itemslider-brands-07b648afe672ff342f9fc3309b28b216" class="itemslider itemslider-responsive brand-slider">
                                                   <div class="item">
                                                      <a class="fade-on-hover" href="catalogsearch/result/index8f4b.html?q=Brandisimi" title="Click to see more products from Brandisimi"><img class="lazyOwl" data-src="http://ultimo.infortis-themes.com/demo/media/wysiwyg/infortis/brands/brandisimi.png" alt="Brandisimi" /></a>
                                                   </div>
                                                   <div class="item">
                                                      <a class="fade-on-hover" href="catalogsearch/result/index2059.html?q=SampleManufacturer" title="Click to see more products from SampleManufacturer"><img class="lazyOwl" data-src="http://ultimo.infortis-themes.com/demo/media/wysiwyg/infortis/brands/samplemanufacturer.png" alt="SampleManufacturer" /></a>
                                                   </div>
                                                   <div class="item">
                                                      <a class="fade-on-hover" href="catalogsearch/result/index5974.html?q=TheBrand" title="Click to see more products from TheBrand"><img class="lazyOwl" data-src="http://ultimo.infortis-themes.com/demo/media/wysiwyg/infortis/brands/thebrand.png" alt="TheBrand" /></a>
                                                   </div>
                                                   <div class="item">
                                                      <a class="fade-on-hover" href="catalogsearch/result/index6109.html?q=BlueLogo" title="Click to see more products from BlueLogo"><img class="lazyOwl" data-src="http://ultimo.infortis-themes.com/demo/media/wysiwyg/infortis/brands/bluelogo.png" alt="BlueLogo" /></a>
                                                   </div>
                                                   <div class="item">
                                                      <a class="fade-on-hover" href="catalogsearch/result/index9893.html?q=Publisher" title="Click to see more products from Publisher"><img class="lazyOwl" data-src="http://ultimo.infortis-themes.com/demo/media/wysiwyg/infortis/brands/publisher.png" alt="Publisher" /></a>
                                                   </div>
                                                   <div class="item">
                                                      <a class="fade-on-hover" href="catalogsearch/result/index3428.html?q=Company" title="Click to see more products from Company"><img class="lazyOwl" data-src="http://ultimo.infortis-themes.com/demo/media/wysiwyg/infortis/brands/company.png" alt="Company" /></a>
                                                   </div>
                                                   <div class="item">
                                                      <a class="fade-on-hover" href="catalogsearch/result/index597c.html?q=Soundwave" title="Click to see more products from Soundwave"><img class="lazyOwl" data-src="http://ultimo.infortis-themes.com/demo/media/wysiwyg/infortis/brands/soundwave.png" alt="Soundwave" /></a>
                                                   </div>
                                                   <div class="item">
                                                      <a class="fade-on-hover" href="catalogsearch/result/indexa1c6.html?q=LogoFashion" title="Click to see more products from LogoFashion"><img class="lazyOwl" data-src="http://ultimo.infortis-themes.com/demo/media/wysiwyg/infortis/brands/logofashion.png" alt="LogoFashion" /></a>
                                                   </div>
                                                   <div class="item">
                                                      <a class="fade-on-hover" href="catalogsearch/result/indexb201.html?q=Samsung" title="Click to see more products from Samsung"><img class="lazyOwl" data-src="http://ultimo.infortis-themes.com/demo/media/wysiwyg/infortis/brands/samsung.png" alt="Samsung" /></a>
                                                   </div>
                                                   <div class="item">
                                                      <a class="fade-on-hover" href="catalogsearch/result/index2495.html?q=Apple" title="Click to see more products from Apple"><img class="lazyOwl" data-src="http://ultimo.infortis-themes.com/demo/media/wysiwyg/infortis/brands/apple.png" alt="Apple" /></a>
                                                   </div>
                                                </div>
                                                <!-- end: itemslider -->
                                             </div>
                                             <!-- end: itemslider-wrapper -->
                                             <script type="text/javascript">
                                                //<![CDATA[
                                                	jQuery(function($) {

                                                		var owl = $('#itemslider-brands-07b648afe672ff342f9fc3309b28b216');
                                                		owl.owlCarousel({

                                                					lazyLoad: true,

                                                					itemsCustom: [ [0, 1], [320, 2], [480, 2], [768, 3], [960, 4], [1280, 5] ],
                                                			responsiveRefreshRate: 50,

                                                					slideSpeed: 200,

                                                					paginationSpeed: 500,


                                                					autoPlay: 6000,

                                                					stopOnHover: true,

                                                					rewindNav: true,
                                                			rewindSpeed: 600,

                                                					pagination: true,
                                                			paginationSpeed: 600,

                                                			navigation: true,
                                                			navigationText: false

                                                		}); //end: owl

                                                	});
                                                //]]>
                                             </script>
                                             <br/>
                                             <br/>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="postscript"></div>
                                 </div>
                              </div>
                              <div class="main-bottom-container"></div>
                           </div>
                           <div class="footer-container">
                              <div class="footer-container2">
                                 <div class="footer-container3">
                                    <div class="footer-top-container section-container">
                                       <div class="footer-top footer container">
                                          <div class="inner-container">
                                             <div class="section clearer links-wrapper-separators">
                                                <div class="item item-left">
                                                   <ul class="links">
                                                      <li class="first" ><a href="catalog/seo_sitemap/category/index.html" title="Site Map" >Site Map</a></li>
                                                      <li ><a href="catalogsearch/term/popular/index.html" title="Search Terms" >Search Terms</a></li>
                                                      <li ><a href="catalogsearch/advanced/index.html" title="Advanced Search" >Advanced Search</a></li>
                                                      <li ><a href="sales/guest/form/index.html" title="Orders and Returns" >Orders and Returns</a></li>
                                                      <li class=" last" ><a href="contacts/index.html" title="Contact Us" >Contact Us</a></li>
                                                   </ul>
                                                </div>
                                                <div class="item item-right">
                                                   <ul class="links hide-below-768">
                                                      <li class="first">
                                                         <a href="about-us-page.html" title="You can add more custom links here">About Us</a>
                                                      </li>
                                                      <li class="last">
                                                         <a href="customer-service/index.html"  title="You can even replace these links with any other content">Customer Service</a>
                                                      </li>
                                                   </ul>
                                                </div>
                                             </div>
                                             <!-- end: footer-top section -->
                                          </div>
                                          <!-- end: inner-container -->
                                       </div>
                                       <!-- end: footer-top -->
                                    </div>
                                    <div class="footer-primary-container section-container">
                                       <div class="footer-primary footer container">
                                          <div class="inner-container">
                                             <div class="clearer">
                                                <div class="grid12-12">
                                                   <div class="std">
                                                      <div class="grid12-3">
                                                         <div class="mobile-collapsible">
                                                            <h6 class="block-title heading">About Ultimo</h6>
                                                            <div class="block-content">
                                                               <img src="../media/wysiwyg/infortis/ultimo/custom/logo-small.png" alt="Ultimo Theme" style="padding-bottom:10px;" />
                                                               <div class="feature first last">
                                                                  <p>Ultimo is a premium Magento theme with advanced admin module. It’s extremely customizable, easy to use and fully responsive.</p>
                                                                  <h5><a class="go" href="http://themeforest.net/item/ultimo-fluid-responsive-magento-theme/3231798?ref=infortis">Buy this theme</a></h5>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                      <div class="grid12-3">
                                                         <div class="mobile-collapsible">
                                                            <h6 class="block-title heading">Theme Features</h6>
                                                            <div class="block-content">
                                                               <ul class="bullet">
                                                                  <li><a href="ultimo-responsive-magento-theme/index.html">Theme Features</a></li>
                                                                  <li><a href="magento-typography/index.html">Typography</a></li>
                                                                  <li><a href="magento-banners/index.html">Image Banners</a></li>
                                                                  <li><a href="magento-icons/index.html">Font Icons</a></li>
                                                                  <li><a href="magento-grid-system/index.html">Grid System</a></li>
                                                                  <li><a href="magento-responsive-utilities/index.html">Responsive Utilities</a></li>
                                                                  <li><a href="magento-banner-slideshow/index.html">Banner Slideshow</a></li>
                                                                  <li><a href="https_/twitter.com/index.html">Follow Us On Twitter</a></li>
                                                                  <li><a href="http://infortis-themes.com/">Magento Themes</a></li>
                                                               </ul>
                                                            </div>
                                                         </div>
                                                      </div>
                                                      <div class="grid12-3">
                                                         <div class="mobile-collapsible">
                                                            <h6 class="block-title heading">Key Features</h6>
                                                            <div class="block-content">
                                                               <div class="feature feature-icon-hover indent first">
                                                                  <span class="ib ic ic-char">1</span>
                                                                  <p class="no-margin">Unlimited colors, hundreds of customizable elements</p>
                                                               </div>
                                                               <div class="feature feature-icon-hover indent">
                                                                  <span class="ib ic ic-char">2</span>
                                                                  <p class="no-margin ">Customizable responsive layout based on fluid grid</p>
                                                               </div>
                                                               <div class="feature feature-icon-hover indent">
                                                                  <span class="ib ic ic-char">3</span>
                                                                  <p class="no-margin ">50+ placeholders to display custom content</p>
                                                               </div>
                                                               <div class="feature feature-icon-hover indent">
                                                                  <span class="ib ic ic-char">4</span>
                                                                  <p class="no-margin ">Create your custom sub-themes (variants)</p>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                      <div class="grid12-3">
                                                         <div class="mobile-collapsible">
                                                            <h6 class="block-title heading">Company Info</h6>
                                                            <div class="block-content">
                                                               <div class="feature feature-icon-hover indent first">
                                                                  <span class="ib ic ic-phone ic-lg"></span>
                                                                  <p class="no-margin ">Call Us +001 555 801<br/>Fax +001 555 802</p>
                                                               </div>
                                                               <div class="feature feature-icon-hover indent">
                                                                  <span class="ib ic ic-mobile ic-lg"></span>
                                                                  <p class="no-margin ">+77 123 1234<br/>+42 98 9876</p>
                                                               </div>
                                                               <div class="feature feature-icon-hover indent">
                                                                  <span class="ib ic ic-letter ic-lg"></span>
                                                                  <p class="no-margin ">boss@example.com<br/>me@example.com</p>
                                                               </div>
                                                               <div class="feature feature-icon-hover indent last">
                                                                  <span class="ib ic ic-skype ic-lg"></span>
                                                                  <p class="no-margin ">Skype: samplelogin<br/>skype-support</p>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <!-- end: footer-primary section -->
                                             <div class="footer-primary-bottom grid12-12">
                                                <div class="footer-primary-bottom-spacing clearer">
                                                   <div class="item item-left clearer block_footer_primary_bottom_left">
                                                      <div class="social-links ib-wrapper--square">
                                                         <a class="first" href="http://twitter.com/infortis" title="Follow Infortis on Twitter">
                                                         <span class="ib ib-hover ic ic-lg ic-twitter"></span>
                                                         </a>
                                                         <a href="#" title="Join us on Facebook">
                                                         <span class="ib ib-hover ic ic-lg ic-facebook"></span>
                                                         </a>
                                                         <a href="#" title="Join us on Google Plus">
                                                         <span class="ib ib-hover ic ic-lg ic-googleplus"></span>
                                                         </a>
                                                         <a href="#" title="Youtube">
                                                         <span class="ib ib-hover ic ic-lg ic-youtube"></span>
                                                         </a>
                                                         <a href="#" title="Vimeo">
                                                         <span class="ib ib-hover ic ic-lg ic-vimeo"></span>
                                                         </a>
                                                         <a href="#" title="Instagram">
                                                         <span class="ib ib-hover ic ic-lg ic-instagram"></span>
                                                         </a>
                                                         <a href="#" title="Pinterest">
                                                         <span class="ib ib-hover ic ic-lg ic-pinterest"></span>
                                                         </a>
                                                         <a href="#" title="Linked in">
                                                         <span class="ib ib-hover ic ic-lg ic-linkedin"></span>
                                                         </a>
                                                         <a href="#" title="VKontakte">
                                                         <span class="ib ib-hover ic ic-lg ic-vk"></span>
                                                         </a>
                                                         <a href="#" title="Renren">
                                                         <span class="ib ib-hover ic ic-lg ic-renren"></span>
                                                         </a>
                                                      </div>
                                                   </div>
                                                   <div class="item item-right newsletter-wrapper clearer">
                                                      <div id="subscribe-form" class="clearer">
                                                         <form action="http://ultimo.infortis-themes.com/demo/default/newsletter/subscriber/new/" method="post" id="newsletter-validate-detail">
                                                            <div>
                                                               <label for="newsletter">Newsletter</label>
                                                               <div class="input-box">
                                                                  <input type="text" name="email" id="newsletter" title="Sign up for our newsletter" class="input-text required-entry validate-email" />
                                                               </div>
                                                               <button type="submit" title="Subscribe" class="button btn-inline"><span><span>Subscribe</span></span></button>
                                                            </div>
                                                         </form>
                                                      </div>
                                                      <script type="text/javascript">
                                                         //<![CDATA[
                                                             var newsletterSubscriberFormDetail = new VarienForm('newsletter-validate-detail');
                                                             new Varien.searchForm('newsletter-validate-detail', 'newsletter', 'Enter your email address');
                                                         //]]>
                                                      </script>
                                                   </div>
                                                </div>
                                             </div>
                                             <!-- end: footer-primary-bottom -->
                                          </div>
                                          <!-- end: inner-container -->
                                       </div>
                                       <!-- end: footer-primary -->
                                    </div>
                                    <div class="footer-secondary-container section-container">
                                       <div class="footer-secondary footer container">
                                          <div class="inner-container">
                                             <div class="clearer">
                                                <div class="grid12-12">
                                                   <div class="std">
                                                      <div class="grid12-9 no-gutter">
                                                         <div class="grid12-3">
                                                            <div class="mobile-collapsible">
                                                               <h6 class="block-title heading">Questions?</h6>
                                                               <div class="block-content">
                                                                  <ul class="bullet">
                                                                     <li><a href="#">Terms</a></li>
                                                                     <li><a href="#">Conditions</a></li>
                                                                     <li><a href="#">About us</a></li>
                                                                     <li><a href="#">Example</a></li>
                                                                  </ul>
                                                               </div>
                                                            </div>
                                                         </div>
                                                         <div class="grid12-3">
                                                            <div class="mobile-collapsible">
                                                               <h6 class="block-title heading">Shipping</h6>
                                                               <div class="block-content">
                                                                  <ul class="bullet">
                                                                     <li><a href="#">Delivery</a></li>
                                                                     <li><a href="#">Track your order</a></li>
                                                                     <li><a href="#">Buy gift cards</a></li>
                                                                     <li><a href="#">Returns</a></li>
                                                                  </ul>
                                                               </div>
                                                            </div>
                                                         </div>
                                                         <div class="grid12-3">
                                                            <div class="mobile-collapsible">
                                                               <h6 class="block-title heading">About Us</h6>
                                                               <div class="block-content">
                                                                  <ul class="bullet">
                                                                     <li><a href="#">Responsive</a></li>
                                                                     <li><a href="#">Magento themes</a></li>
                                                                     <li><a href="#">E-commerce</a></li>
                                                                     <li><a href="#">The company</a></li>
                                                                  </ul>
                                                               </div>
                                                            </div>
                                                         </div>
                                                         <div class="grid12-3">
                                                            <div class="mobile-collapsible">
                                                               <h6 class="block-title heading">News</h6>
                                                               <div class="block-content">
                                                                  <ul class="bullet">
                                                                     <li><a href="#">What's new</a></li>
                                                                     <li><a href="#">Products</a></li>
                                                                     <li><a href="#">Magento template</a></li>
                                                                     <li><a href="#">Our sites</a></li>
                                                                  </ul>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                      <div class="grid12-3 no-right-gutter">
                                                         <div class="mobile-collapsible">
                                                            <h6 class="block-title heading">Additional Info</h6>
                                                            <div class="block-content">
                                                               <div class="feature indent first feature-icon-hover">
                                                                  <span class="ic ic-lg ic-pin ib" style="background-color: lightskyblue;"></span>
                                                                  <p class="no-margin">Friends Inc., 90 Bedford St<br />New York, NY 041, USA</p>
                                                               </div>
                                                               <div class="feature indent feature-icon-hover">
                                                                  <span class="ic ic-lg ic-twitter ib" style="background-color: lightskyblue;"></span>
                                                                  <p class="no-margin">Follow us on our<br /><a href="http://twitter.com/infortis">Twitter</a> account</p>
                                                               </div>
                                                               <br />
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="footer-bottom-container section-container">
                                       <div class="footer-bottom footer container">
                                          <div class="inner-container">
                                             <div class="clearer">
                                                <div class="item item-left">
                                                   <div class="footer-copyright">Copyright &copy; 2012-2013 Premium Magento Themes by Infortis. All Rights Reserved.<br/>
                                                      This is a demo store. Any orders placed through this store will not be honored or fulfilled.
                                                   </div>
                                                </div>
                                                <div class="item item-right block_footer_payment"><img src="../media/wysiwyg/infortis/ultimo/custom/payment.gif" alt="Payment methods" title="Sample image with payment methods" /></div>
                                                <div class="item item-right">
                                                   <div class="store-switcher">
                                                      <label for="select-store">Select Store:</label>
                                                      <select id="select-store" title="Select Store" onchange="location.href=this.value">
                                                         <option value="" selected="selected">Store 1</option>
                                                         <option value="http://ultimo.infortis-themes.com/demo/second2/">Store 2</option>
                                                         <option value="http://ultimo.infortis-themes.com/demo/third/">Store 3</option>
                                                         <option value="http://ultimo.infortis-themes.com/demo/fourth/">Store 4</option>
                                                         <option value="http://ultimo.infortis-themes.com/demo/fifth/">Store 5</option>
                                                         <option value="http://ultimo.infortis-themes.com/demo/sixth/">Store 6</option>
                                                         <option value="http://ultimo.infortis-themes.com/demo/seventh/">Store 7</option>
                                                         <option value="http://ultimo.infortis-themes.com/demo/eighth/">Store 8</option>
                                                      </select>
                                                   </div>
                                                </div>
                                             </div>
                                             <!-- end: footer-bottom section -->
                                          </div>
                                          <!-- end: inner-container -->
                                       </div>
                                       <!-- end: footer-bottom -->
                                    </div>
                                    <a id="scroll-to-top" class="ic ic-up" href="#top"></a>
                                 </div>
                                 <!-- end: footer-container3 -->
                              </div>
                              <!-- end: footer-container2 -->
                           </div>
                           <!-- end: footer-container -->
                           <script type="text/javascript">
                              //<![CDATA[

                              		var gridItemsEqualHeightApplied = false;
                              function setGridItemsEqualHeight($)
                              {
                              	var $list = $('.category-products-grid');
                              	var $listItems = $list.children();

                              	var centered = $list.hasClass('centered');
                              	var gridItemMaxHeight = 0;
                              	$listItems.each(function() {

                              		$(this).css("height", "auto"); 			var $object = $(this).find('.actions');

                              					if (centered)
                              		{
                              			var objectWidth = $object.width();
                              			var availableWidth = $(this).width();
                              			var space = availableWidth - objectWidth;
                              			var leftOffset = space / 2;
                              			$object.css("padding-left", leftOffset + "px"); 			}

                              					var bottomOffset = parseInt($(this).css("padding-top"));
                              		if (centered) bottomOffset += 10;
                              		$object.css("bottom", bottomOffset + "px");

                              					if ($object.is(":visible"))
                              		{
                              							var objectHeight = $object.height();
                              			$(this).css("padding-bottom", (objectHeight + bottomOffset) + "px");
                              		}


                              		gridItemMaxHeight = Math.max(gridItemMaxHeight, $(this).height());
                              	});

                              	//Apply max height
                              	$listItems.css("height", gridItemMaxHeight + "px");
                              	gridItemsEqualHeightApplied = true;

                              }



                              jQuery(function($) {

                                     // Drop-down
                                     var ddBlockSelector = '.dropdown';
                                     var ddOpenTimeout;
                                     var dMenuPosTimeout;
                                     var DD_DELAY_IN = 200;
                                     var DD_DELAY_OUT = 0;
                                     var DD_ANIMATION_IN = 0;
                                     var DD_ANIMATION_OUT = 0;

                                     $(document).on('mouseenter touchstart', ddBlockSelector, function(e) {

                                         var dd = $(this);
                                         var ddHeading = dd.children('.dropdown-heading');
                                         var ddContent = dd.children('.dropdown-content');

                                         // If dd is not opened yet (or not initialized yet)
                                         var isDdOpened = dd.data('ddOpened');
                                         if (isDdOpened === false || isDdOpened === undefined)
                                         {
                                             // Clear old position of dd menu
                                             ddContent.css("left", "");
                                             ddContent.css("right", "");

                                             // Show dd menu
                                             clearTimeout(ddOpenTimeout);
                                             ddOpenTimeout = setTimeout(function() {

                                                 dd.addClass('open');

                                                 // Set dd open flag
                                                 dd.data('ddOpened', true);

                                             }, DD_DELAY_IN);

                                             ddContent.stop(true, true).delay(DD_DELAY_IN).fadeIn(DD_ANIMATION_IN, "easeOutCubic");

                                             // Set new position of dd menu.
                                             // This code is delayed the same amount of time as dd animation.
                                             clearTimeout(dMenuPosTimeout);
                                             dMenuPosTimeout = setTimeout(function() {

                                                 if (ddContent.offset().left < 0)
                                                 {
                                                     var space = dd.offset().left; // Space available on the left of dd
                                                     ddContent.css("left", (-1)*space);
                                                     ddContent.css("right", "auto");
                                                 }

                                             }, DD_DELAY_IN);

                                         } // end: dd is not opened yet

                                     }).on('mouseleave', ddBlockSelector, function(e) {

                                         var dd = $(this);
                                         var ddContent = dd.children('.dropdown-content');

                                         clearTimeout(ddOpenTimeout); // Clear, to close dd on mouseleave
                                         ddContent.stop(true, true).delay(DD_DELAY_OUT).fadeOut(DD_ANIMATION_OUT, "easeInCubic");
                                         if (ddContent.is(":hidden"))
                                         {
                                             ddContent.hide();
                                         }
                                         dd.removeClass('open');

                                         // Clear dd open flag
                                         dd.data('ddOpened', false);

                                         // After hiding, clear the click event flag
                                         dd.data('ddClickIntercepted', false);

                                     }).on('click', ddBlockSelector, function(e) {

                                         var dd = $(this);
                                         var ddHeading = dd.children('.dropdown-heading');
                                         var ddContent = dd.children('.dropdown-content');

                                         // Only if the heading was clicked
                                         if ($.contains(ddHeading[0], e.target) || ddHeading.is(e.target))
                                         {
                                             // Only after the first click already happened, the second click can close the dropdown
                                             if (dd.data('ddClickIntercepted'))
                                             {
                                                 if (dd.hasClass('open'))
                                                 {
                                                     clearTimeout(ddOpenTimeout); // Clear, to close dd on mouseleave
                                                     ddContent.stop(true, true).delay(DD_DELAY_OUT).fadeOut(DD_ANIMATION_OUT, "easeInCubic");
                                                     if (ddContent.is(":hidden"))
                                                     {
                                                         ddContent.hide();
                                                     }
                                                     dd.removeClass('open');

                                                     // Clear dd open flag
                                                     dd.data('ddOpened', false);

                                                     // After hiding, clear the click event flag
                                                     dd.data('ddClickIntercepted', false);
                                                 }
                                             }
                                             else
                                             {
                                                 // Set the click event flag
                                                 dd.data('ddClickIntercepted', true);
                                             }
                                         }

                                     });



                              			var windowScroll_t;
                              	$(window).scroll(function(){

                              		clearTimeout(windowScroll_t);
                              		windowScroll_t = setTimeout(function() {

                              			if ($(this).scrollTop() > 100)
                              			{
                              				$('#scroll-to-top').fadeIn();
                              			}
                              			else
                              			{
                              				$('#scroll-to-top').fadeOut();
                              			}

                              		}, 500);

                              	});

                              	$('#scroll-to-top').click(function(){
                              		$("html, body").animate({scrollTop: 0}, 600, "easeOutCubic");
                              		return false;
                              	});




                              		var startHeight;
                              		var bpad;
                              		$('.category-products-grid').on('mouseenter', '.item', function() {

                              													if ($(window).width() >= 320)
                              				{

                              										if (gridItemsEqualHeightApplied === false)
                              					{
                              						return false;
                              					}

                              				startHeight = $(this).height();
                              				$(this).css("height", "auto"); //Release height
                              				$(this).find(".display-onhover").fadeIn(400, "easeOutCubic"); //Show elements visible on hover
                              				var h2 = $(this).height();

                              									////////////////////////////////////////////////////////////////
                              				var addtocartHeight = 0;
                              				var addtolinksHeight = 0;


                              										var addtolinksEl = $(this).find('.add-to-links');
                              					if (addtolinksEl.hasClass("addto-onimage") == false)
                              						addtolinksHeight = addtolinksEl.innerHeight(); //.height();

                              										var h3 = h2 + addtocartHeight + addtolinksHeight;
                              					var diff = 0;
                              					if (h3 < startHeight)
                              					{
                              						$(this).height(startHeight);
                              					}
                              					else
                              					{
                              						$(this).height(h3); 							diff = h3 - startHeight;
                              					}
                              									////////////////////////////////////////////////////////////////

                              				$(this).css("margin-bottom", "-" + diff + "px");
                              								}
                              		}).on('mouseleave', '.item', function() {

                              												if ($(window).width() >= 320)
                              				{

                              				//Clean up
                              				$(this).find(".display-onhover").stop(true).hide();
                              				$(this).css("margin-bottom", "");

                              															$(this).height(startHeight);

                              								}
                              		});




                              			$('.products-grid, .products-list').on('mouseenter', '.product-image-wrapper', function() {
                              		$(this).find(".alt-img").fadeIn(400, "easeOutCubic");
                              	}).on('mouseleave', '.product-image-wrapper', function() {
                              		$(this).find(".alt-img").stop(true).fadeOut(400, "easeOutCubic");
                              	});



                              			$('.fade-on-hover').on('mouseenter', function() {
                              		$(this).animate({opacity: 0.75}, 300, 'easeInOutCubic');
                              	}).on('mouseleave', function() {
                              		$(this).stop(true).animate({opacity: 1}, 300, 'easeInOutCubic');
                              	});



                              			var dResize = {

                              		winWidth : 0
                              		, winHeight : 0
                              		, windowResizeTimeout : null

                              		, init : function()
                              		{
                              			dResize.winWidth = $(window).width();
                              			dResize.winHeight = $(window).height();
                              			dResize.windowResizeTimeout;

                              			$(window).on('resize', function(e) {
                              				clearTimeout(dResize.windowResizeTimeout);
                              				dResize.windowResizeTimeout = setTimeout(function() {
                              					dResize.onEventResize(e);
                              				}, 50);
                              			});
                              		}

                              		, onEventResize : function(e)
                              		{
                              			//Prevent from executing the code in IE when the window wasn't actually resized
                              			var winNewWidth = $(window).width();
                              			var winNewHeight = $(window).height();

                              			//Code in this condition will be executed only if window was actually resized
                              			if (dResize.winWidth != winNewWidth || dResize.winHeight != winNewHeight)
                              			{
                              				//Trigger deferred resize event
                              				$(window).trigger("themeResize", e);

                              				//Additional code executed on deferred resize
                              				dResize.onEventDeferredResize();
                              			}

                              			//Update window size variables
                              			dResize.winWidth = winNewWidth;
                              			dResize.winHeight = winNewHeight;
                              		}

                              		, onEventDeferredResize : function() //Additional code, execute after window was actually resized
                              		{
                              			//Products grid: equal height of items
                              								setGridItemsEqualHeight($);

                              		}

                              	}; //end: dResize

                              	dResize.init();



                              });

                              jQuery(window).load(function(){
                              						setGridItemsEqualHeight(jQuery);
                              });
                           </script>
                        </div>
                     </div>
                  </div>
               </body>
            </html>
