SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Table `cms_promotion`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `cms_promotion` ;

CREATE TABLE IF NOT EXISTS `cms_promotion` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `subject` VARCHAR(255) NULL,
  `promotion` TEXT NULL,
  `photo_url` TEXT NULL,
  `link_url` VARCHAR(255) NULL,
  `start_at` DATETIME NULL,
  `end_at` DATETIME NULL,
  `publish` TINYINT NULL,
  `created_at` DATETIME NULL,
  `updated_at` DATETIME NULL,
  `deleted_at` DATETIME NULL,
  `created_by` INT NOT NULL,
  `updated_by` INT NULL,
  `deleted_by` INT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_cms_promotion_users1_idx` (`created_by` ASC),
  UNIQUE INDEX `link_url_UNIQUE` (`link_url` ASC),
  CONSTRAINT `fk_cms_promotion_users1`
    FOREIGN KEY (`created_by`)
    REFERENCES `users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
