<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('cms', function () {
	return view('cms-2.menus.home.content');
});
Route::get('rest', function () {
	return view('emails.user-forgot');
});
Route::get('checko', function () {
	return view('emails.user-checkout');
});

Route::group(['prefix'=>'task-scheduler'],function(){
	Route::get('unreadchat','TaskScheduler\TaskSchedulerController@runUnreadChat');
	Route::get('clear-cache','TaskScheduler\TaskSchedulerController@runClearCache');
});

Route::get('/', 'CMS\Home\HomeController@index');

Route::get('pdf','CRM\Master\UserController@printPdf');

Route::group(['prefix'=>'pull_base_ongkir'],function(){
	Route::get('province', 'ThirdParty\RajaOngkirController@getProvince');
	Route::get('city', 'ThirdParty\RajaOngkirController@getCity');
	Route::get('subdistrict', 'ThirdParty\RajaOngkirController@getSubdistrict');
});

Route::group(['middleware'=>['guest','revalidate']],function(){
	Route::get('login', 'CRM\Auth\LoginController@index')->name('login');
	Route::post('login', 'CRM\Auth\LoginController@post');

	Route::get('register', 'CRM\Auth\RegisterController@index');
	Route::post('register', 'CRM\Auth\RegisterController@post');

	Route::get('forgot', 'CRM\Auth\ForgotPasswordController@index');
	Route::post('forgot', 'CRM\Auth\ForgotPasswordController@post');

	Route::get('r/{eemail}/{token}', 'CRM\Auth\ResetPasswordController@index');
	Route::post('r/{eemail}/{token}', 'CRM\Auth\ResetPasswordController@post');
});

Route::get('v/{redirect}/{eid}', 'CRM\Auth\RegisterController@verify');
Route::get('verify', function () {
	return view('emails.user-verification');
	$user = \App\Models\User::where('email','dian.yulius@gmail.com')->first();
  Mail::to($user->email)->send(new \App\Mail\UserVerificationMail($user));
});

Route::group(['middleware'=>['auth','revalidate']],function(){

	Route::get('nf', 'CRM\Notification\NotificationController@getNotification');
	Route::get('logout', 'CRM\Auth\LoginController@indexLogout')->name('logout');
  Route::get('home', 'CRM\Home\HomeController@index');
	// TRANSACTION
	Route::group(['prefix'=>'s'],function(){
	  Route::get('list', 'CRM\Transaction\TScheduleController@indexList');
	  Route::get('{link_url}', 'CRM\Transaction\TScheduleController@indexDetail');
	});

	Route::get('search', 'CRM\Transaction\TItemController@indexSearch');

	// Route::group(['prefix'=>'search'],function(){
	// 	Route::get('{type}', 'CRM\Transaction\TItemController@indexList');
	// });

	Route::group(['prefix'=>'p'],function(){
  	Route::get('list', 'CRM\Transaction\TItemController@indexList');
		Route::get('estimate/cost', 'ThirdParty\RajaOngkirController@getEstimateCost');
		Route::get('{link_url}', 'CRM\Transaction\TItemController@indexDetail');
  	Route::get('{link_url}/cost', 'CRM\Transaction\TItemController@indexDetail');
		Route::get('{link_url}/discussion', 'CRM\Transaction\TItemController@indexDetail');
		Route::post('{link_url}/discussion', 'CRM\Transaction\TItemController@postDiscussion');
		Route::post('{link_url}', 'CRM\Transaction\TItemController@postDetail');
	});

	Route::group(['prefix'=>'cart'],function(){
		Route::get('/', 'CRM\Transaction\TCartController@index');
		Route::post('/', 'CRM\Transaction\TCartController@checkout');
		Route::get('address', 'CRM\Transaction\TCartController@getAddress');
		Route::post('address', 'CRM\Transaction\TCartController@changeAddress');
		Route::get('courier', 'CRM\Transaction\TCartController@getCourier');
	});

	//MEMBER
	Route::group(['prefix'=>'member'],function (){
		Route::group(['prefix'=>'chat'],function(){
			Route::post('post', 'CRM\Chat\MemberChatController@post');
			Route::get('message', 'CRM\Chat\MemberChatController@getMessage');
		});

		Route::group(['prefix'=>'h'],function(){
			Route::group(['prefix'=>'t'],function(){
				Route::get('/', 'CRM\Transaction\THistoryController@indexPayment');
				Route::get('order', 'CRM\Transaction\THistoryController@indexOrder');
				Route::get('delivery', 'CRM\Transaction\THistoryController@indexDelivery');
			});
		});

	  Route::group(['prefix'=>'profile'],function (){
			Route::get('/', 'CRM\Profile\ProfileController@index')->name('profile.edit');
			Route::post('/', 'CRM\Profile\ProfileController@post');
			Route::get('address', 'CRM\Profile\ProfileController@indexAddress')->name('profile.address');
			Route::post('address', 'CRM\Profile\ProfileController@postAddress');
      Route::post('address/datatable', 'CRM\Profile\ProfileController@dataTableAddress')->name('datatable.profile.address');
			Route::post('address/datatable/detail', 'CRM\Profile\ProfileController@dataTableAddressDetail');
			Route::get('address/country', 'CRM\Profile\ProfileController@countryAddress');
			Route::get('account', 'CRM\Profile\ProfileController@indexAccount')->name('profile.account');
			Route::post('account', 'CRM\Profile\ProfileController@postAccount');
			Route::post('photo', 'CRM\Profile\ProfileController@postPhoto');
			Route::get('address/delete/{eid}/{eua}', 'CRM\Profile\ProfileController@deleteAddress');
      Route::get('sendverify/{eid}', 'CRM\Profile\ProfileController@verify');
		});
	});


	Route::group(['prefix'=>'chat','middleware'=>['admin']],function(){
		Route::get('/', 'CRM\Chat\ChatController@index');
		Route::post('post', 'CRM\Chat\ChatController@post');
		Route::get('room', 'CRM\Chat\ChatController@getRoom');
		Route::get('message', 'CRM\Chat\ChatController@getMessage');
		// Route::post('/', 'CRM\Chat\ChatController@post');
	});
	Route::group(['middleware'=>['admin']],function (){
	  Route::get('dashboard', 'CRM\Dashboard\DashboardController@index');

		Route::group(['prefix'=>'broadcast'],function(){
			Route::get('entry', 'CRM\Master\BroadcastController@index');
			Route::post('entry', 'CRM\Master\BroadcastController@post');
			Route::post('upload', 'CRM\Master\BroadcastController@uploadFile');
			Route::get('users', 'CRM\Master\BroadcastController@getUsers');
			// Route::get('list', 'CRM\Master\BroadcastController@index');
		});

		Route::group(['prefix'=>'cms'],function(){
			Route::group(['prefix'=>'promotion'],function (){
				Route::get('entry', 'CRM\CMS\PromotionController@indexEntry');
				Route::post('entry', 'CRM\CMS\PromotionController@postEntry');
				Route::get('list', 'CRM\CMS\PromotionController@indexList');
	      Route::post('datatable', 'CRM\CMS\PromotionController@dataTable');
	      Route::get('delete/{eid}', 'CRM\CMS\PromotionController@delete');
				Route::get('entry/{eid}', 'CRM\CMS\PromotionController@indexEdit');
				Route::post('entry/{eid}', 'CRM\CMS\PromotionController@postEdit');
			});
		});

		Route::group(['prefix'=>'cash'],function(){
			Route::get('entry', 'CRM\Master\CashController@indexEntry');
			Route::post('entry', 'CRM\Master\CashController@postEntry');
			Route::get('list', 'CRM\Master\CashController@indexList');
      Route::post('datatable', 'CRM\Master\CashController@dataTable');

			Route::get('entry/{eid}', 'CRM\Master\CashController@indexEdit');
			Route::post('entry/{eid}', 'CRM\Master\CashController@postEdit');
		});
	});
  Route::group(['prefix'=>'master','middleware'=>['admin']],function (){
    Route::group(['prefix'=>'schedule'],function (){
			Route::get('entry', 'CRM\Master\ScheduleController@indexEntry');
			Route::post('entry', 'CRM\Master\ScheduleController@postEntry');
			Route::get('list', 'CRM\Master\ScheduleController@indexList');
      Route::post('datatable', 'CRM\Master\ScheduleController@dataTable');
      Route::get('delete/{eid}', 'CRM\Master\ScheduleController@delete');
			Route::get('entry/{eid}', 'CRM\Master\ScheduleController@indexEdit');
			Route::post('entry/{eid}', 'CRM\Master\ScheduleController@postEdit');
		});

    Route::group(['prefix'=>'item-category'],function (){
			Route::get('entry', 'CRM\Master\ItemCategoryController@indexEntry');
			Route::post('entry', 'CRM\Master\ItemCategoryController@postEntry');
			Route::get('list', 'CRM\Master\ItemCategoryController@indexList');
      Route::post('datatable', 'CRM\Master\ItemCategoryController@dataTable');
			Route::get('schedule', 'CRM\Master\ItemCategoryController@itemSchedule');
			Route::get('category', 'CRM\Master\ItemCategoryController@itemCategory');
      Route::get('delete/{eid}', 'CRM\Master\ItemCategoryController@delete');
			Route::get('entry/{eid}', 'CRM\Master\ItemCategoryController@indexEdit');
			Route::post('entry/{eid}', 'CRM\Master\ItemCategoryController@postEdit');
		});

    Route::group(['prefix'=>'item'],function (){
			Route::get('entry', 'CRM\Master\ItemController@indexEntry');
			Route::post('entry', 'CRM\Master\ItemController@postEntry');
			Route::get('list', 'CRM\Master\ItemController@indexList');
      Route::post('datatable', 'CRM\Master\ItemController@dataTable');
			Route::get('schedule', 'CRM\Master\ItemController@itemSchedule');
			Route::get('category', 'CRM\Master\ItemController@itemCategory');
      Route::get('delete/{eid}', 'CRM\Master\ItemController@delete');
			Route::get('entry/{eid}', 'CRM\Master\ItemController@indexEdit');
			Route::post('entry/{eid}', 'CRM\Master\ItemController@postEdit');
		});

    Route::group(['prefix'=>'users'],function (){
      Route::get('print-pdf', 'CRM\Master\UserController@printPdf');
      Route::get('list', 'CRM\Master\UserController@indexList')->name('list.users');
      Route::post('datatable', 'CRM\Master\UserController@dataTable')->name('datatable.users');
      Route::get('delete/{eid}', 'CRM\Master\UserController@delete')->name('delete.users');

			Route::group(['prefix'=>'edit'],function (){
	      Route::post('address/datatable', 'CRM\Master\UserController@dataTableAddress')->name('datatable.profile.address');
				Route::post('address/datatable/detail', 'CRM\Master\UserController@dataTableAddressDetail');
				Route::get('address/country', 'CRM\Master\UserController@countryAddress');
				Route::post('photo', 'CRM\Master\UserController@postPhoto');

				Route::get('address/delete/{eid}/{eua}', 'CRM\Master\UserController@deleteAddress');
				Route::get('address/{eid}', 'CRM\Master\UserController@indexAddress')->name('profile.address');
				Route::post('address/{eid}', 'CRM\Master\UserController@postAddress');
				Route::get('account/{eid}', 'CRM\Master\UserController@indexAccount')->name('profile.account');
				Route::post('account/{eid}', 'CRM\Master\UserController@postAccount');
				Route::get('{eid}', 'CRM\Master\UserController@index')->name('edit.users');
				Route::post('{eid}', 'CRM\Master\UserController@post');
			});
    });
  });
});
