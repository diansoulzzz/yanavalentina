<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
use Carbon\Carbon;
use App\Models\ChatMessage;
use App\Models\ChatRoom;
use App\Models\TaskScheduler;
use App\Mail\UserChatMail;
use App\Models\User;
use Mail;

class MailChatCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'MailChatCommand:sendUnreadChat';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send Unread Chat';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      $time = Carbon::now();
      TaskScheduler::insert(['command' => $this->signature,'created_at'=>$time]);

      $chat_count = ChatMessage::whereNull('emailed_at')
      ->whereDoesntHave('chat_reads', function($query) {
        $query->whereHas('user', function ($query_d){
          $query_d->where('role','<>','1');
        });
      })
      ->get()->count();
      if ($chat_count>0){
        $chat_message = ChatMessage::whereNull('emailed_at')
        ->whereDoesntHave('chat_reads', function($query) {
          $query->whereHas('user', function ($query_d){
            $query_d->where('role','<>','1');
          });
        })
        ->get()->groupBy(function($chat) {
          return $chat->users_email.'|~|'.$chat->users_photo_url.'|~|'.$chat->users_name;
        });

        $chat_mail = ChatMessage::whereNull('emailed_at')
        ->whereDoesntHave('chat_reads', function($query) {
          $query->whereHas('user', function ($query_d){
            $query_d->where('role','<>','1');
          });
        })->update(['emailed_at' => $time]);

        $data =[
          'chat_count'=>$chat_count,
          'chat_message'=>$chat_message
        ];
        // ->where('email','dian.yulius@gmail.com')
        $users = User::where('role','<>','1')->get();
        foreach ($users as $key => $user) {
          $this->sendMail($user,$data);
        }
      }
    }

    public function sendMail($users, $data)
    {
      Mail::to($users->email)->send(new UserChatMail($users, $data));
    }
}
