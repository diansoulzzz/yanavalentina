<?php
namespace App\Http\Controllers\ThirdParty;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Validation\Rule;
use Validator;
use Illuminate\Support\Facades\Auth;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Promise;
use GuzzleHttp\Client;
use App\Models\City;
use App\Models\Province;
use App\Models\SubDistrict;
use DB;

class RajaOngkirController extends Controller
{
    public function getResponse(Request $request, String $method, $url, Array $query, $type='GET')
    {
        if ($type=='GET'){
          $param = 'query';
        } else {
          $param = 'form_params';
        }
        $client = new Client();
        $res = $client->request($method, $url, [
          'headers' => [
              'key' => config('app.key_raja_ongkir'),
          ],
          $param => $query,
        ]);
        $json_response = json_decode($res->getBody());
        return $json_response->rajaongkir;
    }
    public function getProvince(Request $request)
    {
        $response = $this->getResponse($request,'GET','https://pro.rajaongkir.com/api/province',[]);
        if ($response->status->code>200) return 'Invalid';
        $insert = [];
        foreach ($response->results as $key => $value) {
          $insert[]=[
            'id' => $value->province_id,
            'name' => $value->province,
          ];
        }
        DB::statement("SET foreign_key_checks=0");
        Province::truncate();
        DB::statement("SET foreign_key_checks=1");
        Province::insert($insert);
        return 'OK';
    }
    public function getCity(Request $request)
    {
        $response = $this->getResponse($request,'GET','https://pro.rajaongkir.com/api/city',[]);
        if ($response->status->code>200) return 'Invalid';
        $insert = [];
        foreach ($response->results as $key => $value) {
          $insert[]=[
            'id' => $value->city_id,
            'name' => $value->city_name,
            'province_id' => $value->province_id,
            'type' => $value->type,
            'postal_code' => $value->postal_code,
          ];
        }
        DB::statement("SET foreign_key_checks=0");
        City::truncate();
        DB::statement("SET foreign_key_checks=1");
        City::insert($insert);
        return 'OK';
    }
    public function getSubdistrict(Request $request)
    {
        DB::statement("SET foreign_key_checks=0");
        SubDistrict::truncate();
        DB::statement("SET foreign_key_checks=1");
        $cities = City::get();
        foreach ($cities as $key => $value) {
          $pcity = [
            'city' => $value->id
          ];
          $response = $this->getResponse($request,'GET','https://pro.rajaongkir.com/api/subdistrict',$pcity);
          if ($response->status->code>200) return 'Invalid';
          $insert = [];
          foreach ($response->results as $key => $value) {
            $insert[]=[
              'id' => $value->subdistrict_id,
              'name' => $value->subdistrict_name,
              'city_id' => $value->city_id,
            ];
          }
          SubDistrict::insert($insert);
        }
        return 'OK';
    }
    public function getEstimateCost(Request $request)
    {
      $weight = $request->input('weight');
      $destination = $request->input('destination');
      $origin = '5632';
      $courier = 'jne:pos:tiki';
      $originType = 'subdistrict';
      $destinationType = 'subdistrict';
      $fcost = [
        'origin' => $origin,
        'originType' => $originType,
        'destination' => $destination,
        'destinationType' => $destinationType,
        'weight' => $weight,
        'courier' => $courier,
      ];
      // return $pcost;
      $response = $this->getResponse($request,'POST','https://pro.rajaongkir.com/api/cost',$fcost, 'POST');
      // return $response->results;
      if ($response->status->code>200) {
        $result = [
          'status' => 'ERROR',
        ];
        return $result;
      }
      // return $response->results;
      $result = [
        'data' => $response->results,
        // 'data' => $response->results[0]->costs,
        'status' => 'OK',
      ];
      return $result;
    }
    // public static function getCost(Request $request)
    // {
    //   $client = new Client(['base_uri' => 'https://pro.rajaongkir.com']);
    //   $couriers = [
    //     'jne',
    //     'pos',
    //     'tiki',
    //     'rpx',
    //     'esl',
    //     'pcp',
    //     'pandu',
    //     'wahana',
    //     'sicepat',
    //     'jnt',
    //     'pahala',
    //     'cahaya',
    //     'sap', '
    //     jet',
    //     'indah',
    //     'dse',
    //     'slis',
    //     'first',
    //     'ncs',
    //     'star',
    //     'nss'
    //   ];
    //   $asyncPost=[];
    //   $weight = '10';
    //   $destination = '5622';
    //   $origin = '5632';
    //   $courier = 'pos';
    //   $originType = 'subdistrict';
    //   $destinationType = 'subdistrict';
    //   $fcost = [
    //     'origin' => $origin,
    //     'originType' => $originType,
    //     'destination' => $destination,
    //     'destinationType' => $destinationType,
    //     'weight' => $weight,
    //     'courier' => $courier,
    //   ];
    //
    //   foreach ($couriers as $key => $courier) {
    //     $asyncPost[$courier] =
    //       $client->postAsync('api/cost', [
    //         'headers' => [
    //           'key' => config('app.key_raja_ongkir'),
    //         ],
    //         'form_params' => $fcost,
    //       ]);
    //   };
    //   // return $asyncPost;
    //   // $promises = [
    //   //   'image' => $client->getAsync('api/province', [
    //   //     'headers' => [
    //   //       'key' => config('app.key_raja_ongkir'),
    //   //     ]
    //   //   ]),
    //   // ];
    //   // return $promises;
    //   $results = Promise\unwrap($asyncPost);
    //   $results = Promise\settle($asyncPost)->wait();
    //   // $responses = \GuzzleHttp\Promise\unwrap($requestArr);
    //   // return $results['image']['value']->getBody();
    //   $returned = [];
    //   foreach ($results as $key => $cost) {
    //     $data = json_decode($cost['value']->getBody());
    //     $returned[] = $data->rajaongkir;
    //   };
    //   return $returned;
    //   // return response()->json($returned);
    //   // echo $results['image']['value']->getHeader('Content-Length')[0];
    //   // echo $results['png']['value']->getHeader('Content-Length')[0];
    //   // return;
    // }
}
