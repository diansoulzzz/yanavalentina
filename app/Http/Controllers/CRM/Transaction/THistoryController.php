<?php
namespace App\Http\Controllers\CRM\Transaction;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Models\Schedule;
use App\Models\ScheduleImage;
use App\Models\Item;
use App\Models\ItemCategory;
use App\Models\ItemImage;
use App\Models\User;
use App\Models\UsersAddress;
use App\Models\Cart;
use App\Models\InvoiceHeader;
use Auth;
use Yajra\Datatables\Html\Builder;
use DataTables;

class THistoryController extends Controller
{
  public function indexPayment(Request $request, Builder $builder) {
    $title = 'Status Pembayaran (Belum Lunas)';
    if (request()->ajax()) {
      $models = InvoiceHeader::where('users_id',Auth::id())->where('paid_outstanding','<>','0');
      return DataTables::of($models)->make(true);
    }
    $datatable = $builder->columns([
       ['data' => 'invoice', 'name' => 'invoice', 'title' => 'Kodenota'],
       ['data' => 'date', 'name' => 'date', 'title' => 'Tanggal'],
       ['data' => 'courier_service', 'name' => 'courier_service', 'title' => 'Kurir'],
       ['data' => 'format_total_grand', 'name' => 'format_total_grand', 'title' => 'Total'],
    ]);
    return view('crm.menus.transaction.history.content',compact('title','datatable'));
  }
  public function indexOrder(Request $request, Builder $builder) {
    $title = 'Status Pemesanan (Di Proses)';
    if (request()->ajax()) {
      $models = InvoiceHeader::where('users_id',Auth::id())->where('paid_outstanding','=','0');
      return DataTables::of($models)->make(true);
    }
    $datatable = $builder->columns([
       ['data' => 'invoice', 'name' => 'invoice', 'title' => 'Kodenota'],
       ['data' => 'date', 'name' => 'date', 'title' => 'Tanggal'],
       ['data' => 'courier_service', 'name' => 'courier_service', 'title' => 'Kurir'],
       ['data' => 'format_total_grand', 'name' => 'format_total_grand', 'title' => 'Total'],
    ]);
    return view('crm.menus.transaction.history.content',compact('title','datatable'));
  }
  public function indexDelivery(){

  }
}
