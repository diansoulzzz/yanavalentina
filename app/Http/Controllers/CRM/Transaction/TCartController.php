<?php
namespace App\Http\Controllers\CRM\Transaction;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Models\Schedule;
use App\Models\ScheduleImage;
use App\Models\Item;
use App\Models\ItemImage;
use App\Models\User;
use App\Models\UsersAddress;
use App\Models\Cart;
use App\Models\InvoiceHeader;
use App\Models\InvoiceDetail;
use Auth;
use App\Http\Controllers\ThirdParty\RajaOngkirController;
use App\Mail\UserCheckOutMail;
use Mail;

class TCartController extends Controller
{
    public function index(Request $request){
      // $user = User::with('users_addresses_primary')->find(Auth::id());
      // $item = Item::with(['item_images','item_category','item_images_primary'])
      // ->where('publish',1)
      // ->where('link_url',$link_url)
      // ->first();
      return view('crm.menus.transaction.cart.content');
    }
    public function checkout(Request $request){
      // return $request->all();
      $qty_cart = $request->input('qty');
      foreach ($qty_cart as $key => $qty) {
        Cart::find($key)->update(['qty'=>$qty]);
      }
      $courier = (explode("|-|",$request->input('courier_cost')));
      $courier_code = $courier[0];
      $courier_service = $courier[1];
      $courier_cost = $courier[2];
      $carts = Cart::where('users_id',Auth::id())->where('checkout',0)->get();
      $total = $carts->sum('subtotal');
      $users_address_id = $carts->first()->users_address_id;
      // return $users_address_id;
      $total_grand = $total+$courier_cost;
      // return $grandtotal;
      $date_time = Carbon::now();
      $date = Carbon::today();

      $last_invoice = InvoiceHeader::where('date','=',$date)->count()+1;
      $invoice_header = new InvoiceHeader();
      $invoice_header->invoice = $this->generateCode('INV',$date,$last_invoice);
      $invoice_header->date = $date;
      $invoice_header->courier = $courier_code;
      $invoice_header->courier_service = $courier_service;
      $invoice_header->courier_cost = $courier_cost;
      $invoice_header->total = $total;
      $invoice_header->total_grand = $total_grand;
      $invoice_header->paid = 0;
      $invoice_header->paid_outstanding = $total_grand;
      $invoice_header->users_id = Auth::id();
      $invoice_header->users_address_id = $users_address_id;
      $invoice_header->save();

      $invoice_detail = [];
      foreach ($carts as $key => $cart) {
        $invoice_detail[] = new InvoiceDetail([
          'qty' => $cart->qty,
          'price' => $cart->item->price,
          'cart_id' => $cart->id,
          'item_id' => $cart->item->id,
        ]);
      }
      $invoice_header->invoice_details()->saveMany($invoice_detail);
      // id, invoice, date, courier, courier_cost, total, total_grand, paid, paid_outstanding, created_at, updated_at, deleted_at, users_id, users_address_id
      // return $carts;
      Cart::where('users_id',Auth::id())->where('checkout',0)->update(['checkout'=>1]);
      // $invoice_header->invoice_details()->cart->update(['checkout'=>1]);
      $status = [
        'alert'=>'alert-success',
        'message'=>'Checkout berhasil',
      ];
      $users = Auth::user();
      $this->sendCheckoutMail($request,$users);

      return redirect('home')->with('status', $status);
    }
    public function sendCheckoutMail(Request $request, $users)
    {
      Mail::to($users->email)->send(new UserCheckOutMail($users));
    }
    public function changeAddress(Request $request) {
      // return $request->all();
      $users_address_id = $request->input('users_address_id');
      // return $users_address_id;
      $cart = Cart::where('users_id',Auth::id())->where('checkout',0)->update(['users_address_id' => $users_address_id]);
      $status = [
        'alert'=>'alert-success',
        'message'=>'Alamat pengiriman berhasil diganti',
      ];
      return back()->with('status', $status);
    }
    public function getCourier(Request $request) {
      $carts = Cart::with('item')->where('users_id',Auth::id())->where('checkout',0)->get();
      $total_weight = '';
      $destination = '';
      foreach ($carts as $key => $cart) {
        $total_weight = $cart->qty * $cart->item->weight;
        $destination = $cart->users_address->subdistrict_id;
      }
      $weight = $total_weight;
      $request->request->add([
        'weight' => $weight,
        'destination'=> $destination
      ]);
      // $request->request->add(['destination', $destination]);
      // $request->request->set('weight', $weight);
      // return $request->all();
      $shippingcost = new RajaOngkirController();
      $result =  $shippingcost->getEstimateCost($request);
      // return $result;
      $returned = [];
      foreach ($result['data'] as $key => $value) {
        foreach ($value->costs as $key1 => $value1) {
          if (count($value->costs)>0){
            $first_cost = reset($value1->cost);
            $cal_cost = (object) [
              'id'=>$value->code.'|-|'.$value1->service.'|-|'.$first_cost->value,
              'code' =>strtoupper($value->code),
              'name' =>$value->name,
              'service' =>$value1->service,
              'description' =>$value1->description,
              // 'cost' => $value1->cost,
              'cost' => $first_cost,
              // 'costs' =>array_pop(array_reverse($value1->cost)),
            ];
            $estimate_day = $cal_cost->cost->etd;
            if (!$estimate_day) {
              $estimate_day = 'Langsung';
            }
             // && (strpos(strtolower($estimate_day),'Langsung') == false)
            if ((!strpos(strtolower($estimate_day),'hari') !== false) && ($estimate_day!='Langsung')) {
              $estimate_day = $estimate_day. ' HARI';
            }
            $cal_cost->cost->etd = $estimate_day;
            // $cal_cost->cost->id = $value->code.$key1;
            $returned[] = $cal_cost;
          }
        }
      }
      // return $returned;
      $data = [
        'total_count'=> count($returned),
        'incomplete_results'=>'false',
        'items' => $returned,
      ];
      return $data;
      // $address = UsersAddress::where('users_id',Auth::id())->get();
    }
    public function getAddress(Request $request) {
      // return Auth::user();
      $address = UsersAddress::where('users_id',Auth::id())->get();
      $data = [
        'address' => $address
      ];
      if ($address){
        $result = [
          'status'=>'OK',
          'success'=>'1',
          'data'=> $data,
        ];
      } else {
        $result = [
          'status'=>'ERROR',
          'success'=>'0',
        ];
      }
      return $result;
    }
}
