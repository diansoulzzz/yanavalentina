<?php
namespace App\Http\Controllers\CRM\Transaction;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Models\Schedule;
use App\Models\ScheduleImage;
use App\Models\Item;
use App\Models\ItemCategory;
use App\Models\ItemImage;
use App\Models\User;
use App\Models\UsersAddress;
use App\Models\Cart;
use Auth;

class TScheduleController extends Controller
{
    public function indexList(Request $request){
      $schedules = Schedule::whereHas('schedule_images')->with('schedule_images')->where('publish',1)->get();
      return view('crm.menus.transaction.schedule.list',compact('schedules'));
    }

    public function indexDetail(Request $request, $link_url){
      $keyword = $request->input('q');
      $category = $request->input('c');
      $schedule = $request->input('s');
      $p_min = $request->input('p_min');
      $p_max = $request->input('p_max');
      $order_by = $request->input('sb');

      $schedule = Schedule::where('link_url',$link_url)->first();
      // return $schedule;
      $items = Item::with('item_images_primary');
      $items = $items->where('name','like','%'.$keyword.'%');

      if ($p_min) {
        $items = $items->where('price','>=',$p_min);
      }
      if ($p_max) {
        $items = $items->where('price','<=',$p_max);
      }
      if ($schedule) {
        $items = $items->whereIn('schedule_id',$schedule);
      }
      if ($category) {
        $items = $items->whereIn('item_category_id',$category);
      }

      $items = $items->where('publish', true);

      if ($order_by) {
        if($order_by=='price_asc') {
          $order = ['price','asc'];
        } else if ($order_by=='price_desc') {
          $order = ['price','desc'];
        }
        $items = $items->orderBy($order[0], $order[1]);
      }
      // return $items;
      $items = $items->paginate(6);
      $schedules = Schedule::get();
      $items_category = ItemCategory::get();
      return view('crm.menus.transaction.schedule.detail',compact('items','items_category','keyword','request','schedule','schedules'));
    }
}
