<?php
namespace App\Http\Controllers\CRM\Transaction;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Models\Schedule;
use App\Models\ScheduleImage;
use App\Models\Item;
use App\Models\ItemCategory;
use App\Models\ItemImage;
use App\Models\ItemDiscussion;
use App\Models\User;
use App\Models\UsersAddress;
use App\Models\Cart;
use Auth;

class TItemController extends Controller
{
    public function indexSearch(Request $request){
      // return $request->all();
      $keyword = $request->input('q');
      $category = $request->input('c');
      $schedule = $request->input('s');
      $p_min = $request->input('p_min');
      $p_max = $request->input('p_max');
      $order_by = $request->input('sb');

      $items = Item::with('item_images_primary');
      $items = $items->where('name','like','%'.$keyword.'%');
      // $order_by = $items->orderBy('')
      if ($p_min) {
        $items = $items->where('price','>=',$p_min);
      }
      if ($p_max) {
        $items = $items->where('price','<=',$p_max);
      }
      if ($schedule) {
        $items = $items->whereIn('schedule_id',$schedule);
      }
      if ($category) {
        $items = $items->whereIn('item_category_id',$category);
      }
      $items = $items->where('publish', true);
      if ($order_by) {
        if($order_by=='price_asc') {
          $order = ['price','asc'];
        } else if ($order_by=='price_desc') {
          $order = ['price','desc'];
        }
        // return $order[1];
        $items = $items->orderBy($order[0], $order[1]);
      }
      // return response()->json($items);
      $items = $items->paginate(6);
      $schedules = Schedule::get();
      $items_category = ItemCategory::get();
      return view('crm.menus.transaction.item.search',compact('items','items_category','keyword','request','schedules'));
    }
    public function indexDetail(Request $request,$link_url){
      $user = User::with('users_addresses_primary')->find(Auth::id());
      $item = Item::with([
        'item_images',
        'item_category',
        'item_images_primary',
        'item_discussions' => function($query){
          $query->whereNull('item_discussion_id');
        },
        'item_discussions.item_discussions',
      ])
      ->where('publish',1)
      ->where('link_url',$link_url)
      ->first();
      return view('crm.menus.transaction.item.detail',compact('item','user'));
    }
    public function postDiscussion(Request $request,$link_url){
      // return $request->all();
      $users = User::find(Auth::id());
      $item = Item::find($request->input('eid'));
      // return $item;
      if ($request->has('write_comment')){
        $discussion_id = $request->input('write_comment');
        $comment = $request->input('comment');
        $item_discussion = new ItemDiscussion([
          'discussion'=>$comment,
          'users_id'=>$users->id,
          'item_discussion_id'=>$discussion_id,
        ]);
        $item->item_discussions()->save($item_discussion);
        $status = [
          'alert'=>'alert-success',
          'message'=>'Komentar anda berhasil dibuat',
        ];
        return back()->with('status', $status);
      }
      $discussion = $request->input('discussion');
      $item_discussion = new ItemDiscussion([
        'discussion'=>$discussion,
        'users_id'=>$users->id
      ]);
      $item->item_discussions()->save($item_discussion);
      $status = [
        'alert'=>'alert-success',
        'message'=>'Diskusi anda berhasil dibuat',
      ];
      return back()->with('status', $status);
    }
    public function postDetail(Request $request,$link_url){
      $users = User::find(Auth::id());
      $item = Item::find($request->input('eid'));
      $cart = Cart::firstOrNew([
        'item_id' => $item->id,
        'users_id' => $users->id,
        'checkout' => '0',
      ]);
      $cart->price = $item->price;
      $cart->qty = $cart->qty+$request->input('qty');
      $cart->users_address_id = $users->users_addresses_primary->id;
      $cart->save();
      if ($request->has('cart')){
        $status = [
          'alert'=>'alert-success',
          'message'=>'Barang berhasil ditambahkan ke keranjang',
        ];
        return back()->with('status', $status);
      } else {
        $status = [
          'alert'=>'alert-success',
          'message'=>'Barang berhasil ditambahkan ke keranjang',
        ];
        return redirect('cart');
      }
    }
}
