<?php
namespace App\Http\Controllers\CRM\Home;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Models\Schedule;
use App\Models\ScheduleImage;
use App\Models\Item;
use App\Models\ItemImage;
use App\Models\User;
use Auth;
class HomeController extends Controller
{
    public function index(Request $request){
      // $items = Item::with('item_images')->whereHas('item_images', function ($query){
      //     $query->where('primary',1);
      // })->where('publish',1)->get();
      $items = Item::whereHas('item_images', function ($query){
          $query->where('primary',1);
      })->with('item_images')->where('publish',1)->get();
      // return $items;
      $schedules = Schedule::whereHas('schedule_images', function ($query){
          $query->where('primary',1);
      })->with('schedule_images')->where('publish',1)->get();
      return view('crm.menus.home.content',compact('items','schedules'));
    }
}
