<?php
namespace App\Http\Controllers\CRM\Chat;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Validation\Rule;
use Validator;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use Mail;
use App\Mail\UserForgotPasswordMail;
use Illuminate\Support\Facades\Crypt;
use App\Models\ChatMessage;
use App\Models\ChatRoom;
use App\Models\ChatRead;

class ChatController extends Controller
{
    public function index(Request $request)
    {
        // $test = ChatMessage::first();
        // $test =  ChatRoom::with(['chat_messages'])->first();
        // return $test;
        // $test->chat_messages()->chat_reads()
        // ->where('users_id','<>',Auth::id())->whereNull('read_at')->count();
        // return $test;
      // return Auth::id();
      // return $this->getRoom($request);
        return view('crm.menus.chat.content');
    }
    public function getRoom(Request $request)
    {
        $chatroom = ChatRoom::whereHas('chat_messages')->where('users_id','<>',Auth::id())->get()->makeHidden(['user','id','users_id','chat_messages']);
        if ($chatroom){
          $result = [
            'status'=>'OK',
            'success'=>'1',
            'data'=> [
              'room'=>$chatroom,
            ],
          ];
          if ($request->has('ecr')){
            if ($request->input('ecr') <> 0) {
              $chat = $this->getMessage($request);
              $result['data']['chat'] = $chat;
            }
          }
        } else {
          $result = [
            'status'=>'ERROR',
            'success'=>'0',
          ];
        }
        return response()->json($result);
        // return view('crm.menus.chat.content');
    }
    public function getMessage(Request $request)
    {
        $dcr = $request->input('ecr');
        $chatroom = ChatRoom::find($dcr);
        // $read = ChatRead::create(['chat_room_id' => $dcr]);
        // $chatupdate->where('users_id','<>',Auth::id())->update(['read_at'=>Carbon::now()]);
        $chatmessage = ChatMessage::where('chat_room_id',$dcr)->get()->groupBy(function($date) {
          return Carbon::parse($date->created_at)->format('d M Y');
        });
        $message_unread = ChatMessage::where('chat_room_id',$dcr)->get();
        $is_reads = [];
        if (!$message_unread->isEmpty()) {
          foreach ($message_unread as $key => $value) {
            if (!$value->is_read)
            {
              $is_reads[] = [
                'chat_message_id' => $value->id,
                'users_id' => Auth::id(),
                'created_at'=> Carbon::now(),
              ];
            }
          }
          ChatRead::insert($is_reads);
        }
        $is_new = false;
        if (empty($is_reads))
        {
          $is_new = false;
        } else {
          $is_new = true;
        }
        return $chat = [
          'message' => $chatmessage,
          'info' => $chatroom,
          'is_new' => $is_new,
        ];
        // if ($chatmessage){
        //   $result = [
        //     'status'=>'OK',
        //     'success'=>'1',
        //     'data'=> [
        //       'chat'=>$chatmessage,
        //       'room'=>$chatroom,
        //     ],
        //   ];
        // } else {
        //   $result = [
        //     'status'=>'ERROR',
        //     'success'=>'0',
        //   ];
        // }
        // return response()->json($result);
        // return view('crm.menus.chat.content');
    }
    public function post(Request $request)
    {
      $chatmessage = new ChatMessage();
      $chatmessage->chat = $request->input('msg');
      $chatmessage->chat_room_id = $request->input('ecr');
      $chatmessage->users_id = Auth::id();
      $chatmessage->save();
      return $this->getRoom($request);
        // $messages = [
        //     'email.required' => 'Email wajib di isi',
        //     'email.email' => 'Email tidak valid',
        //     'email.exists' => 'Email tidak ditemukan atau belum terdaftar sebelumnya',
        // ];
        // $this->validate($request, [
        //     'email' => 'required|email|exists:users,email',
        // ],$messages);
        // $email = $request->input('email');
        // $users = User::where('email',$email)->first();
        // $users->reset_token = app('auth.password.broker')->createToken($users);
        // // return $users;
        // $this->sendMail($users);
        // return back()->withSuccess('Kami sudah mengirimkan email untuk melakukan reset password anda');

    }
}
