<?php
namespace App\Http\Controllers\CRM\Chat;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Validation\Rule;
use Validator;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use Mail;
use App\Mail\UserForgotPasswordMail;
use Illuminate\Support\Facades\Crypt;
use App\Models\ChatMessage;
use App\Models\ChatRoom;
use App\Models\ChatRead;

class MemberChatController extends Controller
{
    public function getMessage(Request $request)
    {
        $chatroom = ChatRoom::where('users_id',Auth::id())->first();
        if (!$chatroom)
        {
          $chatroom = new Chatroom();
          $chatroom->users_id = Auth::id();
          $chatroom->save();
        }
        $dcr = $chatroom->id;

        $message_unread = ChatMessage::where('chat_room_id',$dcr)->get();
        if (!$message_unread->isEmpty()) {
          $is_reads = [];
          foreach ($message_unread as $key => $value) {
            if (!$value->is_read)
            {
              $is_reads[] = [
                'chat_message_id' => $value->id,
                'users_id' => Auth::id(),
                'created_at'=> Carbon::now(),
              ];
            }
          }
          ChatRead::insert($is_reads);
        }
        $is_new = false;
        if (empty($is_reads))
        {
          $is_new = false;
        } else {
          $is_new = true;
        }
        $chatmessage = ChatMessage::where('chat_room_id',$dcr)->get()->groupBy(function($date) {
          return Carbon::parse($date->created_at)->format('d M Y');
        });

        $chat = [
          'message' => $chatmessage,
          'is_new' => $is_new,
          'is_read' => $is_reads
        ];

        if ($chatmessage){
          $result = [
            'status'=>'OK',
            'success'=>'1',
            'data'=> [
              'chat'=>$chat,
            ],
          ];
        } else {
          $result = [
            'status'=>'ERROR',
            'success'=>'0',
          ];
        }
        return response()->json($result);
    }
    public function post(Request $request)
    {
      $chatroom = ChatRoom::where('users_id',Auth::id())->first();
      if (!$chatroom)
      {
        $chatroom = new Chatroom();
        $chatroom->users_id = Auth::id();
        $chatroom->save();
      }
      $chatmessage = new ChatMessage();
      $chatmessage->chat = $request->input('msg');
      $chatmessage->chat_room_id = $chatroom->id;
      $chatmessage->users_id = Auth::id();
      $chatmessage->save();
      return $this->getMessage($request);
    }
}
