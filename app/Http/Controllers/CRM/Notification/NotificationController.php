<?php
namespace App\Http\Controllers\CRM\Notification;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Validation\Rule;
use Validator;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use Mail;
use App\Mail\UserForgotPasswordMail;
use Illuminate\Support\Facades\Crypt;
use App\Models\ChatMessage;
use App\Models\ChatRoom;
use App\Models\ChatRead;

class NotificationController extends Controller
{
    public function getNotification(Request $request) {
      if (Auth::user()->role<>1)
      {
        $message_unread = ChatMessage::get();
        $is_reads = [];
        if (!$message_unread->isEmpty()) {
          foreach ($message_unread as $key => $value) {
            if (!$value->is_read)
            {
              $is_reads[] = [
                'chat_message_id' => $value->id,
                'users_id' => Auth::id(),
                'created_at'=> Carbon::now(),
              ];
            }
          }
        }
      } else {
          $message_unread = ChatRoom::where('users_id',Auth::id())->first()->chat_messages;
          // return $message_unread;
          $is_reads = [];
          if (!$message_unread->isEmpty()) {
            foreach ($message_unread as $key => $value) {
              if (!$value->is_read)
              {
                $is_reads[] = [
                  'chat_message_id' => $value->id,
                  'users_id' => Auth::id(),
                  'created_at'=> Carbon::now(),
                ];
              }
            }
          }
      }
      return $chat = [
        'm_count' => count($is_reads),
      ];
    }
}
