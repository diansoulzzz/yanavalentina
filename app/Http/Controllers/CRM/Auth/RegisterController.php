<?php
namespace App\Http\Controllers\CRM\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Models\User;
use App\Models\UsersAddress;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;
use App\Mail\UserVerificationMail;
use Mail;
use Illuminate\Support\Facades\Crypt;

class RegisterController extends Controller
{
    public function index(Request $request)
    {
        return view('crm.auth.register');
    }
    public function post(Request $request)
    {
      // return $request->all();
      $messages = [
          'name.required' => 'Nama wajib di isi',
          'birthday.required' => 'Tanggal Lahir wajib di isi',
          'gender.required' => 'Jenis Kelamin wajib di isi',
          'alamat.*.keterangan.required' => 'Alamat wajib di isi',
          // 'alamat.*.utama.required' => 'Alamat utama wajib di centang minimal 1',
          'email.required' => 'Email wajib di isi',
          'email.email' => 'Email tidak valid',
          'email.unique' => 'Email sudah terdaftar sebelumnya',
          'no_telp.required' => 'Nomor Telepon wajib di isi',
          'no_telp.numeric' => 'Nomor Telepon tidak valid',
          'password.required' => 'Password wajib di isi',
          'password.confirmed' => 'Kombinasi password dan password konfirmasi tidak benar',
          'password.min' => 'Password minimal 6 digit',
      ];
      $this->validate($request, [
          'name' => 'required',
          'birthday' => 'required',
          'no_telp' => 'required|numeric',
          'gender' => 'required',Rule::in(['pria', 'wanita']),
          'alamat.*.keterangan' => 'required',
          // 'alamat.*.utama' => 'required',
          'email' => 'required|email|unique:users,email',
          'password' => 'required|min:6|confirmed'
      ],$messages);

      $users = User::create([
          'name' => ucwords($request->input('name')),
          'email' => $request->input('email'),
          'birthday' => Carbon::parse($request->input('birthday')),
          'password' => Hash::make($request->input('password')),
          'api_token' => str_random(60),
          'gender' => $request->input('gender'),
          'phone_number' => $request->input('no_telp'),
      ]);
      $code = 'C'.sprintf('%07s', $users->id);
      $users->update(['code' => $code]);
      $address = [];
      foreach ($request->input('alamat') as $key => $alamat) {
          $address[]= new UsersAddress([
            'address' => $alamat['keterangan'],
            'primary' => (isset($alamat['utama']))
         ]);
      }
      $users->users_addresses()->saveMany($address);
      $users->redirectUrl = 'l';
      $this->sendMail($users);
      return redirect('login')->with('success', 'Registrasi User : <strong>'.$users->email.'</strong> telah berhasil </br>Kode Anda : <strong>'.$users->code.'</strong>');
      // $data = json_decode($request->input('something'), true);
      // return response()->json($data);
      // return $request->all();
      // return $request->json($data);
      // $data = $request->json()->all(); //read json in request
      // return response()->json($data);
      // return $request->json()->all();
      // return response()->json($request->getContent());
      // // return $request->post('alamat');
      // return $request->all();
    }
    public function sendMail($users)
    {
      Mail::to($users->email)->send(new UserVerificationMail($users));
    }
    public function verify(Request $request,$redirect,$eid)
    {
      $did = Crypt::decryptString($eid);
      $users = User::whereNull('verified')->find($did);
      if ($users) {
        $users->verified = Carbon::now();
        $users->save();
        if ($redirect=='p') {
            return redirect('member/profile')->with('success', 'Verifikasi email berhasil dilakukan');
        }
        else {
            return redirect('login')->with('success', 'Verifikasi email berhasil dilakukan silahkan lanjut login');
        }
      }
      return redirect('login')->with('info', 'Verifikasi sudah dilakukan sebelumnya lanjut login');
    }

}
