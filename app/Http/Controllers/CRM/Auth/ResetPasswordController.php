<?php
namespace App\Http\Controllers\CRM\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Validation\Rule;
use Validator;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use Illuminate\Foundation\Auth\ResetsPasswords;
use DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Contracts\Hashing\Hasher as HasherContract;
use Illuminate\Auth\Events\PasswordReset;
class ResetPasswordController extends Controller
{
    public function index(Request $request, $eemail, $token)
    {
        $email = Crypt::decryptString($eemail);
        $reset = DB::table("password_resets")->where('email', $email)->first();
        if ($reset){
            return view('crm.auth.reset')->with('token', $token);
        }
        return redirect('login')->withInfo('Password sudah pernah direset sebelumnya');
        // if ($token != null){
            // return $dtoken;
            // $htoken = Hash::make($dtoken);
            // $email = Crypt::decryptString($eemail);
            // $reset = DB::table("password_resets")->where('email', $email)->first();
            // if(Hash::check($htoken, $reset->token)) {
            //   return 'asd';
            // }
            // return 'bcd';
            // if(Hash::check($token, $reset->token)){
          	// $error = false;
            //   }else{
          	// $error = true;
            //   }
            // if(!Hash::check($value,$parameters[0]))
            // {
            //   return false;
            // }
            // $user = User::where('email', '=', $request->email)->first();
        //     return view('crm.auth.reset')->with('token', $token);
        // }
        // if(DB::table('password_resets')->where('token', $htoken)->exists()) {
        //   return view('crm.auth.reset')->with('token', $htoken);
        // }
    }
    public function post(Request $request, $eemail, $token = null)
    {
        $messages = [
            'token.required' => 'Invalid',
            'password.required' => 'Password wajib di isi',
            'password.confirmed' => 'Kombinasi password dan password konfirmasi tidak benar',
            'password.min' => 'Password minimal 6 digit',
        ];
        $this->validate($request, [
            'token' => 'required',
            'password' => 'required|min:6|confirmed'
        ],$messages);
        $email = Crypt::decryptString($eemail);
        $users = User::where('email',$email)->first();
        $users->password = Hash::make($request->input('password'));
        $users->save();
        app('auth.password.broker')->deleteToken($users);
        return redirect('login')->withSuccess('Password anda berhasil direset silahkan lanjut login');
    }
}
