<?php
namespace App\Http\Controllers\CRM\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Validation\Rule;
use Validator;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function index(Request $request)
    {
        return view('crm.auth.login');
    }
    public function indexLogout(Request $request)
    {
        Auth::logout();
        return redirect('login');
    }
    public function post(Request $request)
    {
        $messages = [
            'email.required' => 'Email wajib di isi',
            'email.email' => 'Email tidak valid',
            'password.required' => 'Password wajib di isi'
        ];
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required|min:6'
        ],$messages);

        $email = $request->input('email');
        $password = $request->input('password');
        $remember = $request->input('remember');
        if (Auth::attempt(['email' => $email, 'password' => $password],$remember)) {
          return redirect('member/profile');
        }
        return redirect(route('login'))->withInput()->withErrors(['status' => 'Username atau Password salah mohon dicek kembali!']);
    }
}
