<?php
namespace App\Http\Controllers\CRM\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Validation\Rule;
use Validator;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use Mail;
use App\Mail\UserForgotPasswordMail;
use Illuminate\Support\Facades\Crypt;

class ForgotPasswordController extends Controller
{
    public function index(Request $request)
    {
        return view('crm.auth.forgot');
    }
    public function post(Request $request)
    {
        $messages = [
            'email.required' => 'Email wajib di isi',
            'email.email' => 'Email tidak valid',
            'email.exists' => 'Email tidak ditemukan atau belum terdaftar sebelumnya',
        ];
        $this->validate($request, [
            'email' => 'required|email|exists:users,email',
        ],$messages);
        $email = $request->input('email');
        $users = User::where('email',$email)->first();
        $users->reset_token = app('auth.password.broker')->createToken($users);
        // return $users;
        $this->sendMail($users);
        return back()->withSuccess('Kami sudah mengirimkan email untuk melakukan reset password anda');
    }
    public function sendMail($users)
    {
      Mail::to($users->email)->send(new UserForgotPasswordMail($users));
    }
}
