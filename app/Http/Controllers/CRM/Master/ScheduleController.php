<?php
namespace App\Http\Controllers\CRM\Master;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Models\User;
use DataTables;
use Illuminate\Support\Facades\DB;
use Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;
use App\Mail\UserVerificationMail;
use Mail;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Storage;
use Validator;
use App\Models\Schedule;
use App\Models\ScheduleImage;
use PDF;
use View;
use Illuminate\Support\Str;

class ScheduleController extends Controller
{
    public function indexEntry(Request $request){
        return view('crm.menus.master.schedule.entry');
    }
    public function indexList(Request $request){
        return view('crm.menus.master.schedule.list');
    }
    public function indexEdit(Request $request, $eid){
      $did = Crypt::decryptString($eid);
      $schedule = Schedule::with('schedule_images')->where('id',$did)->first();
      $schedule->eid = Crypt::encryptString($schedule->id);
      return view('crm.menus.master.schedule.entry',compact('schedule'));
    }
    public function postEdit(Request $request, $eid){
      if($request->ajax()){
        $data = json_decode($request->input('form'),true);
        $request->merge($data);
      }
      return $this->storeEdit($request, $eid);
    }
    public function postEntry(Request $request){
      if($request->ajax()){
        $data = json_decode($request->input('form'),true);
        $request->merge($data);
      }
      return $this->storeNew($request);
    }
    public function storeEdit(Request $request, $eid) {
      $did = Crypt::decryptString($eid);
      $name = $request->input('name');
      $description = $request->input('description');
      $status = ($request->has('status')?'open':'close');
      $publish = $request->has('publish');
      $start_date = Carbon::createFromFormat('d/m/Y', $request->input('start_date'));
      $end_date = Carbon::createFromFormat('d/m/Y',$request->input('end_date'));
      $photo_primary = $request->input('photo_primary');
      $schedule = Schedule::find($did);
      $schedule->update([
        'name' => $name,
        'description' => $description,
        'start_date' => $start_date,
        'end_date' => $end_date,
        'end_date' => $end_date,
        'end_date' => $end_date,
        'status' => $status,
    		'publish' => $publish,
        'updated_by' => Auth::id(),
      ]);
      // return $schedule;
      // return $request->all();
      if ($request->has('removed')) {
        foreach ($request->input('removed') as $key => $removed) {
          ScheduleImage::where('photo_url','=',$removed)->delete();
          Storage::disk('public')->delete($removed);
        }
      };
      $schedule_image=[];
      if ($request->hasFile('photo_url')) {
        foreach ($request->file('photo_url') as $key => $url_photo) {
          $schedule_image[] = new ScheduleImage([
            'photo_url' => Storage::disk('public')->put('schedule/'.$schedule->id, $url_photo),
            'primary' => ($url_photo->getClientOriginalName() == $photo_primary),
            'created_by' => Auth::id(),
            'updated_by' => Auth::id(),
          ]);
        }
      }
      $schedule->schedule_images()
      ->update([
        'primary' => false,
        'updated_by' => Auth::id(),
      ]);

      $schedule->schedule_images()
      ->where('photo_url',$photo_primary)
      ->update([
        'primary' => true,
        'updated_by' => Auth::id(),
      ]);

      $schedule->schedule_images()->saveMany($schedule_image);
      $status = [
        'alert'=>'alert-success',
        'message'=>'Data berhasil diubah',
      ];
      session()->flash('status',$status);
      if($request->ajax()){
        return url()->current();
      } else {
        return back();
      }
    }
    public function storeNew(Request $request) {
      $name = $request->input('name');
      $description = $request->input('description');
      $status = ($request->has('status')?'open':'close');
      $publish = $request->has('publish');
      $start_date = Carbon::createFromFormat('d/m/Y', $request->input('start_date'));
      $end_date = Carbon::createFromFormat('d/m/Y',$request->input('end_date'));
      $photo_primary = $request->input('photo_primary');

      $slug = Str::slug($name);
      $slugCount = count(Schedule::whereRaw("link_url REGEXP '^{$slug}(-[0-9]*)?$'")->get());
      $link_url = ($slugCount > 0) ? "{$slug}-{$slugCount}" : $slug;

      $schedule = Schedule::create([
        'name' => $name,
        'description' => $description,
        'start_date' => $start_date,
        'end_date' => $end_date,
        'end_date' => $end_date,
        'end_date' => $end_date,
        'status' => $status,
    		'publish' => $publish,
        'created_by' => Auth::id(),
        'updated_by' => Auth::id(),
        'link_url' => $link_url,
      ]);
      $schedule_image=[];
      if ($request->hasFile('photo_url')) {
        foreach ($request->file('photo_url') as $key => $url_photo) {
          $schedule_image[] = new ScheduleImage([
            'photo_url' => Storage::disk('public')->put('schedule/'.$schedule->id, $url_photo),
            'primary' => ($url_photo->getClientOriginalName() == $photo_primary),
            'created_by' => Auth::id(),
            'updated_by' => Auth::id(),
          ]);
        }
      }
      $schedule->schedule_images()->saveMany($schedule_image);
      $status = [
        'alert'=>'alert-success',
        'message'=>'Data berhasil ditambah',
      ];
      session()->flash('status',$status);
      return url()->current();
    }

    public function delete(Request $request, $eid)
    {
      $did = Crypt::decryptString($eid);
      $schedule = Schedule::where('id',$did)->first();
      $schedule->delete();
      return back()->with('success', 'Hapus jadwal berhasil.');
    }
    public function dataTable(Request $request)
    {
      $field = ['schedule.id','schedule.name','schedule.start_date','schedule.end_date'];
      $model = DB::table('schedule')
      ->whereNull('schedule.deleted_at')
      ->select($field);
      $datatable = Datatables::of($model)
        ->addColumn('action', function ($item) {
          return '<td class="m-datatable__cell">
            <span style="overflow: visible; position: relative; width: 150px;">
              <div class="dropdown">
                <button class="btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="dropdown" aria-expanded="false">
                  <i class="la la-ellipsis-h"></i>
                </button>
                <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-186px, 33px, 0px);">
                  <a class="dropdown-item schedule-entry" href="'.url('master/schedule/entry/'.Crypt::encryptString($item->id)).'" data-schedule-eid="'.Crypt::encryptString($item->id).'"><i class="la la-edit"></i> Ubah</a>
                  <a class="dropdown-item schedule-delete" href="'.url('master/schedule/delete/'.Crypt::encryptString($item->id)).'" data-schedule-eid="'.Crypt::encryptString($item->id).'"><i class="la la-trash"></i> Hapus</a>
                  <a class="dropdown-item schedule-cash" href="'.url('cash/entry?schedule='.Crypt::encryptString($item->id)).'" data-schedule-eid="'.Crypt::encryptString($item->id).'"><i class="fa fa-dollar-sign"></i> Input Kas</a>
                </div>
              </div>
            </span>
          </td>';
        })
        ->editColumn('start_date', function ($model) {
            return $model->start_date ? with(new Carbon($model->start_date))->toDateTimeString() : '';
        })
        ->editColumn('end_date', function ($model) {
            return $model->end_date ? with(new Carbon($model->end_date))->toDateTimeString() : '';
        })
        ->escapeColumns([])
        ->make();
      return $datatable;
    }

}
