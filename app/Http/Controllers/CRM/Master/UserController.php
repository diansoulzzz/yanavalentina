<?php
namespace App\Http\Controllers\CRM\Master;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Models\User;
use DataTables;
use Illuminate\Support\Facades\DB;
use Auth;
use App\Models\UsersAddress;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;
use App\Mail\UserVerificationMail;
use Mail;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Storage;
use Validator;
use App\Models\City;
use App\Models\Province;
use App\Models\SubDistrict;
use PDF;
use View;
class UserController extends Controller
{
    public function printPdf(Request $requst) {
      $users = User::with('users_addresses_primary')->where('role','=',1)->get();
      // $txt = 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.';
      // return $users;
      $pdf = new PDF();
      $pdf::SetTitle('User Print');
      $pdf::AddPage();
      // $pdf::setCellPaddings(1, 1, 1, 1);
      $pdf::setCellMargins(1, 0, 1, 0);
      $pdf::SetFont('helvetica', '', 8);
      $col = 2;
      $count = 10;
      $col_width = 95;
      $col_height = 53;
      foreach ($users as $key => $user) {
        $break = 0;
        $view = View::make('crm.menus.master.users.pdf', compact('user'));
        $html = $view->render();
        $break = ((($key+1)%$col)==0 ? 1 : 0);
        $pdf::writeHTMLCell($col_width, $col_height, '', '', $html, 1, $break, false, true, '', true);
        if ((($key+1)%$count)==0) {
          $pdf::AddPage();
        }
      }
      $pdf::Output('user-print.pdf');
    }

    public function indexList(Request $request)
    {
        $model = User::with('users_addresses_primary')->select('users.*');
        return view('crm.menus.master.users.list');
    }
    public function delete(Request $request, $eid)
    {
      $did = Crypt::decryptString($eid);
      $users = User::where('id',$did)->where('role','1')->where('id','<>',Auth::id())->first();
      if (!$users) {
        return back()->with('error', 'Anda tidak punya akses untuk menghapus user tersebut.');
      };
      $users->delete();
      return back()->with('success', 'Hapus user berhasil.');
    }
    public function dataTable(Request $request)
    {
      $field = ['users.id','users.code','users.name','users.email','users.gender','users.phone_number','users.birthday','users.created_at','users_address.address'];
      // $model = User::select($field);
      // $model = User::with('users_addresses_primary')->select('users.*');
      $model = DB::table('users')
      ->leftjoin('users_address', 'users_address.users_id', '=', 'users.id')
      ->where('users_address.primary','=','1')
      ->whereNull('users.deleted_at')
      ->select($field);
      $datatable = Datatables::of($model)
        // ->addColumn('action', function ($item) {
        //   return '<td class="uk-text-center">
        //       <a href="'.route('admin.input.kategori', ['i' => Crypt::encryptString($item->id)]).'"><i class="md-icon material-icons">&#xE254;</i></a>
        //       <a class="try-delete" href="'.route('admin.hapus.kategori', ['i' => Crypt::encryptString($item->id)]).'"><i class="md-icon material-icons">&#xE92B;</i></a>
        //   </td>';
        // })
        // ->editColumn('foto_url', function ($item) {
        //   return '<td class="uk-text-center"><img class="md-user-image" src="'.asset($item->foto_url).'" alt=""/></td>';
        // })
        // ->addColumn('address', function ($model) {
        //     return $model->users_addresses_primary->address;
        // })Carbon::parse($request->input('birthday'))
        ->addColumn('action', function ($item) {
          return '<td class="m-datatable__cell">
            <span style="overflow: visible; position: relative; width: 150px;">
              <div class="dropdown">
                <button class="btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="dropdown" aria-expanded="false">
                  <i class="la la-ellipsis-h"></i>
                </button>
                <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-186px, 33px, 0px);">
                  <a class="dropdown-item users-edit" href="'.url('master/users/edit/'.Crypt::encryptString($item->id)).'" data-users-eid="'.Crypt::encryptString($item->id).'"><i class="la la-edit"></i> Ubah</a>
                  <a class="dropdown-item users-delete" href="'.url('master/users/delete/'.Crypt::encryptString($item->id)).'" data-users-eid="'.Crypt::encryptString($item->id).'"><i class="la la-trash"></i> Hapus</a>
                </div>
              </div>
            </span>
          </td>';
        })
        ->editColumn('birthday', function ($model) {
            return $model->birthday ? with(new Carbon($model->birthday))->toDateString() : '';
        })
        ->editColumn('created_at', function ($model) {
            return $model->created_at ? with(new Carbon($model->created_at))->toDateTimeString() : '';
        })
        // ->editColumn('users.birthday', '{{Carbon::parse($birthday)->toDateString()}}')
        ->escapeColumns([])
        ->make();
      return $datatable;
    }

    public function index(Request $request, $eid = null)
    {
        $did = Crypt::decryptString($eid);
        $users = User::find($did);
        $users->eid = Crypt::encryptString($users->id);
        return view('crm.menus.master.users.edit',compact('users'));
    }
    public function indexAddress(Request $request, $eid = null)
    {
        $did = Crypt::decryptString($eid);
        $users = User::find($did);
        $users->eid = Crypt::encryptString($users->id);
        return view('crm.menus.master.users.edit',compact('users'));
    }
    public function indexAccount(Request $request, $eid = null)
    {
        $did = Crypt::decryptString($eid);
        $users = User::find($did);
        $users->eid = Crypt::encryptString($users->id);
        return view('crm.menus.master.users.edit',compact('users'));
    }
    public function post(Request $request)
    {
        $messages = [
            'name.required' => 'Nama wajib di isi',
            'birthday.required' => 'Tanggal Lahir wajib di isi',
            'gender.required' => 'Jenis Kelamin wajib di isi',
            'alamat.*.keterangan.required' => 'Alamat wajib di isi',
            'password.required' => 'Password wajib di isi',
            'password.confirmed' => 'Kombinasi password dan password konfirmasi tidak benar',
            'password.min' => 'Password minimal 6 digit',
        ];
        $this->validate($request, [
            'name' => 'required',
            'birthday' => 'required',
            'gender' => 'required',Rule::in(['pria', 'wanita']),
        ],$messages);
        // return $request->all();
        // return Carbon::createFromFormat('d/m/Y', $request->input('birthday'));
        $did = Crypt::decryptString($request->input('eid'));
        $users = User::find($did)->update([
            'name' => ucwords($request->input('name')),
            'birthday' => Carbon::createFromFormat('d/m/Y', $request->input('birthday')),
            'gender' => $request->input('gender'),
        ]);
        return back()->with('success', 'Biodata telah di ubah');
    }
    public function postPhoto(Request $request)
    {
        $messages = [
            'photo_image.required' => 'Foto wajib diisi',
            'photo_image.image' => 'File harus image',
            'photo_image.dimensions' => 'Panjang dan Tinggi Foto minimal 80 pixel',
        ];
        $validator = Validator::make($request->all(), [
            'photo_image' => 'required|image|dimensions:min_width=80,min_height=80',
        ],$messages);
        if ($validator->fails()) {
            return redirect()->back()->withErrorPhoto($validator->errors())->withInput();
        }
        if ($request->hasFile('photo_image'))
        {
          $did = Crypt::decryptString($request->input('eid'));
          if ($request->file('photo_image')->isValid())
          {
              $photo_image = $request->file('photo_image');
              $users = User::find($did);
              Storage::delete($users->photo_url);
              $users->photo_url = Storage::disk('public')->put('member/avatars/'.$users->id, $photo_image);
              $users->save();
              return back()->with('success_photo', 'Foto berasil diubah');
          }
        }
        // return back()->with('error_photo', 'Foto tidak valid');
    }
    public function postAddress(Request $request)
    {
        $did = Crypt::decryptString($request->input('eid'));
        $users = User::find($did);
        $address_person = $request->input('person');
        $address_no_tel = $request->input('no_tel');
        $address_address = $request->input('address');
        $address_primary = $request->has('primary');
        $address_subdistrict = $request->input('subdistrict');
        if ($address_primary){
          $users->users_addresses()->update(['primary'=>'0']);
        }

        if ($request->has('eua') && $request->input('eua')!='')
        {
          $dua = Crypt::decryptString($request->input('eua'));
          $address = UsersAddress::find($dua);
          $address->person = $address_person;
          $address->telephone = $address_no_tel;
          $address->address = $address_address;
          $address->primary = $address_primary;
          $address->subdistrict_id = $address_subdistrict;
          $address->save();
          return back()->with('success', 'Alamat berhasil diubah');
        }
        $address = new UsersAddress([
          'person' => $address_person,
          'telephone' => $address_no_tel,
          'address' => $address_address,
          'primary' => $address_primary,
          'subdistrict_id' => $address_subdistrict
        ]);
        $users->users_addresses()->save($address);
        return back()->with('success', 'Alamat berhasil ditambahkan');
    }
    public function postAccount(Request $request)
    {
        $messages = [
            'old_password.required' => 'Kata Sandi Lama wajib di isi',
            'password.required' => 'Kata Sandi Baru wajib di isi',
            'password.confirmed' => 'Kombinasi Kata Sandi dan Konfirmasi Kata Sandi tidak benar',
            'old_password.password_hash'    => 'Kombinasi Kata Sandi Lama tidak benar',
            'password.min' => 'Panjang Kata Sandi minimal 6 digit',
        ];
        $did = Crypt::decryptString($request->input('eid'));
        $users = User::find($did)->makeVisible(['password']);
        $hash_old = $users->password;
        $this->validate($request, [
            'password' => 'required|min:6',
            'old_password' => 'required|password_hash:' . $hash_old,
            'password' => 'required|min:6|confirmed',
        ],$messages);

        $users->update([
          'password' => Hash::make($request->input('password')),
        ]);
        return back()->with('success', 'Password telah di ubah');
    }
    public function dataTableAddressDetail(Request $request)
    {
      $did = Crypt::decryptString($request->input('eid'));
      $dua = Crypt::decryptString($request->input('eua'));
      $users_address = UsersAddress::where('id',$dua)->where('users_id',$did)->first();
      $users_address->eid = Crypt::encryptString($users_address->id);
      $users_address->makeHidden(['id','users_id','subdistrict']);
      return $users_address;
      // return $request->all();
    }
    public function dataTableAddress(Request $request)
    {
      // return response()->json(['a'=>$request->input('eid')]);
      $did = Crypt::decryptString($request->input('eid'));
      $field = [
        DB::raw("concat('<td class=\'m-datatable__cell--center m-datatable__cell m-datatable__cell--check\'><span style=\'width: 50px;\'><label class=\'m-checkbox m-checkbox--single m-checkbox--solid m-checkbox--brand\'><input class=\'set-utama\' value=',users_address.id,if((users_address.primary), ' checked ', ' set-utama '),' type=\'checkbox\' disabled=\'disabled\'><span></span></label></span></td>') as `primary`"),
        'users_address.id','users_address.person','users_address.telephone','users_address.address','users_address.users_id','subdistrict.name as subdistrict','city.name as city','province.name as province',
        DB::raw("concat('<td class=\'m-datatable__cell\'>',province.name,',<br />Kota ',city.name,',<br />',subdistrict.name,'<br />Indonesia</td>') as area")
      ];
      $model = DB::table('users_address')
      ->leftjoin('subdistrict', 'users_address.subdistrict_id', '=', 'subdistrict.id')
      ->leftjoin('city', 'subdistrict.city_id', '=', 'city.id')
      ->leftjoin('province', 'city.province_id', '=', 'province.id')
      ->where('users_address.users_id','=',$did)
      ->whereNull('users_address.deleted_at')
      ->select($field);
      $datatable = Datatables::of($model)
      ->addColumn('action', function ($item) {
        return '<td class="m-datatable__cell">
          <span style="overflow: visible; position: relative; width: 150px;">
            <div class="dropdown">
              <button class="btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="dropdown" aria-expanded="false">
                <i class="la la-ellipsis-h"></i>
              </button>
              <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-186px, 33px, 0px);">
                <button class="dropdown-item address-edit" href="javascript:void(0);" data-address-eua="'.Crypt::encryptString($item->id).'"><i class="la la-edit"></i> Ubah</button>
                <a class="dropdown-item address-delete" href="'.url('master/users/edit/address/delete/'.Crypt::encryptString($item->users_id).'/'.Crypt::encryptString($item->id)).'" data-address-eua="'.Crypt::encryptString($item->id).'"><i class="la la-trash"></i> Hapus</a>
              </div>
            </div>
          </span>
        </td>';
      })
      // ->addColumn('area', function ($item) {
      //   return '<td class="m-datatable__cell">'.$item->province.',<br />Kota '.$item->city.',<br />'.$item->subdistrict.'<br />Indonesia'.'</td>';
      // })
      ->filterColumn('area', function($query, $keyword) {
        $query->whereRaw("concat('<td class=\'m-datatable__cell\'>',province.name,',<br />Kota ',city.name,',<br />',subdistrict.name,'<br />Indonesia</td>') like ?", ["%{$keyword}%"]);
      })
      ->escapeColumns([])
      ->make();
      return $datatable;
    }
    public function deleteAddress(Request $request, $eid, $eua)
    {
      $did = Crypt::decryptString($eid);
      $dua = Crypt::decryptString($eua);
      $users_address = UsersAddress::where('id',$dua)->where('users_id',$did)->first();
      if ($users_address->primary)
      {
        return back()->with('error', 'Tidak dapat menghapus alamat utama.');
      }
      $users_address->delete();
      return back()->with('success', 'Alamat telah di hapus.');
    }
    public function countryAddress(Request $request)
    {
      $q = $request->input('q');
      $result = SubDistrict::with('city.province')->whereHas('city', function ($query) use ($q) {
         $query->where('name', 'like', '%'.$q.'%');
         $query->orWhereHas('province', function ($query) use ($q) {
            $query->where('name', 'like', '%'.$q.'%');
         });
      })
      ->orWhere('name', 'like', '%'.$q.'%')
      ->take(10)
      ->orderBy('name','asc')
      ->get()
      ->makeHidden(['city_id','city']);
      $data = [
        'total_count'=> $result->count(),
        'incomplete_results'=>'false',
        'items' => $result,
      ];
      return $data;
    }
    public function verify(Request $request,$eid)
    {
      $did = Crypt::decryptString($eid);
      $users = User::whereNull('verified')->find($did);
      if ($users) {
          $users->redirectUrl = 'p';
          $this->sendMail($users);
          return back()->with('success', 'Mohon cek email anda untuk melakukan verifikasi.');
      }
      return back()->with('success', 'Email anda sudah diverifikasi sebelumnya.');
    }
    public function sendMail($user)
    {
      Mail::to($user->email)->send(new UserVerificationMail($user));
    }
}
