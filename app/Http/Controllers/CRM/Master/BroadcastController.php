<?php
namespace App\Http\Controllers\CRM\Master;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Models\User;
use App\Models\Broadcast;
use App\Models\BroadcastReceiver;
use App\Mail\UserBroadcastMail;
use Auth;
use Mail;
use Illuminate\Support\Facades\Storage;

class BroadcastController extends Controller
{
    public function index(Request $request){
      return view('crm.menus.master.broadcast.entry');
    }
    public function getUsers(Request $request){
      // $users_field = User::select('id','name','email')->get();
      $q = $request->input('q');
      $users_field = User::select('id','name','email')->where('name', 'like', '%'.$q.'%')
      ->orWhere('email','like','%'.$q.'%')
      ->take(10)
      ->orderBy('id','asc')
      ->get();
      $users_verified = new User();
      $users_verified->id = '-1';
      $users_verified->name = 'Semua User (Terverifikasi)';
      $users_verified->email = 'Semua User (Terverifikasi)';

      $users_unverified = new User();
      $users_unverified->id = '-2';
      $users_unverified->name = 'Semua User (Belum Terverifikasi)';
      $users_unverified->email = 'Semua User (Belum Terverifikasi)';

      $users_admin = new User();
      $users_admin->id = '-3';
      $users_admin->name = 'Semua Admin';
      $users_admin->email = 'Semua Admin';

      $users_all = new User();
      $users_all->id = '-4';
      $users_all->name = 'Semua User';
      $users_all->email = 'Semua User';

      $users_merge = $users_field->prepend($users_all)->prepend($users_admin)->prepend($users_unverified)->prepend($users_verified);
      $data = [
        'total_count'=> $users_merge->count(),
        'incomplete_results'=>'false',
        'items' => $users_merge,
      ];
      return $data;
    }
    public function post(Request $request){
      // return $request->all();
      $publish = $request->has('publish');
      $broadcast = new Broadcast();
      $broadcast->message = $request->input('message');
      $broadcast->subject = $request->input('subject');
      $broadcast->users_id = Auth::id();
      $broadcast->publish = $publish;
      $broadcast->save();
      $users_select = $request->input('users_select');
      $users_receiver = collect()->make();
      foreach ($users_select as $key => $value) {
        if ($value=='-1') {
          $users_verified = User::select('id','name','email')->whereNotNull('verified')->get();
          foreach ($users_verified as $key => $value_detail) {
            if (!$users_receiver->contains('id', $value_detail->id)){
              $users_receiver->push($value_detail);
            }
          }
        } else if ($value=='-2') {
          $users_unverified = User::select('id','name','email')->whereNull('verified')->get();
          foreach ($users_unverified as $key => $value_detail) {
            if (!$users_receiver->contains('id', $value_detail->id)){
              $users_receiver->push($value_detail);
            }
          }
        } else if ($value=='-3') {
          $users_admin = User::select('id','name','email')->where('role','<>',1)->get();
          foreach ($users_admin as $key => $value_detail) {
            if (!$users_receiver->contains('id', $value_detail->id)){
              $users_receiver->push($value_detail);
            }
          }
        } else if ($value=='-4') {
          $users_all = User::select('id','name','email')->get();
          foreach ($users_all as $key => $value_detail) {
            if (!$users_receiver->contains('id', $value_detail->id)){
              $users_receiver->push($value_detail);
            }
          }
        } else {
          $users = User::find($value);
          if (!$users_receiver->contains('id', $users->id)){
            $users_receiver->push($users);
          }
        }
      }
      // $user = App\User::find(1);
      // id, broadcast_id, users_id, created_at, updated_at, deleted_at
      $broadcast_receiver=[];
      foreach ($users_receiver as $key => $value) {
        $broadcast_receiver[] = new BroadcastReceiver([
          'users_id' => $value->id,
        ]);
      }
      $old_broadcast_receiver = BroadcastReceiver::where('broadcast_id',$broadcast->id)->get()->pluck('id');
      if ($old_broadcast_receiver->count()>0){
        BroadcastReceiver::destroy($old_broadcast_receiver);
      }
      $broadcast->broadcast_receivers()->saveMany($broadcast_receiver);

      // $tes = [];
      // foreach ($broadcast->broadcast_receivers() as $key => $value) {
      //   $tes[]=$value;
      // }
      // return $broadcast->broadcast_receivers;
      if ($publish){
        $this->sendBroadCast($request,$broadcast);
      }
      $status = [
        'alert'=>'alert-success',
        'message'=>'Broadcast berhasil',
      ];
      return back()->with('status', $status);
    }
    public function sendBroadCast(Request $request, $broadcast)
    {
      foreach ($broadcast->broadcast_receivers as $key => $broadcast_receivers) {
        Mail::to($broadcast_receivers->user->email)->send(new UserBroadcastMail($broadcast_receivers->user,$broadcast_receivers->broadcast));
      }
    }
    public function uploadFile(Request $request){
      $path = Storage::disk('public')->put('broadcast-cache', $request->file('image'));
      return Storage::disk('public')->url($path);
    }
}
