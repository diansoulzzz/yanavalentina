<?php
namespace App\Http\Controllers\CRM\Master;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Models\User;
use DataTables;
use Illuminate\Support\Facades\DB;
use Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;
use App\Mail\UserVerificationMail;
use Mail;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Storage;
use Validator;
use App\Models\Schedule;
use App\Models\ScheduleImage;
use App\Models\Item;
use App\Models\ItemImage;
use App\Models\ItemCategory;
use PDF;
use View;
use Illuminate\Support\Str;

class ItemController extends Controller
{
    public function indexEntry(Request $request){
        return view('crm.menus.master.item.entry');
    }
    public function indexList(Request $request){
        return view('crm.menus.master.item.list');
    }
    public function indexEdit(Request $request, $eid){
      $did = Crypt::decryptString($eid);
      $item = Item::with('item_images')->where('id',$did)->first();
      $item->eid = Crypt::encryptString($item->id);
      return view('crm.menus.master.item.entry',compact('item'));
    }
    public function postEdit(Request $request, $eid){
      if($request->ajax()){
        $data = json_decode($request->input('form'),true);
        $request->merge($data);
      }
      return $this->storeEdit($request, $eid);
    }
    public function postEntry(Request $request){
      if($request->ajax()){
        $data = json_decode($request->input('form'),true);
        $request->merge($data);
      }
      return $this->storeNew($request);
    }
    public function itemSchedule(Request $request) {
      $q = $request->input('q');
      $result = Schedule::where('name', 'like', '%'.$q.'%')
      ->orWhere('description','like','%'.$q.'%')
      ->take(10)
      ->orderBy('name','asc')
      ->get();
      $data = [
        'total_count'=> $result->count(),
        'incomplete_results'=>'false',
        'items' => $result,
      ];
      return $data;
    }
    public function itemCategory(Request $request) {
      $q = $request->input('q');
      $result = ItemCategory::where('name', 'like', '%'.$q.'%')
      ->take(10)
      ->orderBy('name','asc')
      ->get();
      $data = [
        'total_count'=> $result->count(),
        'incomplete_results'=>'false',
        'items' => $result,
      ];
      return $data;
    }
    public function storeEdit(Request $request, $eid) {
      $did = Crypt::decryptString($eid);
      $name = $request->input('name');
      $description = $request->input('description');
      $stock = $request->input('stock');
      $price = $request->input('price');
      $price_buy = $request->input('price_buy');
      $weight = $request->input('weight');
      $long = $request->input('long');
      $width = $request->input('width');
      $height = $request->input('height');
      $publish = $request->has('publish');
      $schedule_id = $request->input('schedule_id');
      $item_category_id = $request->input('item_category_id');
      $photo_primary = $request->input('photo_primary');
      $item = Item::find($did);
      $item->update([
    		'name' => $name,
    		'description' => $description,
    		'stock' => $stock,
    		'price' => $price,
    		'price_buy' => $price_buy,
    		'weight' => $weight,
    		'long' => $long,
    		'width' => $width,
    		'height' => $height,
    		'publish' => $publish,
    		'schedule_id' => $schedule_id,
        'updated_by' => Auth::id(),
      ]);
      if ($item_category_id){
        $item->item_category_id = $item_category_id;
        $item->save();
      }

      // return $item;
      if ($request->has('removed')) {
        foreach ($request->input('removed') as $key => $removed) {
          ItemImage::where('photo_url','=',$removed)->delete();
          Storage::disk('public')->delete($removed);
        }
      };
      $item_image=[];
      if ($request->hasFile('photo_url')) {
        foreach ($request->file('photo_url') as $key => $url_photo) {
          $item_image[] = new ItemImage([
            'photo_url' => Storage::disk('public')->put('item/'.$item->id, $url_photo),
            'primary' => ($url_photo->getClientOriginalName() == $photo_primary),
            'created_by' => Auth::id(),
            'updated_by' => Auth::id(),
          ]);
        }
      }
      $item->item_images()
      ->update([
        'primary' => false,
        'updated_by' => Auth::id(),
      ]);

      $item->item_images()
      ->where('photo_url',$photo_primary)
      ->update([
        'primary' => true,
        'updated_by' => Auth::id(),
      ]);
      $item->item_images()->saveMany($item_image);
      $status = [
        'alert'=>'alert-success',
        'message'=>'Data berhasil diubah',
      ];
      session()->flash('status',$status);
      if($request->ajax()){
        return url()->current();
      } else {
        return back();
      }
    }
    public function storeNew(Request $request) {
      // return $request->all();
      $name = $request->input('name');
      $description = $request->input('description');
      $stock = $request->input('stock');
      $price = $request->input('price');
      $price_buy = $request->input('price_buy');
      $weight = $request->input('weight');
      $long = $request->input('long');
      $width = $request->input('width');
      $height = $request->input('height');
      $publish = $request->has('publish');
      $schedule_id = $request->input('schedule_id');
      $item_category_id = $request->input('item_category_id');
      $photo_primary = $request->input('photo_primary');

      $slug = Str::slug($name);
      $slugCount = count(Item::whereRaw("link_url REGEXP '^{$slug}(-[0-9]*)?$'")->get());
      $link_url = ($slugCount > 0) ? "{$slug}-{$slugCount}" : $slug;

      $item = Item::create([
    		'name' => $name,
    		'description' => $description,
    		'stock' => $stock,
    		'price' => $price,
    		'price_buy' => $price_buy,
    		'weight' => $weight,
    		'long' => $long,
    		'width' => $width,
    		'height' => $height,
    		'publish' => $publish,
    		'schedule_id' => $schedule_id,
        'link_url' => $link_url,
        'created_by' => Auth::id(),
        'updated_by' => Auth::id(),
      ]);
      if ($item_category_id){
        $item->item_category_id = $item_category_id;
        $item->save();
      }
      $item_image=[];
      if ($request->hasFile('photo_url')) {
        foreach ($request->file('photo_url') as $key => $url_photo) {
          $item_image[] = new ItemImage([
            'photo_url' => Storage::disk('public')->put('item/'.$item->id, $url_photo),
            'primary' => ($url_photo->getClientOriginalName() == $photo_primary),
            'created_by' => Auth::id(),
            'updated_by' => Auth::id(),
          ]);
        }
      }
      $item->item_images()->saveMany($item_image);
      $status = [
        'alert'=>'alert-success',
        'message'=>'Data berhasil ditambah',
      ];
      session()->flash('status',$status);
      return url()->current();
    }

    public function delete(Request $request, $eid)
    {
      $did = Crypt::decryptString($eid);
      $item = Item::where('id',$did)->first();
      $item->delete();
      return back()->with('success', 'Hapus jadwal berhasil.');
    }
    public function dataTable(Request $request)
    {
      $field = ['item.id','item.name','item.description','item.publish'];
      $model = DB::table('item')
      ->whereNull('item.deleted_at')
      ->select($field);
      $datatable = Datatables::of($model)
        ->addColumn('action', function ($item) {
          return '<td class="m-datatable__cell">
            <span style="overflow: visible; position: relative; width: 150px;">
              <div class="dropdown">
                <button class="btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="dropdown" aria-expanded="false">
                  <i class="la la-ellipsis-h"></i>
                </button>
                <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-186px, 33px, 0px);">
                  <a class="dropdown-item item-entry" href="'.url('master/item/entry/'.Crypt::encryptString($item->id)).'" data-item-eid="'.Crypt::encryptString($item->id).'"><i class="la la-edit"></i> Ubah</a>
                  <a class="dropdown-item item-delete" href="'.url('master/item/delete/'.Crypt::encryptString($item->id)).'" data-item-eid="'.Crypt::encryptString($item->id).'"><i class="la la-trash"></i> Hapus</a>
                </div>
              </div>
            </span>
          </td>';
        })
        ->escapeColumns([])
        ->make();
      return $datatable;
    }

}
