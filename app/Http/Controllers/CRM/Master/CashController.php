<?php
namespace App\Http\Controllers\CRM\Master;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Models\User;
use App\Models\Broadcast;
use App\Models\Schedule;
use App\Models\BroadcastReceiver;
use App\Models\Cash;
use App\Mail\UserBroadcastMail;
use DataTables;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Storage;
use Auth;
use Illuminate\Support\Facades\Hash;
use Mail;
use Illuminate\Support\Str;
use Yajra\DataTables\Html\Builder;
use Yajra\DataTables\Services\DataTable;

class CashController extends Controller
{
    public function indexEntry(Request $request, Builder $builder){
      if ($request->has('schedule')) {
        $esh = $request->input('schedule');
        $did = Crypt::decryptString($esh);
        $schedule = Schedule::find($did);
        if (request()->ajax()) {
          $field = ['cash.id','cash.receipt_number','cash.date','cash.amount','cash.type','cash.schedule_id'];
          $model = DB::table('cash')
          ->whereNull('cash.deleted_at')
          ->where('cash.schedule_id',$did)
          ->select($field);
          return Datatables::of($model)
          ->addColumn('action', function ($item) {
            return '<td class="m-datatable__cell">
              <span style="overflow: visible; position: relative; width: 150px;">
                <div class="dropdown">
                  <button class="btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="dropdown" aria-expanded="false">
                    <i class="la la-ellipsis-h"></i>
                  </button>
                  <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-186px, 33px, 0px);">
                    <a class="dropdown-item cash-entry" href="'.url('cash/entry/'.Crypt::encryptString($item->id).'?schedule='.Crypt::encryptString($item->schedule_id)).'" data-cash-eid="'.Crypt::encryptString($item->id).'"><i class="la la-edit"></i> Ubah</a>
                    <a class="dropdown-item cash-delete" href="'.url('cash/delete/'.Crypt::encryptString($item->id)).'" data-cash-eid="'.Crypt::encryptString($item->id).'"><i class="la la-trash"></i> Hapus</a>
                  </div>
                </div>
              </span>
            </td>';
          })
          ->escapeColumns([])
          ->make();
        }
        $datatable = $builder->columns([
          ['data' => 'id', 'name' => 'id', 'title' => 'Id'],
          ['data' => 'receipt_number', 'name' => 'receipt_number', 'title' => 'Nomor'],
          ['data' => 'amount', 'name' => 'amount', 'title' => 'Nominal'],
          ['data' => 'type', 'name' => 'type', 'title' => 'Tipe'],
          ['data' => 'action', 'name' => 'action', 'title' => 'Action'],
        ]);
        return view('crm.menus.master.cash-schedule.entry', compact('datatable','schedule'));
      }
      return view('crm.menus.master.cash.entry');
    }
    public function indexEdit(Request $request, Builder $builder, $eid){
      $did = Crypt::decryptString($eid);
      $model = Cash::with('schedule')->where('id',$did)->first();
      $model->eid = Crypt::encryptString($model->id);
      if ($request->has('schedule')) {
        $did = Crypt::decryptString($request->input('schedule'));
        $schedule = Schedule::find($did);
        if (request()->ajax()) {
          $field = ['cash.id','cash.receipt_number','cash.date','cash.amount','cash.type','cash.schedule_id'];
          $model = DB::table('cash')
          ->whereNull('cash.deleted_at')
          ->where('cash.schedule_id',$did)
          ->select($field);
          return Datatables::of($model)
          ->addColumn('action', function ($item) {
            return '<td class="m-datatable__cell">
              <span style="overflow: visible; position: relative; width: 150px;">
                <div class="dropdown">
                  <button class="btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="dropdown" aria-expanded="false">
                    <i class="la la-ellipsis-h"></i>
                  </button>
                  <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-186px, 33px, 0px);">
                    <a class="dropdown-item cash-entry" href="'.url('cash/entry/'.Crypt::encryptString($item->id).'?schedule='.Crypt::encryptString($item->schedule_id)).'" data-cash-eid="'.Crypt::encryptString($item->id).'"><i class="la la-edit"></i> Ubah</a>
                    <a class="dropdown-item cash-delete" href="'.url('cash/delete/'.Crypt::encryptString($item->id)).'" data-cash-eid="'.Crypt::encryptString($item->id).'"><i class="la la-trash"></i> Hapus</a>
                  </div>
                </div>
              </span>
            </td>';
          })
          ->escapeColumns([])
          ->make();
        }
        $datatable = $builder->columns([
          ['data' => 'id', 'name' => 'id', 'title' => 'Id'],
          ['data' => 'receipt_number', 'name' => 'receipt_number', 'title' => 'Nomor'],
          ['data' => 'amount', 'name' => 'amount', 'title' => 'Nominal'],
          ['data' => 'type', 'name' => 'type', 'title' => 'Tipe'],
          ['data' => 'action', 'name' => 'action', 'title' => 'Action'],
        ]);
        return view('crm.menus.master.cash-schedule.entry', compact('model','datatable','schedule'));
      }
      return view('crm.menus.master.cash.entry',compact('model'));
    }
    public function postEdit(Request $request, $eid){
      if($request->ajax()){
        $data = json_decode($request->input('form'),true);
        $request->merge($data);
      }
      $did = Crypt::decryptString($eid);
      $schedule_id = $request->input('schedule_id');
      $date_time = Carbon::now();
      $date = Carbon::today();
      $type = $request->input('type');
      $amount = $request->input('amount');
      $note = $request->input('note');

      $cash = Cash::find($did);
      $cash->type = $type;
      $cash->date = $date;
      $cash->note = $note;
      $cash->amount = $amount;
      $cash->updated_by = Auth::id();
      $cash->schedule_id = $schedule_id;
      $cash->save();
      $esh = Crypt::encryptString($schedule_id);
      $status = [
        'alert'=>'alert-success',
        'message'=>'Nomor Kas '.$cash->receipt_number.' berhasil diedit',
      ];
      if ($request->has('saveandnew'))
      {
        return redirect('cash/entry?schedule='.$esh)->with('status', $status);
      } else {
        return redirect('master/schedule/list')->with('status', $status);
        // return back()->with('status', $status);
      }
    }

    public function indexList(Request $request){
      return view('crm.menus.master.cash.list');
    }
    public function dataTable(Request $request)
    {
      $field = ['cash.id','cash.receipt_number','cash.date','cash.amount','cash.type'];
      $model = DB::table('cash')
      ->whereNull('cash.deleted_at')
      ->select($field);
      $datatable = Datatables::of($model)
        ->addColumn('action', function ($item) {
          return '<td class="m-datatable__cell">
            <span style="overflow: visible; position: relative; width: 150px;">
              <div class="dropdown">
                <button class="btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="dropdown" aria-expanded="false">
                  <i class="la la-ellipsis-h"></i>
                </button>
                <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-186px, 33px, 0px);">
                  <a class="dropdown-item cash-entry" href="'.url('cash/entry/'.Crypt::encryptString($item->id)).'" data-cash-eid="'.Crypt::encryptString($item->id).'"><i class="la la-edit"></i> Ubah</a>
                  <a class="dropdown-item cash-delete" href="'.url('cash/delete/'.Crypt::encryptString($item->id)).'" data-cash-eid="'.Crypt::encryptString($item->id).'"><i class="la la-trash"></i> Hapus</a>
                </div>
              </div>
            </span>
          </td>';
        })
        ->escapeColumns([])
        ->make();
      return $datatable;
    }

    public function postEntry(Request $request){
      // return $request->all();
      $date_time = Carbon::now();
      $date = Carbon::today();
      $last_invoice = Cash::where('date','=',$date)->count()+1;
      $schedule_id = $request->input('schedule_id');
      $type = $request->input('type');
      $amount = $request->input('amount');
      $note = $request->input('note');
      $cash = new Cash();
      $cash->type = $type;
      $cash->date = $date;
      $cash->receipt_number = $this->generateCode('CH',$date,$last_invoice);
      $cash->note = $note;
      $cash->amount = $amount;
      $cash->created_by = Auth::id();
      $cash->updated_by = Auth::id();
      $cash->schedule_id = $schedule_id;
      $cash->save();
      $status = [
        'alert'=>'alert-success',
        'message'=>'Nomor Kas '.$cash->receipt_number.' berhasil diinput',
      ];
      if ($request->has('saveandnew'))
      {
        return redirect('cash/entry?schedule='.$esh)->with('status', $status);
      } else {
        return redirect('master/schedule/list')->with('status', $status);
      }
      // return back()->with('status', $status);
    }
}
