<?php
namespace App\Http\Controllers\CRM\Master;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Models\User;
use DataTables;
use Illuminate\Support\Facades\DB;
use Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;
use App\Mail\UserVerificationMail;
use Mail;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Storage;
use Validator;
use App\Models\Schedule;
use App\Models\ScheduleImage;
use App\Models\Item;
use App\Models\ItemImage;
use App\Models\ItemCategory;
use PDF;
use View;
use Illuminate\Support\Str;

class ItemCategoryController extends Controller
{
    public function indexEntry(Request $request){
        return view('crm.menus.master.item-category.entry');
    }
    public function indexList(Request $request){
        return view('crm.menus.master.item-category.list');
    }
    public function indexEdit(Request $request, $eid){
      $did = Crypt::decryptString($eid);
      $item = ItemCategory::where('id',$did)->first();
      $item->eid = Crypt::encryptString($item->id);
      return view('crm.menus.master.item-category.entry',compact('item'));
    }
    public function postEdit(Request $request, $eid){
      if($request->ajax()){
        $data = json_decode($request->input('form'),true);
        $request->merge($data);
      }
      return $this->storeEdit($request, $eid);
    }
    public function postEntry(Request $request){
      if($request->ajax()){
        $data = json_decode($request->input('form'),true);
        $request->merge($data);
      }
      return $this->storeNew($request);
    }
    public function itemSchedule(Request $request) {
      $q = $request->input('q');
      $result = Schedule::where('name', 'like', '%'.$q.'%')
      ->orWhere('description','like','%'.$q.'%')
      ->take(10)
      ->orderBy('name','asc')
      ->get();
      $data = [
        'total_count'=> $result->count(),
        'incomplete_results'=>'false',
        'items' => $result,
      ];
      return $data;
    }
    public function itemCategory(Request $request) {
      $q = $request->input('q');
      $result = ItemCategory::where('name', 'like', '%'.$q.'%')
      ->take(10)
      ->orderBy('name','asc')
      ->get();
      $data = [
        'total_count'=> $result->count(),
        'incomplete_results'=>'false',
        'items' => $result,
      ];
      return $data;
    }
    public function storeEdit(Request $request, $eid) {
      $did = Crypt::decryptString($eid);
      $name = $request->input('name');
      $publish = $request->has('publish');
      $item = ItemCategory::find($did);
      $item->update([
    		'name' => $name,
    		'publish' => $publish,
        'updated_by' => Auth::id(),
      ]);
      Storage::disk('public')->delete($item->photo_url);
      $item->photo_url = Storage::disk('public')->put('item-category/'.$item->id, $request->file('photo_url'));
      $item->save();
      $status = [
        'alert'=>'alert-success',
        'message'=>'Data berhasil diubah',
      ];
      session()->flash('status',$status);
      if($request->ajax()){
        return url()->current();
      } else {
        return back();
      }
    }
    public function storeNew(Request $request) {
      $name = $request->input('name');
      $publish = $request->has('publish');

      $slug = Str::slug($name);
      $slugCount = count(ItemCategory::whereRaw("link_url REGEXP '^{$slug}(-[0-9]*)?$'")->get());
      $link_url = ($slugCount > 0) ? "{$slug}-{$slugCount}" : $slug;

      $item = ItemCategory::create([
    		'name' => $name,
    		'publish' => $publish,
        'created_by' => Auth::id(),
        'updated_by' => Auth::id(),
        'link_url' => $link_url,
      ]);
      $item->photo_url = Storage::disk('public')->put('item-category/'.$item->id, $request->file('photo_url'));
      $item->save();
      $status = [
        'alert'=>'alert-success',
        'message'=>'Data berhasil ditambah',
      ];
      session()->flash('status',$status);
      if($request->ajax()){
        return url()->current();
      } else {
        return back();
      }
    }

    public function delete(Request $request, $eid)
    {
      $did = Crypt::decryptString($eid);
      $item = ItemCategory::where('id',$did)->first();
      $item->delete();
      return back()->with('success', 'Hapus jadwal berhasil.');
    }
    public function dataTable(Request $request)
    {
      $field = ['item_category.id','item_category.name','item_category.publish'];
      $model = DB::table('item_category')
      ->whereNull('item_category.deleted_at')
      ->select($field);
      $datatable = Datatables::of($model)
        ->addColumn('action', function ($item) {
          return '<td class="m-datatable__cell">
            <span style="overflow: visible; position: relative; width: 150px;">
              <div class="dropdown">
                <button class="btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="dropdown" aria-expanded="false">
                  <i class="la la-ellipsis-h"></i>
                </button>
                <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-186px, 33px, 0px);">
                  <a class="dropdown-item item-category-entry" href="'.url('master/item-category/entry/'.Crypt::encryptString($item->id)).'" data-item-category-eid="'.Crypt::encryptString($item->id).'"><i class="la la-edit"></i> Ubah</a>
                  <a class="dropdown-item item-category-delete" href="'.url('master/item-category/delete/'.Crypt::encryptString($item->id)).'" data-item-category-eid="'.Crypt::encryptString($item->id).'"><i class="la la-trash"></i> Hapus</a>
                </div>
              </div>
            </span>
          </td>';
        })
        ->escapeColumns([])
        ->make();
      return $datatable;
    }

}
