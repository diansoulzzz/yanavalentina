<?php
namespace App\Http\Controllers\CRM\CMS;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Models\User;
use DataTables;
use Illuminate\Support\Facades\DB;
use Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;
use App\Mail\UserVerificationMail;
use Mail;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Storage;
use Validator;
use App\Models\Schedule;
use App\Models\ScheduleImage;
use App\Models\Item;
use App\Models\ItemImage;
use App\Models\ItemCategory;
use App\Models\CmsPromotion;
use PDF;
use View;
use Illuminate\Support\Str;
use Yajra\DataTables\Html\Builder;
use Yajra\DataTables\Services\DataTable;

class PromotionController extends Controller
{
    public function indexEntry(Request $request){
      return view('crm.menus.cms.promotion.entry');
    }
    public function indexList(Request $request, Builder $builder){
      if (request()->ajax()) {
        $field = ['cms_promotion.id','cms_promotion.subject','cms_promotion.publish','cms_promotion.created_at','cms_promotion.updated_at'];
        $model = DB::table('cms_promotion')
        ->whereNull('cms_promotion.deleted_at')
        ->select($field);
        return Datatables::of($model)
        ->addColumn('action', function ($item) {
          return '<td class="m-datatable__cell">
            <span style="overflow: visible; position: relative; width: 150px;">
              <div class="dropdown">
                <button class="btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="dropdown" aria-expanded="false">
                  <i class="la la-ellipsis-h"></i>
                </button>
                <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-186px, 33px, 0px);">
                  <a class="dropdown-item cms-promotion-entry" href="'.url('cms/promotion/entry/'.Crypt::encryptString($item->id)).'" data-cms-promotion-eid="'.Crypt::encryptString($item->id).'"><i class="la la-edit"></i> Ubah</a>
                  <a class="dropdown-item cms-promotion-delete" href="'.url('cms/promotion/delete/'.Crypt::encryptString($item->id)).'" data-cms-promotion-eid="'.Crypt::encryptString($item->id).'"><i class="la la-trash"></i> Hapus</a>
                </div>
              </div>
            </span>
          </td>';
        })
        ->escapeColumns([])
        ->make();
      }
      $builder->parameters([
          'drawCallback' => "function() {
            $('#dataTableBuilder tbody').on('click', 'td.sorting_1', function () {
              thisform.e_init();
            });
            thisform.e_init();
          }",
      ]);
      $datatable = $builder->columns([
        ['data' => 'id', 'name' => 'id', 'title' => 'Id'],
        ['data' => 'subject', 'name' => 'subject', 'title' => 'Judul'],
        ['data' => 'publish', 'name' => 'publish', 'title' => 'Publish'],
        ['data' => 'created_at', 'name' => 'created_at', 'title' => 'Tgl Buat'],
        ['data' => 'updated_at', 'name' => 'updated_at', 'title' => 'Tgl Edit'],
        ['data' => 'action', 'name' => 'action', 'title' => 'Action'],
      ]);

      return view('crm.menus.cms.promotion.list', compact('datatable'));
    }
    public function indexEdit(Request $request, $eid){
      $did = Crypt::decryptString($eid);
      $item = CmsPromotion::where('id',$did)->first();
      $item->eid = Crypt::encryptString($item->id);
      return view('crm.menus.cms.promotion.entry',compact('item'));
    }
    public function postEdit(Request $request, $eid){
      if($request->ajax()){
        $data = json_decode($request->input('form'),true);
        $request->merge($data);
      }
      return $this->storeEdit($request, $eid);
    }
    public function postEntry(Request $request){
      if($request->ajax()){
        $data = json_decode($request->input('form'),true);
        $request->merge($data);
      }
      return $this->storeNew($request);
    }

    public function storeEdit(Request $request, $eid) {
      $did = Crypt::decryptString($eid);
      $subject = $request->input('subject');
      $publish = $request->has('publish');
      $item = CmsPromotion::find($did);
      $item->update([
    		'subject' => $subject,
    		'publish' => $publish,
        'updated_by' => Auth::id(),
      ]);
      Storage::disk('public')->delete($item->photo_url);
      $item->photo_url = Storage::disk('public')->put('cms/promotion/'.$item->id, $request->file('photo_url'));
      $item->save();
      $status = [
        'alert'=>'alert-success',
        'message'=>'Data berhasil diubah',
      ];
      session()->flash('status',$status);
      if($request->ajax()){
        return url()->current();
      } else {
        return back();
      }
    }
    public function storeNew(Request $request) {
      $subject = $request->input('subject');
      $publish = $request->has('publish');

      $slug = Str::slug($subject);
      $slugCount = count(CmsPromotion::whereRaw("link_url REGEXP '^{$slug}(-[0-9]*)?$'")->get());
      $link_url = ($slugCount > 0) ? "{$slug}-{$slugCount}" : $slug;

      $item = CmsPromotion::create([
    		'subject' => $subject,
    		'publish' => $publish,
        'created_by' => Auth::id(),
        'updated_by' => Auth::id(),
        'link_url' => $link_url,
      ]);

      $item->photo_url = Storage::disk('public')->put('cms/promotion/'.$item->id, $request->file('photo_url'));
      $item->save();
      $status = [
        'alert'=>'alert-success',
        'message'=>'Data berhasil ditambah',
      ];
      session()->flash('status',$status);
      if($request->ajax()){
        return url()->current();
      } else {
        return back();
      }
    }

    public function delete(Request $request, $eid)
    {
      return back()->with('success', 'Hapus promosi berhasil.');
      $did = Crypt::decryptString($eid);
      $item = CmsPromotion::where('id',$did)->first();
      $item->delete();
      return back()->with('success', 'Hapus promosi berhasil.');
    }

}
