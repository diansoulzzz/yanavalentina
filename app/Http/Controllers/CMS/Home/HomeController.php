<?php
namespace App\Http\Controllers\CMS\Home;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Models\Item;
use App\Models\CmsPromotion;

class HomeController extends Controller
{
    public function index(Request $request)
    {
      $promotions = CmsPromotion::where('publish',1)->get();
      $items = Item::whereHas('item_images', function ($query){
          $query->where('primary',1);
      })->with('item_images')->where('publish',1)->get();
      // return view('cms-4.index',compact('promotions','items'));
      return view('cms.menus.home.content',compact('promotions','items'));
    }
}
