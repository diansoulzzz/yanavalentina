<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Auth;
use App\Models\Cart;
use App\Models\User;
use Carbon\Carbon;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    private $user;
    private $carts;
    private $carts_exists;
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
          $exists = Cart::where('users_id',Auth::id())->where('checkout',0)->first();

          if (!$exists) {
            $this->carts_exists = false;
          } else {
            $this->carts_exists = true;
          }

          if ($this->carts_exists) {
            $users = User::find(Auth::id());
            $this->carts = Cart::with(['users_address','item.item_images_primary'])->where('users_id',Auth::id())->where('checkout',0)->get();
            $grandtotal =
            $summary = 0;
            $qty = 0;
            foreach ($this->carts as $key => $cart) {
              $summary = $summary + ($cart->qty*$cart->item->price);
              $qty = $qty + $cart->qty;
            }
            $this->carts->qty = $qty;
            $this->carts->summary = $summary;
            view()->share('carts', $this->carts);
          }

          view()->share('carts_exists', $this->carts_exists);
          return $next($request);
        });
    }
    public function generateCode($code,Carbon $date, $counter){
      $morph_counter = sprintf('%05d', $counter);
      return $code.'/'.$date->format('Y/md').'/'.$morph_counter;
    }
}
