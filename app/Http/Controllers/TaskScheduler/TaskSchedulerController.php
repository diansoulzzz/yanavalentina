<?php
namespace App\Http\Controllers\TaskScheduler;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Validation\Rule;
use Validator;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use Mail;
use Illuminate\Support\Facades\Crypt;
use Artisan;
use App\Models\ChatMessage;
use App\Models\ChatRoom;
use App\Models\TaskScheduler;
use App\Mail\UserChatMail;

class TaskSchedulerController extends Controller
{
  // protected $signature = 'MailChatCommand:sendUnreadChat';
  public function runUnreadChat(Request $request) {
    $exitCode = Artisan::call('MailChatCommand:sendUnreadChat');
    return $exitCode;
  }
  public function runClearCache(Request $request) {
    $exitCode = Artisan::call('cache:clear');
    $exitCode = Artisan::call('view:clear');
    $exitCode = Artisan::call('route:clear');
    $exitCode = Artisan::call('clear-compiled');
    $exitCode = Artisan::call('config:cache');
    return $exitCode;
  }

  public function run(Request $request){
    // $time = Carbon::now();
    // TaskScheduler::insert(['command' => $this->signature,'created_at'=>$time]);
    //
    // $chat_count = ChatMessage::whereNull('emailed_at')
    // ->whereDoesntHave('chat_reads', function($query) {
    //   $query->whereHas('user', function ($query_d){
    //     $query_d->where('role','<>','1');
    //   });
    // })
    // ->get()->count();
    // if ($chat_count>0){
    //   $chat_message = ChatMessage::whereNull('emailed_at')
    //   ->whereDoesntHave('chat_reads', function($query) {
    //     $query->whereHas('user', function ($query_d){
    //       $query_d->where('role','<>','1');
    //     });
    //   })
    //   ->get()->groupBy(function($chat) {
    //     return $chat->users_email.'|~|'.$chat->users_photo_url.'|~|'.$chat->users_name;
    //   });
    //
    //   $chat_mail = ChatMessage::whereNull('emailed_at')
    //   ->whereDoesntHave('chat_reads', function($query) {
    //     $query->whereHas('user', function ($query_d){
    //       $query_d->where('role','<>','1');
    //     });
    //   })->update(['emailed_at' => $time]);
    //
    //   $data =[
    //     'chat_count'=>$chat_count,
    //     'chat_message'=>$chat_message
    //   ];
    //
    //   $users = User::where('role','<>','1')->where('email','dian.yulius@gmail.com')->get();
    //   foreach ($users as $key => $user) {
    //     $this->sendMail($user,$data);
    //   }
    // }

    // $users = User::where('role','<>','1')->get();
    //
    // $chat_count = ChatMessage::whereNull('emailed_at')
    // ->whereDoesntHave('chat_reads', function($query) {
    //   $query->whereHas('user', function ($query_d){
    //     $query_d->where('role','<>','1');
    //   });
    // })
    // ->get()->count();
    // $chat_message = ChatMessage::whereNull('emailed_at')
    // ->whereDoesntHave('chat_reads', function($query) {
    //   $query->whereHas('user', function ($query_d){
    //     $query_d->where('role','<>','1');
    //   });
    // })
    // ->get()->groupBy(function($chat) {
    //   return $chat->users_email.'|~|'.$chat->users_photo_url.'|~|'.$chat->users_name;
    // });
    // return $chat_message;
    // return view('emails.user-unread-chat', compact('chat_message','chat_count'));
    // $chat_mail = ChatMessage::whereNull('emailed_at')
    // ->whereDoesntHave('chat_reads', function($query) {
    //   $query->whereHas('user', function ($query_d){
    //     $query_d->where('role','<>','1');
    //   });
    // })->update(['emailed_at' => Carbon::now()]);
    $exitCode = Artisan::call('schedule:run');
    return $exitCode;
  }

}
