<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class IsAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
       if (Auth::user() &&  (Auth::user()->role == 0 || Auth::user()->role == 2)) {
          return $next($request);
       }
       // if ($request->user()->role>0) {
       //     abort(404);
       // };
       return redirect('/member/profile');
    }
}
