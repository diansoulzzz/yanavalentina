<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 19 Aug 2018 13:35:29 +0700.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class InvoiceDetail
 * 
 * @property int $id
 * @property string $qty
 * @property string $price
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property int $cart_id
 * @property int $item_id
 * @property int $invoice_header_id
 * 
 * @property \App\Models\Cart $cart
 * @property \App\Models\InvoiceHeader $invoice_header
 * @property \App\Models\Item $item
 *
 * @package App\Models
 */
class InvoiceDetail extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;
	protected $table = 'invoice_detail';

	protected $casts = [
		'cart_id' => 'int',
		'item_id' => 'int',
		'invoice_header_id' => 'int'
	];

	protected $fillable = [
		'qty',
		'price',
		'cart_id',
		'item_id',
		'invoice_header_id'
	];

	public function cart()
	{
		return $this->belongsTo(\App\Models\Cart::class);
	}

	public function invoice_header()
	{
		return $this->belongsTo(\App\Models\InvoiceHeader::class);
	}

	public function item()
	{
		return $this->belongsTo(\App\Models\Item::class);
	}
}
