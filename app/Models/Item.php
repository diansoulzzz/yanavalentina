<?php

/**
 * Created by Reliese Model.
 * Date: Tue, 28 Aug 2018 09:56:30 +0700.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Item
 *
 * @property int $id
 * @property string $name
 * @property string $description
 * @property float $stock
 * @property float $price
 * @property float $price_buy
 * @property float $weight
 * @property float $long
 * @property float $width
 * @property float $height
 * @property int $publish
 * @property int $schedule_id
 * @property int $item_category_id
 * @property string $link_url
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property int $created_by
 * @property int $updated_by
 * @property int $deleted_by
 *
 * @property \App\Models\ItemCategory $item_category
 * @property \App\Models\Schedule $schedule
 * @property \App\Models\User $user
 * @property \Illuminate\Database\Eloquent\Collection $carts
 * @property \Illuminate\Database\Eloquent\Collection $invoice_details
 * @property \Illuminate\Database\Eloquent\Collection $item_discussions
 * @property \Illuminate\Database\Eloquent\Collection $item_images
 *
 * @package App\Models
 */
class Item extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;
	protected $table = 'item';

	protected $casts = [
		'stock' => 'float',
		'price' => 'float',
		'price_buy' => 'float',
		'weight' => 'float',
		'long' => 'float',
		'width' => 'float',
		'height' => 'float',
		'publish' => 'int',
		'schedule_id' => 'int',
		'item_category_id' => 'int',
		'created_by' => 'int',
		'updated_by' => 'int',
		'deleted_by' => 'int'
	];

	protected $fillable = [
		'name',
		'description',
		'stock',
		'price',
		'price_buy',
		'weight',
		'long',
		'width',
		'height',
		'publish',
		'schedule_id',
		'item_category_id',
		'link_url',
		'created_by',
		'updated_by',
		'deleted_by'
	];

	public function item_category()
	{
		return $this->belongsTo(\App\Models\ItemCategory::class);
	}

	public function schedule()
	{
		return $this->belongsTo(\App\Models\Schedule::class);
	}

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class, 'created_by');
	}

	public function carts()
	{
		return $this->hasMany(\App\Models\Cart::class);
	}

	public function invoice_details()
	{
		return $this->hasMany(\App\Models\InvoiceDetail::class);
	}

	public function item_discussions()
	{
		return $this->hasMany(\App\Models\ItemDiscussion::class)->orderBy('created_at','desc');
	}
	
	public function item_images()
	{
		return $this->hasMany(\App\Models\ItemImage::class)->orderBy('primary','desc');
	}

	public function item_images_primary()
	{
		return $this->hasOne(\App\Models\ItemImage::class, 'item_id')->where('primary',1);
	}
}
