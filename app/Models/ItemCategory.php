<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 19 Aug 2018 13:29:58 +0700.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class ItemCategory
 * 
 * @property int $id
 * @property string $name
 * @property string $photo_url
 * @property int $publish
 * @property string $link_url
 * @property string $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property int $created_by
 * @property int $updated_by
 * @property int $deleted_by
 * 
 * @property \App\Models\User $user
 * @property \Illuminate\Database\Eloquent\Collection $items
 *
 * @package App\Models
 */
class ItemCategory extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;
	protected $table = 'item_category';

	protected $casts = [
		'publish' => 'int',
		'created_by' => 'int',
		'updated_by' => 'int',
		'deleted_by' => 'int'
	];

	protected $fillable = [
		'name',
		'photo_url',
		'publish',
		'link_url',
		'created_by',
		'updated_by',
		'deleted_by'
	];

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class, 'created_by');
	}

	public function items()
	{
		return $this->hasMany(\App\Models\Item::class);
	}
}
