<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 19 Aug 2018 13:29:58 +0700.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;
use Illuminate\Support\Facades\Auth;
use Storage;
use Carbon\Carbon;

/**
 * Class ChatRoom
 *
 * @property int $id
 * @property int $users_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 *
 * @property \App\Models\User $user
 * @property \Illuminate\Database\Eloquent\Collection $chat_messages
 *
 * @package App\Models
 */
class ChatRoom extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;
	protected $table = 'chat_room';
	protected $appends = ['count_new','ecr','message_preview','users_name','users_photo_url'];
	protected $hidden = ['user'];

	protected $casts = [
		'users_id' => 'int'
	];

	protected $fillable = [
		'users_id'
	];

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class, 'users_id');
	}

	public function chat_messages()
	{
		return $this->hasMany(\App\Models\ChatMessage::class);
	}

	public function getEcrAttribute()
	{
		return $this->id;
	}

	public function getCountNewAttribute()
	{
		return $this->chat_messages->where('is_read',false)->count();
	}

	public function getMessagePreviewAttribute()
	{
		if(Auth::check()) {
			if (isset($this->chat_messages()->orderBy('created_at','desc')->first()->chat)) {
				return mb_strimwidth($this->chat_messages()->orderBy('created_at','desc')->first()->chat, 0, 50, " . . .");
			}
		}
		return '';
	}

	public function getUsersNameAttribute()
	{
		return $this->user->name;
	}

	public function getUsersPhotoUrlAttribute()
	{
		return ($this->user->photo_url != '' ? Storage::disk('public')->url($this->user->photo_url) : asset('assets/logo/logo-rd.png'));
	}
}
