<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 19 Aug 2018 13:29:58 +0700.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class City
 * 
 * @property int $id
 * @property string $name
 * @property int $province_id
 * @property string $type
 * @property string $postal_code
 * 
 * @property \App\Models\Province $province
 * @property \Illuminate\Database\Eloquent\Collection $subdistricts
 *
 * @package App\Models
 */
class City extends Eloquent
{
	protected $table = 'city';
	public $timestamps = false;

	protected $casts = [
		'province_id' => 'int'
	];

	protected $fillable = [
		'name',
		'province_id',
		'type',
		'postal_code'
	];

	public function province()
	{
		return $this->belongsTo(\App\Models\Province::class);
	}

	public function subdistricts()
	{
		return $this->hasMany(\App\Models\Subdistrict::class);
	}
}
