<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 20 Aug 2018 17:10:47 +0700.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class UsersAddress
 *
 * @property int $id
 * @property string $person
 * @property string $telephone
 * @property string $address
 * @property string $poscode
 * @property int $primary
 * @property int $users_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property int $subdistrict_id
 *
 * @property \App\Models\Subdistrict $subdistrict
 * @property \App\Models\User $user
 * @property \Illuminate\Database\Eloquent\Collection $carts
 * @property \Illuminate\Database\Eloquent\Collection $invoice_headers
 *
 * @package App\Models
 */
class UsersAddress extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;
	protected $table = 'users_address';
	protected $appends = ['subdistrict_name','city_name','province_name','city_postal_code'];

	protected $casts = [
		'primary' => 'int',
		'users_id' => 'int',
		'subdistrict_id' => 'int'
	];

	protected $fillable = [
		'person',
		'telephone',
		'address',
		'poscode',
		'primary',
		'users_id',
		'subdistrict_id'
	];

	public function subdistrict()
	{
		return $this->belongsTo(\App\Models\Subdistrict::class);
	}

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class, 'users_id');
	}

	public function carts()
	{
		return $this->hasMany(\App\Models\Cart::class);
	}

	public function invoice_headers()
	{
		return $this->hasMany(\App\Models\InvoiceHeader::class);
	}

	public function getSubdistrictNameAttribute()
	{
		if (!empty($this->subdistrict))
		{
				return $this->subdistrict->name;
		}
		return 'Kosong';
	}

	public function getCityNameAttribute()
	{
		if (!empty($this->subdistrict))
		{
						if ($this->subdistrict->city->type=='Kabupaten') {
														return 'Kab. '.$this->subdistrict->city->name;
						}
				return 'Kota. '.$this->subdistrict->city->name;
		}
		return 'Kosong';
	}

	public function getCityPostalCodeAttribute()
	{
		if (!empty($this->subdistrict))
		{
				return $this->subdistrict->city->postal_code;
		}
		return 'Kosong';
	}

	public function getProvinceNameAttribute()
	{
		if (!empty($this->subdistrict))
		{
				return $this->subdistrict->city->province->name;
		}
		return 'Kosong';
	}
}
