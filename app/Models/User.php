<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 19 Aug 2018 13:29:58 +0700.
 */

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
use \Illuminate\Database\Eloquent\SoftDeletes;

	protected $casts = [
		'role' => 'int'
	];

	protected $dates = [
		'birthday',
		'verified'
	];

	protected $hidden = [
		'password',
		'remember_token',
		'api_token'
	];

	protected $fillable = [
		'code',
		'name',
		'email',
		'birthday',
		'gender',
		'phone_number',
		'password',
		'role',
		'remember_token',
		'api_token',
		'verified',
		'photo_url'
	];

	public function carts()
	{
		return $this->hasMany(\App\Models\Cart::class, 'users_id');
	}

	public function chat_messages()
	{
		return $this->hasMany(\App\Models\ChatMessage::class, 'users_id');
	}

	public function chat_reads()
	{
		return $this->hasMany(\App\Models\ChatRead::class, 'users_id');
	}

	public function chat_rooms()
	{
		return $this->hasMany(\App\Models\ChatRoom::class, 'users_id');
	}

  public function cms_promotions()
  {
    return $this->hasMany(\App\Models\CmsPromotion::class, 'created_by');
  }

	public function invoice_headers()
	{
		return $this->hasMany(\App\Models\InvoiceHeader::class, 'users_id');
	}

	public function items()
	{
		return $this->hasMany(\App\Models\Item::class, 'created_by');
	}

	public function item_categories()
	{
		return $this->hasMany(\App\Models\ItemCategory::class, 'created_by');
	}

	public function item_images()
	{
		return $this->hasMany(\App\Models\ItemImage::class, 'created_by');
	}

	public function schedules()
	{
		return $this->hasMany(\App\Models\Schedule::class, 'created_by');
	}

	public function schedule_images()
	{
		return $this->hasMany(\App\Models\ScheduleImage::class, 'created_by');
	}

	public function users_addresses()
	{
		return $this->hasMany(\App\Models\UsersAddress::class, 'users_id');
	}

	public function users_addresses_primary()
	{
		return $this->hasOne(\App\Models\UsersAddress::class, 'users_id')->where('primary',1);
	}
}
