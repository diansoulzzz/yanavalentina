<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 26 Aug 2018 13:40:39 +0700.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Cash
 * 
 * @property int $id
 * @property string $receipt_number
 * @property \Carbon\Carbon $date
 * @property string $note
 * @property float $amount
 * @property string $type
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property int $created_by
 * @property int $updated_by
 * @property int $deleted_by
 * @property int $schedule_id
 * 
 * @property \App\Models\Schedule $schedule
 * @property \App\Models\User $user
 *
 * @package App\Models
 */
class Cash extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;
	protected $table = 'cash';

	protected $casts = [
		'amount' => 'float',
		'created_by' => 'int',
		'updated_by' => 'int',
		'deleted_by' => 'int',
		'schedule_id' => 'int'
	];

	protected $dates = [
		'date'
	];

	protected $fillable = [
		'receipt_number',
		'date',
		'note',
		'amount',
		'type',
		'created_by',
		'updated_by',
		'deleted_by',
		'schedule_id'
	];

	public function schedule()
	{
		return $this->belongsTo(\App\Models\Schedule::class);
	}

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class, 'created_by');
	}
}
