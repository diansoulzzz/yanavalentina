<?php

/**
 * Created by Reliese Model.
 * Date: Tue, 28 Aug 2018 10:39:07 +0700.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class ItemDiscussion
 *
 * @property int $id
 * @property string $discussion
 * @property int $rate
 * @property int $item_id
 * @property int $users_id
 * @property int $item_discussion_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 *
 * @property \App\Models\Item $item
 * @property \App\Models\ItemDiscussion $item_discussion
 * @property \App\Models\User $user
 * @property \Illuminate\Database\Eloquent\Collection $item_discussions
 *
 * @package App\Models
 */
class ItemDiscussion extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;
	protected $table = 'item_discussion';

	protected $casts = [
		'rate' => 'int',
		'item_id' => 'int',
		'users_id' => 'int',
		'item_discussion_id' => 'int'
	];

	protected $fillable = [
		'discussion',
		'rate',
		'item_id',
		'users_id',
		'item_discussion_id'
	];

	public function item()
	{
		return $this->belongsTo(\App\Models\Item::class);
	}

	public function item_discussion()
	{
		return $this->belongsTo(\App\Models\ItemDiscussion::class);
	}

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class, 'users_id');
	}

	public function item_discussions()
	{
		return $this->hasMany(\App\Models\ItemDiscussion::class);
	}
}
