<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 20 Aug 2018 17:10:40 +0700.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Cart
 *
 * @property int $id
 * @property float $qty
 * @property float $price
 * @property int $checkout
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property int $item_id
 * @property int $users_id
 * @property int $users_address_id
 *
 * @property \App\Models\Item $item
 * @property \App\Models\User $user
 * @property \App\Models\UsersAddress $users_address
 * @property \Illuminate\Database\Eloquent\Collection $invoice_details
 *
 * @package App\Models
 */
class Cart extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;
	protected $table = 'cart';
	protected $appends = ['subtotal'];

	protected $casts = [
		'qty' => 'float',
		'price' => 'float',
		'checkout' => 'int',
		'item_id' => 'int',
		'users_id' => 'int',
		'users_address_id' => 'int'
	];

	protected $fillable = [
		'qty',
		'price',
		'checkout',
		'item_id',
		'users_id',
		'users_address_id'
	];

	public function item()
	{
		return $this->belongsTo(\App\Models\Item::class);
	}

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class, 'users_id');
	}

	public function users_address()
	{
		return $this->belongsTo(\App\Models\UsersAddress::class);
	}

	public function invoice_details()
	{
		return $this->hasMany(\App\Models\InvoiceDetail::class);
	}
	public function getSubtotalAttribute(){
		return $this->qty*$this->item->price;
	}
}
