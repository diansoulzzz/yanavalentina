<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 24 Aug 2018 11:52:50 +0700.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class BroadcastReceiver
 * 
 * @property int $id
 * @property int $broadcast_id
 * @property int $users_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * 
 * @property \App\Models\Broadcast $broadcast
 * @property \App\Models\User $user
 *
 * @package App\Models
 */
class BroadcastReceiver extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;
	protected $table = 'broadcast_receiver';

	protected $casts = [
		'broadcast_id' => 'int',
		'users_id' => 'int'
	];

	protected $fillable = [
		'broadcast_id',
		'users_id'
	];

	public function broadcast()
	{
		return $this->belongsTo(\App\Models\Broadcast::class);
	}

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class, 'users_id');
	}
}
