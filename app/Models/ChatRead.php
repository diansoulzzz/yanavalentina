<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 19 Aug 2018 13:29:58 +0700.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class ChatRead
 * 
 * @property int $id
 * @property int $chat_message_id
 * @property int $users_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * 
 * @property \App\Models\ChatMessage $chat_message
 * @property \App\Models\User $user
 *
 * @package App\Models
 */
class ChatRead extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;
	protected $table = 'chat_read';

	protected $casts = [
		'chat_message_id' => 'int',
		'users_id' => 'int'
	];

	protected $fillable = [
		'chat_message_id',
		'users_id'
	];

	public function chat_message()
	{
		return $this->belongsTo(\App\Models\ChatMessage::class);
	}

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class, 'users_id');
	}
}
