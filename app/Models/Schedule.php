<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 26 Aug 2018 13:40:47 +0700.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Schedule
 *
 * @property int $id
 * @property string $name
 * @property string $description
 * @property \Carbon\Carbon $start_date
 * @property \Carbon\Carbon $end_date
 * @property string $status
 * @property int $publish
 * @property string $link_url
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property int $created_by
 * @property int $updated_by
 * @property int $deleted_by
 *
 * @property \App\Models\User $user
 * @property \Illuminate\Database\Eloquent\Collection $cashes
 * @property \Illuminate\Database\Eloquent\Collection $items
 * @property \Illuminate\Database\Eloquent\Collection $schedule_images
 *
 * @package App\Models
 */
class Schedule extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;
	protected $table = 'schedule';

	protected $casts = [
		'publish' => 'int',
		'created_by' => 'int',
		'updated_by' => 'int',
		'deleted_by' => 'int'
	];

	protected $dates = [
		'start_date',
		'end_date'
	];

	protected $fillable = [
		'name',
		'description',
		'start_date',
		'end_date',
		'status',
		'publish',
		'link_url',
		'created_by',
		'updated_by',
		'deleted_by'
	];

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class, 'created_by');
	}

	public function cashes()
	{
		return $this->hasMany(\App\Models\Cash::class);
	}

	public function items()
	{
		return $this->hasMany(\App\Models\Item::class);
	}

	public function schedule_images()
	{
		return $this->hasMany(\App\Models\ScheduleImage::class)->orderBy('primary','desc');
	}

	public function schedule_images_primary()
	{
		return $this->hasOne(\App\Models\ScheduleImage::class, 'schedule_id')->where('primary',1);
	}
}
