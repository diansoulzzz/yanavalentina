<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 19 Aug 2018 13:29:58 +0700.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Province
 * 
 * @property int $id
 * @property string $name
 * 
 * @property \Illuminate\Database\Eloquent\Collection $cities
 *
 * @package App\Models
 */
class Province extends Eloquent
{
	protected $table = 'province';
	public $timestamps = false;

	protected $fillable = [
		'name'
	];

	public function cities()
	{
		return $this->hasMany(\App\Models\City::class);
	}
}
