<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 23 Aug 2018 16:12:06 +0700.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class InvoiceHeader
 *
 * @property int $id
 * @property string $invoice
 * @property \Carbon\Carbon $date
 * @property string $courier
 * @property string $courier_service
 * @property string $courieri_receipt number
 * @property float $courier_cost
 * @property float $total
 * @property float $total_grand
 * @property float $paid
 * @property float $paid_outstanding
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property int $users_id
 * @property int $users_address_id
 *
 * @property \App\Models\User $user
 * @property \App\Models\UsersAddress $users_address
 * @property \Illuminate\Database\Eloquent\Collection $invoice_details
 *
 * @package App\Models
 */
class InvoiceHeader extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;
	protected $table = 'invoice_header';
	protected $appends = ['format_total_grand'];
	protected $casts = [
		'courier_cost' => 'float',
		'total' => 'float',
		'total_grand' => 'float',
		'paid' => 'float',
		'paid_outstanding' => 'float',
		'users_id' => 'int',
		'users_address_id' => 'int'
	];

	protected $dates = [
		'date'
	];

	protected $fillable = [
		'invoice',
		'date',
		'courier',
		'courier_service',
		'courieri_receipt number',
		'courier_cost',
		'total',
		'total_grand',
		'paid',
		'paid_outstanding',
		'users_id',
		'users_address_id'
	];

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class, 'users_id');
	}

	public function users_address()
	{
		return $this->belongsTo(\App\Models\UsersAddress::class);
	}

	public function invoice_details()
	{
		return $this->hasMany(\App\Models\InvoiceDetail::class);
	}
	public function getFormatTotalGrandAttribute(){
		return number_format($this->total_grand);
	}
}
