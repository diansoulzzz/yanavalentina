<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 24 Aug 2018 11:52:46 +0700.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Broadcast
 * 
 * @property int $id
 * @property string $message
 * @property string $subject
 * @property string $photo_url
 * @property int $publish
 * @property int $users_id
 * 
 * @property \App\Models\User $user
 * @property \Illuminate\Database\Eloquent\Collection $broadcast_receivers
 *
 * @package App\Models
 */
class Broadcast extends Eloquent
{
	protected $table = 'broadcast';
	public $timestamps = false;

	protected $casts = [
		'publish' => 'int',
		'users_id' => 'int'
	];

	protected $fillable = [
		'message',
		'subject',
		'photo_url',
		'publish',
		'users_id'
	];

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class, 'users_id');
	}

	public function broadcast_receivers()
	{
		return $this->hasMany(\App\Models\BroadcastReceiver::class);
	}
}
