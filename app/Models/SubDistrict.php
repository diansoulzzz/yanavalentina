<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 19 Aug 2018 13:29:58 +0700.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Subdistrict
 *
 * @property int $id
 * @property string $name
 * @property int $city_id
 *
 * @property \App\Models\City $city
 * @property \Illuminate\Database\Eloquent\Collection $users_addresses
 *
 * @package App\Models
 */
class Subdistrict extends Eloquent
{
	protected $table = 'subdistrict';
	public $timestamps = false;
	protected $appends = ['city_name','province_name','city_postal_code'];

	protected $casts = [
		'city_id' => 'int'
	];

	protected $fillable = [
		'name',
		'city_id'
	];

	public function city()
	{
		return $this->belongsTo(\App\Models\City::class);
	}

	public function users_addresses()
	{
		return $this->hasMany(\App\Models\UsersAddress::class);
	}

	public function getCityNameAttribute()
	{
		if ($this->city->type=='Kabupaten') {
										return 'Kab. '.$this->city->name;
		}
		return 'Kota. '.$this->city->name;
	}

	public function getCityPostalCodeAttribute()
	{
		return $this->city->postal_code;
	}

	public function getProvinceNameAttribute()
	{
		return $this->city->province->name;
	}
}
