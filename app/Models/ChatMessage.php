<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 19 Aug 2018 13:29:58 +0700.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;
use Illuminate\Support\Facades\Auth;
use Storage;
use Carbon\Carbon;
use App\Models\User;

/**
 * Class ChatMessage
 *
 * @property int $id
 * @property string $chat
 * @property string $attachment
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property int $users_id
 * @property int $chat_room_id
 * @property \Carbon\Carbon $emailed_at
 *
 * @property \App\Models\ChatRoom $chat_room
 * @property \App\Models\User $user
 * @property \Illuminate\Database\Eloquent\Collection $chat_reads
 *
 * @package App\Models
 */
class ChatMessage extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;
	protected $table = 'chat_message';
	protected $appends = ['is_opposite','users_name','users_email','users_photo_url','time_chat','is_read','count_read_by_opposite'];
	protected $hidden = ['user','chat_reads'];

	protected $casts = [
		'users_id' => 'int',
		'chat_room_id' => 'int'
	];

	protected $dates = [
		'emailed_at'
	];

	protected $fillable = [
		'chat',
		'attachment',
		'users_id',
		'chat_room_id',
		'emailed_at'
	];

	public function chat_room()
	{
		return $this->belongsTo(\App\Models\ChatRoom::class);
	}

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class, 'users_id');
	}

	public function chat_reads()
	{
		return $this->hasMany(\App\Models\ChatRead::class);
	}

	public function getCountReadByOppositeAttribute()
	{
		if(Auth::check()) {
			if (Auth::user()->role<>1){
				$admins = User::where('role','<>','1')->pluck('id')->toArray();
				$read = $this->chat_reads->whereNotIn('users_id', $admins)->where('users_id','<>',Auth::id())->count();
				return $read;
			} else {
				$read = $this->chat_reads->where('users_id','<>',Auth::id())->count();
				return $read;
			}
		}
		return 0;
	}
	public function getIsReadAttribute()
	{
		if(Auth::check()) {
			$read = $this->chat_reads->where('users_id',Auth::id())->isEmpty();
			if (!$read) {
				return true;
			}
			else {
				return false;
			}
		}
		return false;
	}

	public function getIsOppositeAttribute()
	{
		if(Auth::check()) {
			if ($this->users_id == Auth::id()) {
				return false;
			}
			else {
				return true;
			}
		}
		return false;
	}

	public function getTimeChatAttribute()
	{
		return Carbon::parse($this->created_at)->format('H:i');
	}

	public function getUsersEmailAttribute()
	{
		return $this->user->email;
	}
	public function getUsersNameAttribute()
	{
		return $this->user->name;
	}
	public function getUsersPhotoUrlAttribute()
	{
		return ($this->user->photo_url != '' ? Storage::disk('public')->url($this->user->photo_url) : asset('assets/logo/logo-rd.png'));
	}
}
