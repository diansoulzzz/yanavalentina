<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 19 Aug 2018 13:29:58 +0700.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class ScheduleImage
 * 
 * @property int $id
 * @property string $photo_url
 * @property int $primary
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property int $schedule_id
 * @property int $created_by
 * @property int $updated_by
 * @property int $deleted_by
 * 
 * @property \App\Models\Schedule $schedule
 * @property \App\Models\User $user
 *
 * @package App\Models
 */
class ScheduleImage extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;
	protected $table = 'schedule_image';

	protected $casts = [
		'primary' => 'int',
		'schedule_id' => 'int',
		'created_by' => 'int',
		'updated_by' => 'int',
		'deleted_by' => 'int'
	];

	protected $fillable = [
		'photo_url',
		'primary',
		'schedule_id',
		'created_by',
		'updated_by',
		'deleted_by'
	];

	public function schedule()
	{
		return $this->belongsTo(\App\Models\Schedule::class);
	}

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class, 'created_by');
	}
}
