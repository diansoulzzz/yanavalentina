<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 07 Sep 2018 11:07:43 +0700.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class CmsPromotion
 * 
 * @property int $id
 * @property string $subject
 * @property string $promotion
 * @property string $photo_url
 * @property string $link_url
 * @property \Carbon\Carbon $start_at
 * @property \Carbon\Carbon $end_at
 * @property int $publish
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property int $created_by
 * @property int $updated_by
 * @property int $deleted_by
 * 
 * @property \App\Models\User $user
 *
 * @package App\Models
 */
class CmsPromotion extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;
	protected $table = 'cms_promotion';

	protected $casts = [
		'publish' => 'int',
		'created_by' => 'int',
		'updated_by' => 'int',
		'deleted_by' => 'int'
	];

	protected $dates = [
		'start_at',
		'end_at'
	];

	protected $fillable = [
		'subject',
		'promotion',
		'photo_url',
		'link_url',
		'start_at',
		'end_at',
		'publish',
		'created_by',
		'updated_by',
		'deleted_by'
	];

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class, 'created_by');
	}
}
