<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 19 Aug 2018 13:35:06 +0700.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class TaskScheduler
 * 
 * @property int $id
 * @property string $command
 * @property \Carbon\Carbon $created_at
 *
 * @package App\Models
 */
class TaskScheduler extends Eloquent
{
	protected $table = 'task_scheduler';
	public $timestamps = false;

	protected $fillable = [
		'command'
	];
}
