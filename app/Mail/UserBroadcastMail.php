<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\User;
use Illuminate\Support\Facades\Crypt;

class UserBroadcastMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $users;
    public $broadcast;
    public function __construct($users, $broadcast)
    {
        $this->users = $users;
        $this->broadcast = $broadcast;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('dont-reply@yanavalentina.com')
        ->subject($this->broadcast->subject)
        ->view('emails.user-broadcast')
        ->with([
          'broadcast_subject' => $this->broadcast->subject,
          'broadcast_message' => $this->broadcast->message,
        ]);;
    }
}
