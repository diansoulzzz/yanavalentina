<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\User;
use Illuminate\Support\Facades\Crypt;

class UserVerificationMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $users;
    public function __construct(User $users)
    {
        $this->users = $users;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('dont-reply@yanavalentina.com')
        ->subject('Verifikasi Alamat Email Kamu di Yanavalentina')
        ->view('emails.user-verification')
        ->with([
          'userName' => ucwords($this->users->name),
          'userCode' => $this->users->code,
          'userUrl' => url('v/'.$this->users->redirectUrl.'/'.Crypt::encryptString($this->users->id)),
        ]);;
    }
}
