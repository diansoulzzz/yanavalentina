<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\User;
use Illuminate\Support\Facades\Crypt;

class UserForgotPasswordMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $users;

    public function __construct(User $users)
    {
        $this->users = $users;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('dont-reply@yanavalentina.com')
        ->subject('Atur Ulang Kata Sandi Yanavalentina')
        ->view('emails.user-forgot')
        ->with([
          'userName' => ucwords($this->users->name),
          'userEmail' => ucwords($this->users->email),
          'userUrl' => url('r/'.Crypt::encryptString($this->users->email).'/'.Crypt::encryptString($this->users->reset_token)),
        ]);;
    }
}
