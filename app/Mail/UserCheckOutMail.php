<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
// use App\Models\User;
use Illuminate\Support\Facades\Crypt;

class UserCheckOutMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $users;
    public function __construct($users)
    {
        $this->users = $users;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('dont-reply@yanavalentina.com')
        ->subject('Silahkan lakukan pembayaran di Yanavalentina')
        ->view('emails.user-checkout')
        ->with([
          'userName' => ucwords($this->users->name),
          'userCode' => $this->users->code,
          // 'userUrl' => url('v/'.$this->users->redirectUrl.'/'.Crypt::encryptString($this->users->id)),
        ]);;
    }
}
