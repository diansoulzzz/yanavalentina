<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\User;
use Illuminate\Support\Facades\Crypt;

class UserChatMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $users;
    public $data;
    public function __construct(User $users, $data)
    {
        $this->users = $users;
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('dont-reply@yanavalentina.com')
        ->subject('Anda Memiliki '.$this->data['chat_count'].' Pesan Baru')
        ->view('emails.user-unread-chat')
        ->with([
          'users' => $this->users,
          'data' => $this->data,
          'chat_count' => $this->data['chat_count'],
          'chat_message' => $this->data['chat_message'],
        ]);;
    }
}
